const express = require('express'),
    app = express(),
    port = 3000;

class Server {

    constructor() {
        this.start();
    }

    start() {
        app.listen(port, (err) => {
            console.log('[%s] Listening on http://localhost:%d', process.env.NODE_ENV, port);
        });
    }
}

var server = new Server();