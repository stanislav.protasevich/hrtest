init: composer-update schema-update fixtures-load init-permissions

init-permissions:
	docker-compose exec -T workspace bin/console rbac:init:permissions

up: down docker-up schema-update

docker-up:
	docker-compose up -d --build

docker-down:
	docker-compose down

down: docker-down

perm:
	sudo chown ${USER}:${USER} project -R

test: down up composer-update
	docker-compose exec -T workspace bin/phpunit
	docker-compose down

composer-update:
	docker-compose exec -T workspace composer update

schema-update:
	docker-compose exec -T workspace bin/console doctrine:schema:update --force

cache-flush: cache-redis-flush cache-flush-app

cache-flush-app:
	docker-compose exec -T workspace bin/console cache:clear

cache-redis-flush: cache-redis-flush-all cache-redis-flush-db

cache-redis-flush-all:
	docker-compose exec -T workspace bin/console redis:flushall

cache-redis-flush-db:
	docker-compose exec -T workspace bin/console redis:flushdb

dev: perm up schema-update

fixtures-load:
	docker-compose exec -T workspace bin/console doctrine:fixtures:load --append -n

database-create:
	docker-compose exec -T workspace bin/console doctrine:schema:drop --force && php bin/console doctrine:schema:create

front-update-deps:
	docker run --rm --name front-build -v "${PWD}/front:/data/www/front" -w /usr/src/app node:alpine sh -c "cd /data/www/front && npm install && npm rebuild node-sass --force"

front-build:
	docker run --rm --name front-build -v "${PWD}/front:/data/www/front" -w /usr/src/app node:alpine sh -c "cd /data/www/front && npm run build && mkdir -p /data/www/front/www && rm -rf /data/www/front/www/*  && cp -r /data/www/front/dist/* /data/www/front/www"

create-dev: database-create fixtures-load

migrate-up:
	docker-compose exec -T workspace bin/console doctrine:migrations:migrate -n

websocket-install:
	docker-compose exec -T node npm install

websocket-start:
	docker-compose exec -T node npm run start

publish-storage:
	ln -sf "${PWD}/storage" "${PWD}/project/public"
