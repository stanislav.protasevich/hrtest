let WebSocket = require('ws');

let amqp = require('amqplib/callback_api');
let jwt = require('jsonwebtoken');
let fs = require('fs');
let amqpConn = null;
let util = require('util');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = new Sequelize(process.env.DATABASE_URL, {dialect: 'mysql', operatorsAliases: false});

function debug(data) {
    console.log(util.inspect(data, false, null, true /* enable colors */))
}

/***********************************************************************************************************
 * Socket
 ***********************************************************************************************************/
let server = new WebSocket.Server({
    port: process.env.WS_PORT
});

server.on('connection', function (ws, request) {

    console.log('connected: %s at %s', request.connection.remoteAddress, new Date().toISOString());

    ws.on('message', function (message) {

        try {
            let data = JSON.parse(message);

            if (data.type && data.token && data.type === 'auth') {
                auth(ws, data);
            }

            if (data.notification && data.notification.read) {
                readNotification(data.notification.read)
            }

        } catch (e) {
            debug(e)
        }

    })
});

server.on('close', function (ws, request) {
    console.log('close: %s at %s', request.connection.remoteAddress, new Date().toISOString());
});

let jwtKey = fs.readFileSync(process.env.JWT_PUBLIC_KEY_PATH);

function auth(ws, data) {
    let token = jwt.verify(data.token, jwtKey, {algorithms: ['RS256']});

    console.log('user_id: %s, token: %s', token.jti, data.token);

    ws.user_id = token.jti;
}

/***********************************************************************************************************
 * RabbitMQ
 ***********************************************************************************************************/
const AMQP_QUEUE_NAME = 'notification.v1.alert';
const AMQP_QUEUE_NOTIFICATIONS_PREFIX = 'notification';

startAMQP();

function startAMQP() {
    amqp.connect(process.env.RABBITMQ_URL + "?heartbeat=60", function (err, conn) {
        if (err) {
            console.error("[AMQP]", err.message);
            return setTimeout(startAMQP, 1000);
        }

        conn.on("error", function (err) {
            if (err.message !== "Connection closing") {
                console.error("[AMQP] conn error", err.message);
            }
        });

        conn.on("close", function () {
            console.error("[AMQP] reconnecting");
            return setTimeout(startAMQP, 1000);
        });

        console.log("[AMQP] connected");
        amqpConn = conn;

        whenConnected();
    });
}

function whenConnected() {
    startWorker();
}

function startWorker() {
    amqpConn.createChannel(function (err, ch) {
        if (closeOnErr(err)) return;

        // ch.on("error", function (err) {
        //     console.error("[AMQP] channel error", err.message);
        // });
        // ch.on("close", function () {
        //     console.log("[AMQP] channel closed");
        // });

        ch.consume(AMQP_QUEUE_NAME, function (message) {

            console.log('[AMQP] consumed: %s', message.content);
            try {
                let value = JSON.parse(message.content);

                server.clients.forEach(ws => {
                    if (ws.user_id === value.alertedUserId) {
                        console.log('[AMQP] message: %s for user: %s', message.content, value.user_id);
                        ws.send(message.content.toString());
                    }
                });
            } catch (e) {
                debug(e)
            }
        }, {noAck: true});
    });
}

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

/***********************************************************************************************************
 * DB
 ***********************************************************************************************************/
dbConnect();

function dbConnect() {
    sequelize
        .authenticate()
        .then(() => {
            console.log('[DB] Connection has been established successfully.');
        })
        .catch(err => {
            console.error("[DB] Unable to connect to the database:", err);
            return setTimeout(dbConnect, 1000);
        });
}

/**
 * @type {Model}
 */
const UserAlert = sequelize.define('u_user_alert', {
    id: {type: Sequelize.STRING, primaryKey: true},
    read: Sequelize.BOOLEAN,
    view_at: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
}, {
    timestamps: false,
    freezeTableName: true,
});

function readNotification(id) {
    UserAlert.findByPk(id)
        .then((alert) => {
            if (alert && alert.read === false) {
                makeReadAlert(id);
            }
        });
}

function makeReadAlert(id) {
    UserAlert.update({
        read: true,
        view_at: new Date()
    }, {
        where: {
            id: {
                [Op.eq]: id
            }
        }
    });
}