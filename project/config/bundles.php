<?php

return [
    FOS\RestBundle\FOSRestBundle::class => ['all' => true],
    FOS\OAuthServerBundle\FOSOAuthServerBundle::class => ['all' => true],
    //+ Doctrine
    Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['all' => true, 'test' => true],
    //- Doctrine
    //+ Symfony
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class => ['all' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    //- Symfony
    League\Tactician\Bundle\TacticianBundle::class => ['all' => true],
    Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle::class => ['all' => true],
    Knp\Bundle\SnappyBundle\KnpSnappyBundle::class => ['all' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all' => true],
    Nelmio\ApiDocBundle\NelmioApiDocBundle::class => ['dev' => true],
    Noxlogic\RateLimitBundle\NoxlogicRateLimitBundle::class => ['all' => true],
    OldSound\RabbitMqBundle\OldSoundRabbitMqBundle::class => ['all' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    Snc\RedisBundle\SncRedisBundle::class => ['all' => true],
    Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle::class => ['all' => true],
    WhiteOctober\PagerfantaBundle\WhiteOctoberPagerfantaBundle::class => ['all' => true],
    //+ App
    Infrastructure\UserBundle\InfrastructureUserBundle::class => ['all' => true],
    Infrastructure\CommonBundle\InfrastructureCommonBundle::class => ['all' => true],
    Infrastructure\TimeOffBundle\InfrastructureTimeOffBundle::class => ['all' => true],
    Infrastructure\OauthBundle\InfrastructureOauthBundle::class => ['all' => true],
    Infrastructure\CompanyBundle\InfrastructureCompanyBundle::class => ['all' => true],
    Infrastructure\EmployeeBundle\InfrastructureEmployeeBundle::class => ['all' => true],
    Infrastructure\JobBundle\InfrastructureJobBundle::class => ['all' => true],
    Infrastructure\LeavesBundle\InfrastructureLeavesBundle::class => ['all' => true],
    Infrastructure\LocationBundle\InfrastructureLocationBundle::class => ['all' => true],
    Infrastructure\SystemBundle\InfrastructureSystemBundle::class => ['all' => true],
    Infrastructure\AuthBundle\InfrastructureAuthBundle::class => ['all' => true],
    Infrastructure\ArchiveBundle\InfrastructureArchiveBundle::class => ['all' => true],
    Infrastructure\RbacBundle\InfrastructureRbacBundle::class => ['all' => true],
    Infrastructure\EventBundle\InfrastructureEventBundle::class => ['all' => true],
    Infrastructure\DashboardBundle\InfrastructureDashboardBundle::class => ['all' => true],
    Infrastructure\BlogBundle\InfrastructureBlogBundle::class => ['all' => true],
    Infrastructure\CalendarBundle\InfrastructureCalendarBundle::class => ['all' => true],
    UI\RestBundle\UIRestBundle::class => ['all' => true],
    //- App
];
