<?php

declare(strict_types=1);


namespace Domain\Company\ValueObject;

use Infrastructure\CommonBundle\Factory\ModelFactoryTrait;

class Company implements CompanyInterface
{
    use ModelFactoryTrait;

    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $status;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function __construct(?string $name)
    {
        $this->name = $name ? trim($name) : $name;
    }

    public static function create(?string $name)
    {
        return new self($name);
    }

    /**
     * @return int
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(?int $status): Company
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(?int $id): Company
    {
        $this->id = $id;

        return $this;
    }

}