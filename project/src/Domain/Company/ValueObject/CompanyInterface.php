<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\ValueObject;

interface CompanyInterface
{
    public function getName();
}