<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */


namespace Domain\Company\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Entity\JobTitle;
use Domain\User\Entity\User;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\CompanyBundle\Repository\CompanyRepository")
 * @ORM\Table(name="c_company")
 *
 * @UniqueEntity("name")
 */
class Company
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_DELETED = 'deleted';
    public const STATUSES = [
        self::STATUS_DISABLED,
        self::STATUS_ACTIVE,
        self::STATUS_DELETED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier of the company.")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     *
     * @SWG\Property(type="string", maxLength=255)
     * @Groups({"api","all"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @SWG\Property(type="string", maxLength=255)
     * @Groups({"api","all"})
     */
    protected $image;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled', 'deleted')")
     *
     * @SWG\Property(type="string", enum={"active","disabled", "deleted"})
     * @Groups({"api","all"})
     */
    protected $status;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\Company\Entity\Department",
     *     inversedBy="company",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="c_company_department")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $departments;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\Job\Entity\JobTitle",
     *     inversedBy="company",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="c_company_job_title")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $jobTitles;

    /**
     * @ORM\OneToOne(
     *     targetEntity="Domain\Company\Entity\CompanyAddress",
     *     mappedBy="company", fetch="EXTRA_LAZY", cascade={"persist", "remove"}
     * )
     * @SWG\Property(ref=@Model(type=CompanyAddress::class, groups={"all"}))
     * @Groups("all")
     */
    protected $address;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\Employee\Entity\EmployeeCompany",
     *     mappedBy="company",
     *     fetch="EXTRA_LAZY"
     * )
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $employees;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Domain\User\Entity\User",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $hr;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\User\Entity\User",
     *     inversedBy="companyRecruiters",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="c_company_recruiters")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $recruiters;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedBy;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedAt;

    public function __construct()
    {
        $this->jobTitles = new ArrayCollection();
        $this->employees = new ArrayCollection();
        $this->departments = new ArrayCollection();
        $this->recruiters = new ArrayCollection();
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return Company
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Company
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Company
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Company
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Add department.
     *
     * @param \Domain\Company\Entity\Department $department
     *
     * @return Company
     */
    public function addDepartment(Department $department)
    {
        $this->departments[] = $department;

        return $this;
    }

    /**
     * Remove department.
     *
     * @param \Domain\Company\Entity\Department $department
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDepartment(Department $department)
    {
        return $this->departments->removeElement($department);
    }

    /**
     * Get departments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * Get address.
     *
     * @return \Domain\Company\Entity\CompanyAddress|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address.
     *
     * @param \Domain\Company\Entity\CompanyAddress|null $address
     *
     * @return Company
     */
    public function setAddress(CompanyAddress $address = null)
    {
        $this->address = $address;
        $address->setCompany($this);
        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return Company
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return Company
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add employee.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employee
     *
     * @return Company
     */
    public function addEmployee(EmployeeCompany $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employee
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmployee(EmployeeCompany $employee)
    {
        return $this->employees->removeElement($employee);
    }

    /**
     * Get employees.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Add jobTitle.
     *
     * @param \Domain\Job\Entity\JobTitle $jobTitle
     *
     * @return Company
     */
    public function addJobTitle(JobTitle $jobTitle)
    {
        $this->jobTitles[] = $jobTitle;

        return $this;
    }

    /**
     * Remove jobTitle.
     *
     * @param \Domain\Job\Entity\JobTitle $jobTitle
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeJobTitle(JobTitle $jobTitle)
    {
        return $this->jobTitles->removeElement($jobTitle);
    }

    /**
     * Get jobTitles.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobTitles()
    {
        return $this->jobTitles;
    }

    /**
     * Add recruiter.
     *
     * @param \Domain\User\Entity\User $recruiter
     *
     * @return Company
     */
    public function addRecruiter(User $recruiter)
    {
        $this->recruiters[] = $recruiter;

        return $this;
    }

    /**
     * Remove recruiter.
     *
     * @param \Domain\User\Entity\User $recruiter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRecruiter(User $recruiter)
    {
        return $this->recruiters->removeElement($recruiter);
    }

    /**
     * Get recruiters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecruiters()
    {
        return $this->recruiters;
    }

    public function getRelativeLogoPath(): string
    {
        return "company/{$this->id}/logos";
    }

    /**
     * Get hr.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getHr(): ?User
    {
        return $this->hr;
    }

    /**
     * Set hr.
     *
     * @param \Domain\User\Entity\User|null $hr
     *
     * @return Company
     */
    public function setHr(User $hr = null)
    {
        $this->hr = $hr;

        return $this;
    }
}
