<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */


namespace Domain\Company\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\CompanyBundle\Repository\CompanyAddressRepository")
 * @ORM\Table(name="c_company_address")
 */
class CompanyAddress
{
    public const TYPE_ACTUAL = 'actual';
    public const TYPE_POST = 'post';
    public const TYPE_LEGAL = 'legal';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     * @Expose()
     */
    protected $type;

    /**
     * @ORM\OneToOne(targetEntity="Domain\Company\Entity\Company", inversedBy="address")
     * @ORM\JoinColumn(onDelete="CASCADE", unique=false)
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Country", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Region", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id", nullable=false)
     */
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\City", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $street;
    /**
     * @ORM\Column(type="decimal", length=18, nullable=true, scale=12, precision=18)
     */
    protected $latitude;
    /**
     * @ORM\Column(type="decimal", length=18, nullable=true, scale=12, precision=18)
     */
    protected $longitude;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $postCode;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $building;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $room;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return CompanyAddress
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return CompanyAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return string|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set latitude.
     *
     * @param string|null $latitude
     *
     * @return CompanyAddress
     */
    public function setLatitude($latitude = null)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return string|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set longitude.
     *
     * @param string|null $longitude
     *
     * @return CompanyAddress
     */
    public function setLongitude($longitude = null)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get postCode.
     *
     * @return string|null
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set postCode.
     *
     * @param string|null $postCode
     *
     * @return CompanyAddress
     */
    public function setPostCode($postCode = null)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get building.
     *
     * @return string|null
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set building.
     *
     * @param string|null $building
     *
     * @return CompanyAddress
     */
    public function setBuilding($building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get room.
     *
     * @return string|null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set room.
     *
     * @param string|null $room
     *
     * @return CompanyAddress
     */
    public function setRoom($room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \Domain\Company\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company.
     *
     * @param \Domain\Company\Entity\Company|null $company
     *
     * @return CompanyAddress
     */
    public function setCompany(\Domain\Company\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Domain\Location\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param \Domain\Location\Entity\Country $country
     *
     * @return CompanyAddress
     */
    public function setCountry(?Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \Domain\Location\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set region.
     *
     * @param \Domain\Location\Entity\Region $region
     *
     * @return CompanyAddress
     */
    public function setRegion(?Region $region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \Domain\Location\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param \Domain\Location\Entity\City $city
     *
     * @return CompanyAddress
     */
    public function setCity(?City $city)
    {
        $this->city = $city;

        return $this;
    }
}
