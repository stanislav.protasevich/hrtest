<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */


namespace Domain\Company\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\CompanyBundle\Repository\DepartmentRepository")
 * @ORM\Table(name="c_department")
 *
 * @UniqueEntity("name")
 *
 */
class Department
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUSES = [
        self::STATUS_DISABLED,
        self::STATUS_ACTIVE,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier of the department.")
     * @Groups({"all"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @SWG\Property(type="string", maxLength=255)
     * @Groups({"all"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled')")
     *
     * @SWG\Property(type="string", enum={"active","disabled"})
     * @Groups({"all"})
     */
    protected $status;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true, "default" : 1})
     * @SWG\Property(type="integer")
     * @Groups({"all"})
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    protected $updatedBy;
    /**
     * @ORM\ManyToMany(targetEntity="Domain\Company\Entity\Company", mappedBy="departments", fetch="EXTRA_LAZY")
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    protected $company;
    /**
     * @ORM\Column(type="datetime")
     *
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->company = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Department
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int|null $sortOrder
     *
     * @return Department
     */
    public function setSortOrder(?int $sortOrder = null)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Department
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Department
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User $createdBy
     *
     * @return Department
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User $updatedBy
     *
     * @return Department
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add company.
     *
     * @param \Domain\Company\Entity\Company $company
     *
     * @return Department
     */
    public function addCompany(\Domain\Company\Entity\Company $company)
    {
        $this->company[] = $company;

        return $this;
    }

    /**
     * Remove company.
     *
     * @param \Domain\Company\Entity\Company $company
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompany(\Domain\Company\Entity\Company $company)
    {
        return $this->company->removeElement($company);
    }

    /**
     * Get company.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCompany()
    {
        return $this->company;
    }
}
