<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="c_company_department")
 *
 */
class CompanyDepartment
{
    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\Company", inversedBy="departments")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $company;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\Department", inversedBy="companies")
     */
    protected $department;
}
