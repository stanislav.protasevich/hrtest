<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\Company\Resource;

use Domain\User\Entity\User;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class CompanyRecruitersListResource extends JsonResource
{
    public function fields()
    {
        return (new UserSimpleResource($this->resource))->toArray();
    }
}