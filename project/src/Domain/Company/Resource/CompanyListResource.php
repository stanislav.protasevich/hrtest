<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Resource;

use Domain\Company\Entity\Company;

class CompanyListResource extends CompanyResource
{
    public function fields()
    {
        /** @var Company $company */
        $company = $this->resource[0];
        /** @var int $count */
        $count = (int)$this->resource['employeesCount'];

        $result = $this->prepare($company);

        $result['stats']['employeesCount'] = $count;

        return $result;
    }
}