<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Resource;


use Domain\Company\Entity\CompanyAddress;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Domain\User\Resource\UserResource;

/**
 * @mixin CompanyAddress
 */
class CompanyAddressResource extends UserResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'country' => $this->getLocationData($this->getCountry()),
            'region' => $this->getLocationData($this->getRegion()),
            'city' => $this->getLocationData($this->getCity()),
            'postCode' => $this->getPostCode() ? (int)$this->getPostCode() : null,
            'street' => $this->getStreet(),
            'building' => $this->getBuilding(),
            'room' => $this->getRoom(),
            'latitude' => $this->getLatitude() ? (float)$this->getLatitude() : null,
            'longitude' => $this->getLongitude() ? (float)$this->getLongitude() : null,
        ];
    }

    /**
     * @param $object Country|Region|City
     * @return array
     */
    public function getLocationData($object)
    {
        return [
            'id' => $object->getId(),
            'name' => $object->getName(),
        ];
    }
}