<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Resource;

use Domain\Job\Entity\JobTitle;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin JobTitle
 */
class CompanyJobTitleListResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'status' => $this->getStatus(),
        ];
    }
}