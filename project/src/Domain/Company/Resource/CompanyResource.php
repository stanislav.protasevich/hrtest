<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Resource;


use Domain\Company\Entity\Company;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Company
 */
class CompanyResource extends JsonResource
{
    public function fields()
    {
        return $this->prepare($this->resource);
    }

    protected function prepare(Company $company)
    {
        return [
            'id' => $company->getId(),
            'name' => $company->getName(),
            'status' => $company->getStatus(),
            'image' => FileHelper::absoluteUrl($company->getImage(), $company->getRelativeLogoPath()),
            'address' => new CompanyAddressResource($company->getAddress()),
            'stats' => [
                'employeesCount' => $company->getEmployees()->count(),
            ],
            'hr' => $company->getHr() ? new UserSimpleResource($company->getHr()) : null,
        ];
    }
}
