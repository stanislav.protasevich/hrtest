<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Company\Resource;


use Domain\Company\Entity\Department;

class CompanyViewResource extends CompanyResource
{
    public function fields()
    {
        $fields = [
            'departments' => array_map(function (Department $department) {
                return new DepartmentDetailResource($department);
            }, $this->getDepartments()->getValues()),
            'address' => $this->getAddress() ? [
                'id' => $this->getAddress()->getId(),
                'type' => $this->getAddress()->getType(),
                'country' => $this->getAddress()->getCountry()->getId(),
                'region' => $this->getAddress()->getRegion()->getId(),
                'city' => $this->getAddress()->getCity()->getId(),
                'postCode' => $this->getAddress()->getPostCode(),
                'street' => $this->getAddress()->getStreet(),
                'building' => $this->getAddress()->getBuilding(),
                'room' => $this->getAddress()->getRoom(),
                'latitude' => $this->getAddress()->getLatitude() ? (float)$this->$this->getAddress()() : null,
                'longitude' => $this->getAddress()->getLongitude() ? (float)$this->getAddress()->getLongitude() : null,
            ] : null,
        ];

        return array_merge(parent::fields(), $fields);
    }
}