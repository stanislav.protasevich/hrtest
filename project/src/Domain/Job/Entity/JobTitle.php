<?php

declare(strict_types=1);

namespace Domain\Job\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\JobBundle\Repository\JobTitleRepository")
 * @ORM\Table(name="jb_job_title")
 *
 * @UniqueEntity("name")
 *
 * @ExclusionPolicy("all")
 */
class JobTitle
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled')")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="Domain\Employee\Entity\EmployeeCompany", mappedBy="jobTitle", fetch="EXTRA_LAZY")
     */
    protected $employeeCompanies;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\Company\Entity\Company", mappedBy="jobTitles", fetch="EXTRA_LAZY")
     * @SWG\Property(type="string")
     * @Groups({"all"})
     */
    protected $company;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employeeCompanies = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return JobTitle
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return JobTitle
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return int
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return JobTitle
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User $createdBy
     *
     * @return JobTitle
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User $updatedBy
     *
     * @return JobTitle
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add employeeCompany.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employeeCompany
     *
     * @return JobTitle
     */
    public function addEmployeeCompany(\Domain\Employee\Entity\EmployeeCompany $employeeCompany)
    {
        $this->employeeCompanies[] = $employeeCompany;

        return $this;
    }

    /**
     * Remove employeeCompany.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employeeCompany
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmployeeCompany(\Domain\Employee\Entity\EmployeeCompany $employeeCompany)
    {
        return $this->employeeCompanies->removeElement($employeeCompany);
    }

    /**
     * Get employeeCompanies.
     *
     * @return
     */
    public function getEmployeeCompanies()
    {
        return $this->employeeCompanies;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return JobTitle
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
