<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Dashboard\Resources;


use Carbon\Carbon;
use Domain\User\Entity\User;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class DashboardInfoResource extends JsonResource
{
    public function fields()
    {
        $employeeCompany = $this->getMainEmployeeCompany() ?: $this->getFirstEmployeeCompany();

        $company = $employeeCompany->getCompany();

        return [
            'company' => [
                'name' => $company->getName(),
                'days' => Carbon::instance($employeeCompany->getStartAt())->diffInDays(),
            ],
            'hr' => $company->getHr() ? new UserSimpleResource($company->getHr()) : null,
            'pe' => [
                'date' => Carbon::now()->format(\DateTime::ATOM) //TODO
            ],
            'now' => Carbon::now()->format(\DateTime::ATOM)
        ];
    }
}
