<?php

declare(strict_types=1);

namespace Domain\Leaves\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LeavesBundle\Repository\LeavesRepository")
 * @ORM\Table(name="`lv_leaves`")
 * @ExclusionPolicy("all")
 */
class Leaves
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Leaves\Entity\LeaveRequests", inversedBy="leaves")
     * @ORM\JoinColumn(name="leave_request_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $requests;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $date;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $lengthDays;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected $lengthHours;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $endTime;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Leaves
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get lengthDays.
     *
     * @return float
     */
    public function getLengthDays()
    {
        return $this->lengthDays;
    }

    /**
     * Set lengthDays.
     *
     * @param float $lengthDays
     *
     * @return Leaves
     */
    public function setLengthDays($lengthDays)
    {
        $this->lengthDays = $lengthDays;

        return $this;
    }

    /**
     * Get lengthHours.
     *
     * @return float
     */
    public function getLengthHours()
    {
        return $this->lengthHours;
    }

    /**
     * Set lengthHours.
     *
     * @param float $lengthHours
     *
     * @return Leaves
     */
    public function setLengthHours($lengthHours)
    {
        $this->lengthHours = $lengthHours;

        return $this;
    }

    /**
     * Get startTime.
     *
     * @return \DateTime
     */
    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    /**
     * Set startTime.
     *
     * @param \DateTime $startTime
     *
     * @return Leaves
     */
    public function setStartTime(?\DateTime $startTime = null)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get endTime.
     *
     * @return \DateTime
     */
    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    /**
     * Set endTime.
     *
     * @param \DateTime $endTime
     *
     * @return Leaves
     */
    public function setEndTime(?\DateTime $endTime = null)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get requests.
     *
     * @return \Domain\Leaves\Entity\LeaveRequests|null
     */
    public function getRequests()
    {
        return $this->requests;
    }

    /**
     * Set requests.
     *
     * @param \Domain\Leaves\Entity\LeaveRequests|null $requests
     *
     * @return Leaves
     */
    public function setRequests(\Domain\Leaves\Entity\LeaveRequests $requests = null)
    {
        $this->requests = $requests;

        return $this;
    }
}
