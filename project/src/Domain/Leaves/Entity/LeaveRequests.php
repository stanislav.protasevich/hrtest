<?php

declare(strict_types=1);

namespace Domain\Leaves\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LeavesBundle\Repository\LeavesRequestRepository")
 * @ORM\Table(name="`lv_leave_requests`")
 */
class LeaveRequests
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_DENIED = 'denied';
    public const STATUS_DELETED = 'deleted';

    public const STATUSES = [
        self::STATUS_APPROVED,
        self::STATUS_PENDING,
        self::STATUS_DENIED,
        self::STATUS_DELETED,
    ];

    public const TARGET_SELF = 'self';
    public const TARGET_APPOINTED = 'appointed';
    public const TARGETS = [
        self::TARGET_SELF,
        self::TARGET_APPOINTED,
    ];

    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\TimeOff\Entity\Policy", cascade={"persist"})
     * @ORM\JoinColumn(name="policy_id", referencedColumnName="id", nullable=false)
     */
    protected $policy;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\TimeOff\Entity\TimeOffType", inversedBy="policies", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $timeOffType;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", length=3, nullable=false)
     */
    protected $days;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $endDate;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $reason;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('pending', 'approved', 'denied', 'deleted')")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('self', 'appointed')")
     */
    protected $target;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="substitute_user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $substituteUser;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $updatedBy;
    /**
     * @ORM\OneToMany(targetEntity="Domain\Leaves\Entity\Leaves", mappedBy="requests", fetch="EXTRA_LAZY", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $leaves;

    /**
     * @ORM\OneToMany(targetEntity="Domain\Leaves\Entity\LeaveRequestSupervisor", mappedBy="leaveRequests", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     */
    protected $requestSupervisor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leaves = new ArrayCollection();
        $this->requestSupervisor = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public static function statuses(): array
    {
        return ArrayHelper::keyValueHuman(self::STATUSES);
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get days.
     *
     * @return int
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set days.
     *
     * @param int $days
     *
     * @return LeaveRequests
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate(): ?\DateTime
    {
        return $this->startDate;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime $startDate
     *
     * @return LeaveRequests
     */
    public function setStartDate(?\DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime $endDate
     *
     * @return LeaveRequests
     */
    public function setEndDate(?\DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get reason.
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set reason.
     *
     * @param string $reason
     *
     * @return LeaveRequests
     */
    public function setReason(?string $reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return LeaveRequests
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return LeaveRequests
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return LeaveRequests
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get policy.
     *
     * @return \Domain\TimeOff\Entity\Policy
     */
    public function getPolicy(): Policy
    {
        return $this->policy ?: new Policy();
    }

    /**
     * Set policy.
     *
     * @param \Domain\TimeOff\Entity\Policy $policy
     *
     * @return LeaveRequests
     */
    public function setPolicy(Policy $policy)
    {
        $this->policy = $policy;

        return $this;
    }

    /**
     * Get timeOffType.
     *
     * @return \Domain\TimeOff\Entity\TimeOffType|null
     */
    public function getTimeOffType()
    {
        return $this->timeOffType;
    }

    /**
     * Set timeOffType.
     *
     * @param \Domain\TimeOff\Entity\TimeOffType|null $timeOffType
     *
     * @return LeaveRequests
     */
    public function setTimeOffType(TimeOffType $timeOffType = null)
    {
        $this->timeOffType = $timeOffType;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return LeaveRequests
     */
    public function setUser(?User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get substituteUser.
     *
     * @return \Domain\User\Entity\User
     */
    public function getSubstituteUser()
    {
        return $this->substituteUser;
    }

    /**
     * Set substituteUser.
     *
     * @param \Domain\User\Entity\User $substituteUser
     *
     * @return LeaveRequests
     */
    public function setSubstituteUser(?User $substituteUser)
    {
        $this->substituteUser = $substituteUser;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User $createdBy
     *
     * @return LeaveRequests
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User $updatedBy
     *
     * @return LeaveRequests
     */
    public function setUpdatedBy(User $updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add leaf.
     *
     * @param \Domain\Leaves\Entity\Leaves $leaf
     *
     * @return LeaveRequests
     */
    public function addLeave(Leaves $leaf)
    {
        $this->leaves[] = $leaf;
        $leaf->setRequests($this);

        return $this;
    }

    /**
     * Remove leaf.
     *
     * @param \Domain\Leaves\Entity\Leaves $leaf
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLeave(Leaves $leaf)
    {
        return $this->leaves->removeElement($leaf);
    }

    public function removeLeaves()
    {
        $this->leaves = new ArrayCollection();
    }

    /**
     * Get leaves.
     *
     * @return ArrayCollection
     */
    public function getLeaves()
    {
        return $this->leaves;
    }

    /**
     * Add requestSupervisor.
     *
     * @param \Domain\Leaves\Entity\LeaveRequestSupervisor $requestSupervisor
     *
     * @return LeaveRequests
     */
    public function addRequestSupervisor(\Domain\Leaves\Entity\LeaveRequestSupervisor $requestSupervisor)
    {
        $this->requestSupervisor[] = $requestSupervisor;
        $requestSupervisor->setLeaveRequests($this);

        return $this;
    }

    /**
     * Remove requestSupervisor.
     *
     * @param \Domain\Leaves\Entity\LeaveRequestSupervisor $requestSupervisor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRequestSupervisor(\Domain\Leaves\Entity\LeaveRequestSupervisor $requestSupervisor)
    {
        return $this->requestSupervisor->removeElement($requestSupervisor);
    }

    /**
     * Get requestSupervisor.
     *
     * @return ArrayCollection
     */
    public function getRequestSupervisor()
    {
        return $this->requestSupervisor;
    }

    /**
     * Add leaf.
     *
     * @param \Domain\Leaves\Entity\Leaves $leaf
     *
     * @return LeaveRequests
     */
    public function addLeafe(\Domain\Leaves\Entity\Leaves $leaf)
    {
        $this->leaves[] = $leaf;

        return $this;
    }

    /**
     * Remove leaf.
     *
     * @param \Domain\Leaves\Entity\Leaves $leaf
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLeafe(\Domain\Leaves\Entity\Leaves $leaf)
    {
        return $this->leaves->removeElement($leaf);
    }

    /**
     * Get target.
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set target.
     *
     * @param string $target
     *
     * @return LeaveRequests
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }
}
