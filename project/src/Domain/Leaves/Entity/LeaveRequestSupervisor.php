<?php

declare(strict_types=1);

namespace Domain\Leaves\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LeavesBundle\Repository\LeaveRequestSupervisorRepository")
 * @ORM\Table(name="`lv_leave_request_supervisor`")
 * @ExclusionPolicy("all")
 */
class LeaveRequestSupervisor
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_APPROVED = 'approved';
    public const STATUS_DENIED = 'denied';
    public const STATUSES = [
        self::STATUS_APPROVED,
        self::STATUS_PENDING,
        self::STATUS_DENIED,
    ];

    public const STATUSES_CAN_CHANGE = [
        self::STATUS_APPROVED,
        self::STATUS_DENIED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Leaves\Entity\LeaveRequests", inversedBy="requestSupervisor")
     * @ORM\JoinColumn(name="leave_request_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $leaveRequests;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     * @Expose()
     */
    protected $user;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('pending', 'approved', 'denied')")
     */
    protected $status;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $comment;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return LeaveRequestSupervisor
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return LeaveRequestSupervisor
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return LeaveRequestSupervisor
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return int
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return LeaveRequestSupervisor
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get leaveRequests.
     *
     * @return \Domain\Leaves\Entity\LeaveRequests
     */
    public function getLeaveRequests()
    {
        return $this->leaveRequests;
    }

    /**
     * Set leaveRequests.
     *
     * @param \Domain\Leaves\Entity\LeaveRequests $leaveRequests
     *
     * @return LeaveRequestSupervisor
     */
    public function setLeaveRequests(\Domain\Leaves\Entity\LeaveRequests $leaveRequests)
    {
        $this->leaveRequests = $leaveRequests;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return LeaveRequestSupervisor
     */
    public function setUser(\Domain\User\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
