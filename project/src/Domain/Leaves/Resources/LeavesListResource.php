<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Leaves\Resources;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Resource\UserSimpleResource;

/**
 * @mixin LeaveRequests
 */
class LeavesListResource extends LeaveSimpleResource
{
    public function fields()
    {
        $fields = [
            'user' => (new UserSimpleResource($this->getUser()))->toArray(),
        ];

        return array_merge(parent::fields(), $fields);
    }
}