<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Leaves\Resources;


class LeavesViewResource extends LeavesListResource
{
    public function fields()
    {
        $fields = parent::fields();

//        unset($fields['user']);

        $fields['timeOffType'] = $this->getTimeOffType()->getId();

        return $fields;
    }
}