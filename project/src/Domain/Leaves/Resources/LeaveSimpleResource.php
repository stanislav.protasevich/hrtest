<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Leaves\Resources;

use Domain\Leaves\Entity\LeaveRequests;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Resource\TimeOffTypeResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin LeaveRequests
 */
class LeaveSimpleResource extends JsonResource
{
    public function fields()
    {
        $timeOfType = $this->getTimeOffType();
        $policy = $this->getPolicy();

        return [
            'id' => $this->getId(),
            'timeOffType' => (new TimeOffTypeResource($timeOfType))->toArray(),
            'days' => $this->getDays(),
            'hours' => $this->getDays() * Policy::DEFAULT_LENGTH_HOURS,
            'duration' => sprintf('%s %s', $this->getDays(), $policy->getCountType()),
            'startDate' => $this->getStartDate()->format(\DateTime::ATOM),
            'endDate' => $this->getEndDate()->format(\DateTime::ATOM),
            'budget' => null,
            'status' => $this->getStatus(),
            'reason' => $this->getReason(),
        ];
    }
}