<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Leaves\Resources;


use Domain\TimeOff\Resource\PolicyResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;
use Infrastructure\LeavesBundle\Service\dto\LeaveUserStatsDto;

/**
 * @mixin LeaveUserStatsDto
 */
class LeavesUserStatsResource extends JsonResource
{
    public function fields()
    {
        return [
            'timeOffType' => $this->timeOffType,
            'left' => $this->left,
            'days' => $this->days,
            'total' => $this->total,
            'period' => [
                'start' => $this->period->start,
                'end' => $this->period->end,
            ],
            'policy' => new PolicyResource($this->policy)
        ];
    }
}
