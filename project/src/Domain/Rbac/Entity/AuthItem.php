<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\Rbac\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\RbacBundle\Repository\AuthItemRepository")
 * @ORM\Table(name="auth_item")
 *
 * @UniqueEntity("name")
 */
class AuthItem
{
    public const TYPE_ROLE = 'role';
    public const TYPE_PERMISSION = 'permission';
    public const TYPES = [
        self::TYPE_ROLE,
        self::TYPE_PERMISSION,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=150)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('role', 'permission')")
     */
    protected $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Rbac\Entity\AuthRule", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="rule_name", referencedColumnName="name", nullable=true)
     */
    protected $rule;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $data;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\Rbac\Entity\AuthItem", inversedBy="children", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="auth_item_child",
     *      joinColumns={@ORM\JoinColumn(name="child_name", referencedColumnName="name", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="parent_name", referencedColumnName="name", onDelete="CASCADE")}
     * )
     */
    protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\Rbac\Entity\AuthItem", mappedBy="parent", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     */
    protected $children;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;
    /**
     * @ORM\ManyToMany(targetEntity="Domain\User\Entity\User", inversedBy="permissions", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="auth_assignment",
     *      joinColumns={@ORM\JoinColumn(name="item_name", referencedColumnName="name", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $assignment;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parent = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->assignment = new ArrayCollection();
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return AuthItem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return AuthItem
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return AuthItem
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data.
     *
     * @param string|null $data
     *
     * @return AuthItem
     */
    public function setData($data = null)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return AuthItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return AuthItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get rule.
     *
     * @return \Domain\Rbac\Entity\AuthRule|null
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Set rule.
     *
     * @param \Domain\Rbac\Entity\AuthRule|null $rule
     *
     * @return AuthItem
     */
    public function setRule(\Domain\Rbac\Entity\AuthRule $rule = null)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * Add child.
     *
     * @param \Domain\Rbac\Entity\AuthItem $child
     *
     * @return AuthItem
     */
    public function addChild(\Domain\Rbac\Entity\AuthItem $child)
    {
        $this->children[] = $child;
        $child->addParent($this);

        return $this;
    }

    /**
     * Add parent.
     *
     * @param \Domain\Rbac\Entity\AuthItem $parent
     *
     * @return AuthItem
     */
    public function addParent(\Domain\Rbac\Entity\AuthItem $parent)
    {
        $this->parent[] = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Remove child.
     *
     * @param \Domain\Rbac\Entity\AuthItem $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(\Domain\Rbac\Entity\AuthItem $child)
    {
        $child->removeParent($this);
        return $this->children->removeElement($child);
    }

    /**
     * Remove parent.
     *
     * @param \Domain\Rbac\Entity\AuthItem $parent
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeParent(\Domain\Rbac\Entity\AuthItem $parent)
    {
        return $this->parent->removeElement($parent);
    }

    /**
     * Get children.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add assignment.
     *
     * @param \Domain\User\Entity\User $assignment
     *
     * @return AuthItem
     */
    public function addAssignment(?User $assignment)
    {
        $this->assignment[] = $assignment;

        return $this;
    }

    /**
     * Remove assignment.
     *
     * @param \Domain\User\Entity\User $assignment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAssignment(\Domain\User\Entity\User $assignment)
    {
        return $this->assignment->removeElement($assignment);
    }

    /**
     * Get assignment.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}
