<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Rbac\Resource;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin AuthItem
 */
class RbacPermissionUserList extends JsonResource
{
    public function fields()
    {
        return [
            'name' => $this->getName(),
        ];
    }
}