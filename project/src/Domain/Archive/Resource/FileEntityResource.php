<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Archive\Resource;


use Domain\Archive\Entity\File;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin File
 */
class FileEntityResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'type' => $this->getType(),
            'extension' => $this->getExtension(),
            'file' => $this->getUrl(),
            'size' => $this->getSize(),
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
        ];
    }
}