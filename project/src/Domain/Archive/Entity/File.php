<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\Archive\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\ArchiveBundle\Repository\FileRepository")
 * @ORM\Table(name="ar_file")
 */
class File
{
    public const MIME_TYPE_PDF = 'application/pdf';

    public const MIME_TYPE_TEXT_PLAIN = 'text/plain';
    public const MIME_TYPE_MS_DOC = 'application/msword';
    public const MIME_TYPE_MS_DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    public const MIME_TYPE_MS_EXCEL = 'application/vnd.ms-excel';
    public const MIME_TYPE_MS_EXCELX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    public const MIME_TYPE_MS_POWERPOINT = 'application/vnd.ms-powerpoint';
    public const MIME_TYPE_MS_POWERPOINTX = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';

    public const MIME_TYPE_IMAGE_JPEG = 'image/jpeg';
    public const MIME_TYPE_IMAGE_JPG = 'image/jpg';
    public const MIME_TYPE_IMAGE_PNG = 'image/png';

    public const MIME_TYPE_VIDEO_MPEG = 'video/mpeg';
    public const MIME_TYPE_VIDEO_MP4 = 'video/mp4';
    public const MIME_TYPE_VIDEO_OGG = 'video/ogg';
    public const MIME_TYPE_VIDEO_AVI = 'video/x-msvideo';
    public const MIME_TYPE_VIDEO_WMV = 'video/x-ms-wmv';

    public const MIME_TYPE_ARCHIVE_ZIP = 'application/zip';
    public const MIME_TYPE_ARCHIVE_BZIP = 'application/x-bzip';
    public const MIME_TYPE_ARCHIVE_BZIP2 = 'application/x-bzip2';
    public const MIME_TYPE_ARCHIVE_7Z = 'application/x-7z-compressed';
    public const MIME_TYPE_ARCHIVE_RAR = 'application/x-rar-compressed';
    public const MIME_TYPE_ARCHIVE_TAR = 'application/x-tar';

    public const MIME_TYPE_WEB = [
        self::MIME_TYPE_IMAGE_JPEG,
        self::MIME_TYPE_IMAGE_JPG,
        self::MIME_TYPE_IMAGE_PNG,
        self::MIME_TYPE_VIDEO_MPEG,
        self::MIME_TYPE_VIDEO_MP4,
        self::MIME_TYPE_VIDEO_OGG,
        self::MIME_TYPE_VIDEO_AVI,
        self::MIME_TYPE_VIDEO_WMV,
        self::MIME_TYPE_PDF,
    ];

    public const MIME_TYPE_IMAGES = [
        self::MIME_TYPE_IMAGE_JPEG,
        self::MIME_TYPE_IMAGE_JPG,
        self::MIME_TYPE_IMAGE_PNG,
    ];

    public const MIME_TYPE_VIDEOS = [
        self::MIME_TYPE_VIDEO_MPEG,
        self::MIME_TYPE_VIDEO_MP4,
        self::MIME_TYPE_VIDEO_OGG,
        self::MIME_TYPE_VIDEO_AVI,
        self::MIME_TYPE_VIDEO_WMV,
    ];

    public const MIME_TYPE_DOCS = [
        self::MIME_TYPE_PDF,
        self::MIME_TYPE_TEXT_PLAIN,
        self::MIME_TYPE_MS_DOC,
        self::MIME_TYPE_MS_DOCX,
        self::MIME_TYPE_MS_EXCEL,
        self::MIME_TYPE_MS_EXCELX,
        self::MIME_TYPE_MS_POWERPOINT,
        self::MIME_TYPE_MS_POWERPOINTX,
    ];

    public const MIME_TYPES = [
        self::MIME_TYPE_IMAGE_JPEG,
        self::MIME_TYPE_IMAGE_JPG,
        self::MIME_TYPE_IMAGE_PNG,
        self::MIME_TYPE_VIDEO_MPEG,
        self::MIME_TYPE_VIDEO_MP4,
        self::MIME_TYPE_VIDEO_OGG,
        self::MIME_TYPE_VIDEO_AVI,
        self::MIME_TYPE_VIDEO_WMV,
        self::MIME_TYPE_PDF,
        self::MIME_TYPE_MS_DOC,
        self::MIME_TYPE_MS_DOCX,
        self::MIME_TYPE_MS_EXCEL,
        self::MIME_TYPE_MS_EXCELX,
        self::MIME_TYPE_MS_POWERPOINT,
        self::MIME_TYPE_MS_POWERPOINTX,
        self::MIME_TYPE_ARCHIVE_ZIP,
        self::MIME_TYPE_ARCHIVE_BZIP,
        self::MIME_TYPE_ARCHIVE_BZIP2,
        self::MIME_TYPE_ARCHIVE_7Z,
        self::MIME_TYPE_ARCHIVE_RAR,
        self::MIME_TYPE_ARCHIVE_TAR,
    ];

    public const MIME_TYPE_ARCHIVES = [
        self::MIME_TYPE_ARCHIVE_ZIP,
        self::MIME_TYPE_ARCHIVE_BZIP,
        self::MIME_TYPE_ARCHIVE_BZIP2,
        self::MIME_TYPE_ARCHIVE_7Z,
        self::MIME_TYPE_ARCHIVE_RAR,
        self::MIME_TYPE_ARCHIVE_TAR,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $extension;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $type;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $mimeType;

    /**
     * @ORM\Column(type="integer")
     * @var int Size in bytes
     */
    protected $size;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $updatedBy;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get extension.
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set extension.
     *
     * @param string $extension
     *
     * @return File
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get mimeType.
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Set mimeType.
     *
     * @param string $mimeType
     *
     * @return File
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get size.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set size.
     *
     * @param int $size
     *
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return File
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return File
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return File
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return File
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return File
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getUrl(): string
    {
        /* @var $router UrlGeneratorInterface */
        $router = app('router');

        return $router->generate('api_archive_view', [
            'id' => $this->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}