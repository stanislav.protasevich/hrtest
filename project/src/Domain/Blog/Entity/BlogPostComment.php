<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\User\Entity\User;
use Gedmo\Mapping\Annotation as Gedmo;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="Infrastructure\BlogBundle\Repository\BlogPostCommentRepository")
 * @ORM\Table(name="b_blog_post_comment")
 */
class BlogPostComment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier.")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Blog\Entity\BlogPost", inversedBy="comments")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $post;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedBy;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedAt;

    protected $cast = [
        'createdAt' => 'datetime:ATOM',
        'updatedAt' => 'datetime:ATOM',
    ];

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;
    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;
    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;
    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Domain\Blog\Entity\BlogPostComment")
     * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Domain\Blog\Entity\BlogPostComment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @ORM\OneToMany(targetEntity="Domain\Blog\Entity\BlogPostComment", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return BlogPostComment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return BlogPostComment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return BlogPostComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get post.
     *
     * @return \Domain\Blog\Entity\BlogPost|null
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set post.
     *
     * @param \Domain\Blog\Entity\BlogPost|null $post
     *
     * @return BlogPostComment
     */
    public function setPost(BlogPost $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return BlogPostComment
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return BlogPostComment
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get lft.
     *
     * @return string
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lft.
     *
     * @param string $lft
     *
     * @return BlogPostComment
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lvl.
     *
     * @return string
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set lvl.
     *
     * @param string $lvl
     *
     * @return BlogPostComment
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get rgt.
     *
     * @return string
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set rgt.
     *
     * @param string $rgt
     *
     * @return BlogPostComment
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get root.
     *
     * @return \Domain\Blog\Entity\BlogPostComment|null
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set root.
     *
     * @param \Domain\Blog\Entity\BlogPostComment|null $root
     *
     * @return BlogPostComment
     */
    public function setRoot(BlogPostComment $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \Domain\Blog\Entity\BlogPostComment|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent.
     *
     * @param \Domain\Blog\Entity\BlogPostComment|null $parent
     *
     * @return BlogPostComment
     */
    public function setParent(BlogPostComment $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add child.
     *
     * @param \Domain\Blog\Entity\BlogPostComment $child
     *
     * @return BlogPostComment
     */
    public function addChild(BlogPostComment $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \Domain\Blog\Entity\BlogPostComment $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(BlogPostComment $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }
}
