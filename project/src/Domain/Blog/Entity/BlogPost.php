<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\User\Entity\User;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\BlogBundle\Repository\BlogPostRepository")
 * @ORM\Table(name="b_blog_post")
 */
class BlogPost
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_DELETED = 'deleted';
    public const STATUSES = [
        self::STATUS_DISABLED,
        self::STATUS_ACTIVE,
        self::STATUS_DELETED,
    ];

    public const VISIBILITY_ALL = 'all';
    public const VISIBILITY_PROJECT = 'project';
    public const VISIBILITY_LOCATION = 'location';
    public const VISIBILITIES = [
        self::VISIBILITY_ALL,
        self::VISIBILITY_PROJECT,
        self::VISIBILITY_LOCATION
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier.")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @SWG\Property(type="string", maxLength=255)
     * @Groups({"api","all"})
     */
    protected $title;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @SWG\Property(type="string")
     *
     * @Groups("all")
     */
    protected $image;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled', 'deleted')")
     *
     * @SWG\Property(type="string", enum={"active","disabled", "deleted"})
     * @Groups({"api","all"})
     */
    protected $status;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 0})
     *
     * @SWG\Property(type="integer")
     * @Groups({"api","all"})
     */
    protected $viewCount;
    /**
     * @ORM\ManyToMany(targetEntity="Domain\Blog\Entity\BlogCategory", inversedBy="posts", fetch="EXTRA_LAZY", cascade={"persist"} )
     * @ORM\JoinTable("b_blog_post_to_category")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $categories;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedBy;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedAt;
    /**
     * @ORM\OneToMany(targetEntity="Domain\Blog\Entity\BlogPostComment", mappedBy="post")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $comments;
    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('all', 'project', 'location')")
     * @SWG\Property(type="string", enum={"all","project", "location"})
     *
     * @Groups({"api","all"})
     */
    private $visibility;

    /**
     * @ORM\OneToMany(targetEntity="BlogPostDestination", mappedBy="post", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $destination;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->destination = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name.
     *
     * @param string $title
     *
     * @return BlogPost
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return BlogPost
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return BlogPost
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return BlogPost
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return BlogPost
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Add category.
     *
     * @param \Domain\Blog\Entity\BlogCategory $category
     *
     * @return BlogPost
     */
    public function addCategory(BlogCategory $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category.
     *
     * @param \Domain\Blog\Entity\BlogCategory $category
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCategory(BlogCategory $category)
    {
        return $this->categories->removeElement($category);
    }

    /**
     * Get categories.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return BlogPost
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return BlogPost
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return BlogPost
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getRelativeImagePath(): string
    {
        return "blog/post/{$this->id}";
    }

    /**
     * Get viewCount.
     *
     * @return int
     */
    public function getViewCount(): int
    {
        return $this->viewCount;
    }

    /**
     * Set viewCount.
     *
     * @param int $viewCount
     *
     * @return BlogPost
     */
    public function setViewCount(int $viewCount)
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * Set viewCount.
     *
     * @return void
     */
    public function addViewCount(): void
    {
        $this->viewCount++;
    }

    /**
     * Add comment.
     *
     * @param \Domain\Blog\Entity\BlogPostComment $comment
     *
     * @return BlogPost
     */
    public function addComment(BlogPostComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment.
     *
     * @param \Domain\Blog\Entity\BlogPostComment $comment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeComment(BlogPostComment $comment)
    {
        return $this->comments->removeElement($comment);
    }

    /**
     * Get comments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Get visibility.
     *
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set visibility.
     *
     * @param string $visibility
     *
     * @return BlogPost
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Add destination.
     *
     * @param \Domain\Blog\Entity\BlogPostDestination $destination
     *
     * @return BlogPost
     */
    public function addDestination(BlogPostDestination $destination)
    {
        $this->destination[] = $destination;

        $destination->setPost($this);
        return $this;
    }

    /**
     * Remove destination.
     *
     * @param \Domain\Blog\Entity\BlogPostDestination $destination
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDestination(BlogPostDestination $destination)
    {
        return $this->destination->removeElement($destination);
    }

    /**
     * Get destination.
     *
     * @return \Doctrine\Common\Collections\Collection|BlogPostDestination[]
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
