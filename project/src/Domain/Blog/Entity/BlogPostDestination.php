<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\Location\Entity\City;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="b_blog_post_destination")
 *
 * @UniqueEntity("title")
 */
class BlogPostDestination
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier.")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Blog\Entity\BlogPost", inversedBy="destination")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $post;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\City", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=true)
     * @Groups({"api","all"})
     */
    protected $city;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\Company", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=true)
     * @Groups({"api","all"})
     */
    protected $company;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get post.
     *
     * @return \Domain\Blog\Entity\BlogPost|null
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set post.
     *
     * @param \Domain\Blog\Entity\BlogPost|null $post
     *
     * @return BlogPostDestination
     */
    public function setPost(BlogPost $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \Domain\Location\Entity\City|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param \Domain\Location\Entity\City|null $city
     *
     * @return BlogPostDestination
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Set company.
     *
     * @param \Domain\Company\Entity\Company|null $company
     *
     * @return BlogPostDestination
     */
    public function setCompany(\Domain\Company\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \Domain\Company\Entity\Company|null
     */
    public function getCompany()
    {
        return $this->company;
    }
}
