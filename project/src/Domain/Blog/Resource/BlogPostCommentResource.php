<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Resource;


use Domain\Blog\Entity\BlogPostComment;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin BlogPostComment
 */
class BlogPostCommentResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'content' => $this->getContent(),
            'createdBy' => new UserSimpleResource($this->getCreatedBy()),
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
        ];
    }
}
