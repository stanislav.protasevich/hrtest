<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Resource;


use Domain\Blog\Entity\BlogPost;

/**
 * @mixin BlogPost
 */
class BlogPostListResource extends BlogPostResource
{
}
