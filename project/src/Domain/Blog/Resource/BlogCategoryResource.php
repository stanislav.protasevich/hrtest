<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Resource;


use Domain\Blog\Entity\BlogCategory;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin BlogCategory
 */
class BlogCategoryResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'status' => $this->getStatus(),
            'image' => FileHelper::absoluteUrl($this->getImage(), $this->getRelativeImagePath()),
            'createdBy' => $this->getCreatedBy()->getId(),
            'updatedBy' => $this->getUpdatedBy()->getId(),
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
        ];
    }
}
