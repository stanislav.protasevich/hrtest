<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Blog\Resource;


use Domain\Blog\Entity\BlogCategory;
use Domain\Blog\Entity\BlogPost;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin BlogPost
 */
class BlogPostResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'status' => $this->getStatus(),
            'image' => FileHelper::absoluteUrl($this->getImage(), $this->getRelativeImagePath()),
            'viewCount' => $this->getViewCount(),
            'categories' => array_map(
                function (BlogCategory $category) {
                    return new BlogCategoryResource($category);
                },
                $this->getCategories()->getValues()
            ),
            'createdBy' => new UserSimpleResource($this->getCreatedBy()),
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
            'commentsCount' => $this->getComments()->count(),
        ];
    }
}
