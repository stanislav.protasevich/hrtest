<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\User\Entity\EmergencyContact;
use Domain\User\Entity\User;
use Domain\User\Entity\UserChildren;
use Domain\User\Entity\UserFieldValue;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;

/**
 * @mixin User
 */
class UserViewResource extends UserResource
{
    public function fields()
    {
        $data = [
//            'employeeCompanies' => array_map(function (EmployeeCompany $company) {
//                return new EmployeeCompanyResource($company);
//            }, $this->getEmployeeCompanies()->getValues()),
//            'skills' => array_map(
//                function (Skill $skill) {
//                    return new SkillResource($skill);
//                },
//                $this->getSkills()->getValues()
//            ),
//            'supervisors' => array_map(
//                function (User $user) {
//                    return new UserSupervisorResource($user);
//                },
//                $this->getSupervisors()->getValues()
//            ),
            'children' => array_map(
                function (UserChildren $children) {
                    return new UserChildrenResource($children);
                },
                $this->getChildren()->getValues()
            ),

            'emergencyContacts' => array_map(
                function (EmergencyContact $item) {
                    return new EmergencyContactResource($item);
                },
                $this->getEmergencyContacts()->getValues()
            ),

            'fieldValues' => array_map(function (UserFieldValue $item) {
                return [
                    'field' => $item->getField()->getId(),
                    'group' => $item->getField()->getGroup(),
                    'value' => $item->getValue(),
                ];
            }, $this->getFieldValues()->getValues()),

            'employeeCompany' => $this->getMainEmployeeCompany() ? new EmployeeCompanyResource($this->getMainEmployeeCompany()) : null,

            'hobbies' => ArrayHelper::getColumn($this->getHobbies()->getValues(), 'id'),

            'employee' => new UserEmployeeResource($this->getEmployee()),

            'recruiters' => ArrayHelper::getColumn($this->getRecruiters()->getValues(), 'id'),
        ];

        return array_merge(parent::fields(), $data);
    }
}
