<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class UserSimpleResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'email' => $this->getEmail(),
            'status' => $this->getStatus(),
            'profile' => new UserProfileResource($this->getProfile()),
        ];
    }
}