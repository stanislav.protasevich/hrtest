<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;

/**
 * @mixin User
 */
class UserViewCompanyResource extends UserResource
{
    public function fields()
    {
        return [
            'employeeCompanies' => array_map(function (EmployeeCompany $company) {
                return new EmployeeCompanyResource($company);
            }, $this->getEmployeeCompanies()->getValues()),
        ];
    }
}