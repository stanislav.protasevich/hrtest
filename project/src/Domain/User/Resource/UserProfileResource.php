<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserProfile;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserProfile
 */
class UserProfileResource extends JsonResource
{
    public function fields()
    {
        return [
            'firstName' => $this->getFirstName(),
            'middleName' => $this->getMiddleName(),
            'surname' => $this->getSurname(),
            'fullName' => $this->getFullName(),
            'birthday' => function (UserProfile $profile) {
                return $profile->getBirthday() ? $profile->getBirthday()->format(\DateTime::ATOM) : null;
            },
            'maritalStatus' => $this->getMaritalStatus(),
            'marriageAnniversary' => function (UserProfile $profile) {
                return $profile->getMarriageAnniversary() ? $profile->getMarriageAnniversary()->format(\DateTime::ATOM) : null;
            },
            'gender' => $this->getGender(),
            'avatar' => function (UserProfile $profile) {
                return $profile->getAvatarUrl();
            },
            'phone' => $this->getPhone(),
        ];
    }
}