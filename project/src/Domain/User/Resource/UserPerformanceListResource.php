<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserPerformance
 */
class UserPerformanceListResource extends JsonResource
{
    public function fields()
    {
        $formsList = UserPerformance::FORM_MAPPING[$this->getType()];

        $forms = [];

        foreach ($formsList as $author) {

            /** @var UserPerformanceForm $peForm */
            $peForm = $this->getPerformanceForm()->filter(function (UserPerformanceForm $performanceForm) use ($author) {
                return $performanceForm->getAuthor() === $author;
            })->first();

            $form = [
                'completed' => false,
                'author' => $author,
            ];

            if ($peForm) {
                $form['id'] = $peForm->getId();
                $form['completed'] = true;
            }

            $forms[] = $form;
        }

        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'status' => $this->getStatus(),
            'comment' => $this->getComment(),
            'date' => $this->getDate()->format(\DateTime::ATOM),
            'nextDate' => $this->getNextDate() ? $this->getNextDate()->format(\DateTime::ATOM) : null,
            'forms' => $forms,
        ];
    }
}