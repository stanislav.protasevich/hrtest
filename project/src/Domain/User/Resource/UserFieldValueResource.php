<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserFieldValue;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserFieldValue
 */
class UserFieldValueResource extends JsonResource
{
    public function fields()
    {
        return [
            'field' => $this->getField()->getId(),
            'group' => $this->getField()->getGroup(),
            'value' => $this->getValue()
        ];
    }
}