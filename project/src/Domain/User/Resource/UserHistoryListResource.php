<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Resources\LeaveSimpleResource;

/**
 * @mixin LeaveRequests
 */
class UserHistoryListResource extends LeaveSimpleResource
{
}