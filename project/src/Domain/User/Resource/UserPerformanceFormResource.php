<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserPerformanceForm
 */
class UserPerformanceFormResource extends JsonResource
{
    public function fields()
    {
        $data = $this->getData();

        $attributeDescriptions = $data['descriptions'] ?? [];

        if (isset($data['descriptions'])) {
            unset($data['descriptions']);
        }

        $form = [
            'general' => [
                'jobTitle' => $this->getJobTitle()->getId(),
                'city' => $this->getCity()->getId(),
                'supervisor' => $this->getSupervisor()->getId(),
                'completionAt' => $this->getCompletionAt()->format(\DateTime::ATOM),
                'startAt' => $this->getStartAt()->format(\DateTime::ATOM),
                'endAt' => $this->getEndAt()->format(\DateTime::ATOM),
                'meetingAt' => $this->getMeetingAt()->format(\DateTime::ATOM),
            ],
        ];

        return array_merge([
            'id' => $this->getId(),
            'performance' => $this->getPerformance()->getId(),
            'type' => $this->getType(),
            'author' => $this->getAuthor(),
            'employee' => $this->getEmployee()->getId(),
            'position' => $this->getJobTitle()->getId(),
        ], $form, $data);
    }
}