<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\User\Entity\User;

/**
 * @mixin User
 */
class UserSupervisorsResource extends UserFollowersResource
{

}