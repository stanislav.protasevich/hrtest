<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\User\Entity\UserAddress;

/**
 * @mixin UserAddress
 */
class UserAddressResource extends UserResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'country' => $this->getCountry()->getName(),
            'region' => $this->getRegion()->getName(),
            'city' => $this->getCity()->getName(),
            'postCode' => $this->getPostCode() ? (int)$this->getPostCode() : null,
            'street' => $this->getStreet(),
            'building' => $this->getBuilding(),
            'room' => $this->getRoom(),
            'latitude' => $this->getLatitude() ? (float)$this->getLatitude() : null,
            'longitude' => $this->getLongitude() ? (float)$this->getLongitude() : null,
            'sortOrder' => $this->getSortOrder(),
        ];
    }
}