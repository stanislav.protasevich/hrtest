<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserAlert;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserAlert
 */
class UserAlertResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'user' => new UserSimpleResource($this->getUser()),
            'alertedUserId' => $this->getAlertedUser()->getId(),
            'contentType' => $this->getContentType(),
            'contentId' => $this->getContentId(),
            'action' => $this->getAction(),
            'message' => $this->getMessage(),
            'createdAt' => $this->getCreatedAt(),
            'viewAt' => $this->getViewAt() ? $this->getViewAt()->format(\DateTime::ATOM) : null,
            'read' => $this->getRead(),
        ];
    }
}
