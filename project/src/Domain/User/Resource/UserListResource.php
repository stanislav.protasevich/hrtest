<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

class UserListResource extends UserResource
{
    public function fields()
    {
        return array_merge(parent::fields(), [
            'address' => new UserAddressResource($this->getAddress()),
            'employeeCompany' => $this->getMainEmployeeCompany() ? new EmployeeCompanyResource($this->getMainEmployeeCompany()) : null,
        ]);
    }
}