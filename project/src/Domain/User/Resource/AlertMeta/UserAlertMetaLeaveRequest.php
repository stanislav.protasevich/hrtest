<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Resource\AlertMeta;

use Domain\Leaves\Entity\LeaveRequests;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin LeaveRequests
 */
class UserAlertMetaLeaveRequest extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'status' => $this->getStatus(),
            'days' => $this->getDays(),
            'startDate' => $this->getStartDate(),
            'endDate' => $this->getEndDate(),
            'reason' => $this->getReason(),
        ];
    }
}