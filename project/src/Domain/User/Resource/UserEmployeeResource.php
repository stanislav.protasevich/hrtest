<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Domain\User\Resource;


use Domain\User\Entity\UserEmployee;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;
use Infrastructure\CompanyBundle\Helpers\CompanyAddressHelper;

/**
 * @mixin UserEmployee
 */
class UserEmployeeResource extends JsonResource
{
    public function fields()
    {
        return [
            'jobOfferDate' => $this->getJobOfferDate() ? $this->getJobOfferDate()->format(\DateTime::ATOM) : null,
            'companyAddress' => $this->getCompanyAddress() ? $this->getCompanyAddress()->getId() : null
//            'companyAddress' => [
//        'value' => $this->getCompanyAddress()->getId(),
//        'label' => CompanyAddressHelper::addressFromArray(
//            CompanyAddressHelper::processFields(
//                $this->getCompanyAddress()
//            )
//        )
//    ]
        ];
    }
}
