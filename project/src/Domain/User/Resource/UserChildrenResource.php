<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\UserChildren;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin UserChildren
 */
class UserChildrenResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'birthday' => function (UserChildren $children) {
                return $children->getBirthday() ? $children->getBirthday()->format(\DateTime::ATOM) : null;
            },
            'age' => $this->getAge(),
            'gender' => $this->getGender(),
            'sortOrder' => $this->getSortOrder(),
        ];
    }
}