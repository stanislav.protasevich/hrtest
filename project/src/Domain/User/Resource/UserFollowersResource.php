<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin User
 */
class UserFollowersResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'profile' => new UserProfileResource($this->getProfile()),
            'employeeCompany' => $this->getMainEmployeeCompany() ? function (User $user) {
                $employeeCompany = $user->getMainEmployeeCompany();
                return [
                    'id' => $employeeCompany->getId(),
                    'jobTitle' => $employeeCompany->getJobTitle()->getName(),
                ];
            } : null,
        ];
    }
}