<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Resource;


use Carbon\Carbon;
use Domain\Company\Resource\CompanyResource;
use Domain\Company\Resource\DepartmentResource;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Resource\JobTitleResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin EmployeeCompany
 */
class EmployeeCompanyResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'company' => $this->getCompany() ? new CompanyResource($this->getCompany()) : null,
            'department' => $this->getDepartment() ? new DepartmentResource($this->getDepartment()) : null,
            'jobTitle' => $this->getJobTitle() ? new JobTitleResource($this->getJobTitle()) : null,
            'status' => $this->getStatus(),
            'salary' => $this->getSalary(),
            'salaryIncrease' => $this->getSalaryIncrease(),
            'workType' => $this->getWorkType(),
            'currency' => $this->getCurrency() ? $this->getCurrency()->getId() : null,
            'isMain' => (bool)$this->getIsMain(),
            'stats' => [
                'atWork' => $this->getStartAt() ? Carbon::instance($this->getStartAt())->diffForHumans(null, true, false, 2) : null,
            ],
            'startAt' => function (EmployeeCompany $employeeCompany) {
                return $employeeCompany->getStartAt() ? $employeeCompany->getStartAt()->format(\DateTime::ATOM) : null;
            },
            'endAt' => function (EmployeeCompany $employeeCompany) {
                return $employeeCompany->getEndAt() ? $employeeCompany->getEndAt()->format(\DateTime::ATOM) : null;
            },
            'probationPeriod' => $this->getProbationPeriod() ? $this->getProbationPeriod()->format(\DateTime::ATOM) : null
        ];
    }
}
