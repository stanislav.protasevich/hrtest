<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Resource;


use Domain\User\Entity\EmergencyContact;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin EmergencyContact
 */
class EmergencyContactResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'contactType' => $this->getContactType(),
            'contact' => $this->getContact(),
            'sortOrder' => $this->getSortOrder(),
        ];
    }
}