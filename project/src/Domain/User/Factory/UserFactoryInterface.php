<?php

declare(strict_types=1);

namespace Domain\User\Factory;


use Domain\User\Entity\User;

interface UserFactoryInterface
{
    public function create(User $user);
}