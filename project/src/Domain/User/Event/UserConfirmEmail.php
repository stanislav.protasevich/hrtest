<?php

declare(strict_types=1);

namespace Domain\User\Event;

use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserConfirmEmail extends Event
{
    public const EVENT_BEFORE_CONFIRM = 'beforeUserConfirmEmail';
    public const EVENT_AFTER_CONFIRM = 'afterUserConfirmEmail';

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}