<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserPerformanceRepository")
 * @ORM\Table(name="u_user_performance")
 */
class UserPerformance
{
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_PENDING = 'pending';
    public const STATUS_CANCELED = 'canceled';
    public const STATUS_OVERDUE = 'overdue';
    public const STATUSES = [
        self::STATUS_COMPLETED,
        self::STATUS_PENDING,
        self::STATUS_CANCELED,
        self::STATUS_OVERDUE,
    ];

    public const TYPE_TRIAL = 'trial';
    public const TYPE_FIRST_PE = 'first';
    public const TYPE_SECOND_PE = 'second';
    public const TYPES = [
        self::TYPE_TRIAL,
        self::TYPE_FIRST_PE,
        self::TYPE_SECOND_PE,
    ];

    public const PERIOD_TRIAL = 'P3M';
    public const PERIOD_FIRST = 'P6M';
    public const PERIOD_SECOND = 'P1Y1M';

    public const FORM_MAPPING = [
        self::TYPE_TRIAL => [
            UserPerformanceForm::AUTHOR_HEAD,
        ],
        self::TYPE_FIRST_PE => [
            UserPerformanceForm::AUTHOR_EMPLOYEE,
            UserPerformanceForm::AUTHOR_HEAD,
        ],
        self::TYPE_SECOND_PE => [
            UserPerformanceForm::AUTHOR_EMPLOYEE,
            UserPerformanceForm::AUTHOR_HEAD,
        ],
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Groups({"api","all"})
     */
    protected $id;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('trial', 'first', 'second')")
     * @SWG\Property(type="string", enum={"trial", "first", "second"})
     * @Groups({"api","all"})
     */
    protected $type;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('completed', 'pending', 'canceled', 'overdue')")
     * @SWG\Property(type="string", enum={"completed", "pending", "canceled", "overdue"})
     * @Groups({"api","all"})
     */
    protected $status;
    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"api","all"})
     */
    protected $comment;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\User\Entity\UserPerformanceForm",
     *     mappedBy="performance", fetch="EXTRA_LAZY", cascade={"persist", "remove"}
     * )
     */
    protected $performanceForm;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $user;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $nextDate;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedBy;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->performanceForm = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return UserPerformance
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return UserPerformance
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return UserPerformance
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get nextDate.
     *
     * @return \DateTime
     */
    public function getNextDate()
    {
        return $this->nextDate;
    }

    /**
     * Set nextDate.
     *
     * @param \DateTime $nextDate
     *
     * @return UserPerformance
     */
    public function setNextDate($nextDate)
    {
        $this->nextDate = $nextDate;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserPerformance
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return UserPerformance
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return UserPerformance
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return UserPerformance
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add performanceForm.
     *
     * @param \Domain\User\Entity\UserPerformanceForm $performanceForm
     *
     * @return UserPerformance
     */
    public function addPerformanceForm(UserPerformanceForm $performanceForm)
    {
        $this->performanceForm[] = $performanceForm;

        return $this;
    }

    /**
     * Remove performanceForm.
     *
     * @param \Domain\User\Entity\UserPerformanceForm $performanceForm
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePerformanceForm(UserPerformanceForm $performanceForm)
    {
        return $this->performanceForm->removeElement($performanceForm);
    }

    /**
     * Get performanceForm.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerformanceForm()
    {
        return $this->performanceForm;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserPerformance
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return UserPerformance
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
