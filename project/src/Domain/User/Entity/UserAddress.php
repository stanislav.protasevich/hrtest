<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="u_user_address")
 */
class UserAddress
{
    public const ADDRESS_TYPE_POST = 'post';
    public const ADDRESS_TYPE_ACTUAL = 'actual';
    public const ADDRESS_TYPES = [
        self::ADDRESS_TYPE_POST,
        self::ADDRESS_TYPE_ACTUAL,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('post', 'actual')")
     */
    protected $type;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="address")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Country", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", nullable=false)
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Region", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id", nullable=false)
     */
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\City", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $street;
    /**
     * @ORM\Column(type="decimal", length=18, nullable=true, scale=12, precision=18)
     */
    protected $latitude;
    /**
     * @ORM\Column(type="decimal", length=18, nullable=true, scale=12, precision=18)
     */
    protected $longitude;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $postCode;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $building;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $room;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return UserAddress
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return UserAddress
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set street.
     *
     * @param string $street
     *
     * @return UserAddress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street.
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set latitude.
     *
     * @param string|null $latitude
     *
     * @return UserAddress
     */
    public function setLatitude($latitude = null)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude.
     *
     * @return string|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude.
     *
     * @param string|null $longitude
     *
     * @return UserAddress
     */
    public function setLongitude($longitude = null)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude.
     *
     * @return string|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set postCode.
     *
     * @param string|null $postCode
     *
     * @return UserAddress
     */
    public function setPostCode($postCode = null)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode.
     *
     * @return string|null
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set building.
     *
     * @param string|null $building
     *
     * @return UserAddress
     */
    public function setBuilding($building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building.
     *
     * @return string|null
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set room.
     *
     * @param string|null $room
     *
     * @return UserAddress
     */
    public function setRoom($room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room.
     *
     * @return string|null
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserAddress
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set country.
     *
     * @param \Domain\Location\Entity\Country $country
     *
     * @return UserAddress
     */
    public function setCountry(\Domain\Location\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Domain\Location\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region.
     *
     * @param \Domain\Location\Entity\Region $region
     *
     * @return UserAddress
     */
    public function setRegion(\Domain\Location\Entity\Region $region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \Domain\Location\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city.
     *
     * @param \Domain\Location\Entity\City $city
     *
     * @return UserAddress
     */
    public function setCity(\Domain\Location\Entity\City $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \Domain\Location\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
}
