<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="u_emergency_contact")
 *
 * @ExclusionPolicy("all")
 */
class EmergencyContact
{
    public const TYPE_MOTHER = 'mother';
    public const TYPE_FATHER = 'father';
    public const TYPE_BROTHER = 'brother';
    public const TYPE_SISTER = 'sister';
    public const TYPE_SON = 'son';
    public const TYPE_DAUGHTER = 'daughter';
    public const TYPES = [
        self::TYPE_MOTHER,
        self::TYPE_FATHER,
        self::TYPE_BROTHER,
        self::TYPE_SISTER,
        self::TYPE_SON,
        self::TYPE_DAUGHTER,
    ];

    public const CONTACT_TYPE_PHONE = 'phone';
    public const CONTACT_TYPES = [
        self::CONTACT_TYPE_PHONE,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", inversedBy="emergencyContacts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('mother', 'father', 'brother', 'sister', 'son', 'daughter')"))
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $type;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('phone')"))
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $contactType;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $contact;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true, "default" : 1})
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EmergencyContact
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return EmergencyContact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get contactType.
     *
     * @return string
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * Set contactType.
     *
     * @param string $contactType
     *
     * @return EmergencyContact
     */
    public function setContactType($contactType)
    {
        $this->contactType = $contactType;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact.
     *
     * @param string $contact
     *
     * @return EmergencyContact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int|null $sortOrder
     *
     * @return EmergencyContact
     */
    public function setSortOrder($sortOrder = null)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EmergencyContact
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return int
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EmergencyContact
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return EmergencyContact
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
