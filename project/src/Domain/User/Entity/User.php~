<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Domain\Employee\Entity\EmployeeCompany;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="u_user")
 *
 * @UniqueEntity("email")
 */
class User implements UserInterface
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_EMAIL_BOUNCE = 'email_bounce';
    public const STATUS_EMAIL_CONFIRM = 'email_confirm';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
        self::STATUS_EMAIL_BOUNCE,
        self::STATUS_EMAIL_CONFIRM,
    ];
    public const STATUSES_CHANGED = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the user.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @SWG\Property(type="string", maxLength=255)
     * @Groups("all")
     */
    protected $email;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled', 'email_bounce', 'email_confirm')")
     * @SWG\Property(type="string", enum={"active","disabled","email_bounce","email_confirm"})
     * @Groups("all")
     */
    protected $status;
    /**
     * @ORM\Column(type="string")
     */
    protected $passwordHash;

    protected $password;
    /**
     * @ORM\Column(type="string")
     */
    protected $salt;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $passwordResetToken;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\Employee\Entity\EmployeeCompany",
     *     mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist"},
     *     orphanRemoval=true
     * )
     * @SWG\Property(ref=@Model(type=EmployeeCompany::class, groups={"all"}))
     * @Groups("all")
     */
    protected $employeeCompanies;
    /**
     * @ORM\OneToOne(
     *     targetEntity="Domain\User\Entity\UserProfile",
     *     mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist", "remove"}
     * )
     * @SWG\Property(ref=@Model(type=UserProfile::class, groups={"all"}))
     * @Groups("all")
     */
    protected $profile;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\User\Entity\EmergencyContact",
     *     mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $emergencyContacts;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\User\Entity\UserChildren",
     *     mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $children;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="Domain\User\Entity\UserConnectedAccount", mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     */
    protected $connectedAccount;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\User\Entity\Skill",
     *     inversedBy="user",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     *  )
     * @ORM\JoinTable(name="u_user_skill")
     * @ORM\OrderBy({"sortOrder" = "DESC"})
     */
    protected $skills;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\User\Entity\User",
     *     inversedBy="subordinates",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="u_user_supervisor",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="supervisor_id", referencedColumnName="id")}
     * )
     */
    protected $supervisors;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\User\Entity\User", mappedBy="supervisors")
     */
    protected $subordinates;

    /**
     * @ORM\OneToOne(targetEntity="Domain\User\Entity\UserAddress", mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     */
    protected $address;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\User\Entity\UserFieldValue",
     *     mappedBy="user",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     */
    protected $fieldValues;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\User\Entity\User",
     *     inversedBy="following",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinTable(name="u_user_follow",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="follow_user_id", referencedColumnName="id")}
     * )
     */
    protected $followers;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\User\Entity\User", mappedBy="followers")
     */
    protected $following;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->employeeCompanies = new ArrayCollection();
        $this->emergencyContacts = new ArrayCollection();
        $this->connectedAccount = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->supervisors = new ArrayCollection();
        $this->subordinates = new ArrayCollection();
        $this->fieldValues = new ArrayCollection();

        $this->followers = new ArrayCollection();
        $this->following = new ArrayCollection();

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->setSalt();
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt(\DateTime $created_at): void
    {
        $this->createdAt = $created_at;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param int $updated_at
     */
    public function setUpdatedAt(\DateTime $updated_at): void
    {
        $this->updatedAt = $updated_at;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPasswordHash(): ?string
    {
        return $this->passwordHash;
    }

    /**
     * Set passwordHash.
     *
     * @param string $passwordHash
     *
     * @return User
     */
    public function setPasswordHash($passwordHash)
    {
        $this->passwordHash = $passwordHash;

        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * Set salt.
     *
     * @param string|null $salt
     * @return User
     */
    public function setSalt(string $salt = null)
    {
        $this->salt = $salt ?: md5(uniqid('', true));

        return $this;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
//        unset($this->password_hash);
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function can($operation)
    {
        if ($this->getId() !== 1) {
            throw new AccessDeniedException('You are not allowed to perform this action.');
        }
    }

    /**
     * @return int
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get passwordResetToken.
     *
     * @return string
     */
    public function getPasswordResetToken(): ?string
    {
        return $this->passwordResetToken;
    }

    /**
     * Set passwordResetToken.
     *
     * @param string $passwordResetToken
     *
     * @return User
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[]
     */
    public function getRoles()
    {
        return [RbacPermissions::ROLE_USER];
    }

    /**
     * Get profile.
     *
     * @return \Domain\User\Entity\UserProfile|null
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set profile.
     *
     * @param \Domain\User\Entity\UserProfile|null $profile
     *
     * @return User
     */
    public function setProfile(\Domain\User\Entity\UserProfile $profile = null)
    {
        $this->profile = $profile;
        $profile->setUser($this);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->passwordHash;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getNewPassword(): ?string
    {
        return $this->password;
    }

    /**
     * Add employeeCompany.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employeeCompany
     *
     * @return User
     */
    public function addEmployeeCompany(\Domain\Employee\Entity\EmployeeCompany $employeeCompany)
    {
        $this->employeeCompanies[] = $employeeCompany;
        $employeeCompany->setUser($this);

        return $this;
    }

    /**
     * Remove employeeCompany.
     *
     * @param \Domain\Employee\Entity\EmployeeCompany $employeeCompany
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmployeeCompany(\Domain\Employee\Entity\EmployeeCompany $employeeCompany)
    {
        return $this->employeeCompanies->removeElement($employeeCompany);
    }

    /**
     * @return EmployeeCompany
     */
    public function getMainEmployeeCompany()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isMain', 123));

        return $this->getEmployeeCompanies()->matching($criteria)->first();
    }

    /**
     * Get employeeCompanies.
     *
     * @return ArrayCollection
     */
    public function getEmployeeCompanies()
    {
        return $this->employeeCompanies;
    }

    /**
     * Add emergencyContact.
     *
     * @param \Domain\User\Entity\EmergencyContact $emergencyContact
     *
     * @return User
     */
    public function addEmergencyContact(EmergencyContact $emergencyContact)
    {
        $this->emergencyContacts[] = $emergencyContact;

        $emergencyContact->setUser($this);

        return $this;
    }

    /**
     * Remove emergencyContact.
     *
     * @param \Domain\User\Entity\EmergencyContact $emergencyContact
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEmergencyContact(EmergencyContact $emergencyContact)
    {
        return $this->emergencyContacts->removeElement($emergencyContact);
    }

    /**
     * Get emergencyContacts.
     *
     * @return ArrayCollection
     */
    public function getEmergencyContacts()
    {
        return $this->emergencyContacts;
    }

    /**
     * Add child.
     *
     * @param \Domain\User\Entity\UserChildren $child
     *
     * @return User
     */
    public function addChild(UserChildren $child)
    {
        $this->children[] = $child;
        $child->setUser($this);

        return $this;
    }

    /**
     * Remove child.
     *
     * @param \Domain\User\Entity\UserChildren $child
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChild(UserChildren $child)
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add connectedAccount.
     *
     * @param \Domain\User\Entity\UserConnectedAccount $connectedAccount
     *
     * @return User
     */
    public function addConnectedAccount(\Domain\User\Entity\UserConnectedAccount $connectedAccount)
    {
        $this->connectedAccount[] = $connectedAccount;

        return $this;
    }

    /**
     * Remove connectedAccount.
     *
     * @param \Domain\User\Entity\UserConnectedAccount $connectedAccount
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConnectedAccount(\Domain\User\Entity\UserConnectedAccount $connectedAccount)
    {
        return $this->connectedAccount->removeElement($connectedAccount);
    }

    /**
     * Get connectedAccount.
     *
     * @return ArrayCollection
     */
    public function getConnectedAccount()
    {
        return $this->connectedAccount;
    }


    /**
     * Add skill.
     *
     * @param \Domain\User\Entity\Skill $skill
     *
     * @return User
     */
    public function addSkill(Skill $skill)
    {
        $this->skills[] = $skill;
        $skill->addUser($this);

        return $this;
    }

    /**
     * Remove skill.
     *
     * @param \Domain\User\Entity\Skill $skill
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSkill(Skill $skill)
    {
        $skill->removeUser($this);

        return $this->skills->removeElement($skill);
    }

    /**
     * Get skills.
     *
     * @return ArrayCollection
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Add supervisor.
     *
     * @param \Domain\User\Entity\User $supervisor
     *
     * @return User
     */
    public function addSupervisor(User $supervisor)
    {
        $this->supervisors[] = $supervisor;

        return $this;
    }

    /**
     * Remove supervisor.
     *
     * @param \Domain\User\Entity\User $supervisor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSupervisor(User $supervisor)
    {
        return $this->supervisors->removeElement($supervisor);
    }

    /**
     * Get supervisors.
     *
     * @return ArrayCollection
     */
    public function getSupervisors()
    {
        return $this->supervisors;
    }

    /**
     * Get address.
     *
     * @return \Domain\User\Entity\UserAddress|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address.
     *
     * @param \Domain\User\Entity\UserAddress|null $address
     *
     * @return User
     */
    public function setAddress(UserAddress $address = null)
    {
        $this->address = $address;
        $address->setUser($this);

        return $this;
    }

    /**
     * Add subordinate.
     *
     * @param \Domain\User\Entity\User $subordinate
     *
     * @return User
     */
    public function addSubordinate(User $subordinate)
    {
        $this->subordinates[] = $subordinate;

        return $this;
    }

    /**
     * Remove subordinate.
     *
     * @param \Domain\User\Entity\User $subordinate
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSubordinate(User $subordinate)
    {
        return $this->subordinates->removeElement($subordinate);
    }

    /**
     * Get subordinates.
     *
     * @return ArrayCollection
     */
    public function getSubordinates()
    {
        return $this->subordinates;
    }

    /**
     * Add fieldValue.
     *
     * @param \Domain\User\Entity\UserFieldValue $fieldValue
     *
     * @return User
     */
    public function addFieldValue(\Domain\User\Entity\UserFieldValue $fieldValue)
    {
        $this->fieldValues[] = $fieldValue;
        $fieldValue->setUser($this);
        return $this;
    }

    /**
     * Remove fieldValue.
     *
     * @param \Domain\User\Entity\UserFieldValue $fieldValue
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFieldValue(\Domain\User\Entity\UserFieldValue $fieldValue)
    {
        return $this->fieldValues->removeElement($fieldValue);
    }

    /**
     * Get fieldValues.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFieldValues()
    {
        return $this->fieldValues;
    }

    /**
     * Add follower.
     *
     * @param \Domain\User\Entity\User $follower
     *
     * @return User
     */
    public function addFollower(?User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower.
     *
     * @param \Domain\User\Entity\User $follower
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFollower(?User $follower)
    {
        return $this->followers->removeElement($follower);
    }

    /**
     * Get followers.
     *
     * @return \Doctrine\Common\Collections\Collection|User[]
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add following.
     *
     * @param \Domain\User\Entity\User $following
     *
     * @return User
     */
    public function addFollowing(\Domain\User\Entity\User $following)
    {
        $this->following[] = $following;

        return $this;
    }

    /**
     * Remove following.
     *
     * @param \Domain\User\Entity\User $following
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFollowing(\Domain\User\Entity\User $following)
    {
        return $this->following->removeElement($following);
    }

    /**
     * Get following.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowing()
    {
        return $this->following;
    }
}
