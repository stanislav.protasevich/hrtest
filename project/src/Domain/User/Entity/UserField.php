<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserFieldRepository")
 * @ORM\Table(name="u_user_field")
 */
class UserField
{
    const MATCH_TYPE_REGEX = 'regex';
    const MATCH_TYPE_VALIDATOR = 'validator';

    public const GROUP_PERSONAL = 'personal';
    public const GROUP_CONTACT = 'contact';
    public const GROUP_PREFERENCES = 'preferences';
    public const GROUP_SOCIAL = 'social';

    public const GROUP_CONTACT_EMAIL = 'email';
    public const GROUP_CONTACT_PHONE = 'phone';
    public const GROUP_CONTACT_SKYPE = 'skype';
    public const GROUP_CONTACT_SECOND_PHONE = 'second_phone';
    public const GROUP_CONTACT_TELEGRAM = 'telegram';
    public const GROUP_SOCIAL_FACEBOOK = 'facebook';
    public const GROUP_SOCIAL_LINKEDIN = 'linkedin';
    public const GROUP_SOCIAL_INSTAGRAM = 'instagram';

    public const REGEX_PHONE = '^[0-9\-\(\)\/\+\s]*$';
    public const REGEX_SOCIAL = '^[a-zA-Z0-9-_.,@:]+$';
    public const REGEX_EMAIL = '^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$';

    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('personal', 'contact', 'preferences', 'social')")
     */
    protected $group;
    /**
     * @ORM\Column(type="integer")
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('validator', 'regex')")
     */
    protected $matchType;

    /**
     * @ORM\Column(type="json", options={"jsonb": true})
     */
    protected $matchParams;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    protected $required;

    /**
     * @ORM\Column(type="integer")
     */
    protected $matchLength;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\User\Entity\UserFieldValue",
     *     mappedBy="field",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $values;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param string $id
     *
     * @return UserField
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group.
     *
     * @param string $group
     *
     * @return UserField
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return UserField
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set matchType.
     *
     * @param string $matchType
     *
     * @return UserField
     */
    public function setMatchType($matchType)
    {
        $this->matchType = $matchType;

        return $this;
    }

    /**
     * Get matchType.
     *
     * @return string
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * Set matchParams.
     *
     * @param json $matchParams
     *
     * @return UserField
     */
    public function setMatchParams($matchParams)
    {
        $this->matchParams = $matchParams;

        return $this;
    }

    /**
     * Get matchParams.
     *
     * @return json
     */
    public function getMatchParams()
    {
        return $this->matchParams;
    }

    /**
     * Set matchLength.
     *
     * @param int $matchLength
     *
     * @return UserField
     */
    public function setMatchLength($matchLength)
    {
        $this->matchLength = $matchLength;

        return $this;
    }

    /**
     * Get matchLength.
     *
     * @return int
     */
    public function getMatchLength()
    {
        return $this->matchLength;
    }

    /**
     * Add value.
     *
     * @param \Domain\User\Entity\UserFieldValue $value
     *
     * @return UserField
     */
    public function addValue(\Domain\User\Entity\UserFieldValue $value)
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * Remove value.
     *
     * @param \Domain\User\Entity\UserFieldValue $value
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeValue(\Domain\User\Entity\UserFieldValue $value)
    {
        return $this->values->removeElement($value);
    }

    /**
     * Get values.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     */
    public function setRequired(bool $required): void
    {
        $this->required = $required;
    }

    /**
     * Get required.
     *
     * @return bool
     */
    public function getRequired()
    {
        return $this->required;
    }
}
