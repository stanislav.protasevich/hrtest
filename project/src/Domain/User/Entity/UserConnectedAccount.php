<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserConnectedAccountRepository")
 * @ORM\Table(name="u_user_connected_account")
 */
class UserConnectedAccount
{
    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\ConnectedAccountProvider", inversedBy="connectedAccount")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="name", onDelete="CASCADE")
     *
     * @Groups({"api", "all"})
     */
    protected $connectedAccountProvider;

    /**
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", inversedBy="connectedAccount")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Groups({"api", "all"})
     */
    protected $user;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    protected $extraData;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api", "all"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api", "all"})
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get extraData.
     *
     * @return json
     */
    public function getExtraData()
    {
        return $this->extraData;
    }

    /**
     * Set extraData.
     *
     * @param array $extraData
     *
     * @return UserConnectedAccount
     */
    public function setExtraData(array $extraData)
    {
        $this->extraData = json_encode($extraData);

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserConnectedAccount
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return UserConnectedAccount
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get connectedAccountProvider.
     *
     * @return \Domain\User\Entity\ConnectedAccountProvider|null
     */
    public function getConnectedAccountProvider()
    {
        return $this->connectedAccountProvider;
    }

    /**
     * Set connectedAccountProvider.
     *
     * @param \Domain\User\Entity\ConnectedAccountProvider|null $connectedAccountProvider
     *
     * @return UserConnectedAccount
     */
    public function setConnectedAccountProvider(
        \Domain\User\Entity\ConnectedAccountProvider $connectedAccountProvider = null
    )
    {
        $this->connectedAccountProvider = $connectedAccountProvider;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserConnectedAccount
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
