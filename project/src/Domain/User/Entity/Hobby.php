<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\HobbyRepository")
 * @ORM\Table(name="u_hobby")
 */
class Hobby
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToMany(
     *     targetEntity="Domain\User\Entity\User",
     *     mappedBy="hobbies",
     *     fetch="EXTRA_LAZY"
     * )
     */
    protected $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return Hobby
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Add user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return Hobby
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(User $user)
    {
        return $this->user->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }
}
