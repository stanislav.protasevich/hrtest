<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Service\PackageService;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="u_user_profile")
 *
 * @property string $firstName
 */
class UserProfile
{
    public const GENDER_MAN = 'm';
    public const GENDER_WOMAN = 'w';
    public const GENDERS = [
        self::GENDER_MAN,
        self::GENDER_WOMAN,
    ];

    public const WORK_TYPE_FULL_TIME = 'full time';
    public const WORK_TYPE_PART_TIME = 'part time';
    public const WORK_TYPES = [
        self::WORK_TYPE_FULL_TIME,
        self::WORK_TYPE_PART_TIME,
    ];

    public const MARITAL_STATUS_MARRIED = 'married';
    public const MARITAL_STATUS_SINGLE = 'single';
    public const MARITAL_STATUS_DIVORCED = 'divorced';
    public const MARITAL_STATUS_WIDOWED = 'widowed';
    public const MARITAL_STATUSES = [
        self::MARITAL_STATUS_MARRIED,
        self::MARITAL_STATUS_SINGLE,
        self::MARITAL_STATUS_DIVORCED,
        self::MARITAL_STATUS_WIDOWED
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the user profile.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @SWG\Property(type="string", maxLength=255)
     *
     * @Groups({"api", "all"})
     */
    protected $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(type="string", maxLength=255)
     *
     * @Groups({"api", "all"})
     */
    protected $middleName;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @SWG\Property(type="string", maxLength=255)
     *
     * @Groups({"api", "all"})
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     * @SWG\Property(type="string", maxLength=255)
     *
     * @Groups({"api", "all"})
     */
    protected $phone;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @SWG\Property(type="date")
     *
     * @Groups({"api", "all"})
     */
    protected $birthday;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('m', 'w')"))
     * @SWG\Property(type="string", enum={"m","w"})
     *
     * @Groups("all")
     */
    protected $gender;

    /**
     * @ORM\OneToOne(targetEntity="Domain\User\Entity\User", inversedBy="profile")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @SWG\Property(type="string")
     *
     * @Groups("all")
     */
    protected $avatar;


    //     * @ORM\Column(type="string", columnDefinition="ENUM('married', 'single', 'divorced', 'widowed')"), nullable=true)
    //     * @SWG\Property(type="string", enum={"married", "single", "divorced", "widowed"})
    /**
     * @ORM\Column(type="boolean")
     * @Groups({"api", "all"})
     */
    protected $maritalStatus;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @SWG\Property(type="date", maxLength=255)
     *
     * @Groups({"api", "all"})
     */
    protected $marriageAnniversary;

    /**
     * Get gender.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set gender.
     *
     * @param string|null $gender
     *
     * @return UserProfile
     */
    public function setGender($gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * Set birthday.
     *
     * @param \DateTime $birthday
     *
     *
     * @return UserProfile
     */
    public function setBirthday(?\DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get middleName.
     *
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * Set middleName.
     *
     * @param string|null $middleName
     *
     * @return UserProfile
     */
    public function setMiddleName(string $middleName = null)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . ' ' . $this->getSurname();
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return UserProfile
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get surname.
     *
     * @return string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * Set surname.
     *
     * @param string $surname
     *
     * @return UserProfile
     */
    public function setSurname(?string $surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get maritalStatus.
     *
     * @return string
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * Set maritalStatus.
     *
     * @param string $maritalStatus
     *
     * @return UserProfile
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get marriageAnniversary.
     *
     * @return \DateTime|null
     */
    public function getMarriageAnniversary()
    {
        return $this->marriageAnniversary;
    }

    /**
     * Set marriageAnniversary.
     *
     * @param \DateTime|null $marriageAnniversary
     *
     * @return UserProfile
     */
    public function setMarriageAnniversary($marriageAnniversary = null)
    {
        $this->marriageAnniversary = $marriageAnniversary;

        return $this;
    }

    public function getAvatarUrl(): ?string
    {
        /* @var $package PackageService */
        $package = app(PackageService::class);

        $avatar = $this->getAvatar();

        $path = 'user/' . $this->getUser()->getId() . '/avatars/' . $avatar;

        if ($avatar && $package->isAbsoluteUrl($avatar)) {
            return $avatar;
        }

        return $avatar ? $package->getUrl(FileHelper::normalizePath($path)) : null;
    }

    /**
     * Get avatar.
     *
     * @return string|null
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set avatar.
     *
     * @param string|null $avatar
     *
     * @return UserProfile
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserProfile
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }
}
