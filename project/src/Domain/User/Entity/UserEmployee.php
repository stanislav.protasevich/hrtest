<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\Company\Entity\CompanyAddress;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserEmployeeRepository")
 * @ORM\Table(name="u_user_employee")
 */
class UserEmployee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the user profile.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Domain\User\Entity\User", inversedBy="employee")
     * @ORM\JoinColumn(onDelete="CASCADE")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\CompanyAddress", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    protected $companyAddress;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @SWG\Property(type="date")
     *
     * @Groups({"api", "all"})
     */
    protected $jobOfferDate;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dismissalReason;

    /**
     * Get jobOfferDate.
     *
     * @return \DateTime
     */
    public function getJobOfferDate(): ?\DateTime
    {
        return $this->jobOfferDate;
    }

    /**
     * Set jobOfferDate.
     *
     * @param \DateTime $jobOfferDate
     *
     * @return UserEmployee
     */
    public function setJobOfferDate(?\DateTime $jobOfferDate)
    {
        $this->jobOfferDate = $jobOfferDate;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return UserEmployee
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get companyAddress.
     *
     * @return \Domain\Company\Entity\CompanyAddress
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * Set companyAddress.
     *
     * @param \Domain\Company\Entity\CompanyAddress $companyAddress
     *
     * @return UserEmployee
     */
    public function setCompanyAddress(?CompanyAddress $companyAddress)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDismissalReason(): ?string
    {
        return $this->dismissalReason;
    }

    /**
     * @param string $dismissalReason
     */
    public function setDismissalReason(?string $dismissalReason): void
    {
        $this->dismissalReason = $dismissalReason;
    }
}
