<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserAlertRepository")
 * @ORM\Table(name="u_user_alert")
 */
class UserAlert
{
    public const ALERT_LEAVE_REQUEST = 'leave request';

    public const ALERT_ACTION_CREATE = 'create';
    public const ALERT_ACTION_UPDATE = 'update';
    public const ALERT_ACTION_DELETE = 'delete';
    public const ALERT_ACTION_APPROVE = 'approve';
    public const ALERT_ACTION_DENIED = 'denied';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", fetch="EXTRA_LAZY")
     */
    protected $alertedUser;

    /**
     * @ORM\ManyToOne(targetEntity="User", fetch="EXTRA_LAZY")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $contentType;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $contentId;

    /**
     * @ORM\Column(type="string", length=30, nullable=false)
     */
    protected $action;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $message;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $viewAt;

    /**
     * @ORM\Column(type="boolean", nullable=false, name="`read`")
     */
    protected $read;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contentType.
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set contentType.
     *
     * @param string $contentType
     *
     * @return UserAlert
     */
    public function setContentType(string $contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentId.
     *
     * @return string
     */
    public function getContentId()
    {
        return $this->contentId;
    }

    /**
     * Set contentId.
     *
     * @param string $contentId
     */
    public function setContentId(string $contentId)
    {
        $this->contentId = $contentId;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return UserAlert
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserAlert
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get viewAt.
     *
     * @return \DateTime
     */
    public function getViewAt()
    {
        return $this->viewAt;
    }

    /**
     * Set viewAt.
     *
     * @param \DateTime $viewAt
     *
     * @return UserAlert
     */
    public function setViewAt($viewAt)
    {
        $this->viewAt = $viewAt;

        return $this;
    }

    /**
     * Get alertedUser.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getAlertedUser()
    {
        return $this->alertedUser;
    }

    /**
     * Set alertedUser.
     *
     * @param \Domain\User\Entity\User|null $alertedUser
     *
     * @return UserAlert
     */
    public function setAlertedUser(User $alertedUser = null)
    {
        $this->alertedUser = $alertedUser;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserAlert
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getRead(): bool
    {
        return $this->read;
    }

    /**
     * @param boolean $read
     */
    public function setRead(bool $read): void
    {
        $this->read = $read;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
