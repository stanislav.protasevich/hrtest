<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\UserPerformanceFormRepository")
 * @ORM\Table(name="u_user_performance_form")
 */
class UserPerformanceForm
{
    public const AUTHOR_EMPLOYEE = 'employee';
    public const AUTHOR_HEAD = 'head';
    public const AUTHOR_HR = 'hr';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Domain\User\Entity\UserPerformance",
     *     inversedBy="performanceForm", fetch="EXTRA_LAZY"
     * )
     * @Groups({"api","all"})
     */
    protected $performance;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('trial', 'first', 'second')")
     * @SWG\Property(type="string", enum={"trial", "first", "second"})
     * @Groups({"api","all"})
     */
    protected $type;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('employee', 'head', 'hr')")
     * @SWG\Property(type="string", enum={"employee", "head", "hr"})
     * @Groups({"api","all"})
     */
    protected $author;
    /**
     * @ORM\Column(type="json_array")
     */
    protected $data;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $employee;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="supervisor_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $supervisor;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\Job\Entity\JobTitle")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $jobTitle;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $city;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $completionAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $startAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $endAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $meetingAt;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedBy;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return UserPerformanceForm
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get author.
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set author.
     *
     * @param string $author
     *
     * @return UserPerformanceForm
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set data.
     *
     * @param array $data
     *
     * @return UserPerformanceForm
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserPerformanceForm
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return UserPerformanceForm
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get performance.
     *
     * @return \Domain\User\Entity\UserPerformance|null
     */
    public function getPerformance()
    {
        return $this->performance;
    }

    /**
     * Set performance.
     *
     * @param \Domain\User\Entity\UserPerformance|null $performance
     *
     * @return UserPerformanceForm
     */
    public function setPerformance(\Domain\User\Entity\UserPerformance $performance = null)
    {
        $this->performance = $performance;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return UserPerformanceForm
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return UserPerformanceForm
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get completionAt.
     *
     * @return \DateTime
     */
    public function getCompletionAt()
    {
        return $this->completionAt;
    }

    /**
     * Set completionAt.
     *
     * @param \DateTime $completionAt
     *
     * @return UserPerformanceForm
     */
    public function setCompletionAt($completionAt)
    {
        $this->completionAt = $completionAt;

        return $this;
    }

    /**
     * Get startAt.
     *
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set startAt.
     *
     * @param \DateTime $startAt
     *
     * @return UserPerformanceForm
     */
    public function setStartAt($startAt)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get endAt.
     *
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set endAt.
     *
     * @param \DateTime $endAt
     *
     * @return UserPerformanceForm
     */
    public function setEndAt($endAt)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get meetingAt.
     *
     * @return \DateTime
     */
    public function getMeetingAt()
    {
        return $this->meetingAt;
    }

    /**
     * Set meetingAt.
     *
     * @param \DateTime $meetingAt
     *
     * @return UserPerformanceForm
     */
    public function setMeetingAt($meetingAt)
    {
        $this->meetingAt = $meetingAt;

        return $this;
    }

    /**
     * Get employee.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set employee.
     *
     * @param \Domain\User\Entity\User|null $employee
     *
     * @return UserPerformanceForm
     */
    public function setEmployee(\Domain\User\Entity\User $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get supervisor.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }

    /**
     * Set supervisor.
     *
     * @param \Domain\User\Entity\User|null $supervisor
     *
     * @return UserPerformanceForm
     */
    public function setSupervisor(\Domain\User\Entity\User $supervisor = null)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get jobTitle.
     *
     * @return \Domain\Job\Entity\JobTitle|null
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set jobTitle.
     *
     * @param \Domain\Job\Entity\JobTitle|null $jobTitle
     *
     * @return UserPerformanceForm
     */
    public function setJobTitle(\Domain\Job\Entity\JobTitle $jobTitle = null)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \Domain\Location\Entity\City|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param \Domain\Location\Entity\City|null $city
     *
     * @return UserPerformanceForm
     */
    public function setCity(\Domain\Location\Entity\City $city = null)
    {
        $this->city = $city;

        return $this;
    }
}
