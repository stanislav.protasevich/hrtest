<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\UserBundle\Repository\ConnectedAccountProviderRepository")
 * @ORM\Table(name="u_connected_account_provider")
 */
class ConnectedAccountProvider
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
    ];

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=25, unique=true)
     *
     * @SWG\Property(description="The unique identifier.")
     * @Groups({"api", "all"})
     */
    protected $name;

    /**
     * @ORM\Column(type="json")
     *
     * @Groups({"api", "all"})
     */
    protected $options;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     *
     * @Groups({"api", "all"})
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled')")
     *
     * @Groups({"api", "all"})
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="Domain\User\Entity\UserConnectedAccount", mappedBy="connectedAccountProvider", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     */
    protected $connectedAccount;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->connectedAccount = new ArrayCollection();
    }

    /**
     * Get options.
     *
     * @return json
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options.
     *
     * @param array $options
     *
     * @return ConnectedAccountProvider
     */
    public function setOptions(array $options = [])
    {
        $this->options = json_encode($options);

        return $this;
    }

    public function getOptionsArray()
    {
        return json_decode($this->options, true);
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return ConnectedAccountProvider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return ConnectedAccountProvider
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return ConnectedAccountProvider
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Add connectedAccount.
     *
     * @param \Domain\User\Entity\UserConnectedAccount $connectedAccount
     *
     * @return ConnectedAccountProvider
     */
    public function addConnectedAccount(\Domain\User\Entity\UserConnectedAccount $connectedAccount)
    {
        $this->connectedAccount[] = $connectedAccount;

        return $this;
    }

    /**
     * Remove connectedAccount.
     *
     * @param \Domain\User\Entity\UserConnectedAccount $connectedAccount
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeConnectedAccount(\Domain\User\Entity\UserConnectedAccount $connectedAccount)
    {
        return $this->connectedAccount->removeElement($connectedAccount);
    }

    /**
     * Get connectedAccount.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getConnectedAccount()
    {
        return $this->connectedAccount;
    }
}
