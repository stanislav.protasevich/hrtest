<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Domain\User\Entity;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 * @ORM\Table(name="u_user_children")
 *
 * @ExclusionPolicy("all")
 */
class UserChildren
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", inversedBy="children")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $birthday;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('m', 'w')"))
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $gender;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned":true, "default" : 1})
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return UserChildren
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get birthday.
     *
     * @return \DateTime|null
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * Set birthday.
     *
     * @param \DateTime|null $birthday
     *
     * @return UserChildren
     */
    public function setBirthday(?\DateTime $birthday = null)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get gender.
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set gender.
     *
     * @param string $gender
     *
     * @return UserChildren
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int|null
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int|null $sortOrder
     *
     * @return UserChildren
     */
    public function setSortOrder($sortOrder = null)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserChildren
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return int
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return UserChildren
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return UserChildren
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->birthday ? Carbon::instance($this->birthday)->diffForHumans(null, true, false, 2) : null;
    }
}
