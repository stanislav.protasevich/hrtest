<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\ValueObject;

interface UserPasswordResetInterface
{
    /**
     * @return null|string
     */
    public function getToken(): ?string;

    /**
     * @return null|string
     */
    public function getPassword(): ?string;

    /**
     * @return null|string
     */
    public function getRepassword(): ?string;
}