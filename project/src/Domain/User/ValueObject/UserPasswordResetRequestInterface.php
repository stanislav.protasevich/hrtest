<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\User\ValueObject;


interface UserPasswordResetRequestInterface
{
    /**
     * @return null|string
     */
    public function getEmail(): ?string;
}