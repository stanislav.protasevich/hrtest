<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\ValueObject;

/**
 * @property string $email
 */
final class UserPasswordResetRequest implements UserPasswordResetRequestInterface
{
    /**
     * @var null|string
     */
    private $email;

    private function __construct(?string $email)
    {
        $this->email = $email ? trim($email) : $email;
    }

    /**
     * @param null|string $email
     * @return UserPasswordResetRequest
     */
    public static function create(?string $email)
    {
        return new self($email);
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
}