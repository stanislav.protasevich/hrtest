<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\ValueObject;

/**
 * @property string $email
 * @property string $password
 */
final class UserLogin implements UserLoginInterface
{
    /**
     * @var null|string
     */
    private $email;

    /**
     * @var null|string
     */
    private $password;

    private function __construct(?string $email, ?string $password)
    {
        $this->email = $email ? trim($email) : $email;
        $this->password = $password ? trim($password) : $password;
    }

    /**
     * @param null|string $email
     * @param null|string $password
     * @return UserLogin
     */
    public static function create(?string $email, ?string $password)
    {
        return new self($email, $password);
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
}