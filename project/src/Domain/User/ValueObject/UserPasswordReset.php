<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\User\ValueObject;

/**
 * @property string $token
 * @property string $password
 * @property string $repassword
 */
final class UserPasswordReset implements UserPasswordResetInterface
{
    /**
     * @var null|string
     */
    private $token;
    /**
     * @var null|string
     */
    private $password;
    /**
     * @var null|string
     */
    private $repassword;

    private function __construct(?string $token, ?string $password, ?string $repassword)
    {
        $this->token = $token ? trim($token) : $token;
        $this->password = $password ? trim($password) : $password;
        $this->repassword = $repassword ? trim($repassword) : $repassword;
    }

    /**
     * @param null|string $token
     * @param null|string $password
     * @param null|string $repassword
     * @return UserPasswordReset
     */
    public static function create(?string $token, ?string $password, ?string $repassword)
    {
        return new self($token, $password, $repassword);
    }

    /**
     * @return null|string
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return null|string
     */
    public function getRepassword(): ?string
    {
        return $this->repassword;
    }
}