<?php

declare(strict_types=1);

namespace Domain\TimeOffType\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\Policy\Entity\Policy;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\TimeOffTypeBundle\Repository\TimeOffTypeRepository")
 * @ORM\Table(name="time_off_type")
 * @UniqueEntity("name")
 * @ExclusionPolicy("all")
 */
class TimeOffType
{
    public const TRACK_TIME_DAYS = 'days';
    public const TRACK_TIME_HOURS = 'hours';
    public const TRACK_TIMES = [
        self::TRACK_TIME_HOURS,
        self::TRACK_TIME_DAYS,
    ];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_INACTIVE = 'disabled';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_INACTIVE,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'hours')"))
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $trackTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Expose()
     */
    protected $showInCalendar;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Expose()
     */
    protected $icon;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     *
     * @Assert\Regex("/^\#\w{6}+$/i")
     *
     * @Expose()
     */
    protected $calendarColor;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Expose()
     */
    protected $description;

    /**
     * @ORM\Column(type="integer", length=1, nullable=false)
     */
    protected $status;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="integer")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="integer")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=false)
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $updatedBy;

    /**
     * @ORM\OneToMany(targetEntity="Domain\Policy\Entity\Policy", mappedBy="timeOffTypeAssign", fetch="EXTRA_LAZY")
     */
    protected $policies;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Domain\TimeOffType\Entity\TimeOffTypeAssign",
     *     mappedBy="timeOffType",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $timeOffTypeAssign;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->policies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->timeOffTypeAssign = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
