<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Calendar\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\User\Entity\User;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\CalendarBundle\Repository\CalendarRepository")
 * @ORM\Table(name="cl_calendar")
 */
class Calendar
{
    public const REPEAT_NEVER = 'never';
    public const REPEAT_EVERYDAY = 'everyday';
    public const REPEAT_WEEKLY = 'weekly';
    public const REPEAT_QUARTERLY = 'quarterly';
    public const REPEAT_MONTHLY = 'monthly';
    public const REPEAT_YEARLY = 'yearly';
    public const REPEATS = [
        self::REPEAT_NEVER,
        self::REPEAT_EVERYDAY,
        self::REPEAT_WEEKLY,
        self::REPEAT_QUARTERLY,
        self::REPEAT_MONTHLY,
        self::REPEAT_YEARLY,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier.")
     * @Groups({"api","all"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=false, nullable=false)
     *
     * @SWG\Property(type="string", maxLength=255)
     * @Groups({"api","all"})
     */
    protected $title;
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $description;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $date;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('never', 'everyday', 'weekly', 'quarterly', 'monthly', 'yearly')", name="`repeat`")
     * @SWG\Property(type="string", enum={"never", "everyday", "weekly", "quarterly", "monthly", "yearly"})
     * @Groups("all")
     */
    protected $repeat;

    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedAt;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     * @SWG\Property(type="string")
     * @Groups({"api","all"})
     */
    protected $updatedBy;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Calendar
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Calendar
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Calendar
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Calendar
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Calendar
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return Calendar
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return Calendar
     */
    public function setUpdatedBy(User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get repeat.
     *
     * @return string
     */
    public function getRepeat()
    {
        return $this->repeat;
    }

    /**
     * Set repeat.
     *
     * @param string $repeat
     *
     * @return Calendar
     */
    public function setRepeat($repeat)
    {
        $this->repeat = $repeat;

        return $this;
    }
}
