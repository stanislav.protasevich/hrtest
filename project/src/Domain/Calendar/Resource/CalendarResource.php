<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Calendar\Resource;


use Domain\Calendar\Entity\Calendar;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Calendar
 */
class CalendarResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'repeat' => $this->getRepeat(),
            'date' => $this->getDate()->format(\DateTime::ATOM),
            'createdBy' => $this->getCreatedBy()->getId(),
            'updatedBy' => $this->getUpdatedBy()->getId(),
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
        ];
    }
}
