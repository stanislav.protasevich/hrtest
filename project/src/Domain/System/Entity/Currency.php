<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\System\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\SystemBundle\Repository\CurrencyRepository")
 * @ORM\Table(name="cm_currency")
 *
 * @UniqueEntity({"name", "code_symbol", "code_number"})
 */
class Currency
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=5, nullable=false)
     * @Expose()
     */
    protected $codeSymbol;

    /**
     * @ORM\Column(type="string", length=5, nullable=false)
     * @Expose()
     */
    protected $codeNumber;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     * @Expose()
     */
    protected $sortOrder;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled')")
     * @SWG\Property(type="string", enum={"active","disabled"})
     * @Groups("all")
     */
    protected $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get codeSymbol.
     *
     * @return string
     */
    public function getCodeSymbol()
    {
        return $this->codeSymbol;
    }

    /**
     * Set codeSymbol.
     *
     * @param string $codeSymbol
     *
     * @return Currency
     */
    public function setCodeSymbol($codeSymbol)
    {
        $this->codeSymbol = $codeSymbol;

        return $this;
    }

    /**
     * Get codeNumber.
     *
     * @return string
     */
    public function getCodeNumber()
    {
        return $this->codeNumber;
    }

    /**
     * Set codeNumber.
     *
     * @param string $codeNumber
     *
     * @return Currency
     */
    public function setCodeNumber($codeNumber)
    {
        $this->codeNumber = $codeNumber;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return Currency
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Currency
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Currency
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Currency
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
