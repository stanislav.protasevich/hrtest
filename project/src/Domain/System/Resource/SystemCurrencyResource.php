<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\System\Resource;

use Domain\System\Entity\Currency;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Currency
 */
class SystemCurrencyResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'codeSymbol' => $this->getCodeSymbol(),
            'codeNumber' => $this->getCodeNumber(),
            'sortOrder' => $this->getSortOrder(),
        ];
    }
}