<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\TimeOff\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\Company\Entity\Department;
use Domain\User\Entity\User;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\TimeOffBundle\Repository\PolicyRepository")
 * @ORM\Table(name="`to_policy`")
 *
 * @UniqueEntity("name")
 */
class Policy
{
    public const TYPE_COMMON = 'common';
    public const TYPE_USER = 'user';
    public const TYPE_DEPARTMENT = 'department';
    public const TYPES = [
        self::TYPE_COMMON,
        self::TYPE_USER,
        self::TYPE_DEPARTMENT,
    ];

    public const COUNT_TYPE_HOURS = 'hours';
    public const COUNT_TYPE_DAYS = 'days';
    public const COUNT_TYPES = [
        self::COUNT_TYPE_DAYS,
        self::COUNT_TYPE_HOURS,
    ];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
    ];

    public const FREQUENCY_TYPE_DAYS = 'days';
    public const FREQUENCY_TYPE_WEEKS = 'weeks';
    public const FREQUENCY_TYPE_MONTHS = 'months';
    public const FREQUENCY_TYPE_QUARTERLY = 'quarterly';
    public const FREQUENCY_TYPE_YEARS = 'years';
    public const FREQUENCY_TYPES = [
        self::FREQUENCY_TYPE_DAYS,
        self::FREQUENCY_TYPE_WEEKS,
        self::FREQUENCY_TYPE_MONTHS,
        self::FREQUENCY_TYPE_QUARTERLY,
        self::FREQUENCY_TYPE_YEARS,
    ];

    public const IN_ROW_TYPE_DAYS = 'days';
    public const IN_ROW_TYPE_WEEKS = 'weeks';
    public const IN_ROW_TYPE_MONTHS = 'months';
    public const IN_ROW_TYPE_YEARS = 'years';
    public const IN_ROW_TYPES = [
        self::IN_ROW_TYPE_DAYS,
        self::IN_ROW_TYPE_WEEKS,
        self::IN_ROW_TYPE_MONTHS,
        self::IN_ROW_TYPE_YEARS,
    ];

    public const FIRST_BEGIN_TYPE_DAYS = 'days';
    public const FIRST_BEGIN_TYPE_WEEKS = 'weeks';
    public const FIRST_BEGIN_TYPE_MONTHS = 'months';
    public const FIRST_BEGIN_TYPE_YEARS = 'years';
    public const FIRST_BEGIN_TYPES = [
        self::FIRST_BEGIN_TYPE_DAYS,
        self::FIRST_BEGIN_TYPE_WEEKS,
        self::FIRST_BEGIN_TYPE_MONTHS,
        self::FIRST_BEGIN_TYPE_YEARS,
    ];

    public const DEFAULT_LENGTH_DAYS = 1;
    public const DEFAULT_LENGTH_HOURS = 8;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('common', 'user', 'department')")
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    protected $name;

    /**
     * @var integer Максимальная продолжительность отпуска, напр. 14 единиц (берет исчесление из $countType)
     * @ORM\Column(type="integer", length=3, nullable=false)
     */
    protected $maxCountAtTime;

    /**
     * @var string тип максимальной прододжительности
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'hours')", nullable=false)
     */
    protected $countType;

    /**
     * @var integer Частота использования, напр. 1
     * @ORM\Column(type="integer", length=3, nullable=false)
     */
    protected $frequencyOfUse;

    /**
     * @var string Частота использования, напр. в квартал
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'weeks', 'months', 'quarterly', 'years')", nullable=false)
     */
    protected $frequencyType;

    /**
     * @var boolean Возможность брать отпуск подряд
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $inRow;

    /**
     * @var integer Если $inRow = false, то перерыв между отпусками, напр. 1
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    protected $inRowInterval;

    /**
     * @var string Если $inRow = false, в месяц.
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'weeks', 'months', 'years')", nullable=false)
     */
    protected $inRowType;

    /**
     * @var integer когда начисляется первый раз, нарп. 6
     * @ORM\Column(type="integer", length=3, nullable=false)
     */
    protected $firstBeginsInterval;

    /**
     * @var string когда начисляется первый раз, нарп. месяц
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'weeks', 'months', 'years')", nullable=false)
     */
    protected $firstBeginsType;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     */
    protected $updatedBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\TimeOff\Entity\TimeOffType", inversedBy="policies", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $timeOffType;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\Company\Entity\Department", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="to_policy_assign_department")
     *
     * @Assert\Valid()
     */
    protected $assignDepartments;

    /**
     * @ORM\ManyToMany(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="to_policy_assign_user")
     *
     * @Assert\Valid()
     */
    protected $assignUsers;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled')")
     * @SWG\Property(type="string", enum={"active", "disabled"})
     */
    protected $status;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->type = self::TYPE_COMMON;
        $this->name = 'Default';
        $this->maxCountAtTime = 7;
        $this->countType = self::COUNT_TYPE_DAYS;
        $this->frequencyOfUse = 1;
        $this->frequencyType = self::FREQUENCY_TYPE_MONTHS;
        $this->inRow = false;
        $this->inRowInterval = 1;
        $this->inRowType = self::IN_ROW_TYPE_MONTHS;
        $this->firstBeginsInterval = 6;
        $this->firstBeginsType = self::FIRST_BEGIN_TYPE_MONTHS;

        $this->assignDepartments = new ArrayCollection();
        $this->assignUsers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Policy
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Policy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get maxCountAtTime.
     *
     * @return int
     */
    public function getMaxCountAtTime()
    {
        return $this->maxCountAtTime;
    }

    /**
     * Set maxCountAtTime.
     *
     * @param int $maxCountAtTime
     *
     * @return Policy
     */
    public function setMaxCountAtTime($maxCountAtTime)
    {
        $this->maxCountAtTime = $maxCountAtTime;

        return $this;
    }

    /**
     * Get frequencyOfUse.
     *
     * @return int
     */
    public function getFrequencyOfUse()
    {
        return $this->frequencyOfUse;
    }

    /**
     * Set frequencyOfUse.
     *
     * @param int $frequencyOfUse
     *
     * @return Policy
     */
    public function setFrequencyOfUse($frequencyOfUse)
    {
        $this->frequencyOfUse = $frequencyOfUse;

        return $this;
    }

    /**
     * Get frequencyType.
     *
     * @return string
     */
    public function getFrequencyType()
    {
        return $this->frequencyType;
    }

    /**
     * Set frequencyType.
     *
     * @param string $frequencyType
     *
     * @return Policy
     */
    public function setFrequencyType($frequencyType)
    {
        $this->frequencyType = $frequencyType;

        return $this;
    }

    /**
     * Get inRow.
     *
     * @return bool
     */
    public function getInRow()
    {
        return $this->inRow;
    }

    /**
     * Set inRow.
     *
     * @param bool $inRow
     *
     * @return Policy
     */
    public function setInRow($inRow)
    {
        $this->inRow = $inRow;

        return $this;
    }

    /**
     * Get inRowInterval.
     *
     * @return int|null
     */
    public function getInRowInterval()
    {
        return $this->inRowInterval;
    }

    /**
     * Set inRowInterval.
     *
     * @param int|null $inRowInterval
     *
     * @return Policy
     */
    public function setInRowInterval($inRowInterval = null)
    {
        $this->inRowInterval = $inRowInterval;

        return $this;
    }

    /**
     * Get inRowType.
     *
     * @return string
     */
    public function getInRowType()
    {
        return $this->inRowType;
    }

    /**
     * Set inRowType.
     *
     * @param string $inRowType
     *
     * @return Policy
     */
    public function setInRowType($inRowType)
    {
        $this->inRowType = $inRowType;

        return $this;
    }

    /**
     * Get firstBeginsInterval.
     *
     * @return int
     */
    public function getFirstBeginsInterval()
    {
        return $this->firstBeginsInterval;
    }

    /**
     * Set firstBeginsInterval.
     *
     * @param int $firstBeginsInterval
     *
     * @return Policy
     */
    public function setFirstBeginsInterval($firstBeginsInterval)
    {
        $this->firstBeginsInterval = $firstBeginsInterval;

        return $this;
    }

    /**
     * Get firstBeginsType.
     *
     * @return string
     */
    public function getFirstBeginsType()
    {
        return $this->firstBeginsType;
    }

    /**
     * Set firstBeginsType.
     *
     * @param string $firstBeginsType
     *
     * @return Policy
     */
    public function setFirstBeginsType($firstBeginsType)
    {
        $this->firstBeginsType = $firstBeginsType;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Policy
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Policy
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User $createdBy
     *
     * @return Policy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User $updatedBy
     *
     * @return Policy
     */
    public function setUpdatedBy(User $updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get timeOffType.
     *
     * @return \Domain\TimeOff\Entity\TimeOffType|null
     */
    public function getTimeOffType()
    {
        return $this->timeOffType;
    }

    /**
     * Set timeOffType.
     *
     * @param \Domain\TimeOff\Entity\TimeOffType|null $timeOffType
     *
     * @return Policy
     */
    public function setTimeOffType(TimeOffType $timeOffType = null)
    {
        $this->timeOffType = $timeOffType;

        return $this;
    }

    /**
     * Add assignDepartment.
     *
     * @param \Domain\Company\Entity\Department $assignDepartment
     *
     * @return Policy
     */
    public function addAssignDepartment(Department $assignDepartment)
    {
        $this->assignDepartments[] = $assignDepartment;

        return $this;
    }

    /**
     * Remove assignDepartment.
     *
     * @param \Domain\Company\Entity\Department $assignDepartment
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAssignDepartment(Department $assignDepartment)
    {
        return $this->assignDepartments->removeElement($assignDepartment);
    }

    /**
     * Get assignDepartments.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignDepartments()
    {
        return $this->assignDepartments;
    }

    /**
     * Add assignUser.
     *
     * @param \Domain\User\Entity\User $assignUser
     *
     * @return Policy
     */
    public function addAssignUser(User $assignUser)
    {
        $this->assignUsers[] = $assignUser;

        return $this;
    }

    /**
     * Remove assignUser.
     *
     * @param \Domain\User\Entity\User $assignUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAssignUser(User $assignUser)
    {
        return $this->assignUsers->removeElement($assignUser);
    }

    /**
     * Get assignUsers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignUsers()
    {
        return $this->assignUsers;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Policy
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountType(): string
    {
        return $this->countType ?: self::COUNT_TYPE_DAYS;
    }

    /**
     * @param string $countType
     */
    public function setCountType(string $countType): void
    {
        $this->countType = $countType;
    }

    public function getOffsetCountType(): int
    {
        return $this->countType === static::COUNT_TYPE_DAYS ? static::DEFAULT_LENGTH_DAYS : static::DEFAULT_LENGTH_HOURS;
    }
}
