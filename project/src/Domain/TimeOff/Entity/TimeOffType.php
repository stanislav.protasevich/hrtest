<?php

declare(strict_types=1);

namespace Domain\TimeOff\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\TimeOffBundle\Repository\TimeOffTypeRepository")
 * @ORM\Table(name="to_time_off_type")
 *
 * @UniqueEntity("name")
 *
 * @ExclusionPolicy("all")
 */
class TimeOffType
{
    public const TRACK_TIME_DAYS = 'days';
    public const TRACK_TIME_HOURS = 'hours';
    public const TRACK_TIMES = [
        self::TRACK_TIME_HOURS,
        self::TRACK_TIME_DAYS,
    ];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_DELETED = 'deleted';
    public const STATUSES = [
        self::STATUS_ACTIVE,
        self::STATUS_DISABLED,
        self::STATUS_DELETED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @Expose()
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     * @Expose()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('days', 'hours')")
     * @Groups({"api"})
     * @Expose()
     */
    protected $trackTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Expose()
     */
    protected $showInCalendar;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Expose()
     */
    protected $icon;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $paid;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     *
     * @Assert\Regex("/^\#\w{6}+$/i")
     *
     * @Expose()
     */
    protected $calendarColor;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Expose()
     */
    protected $description;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('active', 'disabled', 'deleted')")
     */
    protected $status;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @Groups({"api"})
     * @Expose()
     */
    protected $updatedBy;
    /**
     * @ORM\OneToMany(targetEntity="Domain\TimeOff\Entity\Policy", mappedBy="timeOffType", fetch="EXTRA_LAZY")
     */
    protected $policies;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"api"})
     * @Expose()
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->policies = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return TimeOffType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get trackTime.
     *
     * @return string
     */
    public function getTrackTime()
    {
        return $this->trackTime;
    }

    /**
     * Set trackTime.
     *
     * @param string $trackTime
     *
     * @return TimeOffType
     */
    public function setTrackTime($trackTime)
    {
        $this->trackTime = $trackTime;

        return $this;
    }

    /**
     * Get showInCalendar.
     *
     * @return bool|null
     */
    public function getShowInCalendar()
    {
        return $this->showInCalendar;
    }

    /**
     * Set showInCalendar.
     *
     * @param bool|null $showInCalendar
     *
     * @return TimeOffType
     */
    public function setShowInCalendar($showInCalendar = null)
    {
        $this->showInCalendar = $showInCalendar;

        return $this;
    }

    /**
     * Get icon.
     *
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set icon.
     *
     * @param string|null $icon
     *
     * @return TimeOffType
     */
    public function setIcon($icon = null)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get calendarColor.
     *
     * @return string|null
     */
    public function getCalendarColor()
    {
        return $this->calendarColor;
    }

    /**
     * Set calendarColor.
     *
     * @param string|null $calendarColor
     *
     * @return TimeOffType
     */
    public function setCalendarColor($calendarColor = null)
    {
        $this->calendarColor = $calendarColor;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return TimeOffType
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return TimeOffType
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param mixed $paid
     */
    public function setPaid($paid): void
    {
        $this->paid = $paid;
    }

    /**
     * Get createdAt.
     *
     * @return int
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return TimeOffType
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return int
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return TimeOffType
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User $createdBy
     *
     * @return TimeOffType
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User $updatedBy
     *
     * @return TimeOffType
     */
    public function setUpdatedBy(User $updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Add policy.
     *
     * @param \Domain\TimeOff\Entity\Policy $policy
     *
     * @return TimeOffType
     */
    public function addPolicy(Policy $policy)
    {
        $this->policies[] = $policy;

        return $this;
    }

    /**
     * Remove policy.
     *
     * @param \Domain\TimeOff\Entity\Policy $policy
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePolicy(Policy $policy)
    {
        return $this->policies->removeElement($policy);
    }

    /**
     * Get policies.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection|Policy[]
     */
    public function getPolicies()
    {
        return $this->policies;
    }
}
