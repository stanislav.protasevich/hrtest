<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\TimeOff\Resource;


use Domain\TimeOff\Entity\Policy;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Policy
 */
class PolicyResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'maxCountAtTime' => $this->getMaxCountAtTime(),
            'frequencyOfUse' => $this->getFrequencyOfUse(),
            'frequencyType' => $this->getFrequencyType(),
            'inRow' => $this->getInRow(),
            'inRowInterval' => $this->getInRowInterval(),
            'inRowType' => $this->getInRowType(),
            'firstBeginsInterval' => $this->getFirstBeginsInterval(),
            'firstBeginsType' => $this->getFirstBeginsType(),
            'countType' => $this->getCountType(),
            'status' => $this->getStatus(),
            'timeOffType' => new TimeOffTypeResource($this->getTimeOffType()),
            'createdBy' => $this->getCreatedBy() ? $this->getCreatedBy()->getId() : null,
            'updatedBy' => $this->getUpdatedBy() ? $this->getUpdatedBy()->getId() : null,
            'createdAt' => $this->getCreatedAt()->format(\DateTime::ATOM),
            'updatedAt' => $this->getUpdatedAt()->format(\DateTime::ATOM),
        ];
    }
}
