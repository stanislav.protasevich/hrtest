<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\TimeOff\Resource;


use Domain\TimeOff\Entity\AccrualSchedule;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin AccrualSchedule
 */
class AccrualScheduleResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'beginsInterval' => $this->getBeginsInterval(),
            'beginsType' => $this->getBeginsType(),
            'hours' => $this->getHours(),
            'interval' => $this->getInterval(),
            'maximumHours' => $this->getMaximumHours(),
            'allowCarryOver' => $this->getAllowCarryOver(),
        ];
    }
}