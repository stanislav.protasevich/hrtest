<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Domain\TimeOff\Resource;


use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin TimeOffType
 */
class TimeOffTypeResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'trackTime' => $this->getTrackTime(),
            'icon' => $this->getIcon(),
            'description' => $this->getDescription(),
            'calendarColor' => $this->getCalendarColor(),
            'showInCalendar' => $this->getShowInCalendar(),
            'status' => $this->getStatus(),
            'paid' => $this->getPaid(),
        ];
    }
}