<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Auth\Resource;

use Domain\User\Entity\User;
use Domain\User\Resource\UserResource;

/**
 * @mixin User
 */
class LoginResource extends UserResource
{
    /**
     * @var string
     */
    private $token;

    public function __construct($resource, string $token)
    {
        parent::__construct($resource);
        $this->token = $token;
    }

    public function fields()
    {
        $fields = parent::fields();

        return [
            'token' => $this->token,
            'info' => $fields,
        ];
    }
}
