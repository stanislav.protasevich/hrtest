<?php

declare(strict_types=1);


namespace Domain\Location\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LocationBundle\Repository\CityRepository")
 * @ORM\Table(name="lc_city")
 */
class City
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the city.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @SWG\Property(type="string", maxLength=255)
     * @Groups("all")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     * @SWG\Property(type="integer")
     * @Groups("all")
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Country", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @SWG\Property(ref=@Model(type=Country::class, groups={"all"}))
     * @Groups("all")
     */
    protected $country;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Region", inversedBy="cities", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @SWG\Property(ref=@Model(type=Region::class, groups={"all"}))
     * @Groups("all")
     */
    protected $region;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return City
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Domain\Location\Entity\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param \Domain\Location\Entity\Country|null $country
     *
     * @return City
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \Domain\Location\Entity\Region|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set region.
     *
     * @param \Domain\Location\Entity\Region|null $region
     *
     * @return City
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;

        return $this;
    }
}
