<?php

declare(strict_types=1);


namespace Domain\Location\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LocationBundle\Repository\RegionRepository")
 * @ORM\Table(name="lc_region")
 */
class Region
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the region.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @SWG\Property(type="string", maxLength=255)
     * @Groups("all")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     * @SWG\Property(type="integer")
     * @Groups("all")
     */
    protected $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Location\Entity\Country", inversedBy="regions", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $country;

    /**
     * @ORM\OneToMany(targetEntity="Domain\Location\Entity\City", mappedBy="region", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $cities;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return Region
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \Domain\Location\Entity\Country|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param \Domain\Location\Entity\Country|null $country
     *
     * @return Region
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add city.
     *
     * @param \Domain\Location\Entity\City $city
     *
     * @return Region
     */
    public function addCity(\Domain\Location\Entity\City $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city.
     *
     * @param \Domain\Location\Entity\City $city
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCity(\Domain\Location\Entity\City $city)
    {
        return $this->cities->removeElement($city);
    }

    /**
     * Get cities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }
}
