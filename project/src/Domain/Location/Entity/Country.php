<?php

declare(strict_types=1);


namespace Domain\Location\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\LocationBundle\Repository\CountryRepository")
 * @ORM\Table(name="lc_country")
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     * @SWG\Property(description="The unique identifier of the country.")
     * @Groups("all")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @SWG\Property(type="string", maxLength=255)
     * @Groups("all")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true, "default" : 1})
     * @SWG\Property(type="integer")
     * @Groups("all")
     */
    protected $sortOrder;

    /**
     * @ORM\OneToMany(targetEntity="Domain\Location\Entity\Region", mappedBy="country", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $regions;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get sortOrder.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set sortOrder.
     *
     * @param int $sortOrder
     *
     * @return Country
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add region.
     *
     * @param \Domain\Location\Entity\Region $region
     *
     * @return Country
     */
    public function addRegion(\Domain\Location\Entity\Region $region)
    {
        $this->regions[] = $region;

        return $this;
    }

    /**
     * Remove region.
     *
     * @param \Domain\Location\Entity\Region $region
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRegion(\Domain\Location\Entity\Region $region)
    {
        return $this->regions->removeElement($region);
    }

    /**
     * Get regions.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }
}
