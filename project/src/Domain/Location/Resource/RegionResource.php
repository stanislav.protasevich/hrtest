<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Location\Resource;


use Domain\Location\Entity\City;
use Domain\Location\Entity\Region;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Region
 */
class RegionResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'country' => $this->getCountry()->getName(),
            'sortOrder' => $this->getSortOrder(),
            'cities' => array_map(function (City $city) {
                return new CityResource($city);
            }, $this->getCities()->getValues())
        ];
    }
}