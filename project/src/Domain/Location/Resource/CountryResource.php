<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Location\Resource;


use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin Country
 */
class CountryResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'sortOrder' => $this->getSortOrder(),
            'regions' => array_map(function (Region $region) {
                return new RegionResource($region);
            }, $this->getRegions()->getValues())
        ];
    }
}