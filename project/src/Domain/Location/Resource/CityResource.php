<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Location\Resource;


use Domain\Location\Entity\City;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin City
 */
class CityResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'country' => $this->getCountry()->getName(),
            'region' => $this->getRegion()->getName(),
            'sortOrder' => $this->getSortOrder(),
        ];
    }
}