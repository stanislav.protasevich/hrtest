<?php

declare(strict_types=1);

namespace Domain\Employee\Factory;


use Domain\Employee\Entity\EmployeeCompany;

interface EmployeeAddFactoryInterface
{
    public function save(EmployeeCompany $company);
}