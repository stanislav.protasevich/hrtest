<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Employee\Resource;


use Domain\Company\Resource\CompanyResource;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Resource\UserSimpleResource;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin EmployeeCompany
 */
class EmployeeCompanyResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'user' => new UserSimpleResource($this->getUser()),
            'company' => new CompanyResource($this->getCompany()),
            'jobTitleId' => $this->getJobTitle()->getId(),
            'isMain' => $this->getIsMain(),
            'currencyId' => $this->getCurrency()->getId(),
            'salary' => $this->getSalary(),
            'workType' => $this->getWorkType(),
            'status' => $this->getStatus(),
            'startAt' => $this->getStartAt() ? $this->getStartAt()->format(\DateTime::ATOM) : null,
            'endAt' => $this->getEndAt() ? $this->getEndAt()->format(\DateTime::ATOM) : null,
            'salaryIncrease' => $this->getSalaryIncrease(),
            'probationPeriod' => $this->getProbationPeriod() ? $this->getProbationPeriod()->format(\DateTime::ATOM) : null,
            'createdAt' => $this->getCreatedAt() ? $this->getCreatedAt()->format(\DateTime::ATOM) : null,
            'updatedAt' => $this->getUpdatedAt() ? $this->getUpdatedAt()->format(\DateTime::ATOM) : null,
            'createdBy' => $this->getCreatedBy()->getId(),
            'updatedBy' => $this->getUpdatedBy()->getId(),
        ];
    }
}
