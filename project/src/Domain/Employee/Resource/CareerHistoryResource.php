<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Employee\Resource;


use Domain\Employee\Entity\CareerHistory;
use Infrastructure\CommonBundle\Resources\Json\JsonResource;

/**
 * @mixin CareerHistory
 */
class CareerHistoryResource extends JsonResource
{
    public function fields()
    {
        return [
            'id' => $this->getId(),
            'type' => $this->getType(),
            'userBy' => $this->getUser()->getId(),
            'companyId' => $this->getCompany()->getId(),
            'jobTitleId' => $this->getJobTitle()->getId(),
            'description' => $this->getDescription(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt(),
            'createdBy' => $this->getCreatedBy()->getId(),
            'updatedBy' => $this->getUpdatedBy()->getId(),
        ];
    }
}