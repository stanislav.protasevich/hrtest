<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Domain\Employee\Entity;


use Doctrine\ORM\Mapping as ORM;
use Domain\Job\Entity\JobTitle;
use Swagger\Annotations as SWG;
/**
 * @ORM\Entity(repositoryClass="Infrastructure\EmployeeBundle\Repository\CareerHistoryRepository")
 * @ORM\Table(name="em_career_history")
 */
class CareerHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('employed', 'maternity leave', 'dismissed')"), nullable=false)
     * @SWG\Property(type="string", enum={"employed", "maternity leave", "dismissed"})
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\Company", fetch="EXTRA_LAZY", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=false)
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Job\Entity\JobTitle", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $jobTitle;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $createdBy;
    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id", onDelete="CASCADE")
     *
     */
    private $updatedBy;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return CareerHistory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User $user
     *
     * @return CareerHistory
     */
    public function setUser(\Domain\User\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \Domain\Company\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company.
     *
     * @param \Domain\Company\Entity\Company $company
     *
     * @return CareerHistory
     */
    public function setCompany(\Domain\Company\Entity\Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get jobTitle.
     *
     * @return \Domain\Job\Entity\JobTitle
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set jobTitle.
     *
     * @param \Domain\Job\Entity\JobTitle $jobTitle
     *
     * @return CareerHistory
     */
    public function setJobTitle(JobTitle $jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return CareerHistory
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return CareerHistory
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return CareerHistory
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return CareerHistory
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return CareerHistory
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }
}
