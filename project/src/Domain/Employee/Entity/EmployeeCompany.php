<?php

declare(strict_types=1);

namespace Domain\Employee\Entity;

use Doctrine\ORM\Mapping as ORM;
use Domain\Company\Entity\Company;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Infrastructure\RbacBundle\Annotation\Gate;
use Swagger\Annotations as SWG;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\EmployeeBundle\Repository\EmployeeCompanyRepository")
 * @ORM\Table(name="em_employee_company")
 *
 * @UniqueEntity({"user", "company", "department", "jobTitle"})
 */
class EmployeeCompany
{
    public const WORK_TYPE_FULL_TIME = 'full time';
    public const WORK_TYPE_PART_TIME = 'part time';
    public const WORK_TYPE_REMOTE = 'remote';
    public const WORK_TYPES = [
        self::WORK_TYPE_FULL_TIME,
        self::WORK_TYPE_PART_TIME,
        self::WORK_TYPE_REMOTE,
    ];

    public const STATUS_EMPLOYED = 'employed';
    public const STATUS_MATERNITY_LEAVE = 'maternity leave';
    public const STATUS_DISMISSED = 'dismissed';
    public const STATUSES = [
        self::STATUS_EMPLOYED,
        self::STATUS_MATERNITY_LEAVE,
        self::STATUS_DISMISSED,
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="guid")
     *
     * @SWG\Property(description="The unique identifier of the employee company.")
     * @Groups({"api", "all"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Domain\Company\Entity\Company",
     *     inversedBy="employees",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     *
     * @Groups({"api", "all"})
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User", inversedBy="employeeCompanies", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user_id", onDelete="CASCADE")
     */
    protected $user;

    /**
     * @ORM\Column(type="boolean", options={"unsigned":true, "default" : false})
     * @Groups({"api", "all"})
     */
    protected $isMain;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Job\Entity\JobTitle", inversedBy="employeeCompanies", fetch="EXTRA_LAZY", cascade={"persist"})
     * @Groups({"api","all"})
     */
    protected $jobTitle;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\Company\Entity\Department", fetch="EXTRA_LAZY", cascade={"persist"})
     * @Groups({"api","all"})
     */
    protected $department;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('employed', 'maternity leave', 'dismissed')"), nullable=false)
     * @SWG\Property(type="string", enum={"employed", "maternity leave", "dismissed"})
     *
     * @Groups({"api", "all"})
     */
    protected $status;

    /**
     * @ORM\Column(type="float", options={"unsigned":true, "default" : 0})
     *
     * @Groups({"api", "all"})
     * @Gate(Infrastructure\UserBundle\Rbac\UserPermissions::PERMISSION_USER_COMPANY_SALARY_VIEW)
     */
    protected $salary;

    /**
     * @ORM\Column(type="float", options={"unsigned":true, "default" : 0}, nullable=true)
     *
     * @Groups({"api", "all"})
     */
    protected $salaryIncrease;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\System\Entity\Currency", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="currency_id", onDelete="CASCADE")
     * @Groups({"api", "all"})
     */
    protected $currency;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('full time', 'part time', 'remote')"), nullable=false)
     * @SWG\Property(type="string", enum={"full time", "part time", "remote"})
     *
     * @Groups({"api", "all"})
     */
    protected $workType;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @SWG\Property(type="date")
     *
     * @Groups({"api", "all"})
     */
    protected $probationPeriod;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @SWG\Property(type="datetime")
     *
     * @Groups({"api", "all"})
     */
    protected $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @SWG\Property(type="datetime")
     *
     * @Groups({"api", "all"})
     */
    protected $endAt;

    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="datetime")
     *
     * @Groups({"api","all"})
     */
    protected $createdAt;
    /**
     * @ORM\Column(type="datetime")
     * @SWG\Property(type="datetime")
     *
     * @Groups({"api","all"})
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    protected $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="Domain\User\Entity\User")
     * @ORM\JoinColumn(name="updated_by", referencedColumnName="id")
     */
    protected $updatedBy;

    public static function statuses(): array
    {
        return ArrayHelper::keyValueHuman(self::STATUSES);
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EmployeeCompany
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime|null
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EmployeeCompany
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \Domain\Company\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set company.
     *
     * @param \Domain\Company\Entity\Company $company
     *
     * @return EmployeeCompany
     */
    public function setCompany(?Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \Domain\User\Entity\User|null $user
     *
     * @return EmployeeCompany
     */
    public function setUser(\Domain\User\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get department.
     *
     * @return \Domain\Company\Entity\Department|null
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set department.
     *
     * @param \Domain\Company\Entity\Department|null $department
     *
     * @return EmployeeCompany
     */
    public function setDepartment(\Domain\Company\Entity\Department $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy.
     *
     * @param \Domain\User\Entity\User|null $createdBy
     *
     * @return EmployeeCompany
     */
    public function setCreatedBy(\Domain\User\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return \Domain\User\Entity\User|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set updatedBy.
     *
     * @param \Domain\User\Entity\User|null $updatedBy
     *
     * @return EmployeeCompany
     */
    public function setUpdatedBy(\Domain\User\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get isMain.
     *
     * @return bool
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set isMain.
     *
     * @param bool $isMain
     *
     * @return EmployeeCompany
     */
    public function setIsMain(bool $isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get jobTitle.
     *
     * @return \Domain\Job\Entity\JobTitle|null
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set jobTitle.
     *
     * @param \Domain\Job\Entity\JobTitle|null $jobTitle
     *
     * @return EmployeeCompany
     */
    public function setJobTitle(\Domain\Job\Entity\JobTitle $jobTitle = null)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return EmployeeCompany
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get salary.
     *
     * @return float
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set salary.
     *
     * @param float $salary
     *
     * @return EmployeeCompany
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get workType.
     *
     * @return string
     */
    public function getWorkType()
    {
        return $this->workType;
    }

    /**
     * Set workType.
     *
     * @param string $workType
     *
     * @return EmployeeCompany
     */
    public function setWorkType($workType)
    {
        $this->workType = $workType;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return \Domain\System\Entity\Currency|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set currency.
     *
     * @param \Domain\System\Entity\Currency|null $currency
     *
     * @return EmployeeCompany
     */
    public function setCurrency(\Domain\System\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get startAt.
     *
     * @return \DateTime|null
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * Set startAt.
     *
     * @param \DateTime|null $startAt
     *
     * @return EmployeeCompany
     */
    public function setStartAt($startAt = null)
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * Get endAt.
     *
     * @return \DateTime|null
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * Set endAt.
     *
     * @param \DateTime|null $endAt
     *
     * @return EmployeeCompany
     */
    public function setEndAt($endAt = null)
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * Get salaryIncrease.
     *
     * @return float
     */
    public function getSalaryIncrease()
    {
        return $this->salaryIncrease;
    }

    /**
     * Set salaryIncrease.
     *
     * @param float $salaryIncrease
     *
     * @return EmployeeCompany
     */
    public function setSalaryIncrease($salaryIncrease)
    {
        $this->salaryIncrease = $salaryIncrease;

        return $this;
    }

    /**
     * Get probationPeriod.
     *
     * @return \DateTime|null
     */
    public function getProbationPeriod()
    {
        return $this->probationPeriod;
    }

    /**
     * Set probationPeriod.
     *
     * @param \DateTime|null $probationPeriod
     *
     * @return EmployeeCompany
     */
    public function setProbationPeriod(\DateTime $probationPeriod = null)
    {
        $this->probationPeriod = $probationPeriod;

        return $this;
    }
}
