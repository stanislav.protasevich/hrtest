<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Domain\Oauth\Entity;


use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Model\AccessTokenInterface;

/**
 * @ORM\Entity(repositoryClass="Infrastructure\AuthBundle\Repository\AccessTokenRepository")
 * @ORM\Table(name="oauth_access_token")
 */
class AccessToken extends Token implements AccessTokenInterface
{
    public const GRANT_TYPE_PASSWORD = 'password';
    public const GRANT_TYPE_REFRESH_TOKEN = 'refresh_token';
    public const GRANT_TYPES = [
        self::GRANT_TYPE_PASSWORD,
        self::GRANT_TYPE_REFRESH_TOKEN,
    ];
}
