<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Employee;


use Infrastructure\EmployeeBundle\Command\EmployeeCompanyListCommand;
use Infrastructure\EmployeeBundle\Rbac\EmployeeCompanyPermissions;
use Infrastructure\EmployeeBundle\Service\EmployeeCompanySearchService;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class EmployeeCompanyListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(EmployeeCompanyPermissions::PERMISSION_EMPLOYEE_COMPANY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the employees companies list."
     * )
     * @SWG\Parameter(
     *     name="EmployeeCompanySearch[startAt]",
     *     in="query",
     *     type="string",
     *     description="The start as date from to"
     * )
     * @SWG\Parameter(
     *     name="EmployeeCompanySearch[probationPeriod]",
     *     in="query",
     *     type="string",
     *     description="The probation period as date from to"
     * )
     * @SWG\Parameter(
     *     name="startAtFormat",
     *     in="query",
     *     type="string",
     *     @SWG\Items(
     *        type="string",
     *        enum={"year", "month"}
     *     ),
     *     description="The startAt format for filtering. Accept year, month. Default is year."
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *        type="string",
     *        enum={"startAt", "probationPeriod"}
     *     ),
     *     collectionFormat="multi",
     *     description="Employees companies sort by params"
     * )
     * @SWG\Tag(name="Employee")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $command = new EmployeeCompanyListCommand($request->get('EmployeeCompanySearch', []));

        $startFormat = $request->get('startAtFormat');
        $command->startAtFormat = in_array($startFormat, [EmployeeCompanySearchService::START_AT_MONTH, EmployeeCompanySearchService::START_AT_YEAR]) ? $startFormat : EmployeeCompanySearchService::START_AT_YEAR;

        return $this->render(
            $this->handle($command)
        );
    }
}
