<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Employee;

use Domain\Employee\Entity\EmployeeCompany;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class EmployeeStatusesController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_STATUSES_PRIMARY, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the employee statuses in company."
     * )
     * @SWG\Tag(name="Employee")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            EmployeeCompany::statuses()
        );
    }
}