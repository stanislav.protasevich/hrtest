<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Employee;

use Infrastructure\EmployeeBundle\Command\CareerHistorySearchCommand;
use Infrastructure\EmployeeBundle\Rbac\CareerHistoryPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class CareerHistoryViewController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CareerHistoryPermissions::PERMISSION_CAREER_HISTORY_VIEW, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the employee career history."
     * )
     * @SWG\Parameter(
     *     name="CareerHistorySearch[userBy]",
     *     in="query",
     *     type="string",
     *     description="The user ID"
     * )
     * @SWG\Parameter(
     *     name="CareerHistorySearch[companyId]",
     *     in="query",
     *     type="string",
     *     description="The company ID"
     * )
     * @SWG\Parameter(
     *     name="CareerHistorySearch[jobTitleId]",
     *     in="query",
     *     type="string",
     *     description="The job title ID"
     * )
     * @SWG\Parameter(
     *     name="CareerHistorySearch[type]",
     *     in="query",
     *     type="string",
     *     description="The type of history",
     *     @SWG\Schema(
     *       type="string",
     *       enum={"hire","migrated","dismissed"}
     *     )
     * )
     *
     * @SWG\Tag(name="Employee")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CareerHistorySearchCommand($request->get('CareerHistorySearch', []))
            )
        );
    }
}