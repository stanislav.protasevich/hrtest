<?php

declare(strict_types=1);

namespace UI\RestBundle\Controller\Employee;


use Domain\Employee\Entity\EmployeeCompany;
use Infrastructure\EmployeeBundle\Factory\Form\EmployeeToCompanyType;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractController;

class EmployeeAddController extends AbstractController
{
//    public const PERMISSION_EMPLOYEE_ADD_TO_COMPANY = 'employeeAddToCompany';
//
//    /**
//     * @inheritdoc
//     */
//    public function init()
//    {
//        if (!$this->isGranted(self::PERMISSION_EMPLOYEE_ADD_TO_COMPANY, self::class)) {
//            $this->denyAccessUnlessGranted(self::PERMISSION_EMPLOYEE_ADD_TO_COMPANY, self::class);
//        }
//
//        parent::init();
//    }
//
//    /**
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     */
//    public function __invoke(Request $request)
//    {
//        $model = new EmployeeCompany();
//        $form = $this->createForm(EmployeeToCompanyType::class, $model);
//        $this->processForm($request, $form);
//
//        return $this->render(($this->handler)($model));
//    }
}