<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace UI\RestBundle\Controller\Hobby;


use Domain\User\Entity\Hobby;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class HobbyListController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the hobby list."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Hobby")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_HOBBY_LIST);

        return $this->render(
            $this->getEm()->getRepository(Hobby::class)->findPrimaryList()
        );
    }
}