<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace UI\RestBundle\Controller\System;


use Infrastructure\SystemBundle\Command\CurrencyListCommand;
use Infrastructure\SystemBundle\Rbac\CurrencyPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class CurrencyListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CurrencyPermissions::PERMISSION_SYSTEM_CURRENCY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the currency list."
     * )
     * @SWG\Tag(name="System")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CurrencyListCommand()
            )
        );
    }
}