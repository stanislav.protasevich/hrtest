<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Auth;

use Infrastructure\AuthBundle\Command\ConfirmEmailCommand;
use Infrastructure\CommonBundle\Helpers\Json;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class UserConfirmEmailController extends AbstractBusController
{
    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns true"
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="body",
     *     description="The confirm token",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Auth")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $data = Json::decode($request->getContent());
        $command = new ConfirmEmailCommand($data ?? []);

        return $this->render(
            $this->handle($command)
        );
    }
}
