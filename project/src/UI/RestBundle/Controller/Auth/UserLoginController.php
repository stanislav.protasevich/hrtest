<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Auth;

use Infrastructure\AuthBundle\Command\LoginCommand;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;

class UserLoginController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns access token and user info"
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     description="The email",
     *     required=true,
     *     @SWG\Schema(
     *       type="email",
     *       example="test@email.com"
     *     )
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     description="The password",
     *     required=true,
     *     @SWG\Schema(
     *       type="password",
     *       example="secret",
     *       minLength=6,
     *       maxLength=16
     *     )
     * )
     * @SWG\Tag(name="Auth")
     *
     * @RateLimit(limit=10, period=360)
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $command = new LoginCommand($data ?? []);
        $command->userAgent = $request->server->get('HTTP_USER_AGENT');
        $command->ip = $request->getClientIp();

        return $this->render($this->handle($command));
    }
}