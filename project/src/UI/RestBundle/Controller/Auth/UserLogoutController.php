<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Auth;

use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Command\logoutCommand;
use Infrastructure\AuthBundle\Security\UserIdentityInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use UI\RestBundle\Controller\AbstractBusController;

class UserLogoutController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns true"
     * )
     * @SWG\Tag(name="Auth")
     *
     * @param UserInterface|User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(UserInterface $user, Request $request)
    {
        $accessToken = $this->container->get(UserIdentityInterface::class)->getCredentials($request);

        return $this->render($this->handle(
            new logoutCommand($user, $accessToken)
        ));
    }
}