<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Auth;

use Infrastructure\AuthBundle\Command\UserPasswordResetCommand;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\{
    Request, Response
};
use UI\RestBundle\Controller\AbstractBusController;

class UserPasswordResetController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns array with user email"
     * )
     * @SWG\Parameter(
     *     name="token",
     *     in="body",
     *     description="The reset token",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     description="The password",
     *     required=true,
     *     @SWG\Schema(
     *       type="password",
     *       example="secret",
     *       minLength=6,
     *       maxLength=16
     *     )
     * )
     * @SWG\Tag(name="Auth")
     *
     * @param Request $request
     * @return string|\Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(Request $request)
    {
        if ($request->getMethod() === Request::METHOD_POST) {
            $data = json_decode($request->getContent(), true);

            $command = new UserPasswordResetCommand($data ?? []);

            return $this->render(
                $this->handle($command)
            );
        }

        return $this->render(null, Response::HTTP_NO_CONTENT);
    }
}