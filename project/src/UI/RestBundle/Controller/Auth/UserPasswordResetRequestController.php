<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Auth;

use Infrastructure\AuthBundle\Command\UserPasswordResetRequestCommand;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserPasswordResetRequestController extends AbstractBusController
{
    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns true"
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     description="The email",
     *     required=true,
     *     @SWG\Schema(
     *       type="email",
     *       example="test@email.com"
     *     )
     * )
     * @SWG\Tag(name="Auth")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $command = new UserPasswordResetRequestCommand($data ?? []);

        return $this->render($this->handle($command));
    }
}