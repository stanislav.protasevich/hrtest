<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Category;

use Infrastructure\BlogBundle\Command\Category\BlogCategoryDeleteCommand;
use Infrastructure\BlogBundle\Helpers\BlogCategoryHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class BlogCategoryDeleteController extends AbstractBusController
{
    use BlogCategoryHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $category = $this->findOrFail($id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_CATEGORY_DELETE, $category);

        if ($category->getPosts()->count()) {
            throw FormException::withMessages(
                [sprintf('Blog category id: "%s" has active links. Remove all links.', $category->getId())]
            );
        }

        return $this->render(
            $this->handle(
                new BlogCategoryDeleteCommand($category)
            )
        );
    }
}
