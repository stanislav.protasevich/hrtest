<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Category;

use Infrastructure\BlogBundle\Command\Category\BlogCategoryEditCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogCategoryType;
use Infrastructure\BlogBundle\Helpers\BlogCategoryHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Nelmio\ApiDocBundle\Annotation\Model;
use Domain\Blog\Entity\BlogCategory;

class BlogCategoryEditController extends AbstractBusController
{
    use BlogCategoryHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the blog category.",
     *     @Model(type=BlogCategory::class, groups={"all"})
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The blog category title.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The blog category image (in base64).",
     *     @SWG\Schema(
     *         type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The blog category status.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $category = $this->findOrFail($id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_CATEGORY_EDIT, $category);

        $form = $this->createForm(BlogCategoryType::class, $category, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new BlogCategoryEditCommand($category)
            )
        );
    }
}
