<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Category;


use Domain\Blog\Entity\BlogCategory;
use Infrastructure\BlogBundle\Command\Category\BlogCategoryCreateCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogCategoryType;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogCategoryCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(BlogPermissions::PERMISSION_BLOG_CATEGORY_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the status."
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The blog category title.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The blog category image (in base64).",
     *     @SWG\Schema(
     *         type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The blog category status.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $model = new BlogCategory();

        $form = $this->createForm(BlogCategoryType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new BlogCategoryCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}
