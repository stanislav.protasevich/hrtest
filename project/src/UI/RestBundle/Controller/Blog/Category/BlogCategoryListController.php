<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Category;

use Infrastructure\BlogBundle\Command\Category\BlogCategoryListCommand;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogCategoryListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(BlogPermissions::PERMISSION_BLOG_CATEGORY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the blog categories list."
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"id", "title", "status", "createdAt"}
     *     ),
     *     collectionFormat="multi",
     *     description="The blog categories sort by params, etc. &sort=id,title,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new BlogCategoryListCommand($request->get('BlogCategorySearch', []))
            )
        );
    }
}
