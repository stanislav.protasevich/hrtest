<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Comment;

use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentEditCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogPostCommentType;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostCommentEditController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the comment data."
     * )
     * @SWG\Parameter(
     *     name="parent",
     *     in="body",
     *     description="The blog post parent comment.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="content",
     *     in="body",
     *     description="The blog post comment content.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $blogId
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $blogId, string $id, Request $request)
    {
        $comment = $this->findCommentOrFail($blogId, $id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_COMMENTS_EDIT, $comment);

        $form = $this->createForm(BlogPostCommentType::class, $comment);
        $form->add('parent', null, ['disabled' => true]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new BlogPostCommentEditCommand($comment)
            )
        );
    }
}
