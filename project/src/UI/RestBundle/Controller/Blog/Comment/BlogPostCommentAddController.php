<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Comment;

use Domain\Blog\Entity\BlogPostComment;
use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentAddCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogPostCommentType;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostCommentAddController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the comment id."
     * )
     * @SWG\Parameter(
     *     name="parent",
     *     in="body",
     *     description="The blog post parent comment.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="content",
     *     in="body",
     *     description="The blog post comment content.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $blogId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $blogId, Request $request)
    {
        $blog = $this->findOrFail($blogId);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_COMMENTS_ADD, $blog);

        $comment = new BlogPostComment();
        $form = $this->createForm(BlogPostCommentType::class, $comment);
        $this->processForm($request, $form);

        $comment->setPost($blog);

        return $this->render(
            $this->handle(
                new BlogPostCommentAddCommand($comment)
            ),
            Response::HTTP_CREATED
        );
    }
}
