<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Comment;


use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentRemoveCommand;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\CommonBundle\Http\Response;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostCommentRemoveController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $blogId
     * @param string $id
     * @param Request $request
     * @return Response|\Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $blogId, string $id, Request $request)
    {
        $comment = $this->findCommentOrFail($blogId, $id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_COMMENTS_REMOVE, $comment);

        return $this->render(
            $this->handle(
                new BlogPostCommentRemoveCommand($comment)
            )
        );
    }
}
