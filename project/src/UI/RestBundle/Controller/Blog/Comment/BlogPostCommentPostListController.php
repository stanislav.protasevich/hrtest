<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog\Comment;

use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentPostListCommand;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostCommentPostListController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the comment id."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $blogId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $blogId, Request $request)
    {
        $comment = $this->findOrFail($blogId);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_COMMENTS_VIEW, $comment);

        return $this->render(
            $this->handle(
                new BlogPostCommentPostListCommand($comment)
            )
        );
    }
}
