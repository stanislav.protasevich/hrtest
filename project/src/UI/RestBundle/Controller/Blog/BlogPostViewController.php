<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog;

use Domain\Blog\Entity\BlogPost;
use Infrastructure\BlogBundle\Command\BlogPostViewCommand;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostViewController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the blog post.",
     *     @Model(type=BlogPost::class, groups={"all"})
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $id, Request $request)
    {
        $blog = $this->findOrFail($id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_LIST, $blog);
        $this->canAccess($blog);

        return $this->render(
            $this->handle(
                new BlogPostViewCommand($blog)
            )
        );
    }
}
