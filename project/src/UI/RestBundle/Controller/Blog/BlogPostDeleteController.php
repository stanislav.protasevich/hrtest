<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog;


use Infrastructure\BlogBundle\Command\BlogPostDeleteCommand;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class BlogPostDeleteController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $id, Request $request)
    {
        $blog = $this->findOrFail($id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_DELETE, $blog);

        return $this->render(
            $this->handle(
                new BlogPostDeleteCommand($blog)
            )
        );
    }
}
