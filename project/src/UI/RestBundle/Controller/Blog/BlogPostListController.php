<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog;


use Infrastructure\BlogBundle\Command\BlogPostListCommand;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class BlogPostListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(BlogPermissions::PERMISSION_BLOG_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the blog list."
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"id", "title", "status", "categories", "createdAt", "createdBy"}
     *     ),
     *     collectionFormat="multi",
     *     description="The blog post sort by params, etc. &sort=id,title,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $command = new BlogPostListCommand($request->get('BlogSearch', []));
        $command->request = $request;
        $command->fullAccess = Gate::can(RbacPermissions::ROLE_ADMINISTRATOR);

        return $this->render(
            $this->handle(
                $command
            )
        );
    }
}
