<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog;


use Infrastructure\BlogBundle\Command\BlogPostEditCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogPostType;
use Infrastructure\BlogBundle\Helpers\BlogHelpersTrait;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Domain\Blog\Entity\BlogPost;

class BlogPostEditController extends AbstractBusController
{
    use BlogHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the blog.",
     *     @Model(type=BlogPost::class, groups={"all"})
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The blog post title.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="content",
     *     in="body",
     *     description="The blog post content.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The blog post image (in base64).",
     *     @SWG\Schema(
     *         type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The blog post status.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled","deleted"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(string $id, Request $request)
    {
        $blog = $this->findOrFail($id);

        $this->isGranted(BlogPermissions::PERMISSION_BLOG_EDIT, $blog);

        $form = $this->createForm(BlogPostType::class, $blog, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new BlogPostEditCommand($blog)
            )
        );
    }
}
