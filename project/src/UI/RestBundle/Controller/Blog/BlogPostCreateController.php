<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Blog;


use Domain\Blog\Entity\BlogPost;
use Infrastructure\BlogBundle\Command\BlogPostCreateCommand;
use Infrastructure\BlogBundle\Factory\Form\BlogPostType;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class BlogPostCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(BlogPermissions::PERMISSION_BLOG_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the created blog post id."
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The blog post title.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="content",
     *     in="body",
     *     description="The blog post content.",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The blog post image (in base64).",
     *     @SWG\Schema(
     *         type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The blog post status.",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled","deleted"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Blog")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $model = new BlogPost();

        $form = $this->createForm(BlogPostType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new BlogPostCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}
