<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Archive;


use Infrastructure\ArchiveBundle\Command\ArchiveFileUserAttachCommand;
use Infrastructure\ArchiveBundle\Rbac\ArchivePermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class ArchiveFileUserAttachController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(ArchivePermissions::PERMISSION_ARCHIVE_USER_ATTACH, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the file."
     * )
     * @SWG\Tag(name="Archive")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new ArchiveFileUserAttachCommand(
                    $request->files->get('file'),
                    $request->get('user')
                )
            )
        );
    }
}