<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Archive;


use Domain\Archive\Entity\File;
use Infrastructure\ArchiveBundle\Command\ArchiveFileViewCommand;
use Infrastructure\ArchiveBundle\Rbac\ArchivePermissions;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class ArchiveFileViewController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the file."
     * )
     * @SWG\Tag(name="Archive")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $file = $this->getEm()->getRepository(File::class)->find($id);

        if (!$file) {
            throw new ModelNotFoundException(File::class, $id);
        }

        $this->isGranted(ArchivePermissions::PERMISSION_ARCHIVE_FILE_VIEW, $file);

        return $this->render(
            $this->handle(
                new ArchiveFileViewCommand($file)
            )
        );
    }
}
