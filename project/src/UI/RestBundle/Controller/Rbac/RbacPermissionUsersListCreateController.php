<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Rbac;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacPermissionUsersListCommand;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class RbacPermissionUsersListCreateController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(RbacPermissions::PERMISSION_RBAC_USER_LIST_PERMISSION, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns users by permission."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The role name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Rbac")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $permission = $this->getEm()->getRepository(AuthItem::class)->find($request->get('name'));

        if (is_null($permission)) {
            throw new NotFoundHttpException($this->translate('Permission not found.'));
        }

        return $this->render(
            $this->handle(
                new RbacPermissionUsersListCommand($permission)
            )
        );
    }
}