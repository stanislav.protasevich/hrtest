<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Rbac;

use Infrastructure\RbacBundle\Command\RbacPermissionListCommand;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class RbacPermissionListController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(RbacPermissions::PERMISSION_RBAC_LIST_PERMISSION, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns permissions array."
     * )
     * @SWG\Tag(name="Rbac")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new RbacPermissionListCommand()
            )
        );
    }
}