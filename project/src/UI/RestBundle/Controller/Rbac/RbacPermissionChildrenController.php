<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemChildrenCommand;
use Infrastructure\RbacBundle\Factory\Form\AuthItemPermissionChildrenType;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class RbacPermissionChildrenController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(RbacPermissions::PERMISSION_RBAC_PERMISSIONS_CHILDREN_UPDATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns bool."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The role name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="children",
     *     in="body",
     *     description="The permission/role name",
     *     required=true,
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="string")
     *     )
     * )
     * @SWG\Tag(name="Rbac")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $command = new RbacItemChildrenCommand();

        $form = $this->createForm(AuthItemPermissionChildrenType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                $command
            )
        );
    }
}