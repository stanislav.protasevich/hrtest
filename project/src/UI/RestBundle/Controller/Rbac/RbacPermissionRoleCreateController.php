<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemRoleCreateCommand;
use Infrastructure\RbacBundle\Factory\Form\AuthItemRoleType;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class RbacPermissionRoleCreateController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(RbacPermissions::PERMISSION_RBAC_ROLE_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns bool."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The role name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Rbac")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $model = new AuthItem();

        $form = $this->createForm(AuthItemRoleType::class, $model);

        $this->processForm($request, $form);

        $model->setType(AuthItem::TYPE_ROLE);

        return $this->render(
            $this->handle(
                new RbacItemRoleCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}