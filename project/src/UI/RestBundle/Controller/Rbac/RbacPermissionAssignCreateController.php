<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemAssignCreateCommand;
use Infrastructure\RbacBundle\Factory\Form\AuthItemAssignType;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Domain\User\Entity\User;

class RbacPermissionAssignCreateController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(RbacPermissions::PERMISSION_RBAC_ASSIGN_PERMISSION, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns bool."
     * )
     * @SWG\Parameter(
     *     name="item",
     *     in="body",
     *     description="The rbac permission item",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="assignment",
     *     in="body",
     *     description="The userd ID array",
     *     required=true,
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"all"}))
     *     )
     * )
     * @SWG\Tag(name="Rbac")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $command = new RbacItemAssignCreateCommand();

        $form = $this->createForm(AuthItemAssignType::class, $command, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle($command)
        );
    }
}