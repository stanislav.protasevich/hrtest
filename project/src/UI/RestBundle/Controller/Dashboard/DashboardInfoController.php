<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Dashboard;


use Infrastructure\DashboardBundle\Command\DashboardInfoCommand;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class DashboardInfoController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user main info for dashboard."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Dashboard")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new DashboardInfoCommand(
                    $this->getUser()
                )
            )
        );
    }
}
