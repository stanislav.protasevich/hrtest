<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace UI\RestBundle\Controller\Company\Recruiters;

use Infrastructure\CompanyBundle\Command\Recruiters\CompanyViewRecruitersListCommand;
use Infrastructure\CompanyBundle\Helpers\CompanyHelpersTrait;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyViewRecruitersListController extends AbstractBusController
{
    use CompanyHelpersTrait;

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the company recruiters list."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $company = $this->findOrFail($id);

        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_VIEW_RECRUITERS_LIST, $company);

        return $this->render(
            $this->handle(
                new CompanyViewRecruitersListCommand($company)
            )
        );
    }
}
