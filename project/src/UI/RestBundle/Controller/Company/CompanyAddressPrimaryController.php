<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Company;


use Infrastructure\CompanyBundle\Command\CompanyAddressPrimaryCommand;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyAddressPrimaryController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_LIST, self::class);
    }

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the primary companies address array."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CompanyAddressPrimaryCommand()
            )
        );
    }
}