<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company;


use Doctrine\ORM\EntityManager;
use Domain\Company\Entity\Department;
use Infrastructure\CompanyBundle\Command\DepartmentEditCommand;
use Infrastructure\CompanyBundle\Factory\Form\DepartmentType;
use Infrastructure\CompanyBundle\Rbac\DepartmentPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class DepartmentEditController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the department."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(string $id, Request $request)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $department = $em->getRepository(Department::class)->findOneById($id);

        if (!$department) {
            $translator = $this->container->get('translator');
            throw new NotFoundHttpException($translator->trans('Department not found.'));
        }

        $this->isGranted(DepartmentPermissions::PERMISSION_DEPARTMENT_EDIT, $department);

        $form = $this->createForm(DepartmentType::class, $department, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new DepartmentEditCommand($department)
            )
        );
    }
}