<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company;


use Infrastructure\CompanyBundle\Command\DepartmentSearchCommand;
use Infrastructure\CompanyBundle\Rbac\DepartmentPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class DepartmentListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(DepartmentPermissions::PERMISSION_DEPARTMENT_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the department."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new DepartmentSearchCommand($request->get('DepartmentSearch', []))
            )
        );
    }
}