<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Company;


use Domain\Company\Entity\Company;
use Infrastructure\CompanyBundle\Command\CompanyViewCommand;
use Infrastructure\CompanyBundle\Helpers\CompanyHelpersTrait;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyViewController extends AbstractBusController
{
    use CompanyHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the company.",
     *     @Model(type=Company::class, groups={"all"})
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="query",
     *     type="string",
     *     description="The company ID"
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $company = $this->findOrFail($id);

        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_VIEW, $company);

        return $this->render(
            $this->handle(
                new CompanyViewCommand($company)
            )
        );
    }
}
