<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Company;


use Infrastructure\CompanyBundle\Command\CompanyEditCommand;
use Infrastructure\CompanyBundle\Factory\Form\CompanyType;
use Infrastructure\CompanyBundle\Helpers\CompanyHelpersTrait;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyEditController extends AbstractBusController
{
    use CompanyHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the company ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The company status",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The company logo in base64",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $company = $this->findOrFail($id);

        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_EDIT, $company);

        $form = $this->createForm(CompanyType::class, $company, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new CompanyEditCommand($company)
            )
        );
    }
}
