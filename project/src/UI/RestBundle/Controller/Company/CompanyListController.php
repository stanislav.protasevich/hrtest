<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Company;


use Infrastructure\CompanyBundle\Command\CompanySearchCommand;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_LIST, self::class);
    }

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the paginated companies collection."
     * )
     * @SWG\Parameter(
     *     name="CompanySearch[status]",
     *     in="query",
     *     type="string",
     *     description="The company status"
     * )
     * @SWG\Parameter(
     *     name="CompanySearch[id][]",
     *     in="query",
     *     type="string",
     *     description="The company ID"
     * )
     * @SWG\Parameter(
     *     name="CompanySearch[country]",
     *     in="query",
     *     type="string",
     *     description="The company country ID"
     * )
     * @SWG\Parameter(
     *     name="CompanySearch[city]",
     *     in="query",
     *     type="string",
     *     description="The company city ID"
     * )
     * @SWG\Parameter(
     *     name="CompanySearch[address]",
     *     in="query",
     *     type="string",
     *     description="The company city ID"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"status", "id", "name", "city", "country"}
     *     ),
     *     collectionFormat="multi",
     *     description="The company sort by params, etc. &sort=id,name,-status"
     * )
     * @SWG\Tag(name="Company")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CompanySearchCommand($request->get('CompanySearch', []))
            )
        );
    }
}