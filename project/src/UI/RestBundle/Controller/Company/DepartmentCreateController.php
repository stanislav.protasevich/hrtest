<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company;


use Domain\Company\Entity\Department;
use Infrastructure\CompanyBundle\Command\DepartmentCreateCommand;
use Infrastructure\CompanyBundle\Factory\Form\DepartmentType;
use Infrastructure\CompanyBundle\Rbac\DepartmentPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class DepartmentCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(DepartmentPermissions::PERMISSION_DEPARTMENT_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the department."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $model = new Department();

        $form = $this->createForm(DepartmentType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new DepartmentCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}