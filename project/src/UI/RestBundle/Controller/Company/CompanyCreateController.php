<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Company;


use Domain\Company\Entity\Company;
use Infrastructure\CompanyBundle\Command\CompanyCreateCommand;
use Infrastructure\CompanyBundle\Factory\Form\CompanyType;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the company ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The company status",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="image",
     *     in="body",
     *     description="The company logo in base64",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Company")
     *
     * @param Request $request
     * @param UserInterface $user
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request, UserInterface $user)
    {
        $model = new Company();

        $form = $this->createForm(CompanyType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new CompanyCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}