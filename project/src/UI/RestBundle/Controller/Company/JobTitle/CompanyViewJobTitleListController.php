<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company\JobTitle;


use Infrastructure\CompanyBundle\Command\JobTitle\CompanyViewJobTitleListCommand;
use Infrastructure\CompanyBundle\Helpers\CompanyHelpersTrait;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyViewJobTitleListController extends AbstractBusController
{
    use CompanyHelpersTrait;

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the company job titles list."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $company = $this->findOrFail($id);

        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_VIEW_JOBTITLE_LIST, $company);

        return $this->render(
            $this->handle(
                new CompanyViewJobTitleListCommand($company)
            )
        );
    }
}
