<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company\JobTitle;

use Infrastructure\CompanyBundle\Command\JobTitle\CompanyViewJobTitleAddCommand;
use Infrastructure\CompanyBundle\Factory\Form\CompanyJobTitlesType;
use Infrastructure\CompanyBundle\Helpers\CompanyHelpersTrait;
use Infrastructure\CompanyBundle\Rbac\CompanyPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CompanyViewJobTitleAddController extends AbstractBusController
{
    use CompanyHelpersTrait;

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the company add job titles."
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $company = $this->findOrFail($id);

        $this->isGranted(CompanyPermissions::PERMISSION_COMPANY_VIEW_JOBTITLE_ADD, $company);

        $command = new CompanyViewJobTitleAddCommand($company);

        $form = $this->createForm(CompanyJobTitlesType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle($command)
        );
    }
}
