<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Company;


use Domain\Company\Entity\Department;
use Infrastructure\CompanyBundle\Command\DepartmentViewCommand;
use Infrastructure\CompanyBundle\Rbac\DepartmentPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class DepartmentViewController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the department.",
     *     @Model(type=Department::class, groups={"all"})
     * )
     * @SWG\Tag(name="Company")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(string $id, Request $request)
    {
        $department = $this->getEm()->getRepository(Department::class)->findOneById($id);

        if (!$department) {
            throw new NotFoundHttpException('The requested department does not exist.');
        }

        $this->isGranted(DepartmentPermissions::PERMISSION_DEPARTMENT_VIEW, $department);

        return $this->render(
            $this->handle(
                new DepartmentViewCommand($department)
            )
        );
    }
}