<?php

declare(strict_types=1);

namespace UI\RestBundle\Controller;


use Doctrine\ORM\EntityManager;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\CommonBundle\Helpers\FormHelper;
use Infrastructure\CommonBundle\Http\Traits\ApiResponser;
use Infrastructure\CommonBundle\Web\ApiResponse;
use Infrastructure\RbacBundle\Service\Gate;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractController
{
    use ControllerTrait, ApiResponser;

    private $_em;

    /**
     * @var TranslatorInterface
     */
    private $_translator;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->init();
    }

    public function init()
    {
    }

    protected function render($content = '', ?int $statusCode = Response::HTTP_OK): Response
    {
        if (is_bool($content)) {
            $content = (string)$content;
        }

        return $this->successResponse($content, $statusCode);
    }

    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    protected function isGranted($attributes, $subject = null, ?string $message = null)
    {
        $routeName = $this->container->get('request_stack')->getCurrentRequest()->attributes->get('_route');
        $route = $this->container->get('router')->getRouteCollection()->get($routeName);
        $security = $route->getOption('security');

        if (is_bool($security) && $security === false) {
            return;
        }

        if (!Gate::can($attributes, ['model' => $subject])) {
            $this->throwAccessDeniedException($attributes, $subject, $message);
        }
    }

    protected function throwAccessDeniedException($attributes = null, $subject = null, ?string $message = null)
    {
        $exception = $this->createAccessDeniedException($this->translate($message ?: 'This action is unauthorized.'));

        $exception->setAttributes($attributes);
        $exception->setSubject($subject);

        throw $exception;
    }

    /**
     * {@inheritdoc}
     * @see TranslatorInterface
     */
    protected function translate($id, array $parameters = [], $domain = null, $locale = null)
    {
        if ($this->_translator === null) {
            $this->_translator = $this->container->get('translator');
        }

        return $this->_translator->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @param Request $request
     * @param FormInterface $form
     * @param bool $json
     */
    protected function processForm(Request $request, FormInterface $form, bool $json = true)
    {
        if ($request->getMethod() === Request::METHOD_GET) {
            $data = $request->query->all();
        } else {
            $data = $json ? json_decode($request->getContent(), true) : $request->request->all();

            if (!$json && !empty($request->files->all())) {
                $data = array_merge((array)$data, $request->files->all());
            }
        }

        $clearMissing = $request->getMethod() !== 'PATCH';
        $form->submit($data, $clearMissing);

        if (!$form->isValid()) {
            throw FormException::withMessages(FormHelper::getErrorsFromForm($form));
        }
    }
}
