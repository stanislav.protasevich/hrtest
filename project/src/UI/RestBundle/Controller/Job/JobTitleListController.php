<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Job;


use Infrastructure\JobBundle\Command\JobTitleSearchCommand;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class JobTitleListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(JobPermissions::PERMISSION_JOB_TITLE_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the job titles list."
     * )
     * @SWG\Parameter(
     *     name="JobTitleSearch[id]",
     *     in="query",
     *     type="string",
     *     description="The job title ID"
     * )
     * @SWG\Parameter(
     *     name="JobTitleSearch[name]",
     *     in="query",
     *     type="string",
     *     description="The job title name"
     * )
     * @SWG\Parameter(
     *     name="JobTitleSearch[status]",
     *     in="query",
     *     type="string",
     *     description="The job title status"
     * )
     * @SWG\Tag(name="JobTitle")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new JobTitleSearchCommand($request->get('JobTitleSearch', []))
            )
        );
    }
}
