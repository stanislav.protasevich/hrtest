<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Job;


use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Command\JobTitleCreateCommand;
use Infrastructure\JobBundle\Factory\Form\JobTitleType;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class JobTitleCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(JobPermissions::PERMISSION_JOB_TITLE_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the job title ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="JobTitle")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $model = new JobTitle();
        $form = $this->createForm(JobTitleType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new JobTitleCreateCommand($model)
            )
        );
    }
}
