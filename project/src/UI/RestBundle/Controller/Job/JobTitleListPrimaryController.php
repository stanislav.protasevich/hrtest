<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Job;


use Infrastructure\JobBundle\Command\JobTitleListPrimaryCommand;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class JobTitleListPrimaryController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(JobPermissions::PERMISSION_JOB_TITLE_LIST_PRIMARY, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the primary job titles array."
     * )
     * @SWG\Tag(name="JobTitle")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new JobTitleListPrimaryCommand()
            )
        );
    }
}
