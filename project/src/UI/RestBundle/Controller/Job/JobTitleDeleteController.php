<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Job;


use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Command\JobTitleDeleteCommand;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class JobTitleDeleteController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the boolean."
     * )
     * @SWG\Tag(name="JobTitle")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $model = $this->getEm()->getRepository(JobTitle::class)->find($id);

        if (!$model) {
            $translator = $this->container->get('translator');
            throw new NotFoundHttpException($translator->trans('Job title not found.'));
        }

        $this->isGranted(JobPermissions::PERMISSION_JOB_TITLE_DELETE, $model);

        return $this->render(
            $this->handle(
                new JobTitleDeleteCommand($model)
            )
        );
    }
}
