<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Job;


use Doctrine\ORM\EntityManager;
use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Command\JobTitleEditCommand;
use Infrastructure\JobBundle\Factory\Form\JobTitleType;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class JobTitleEditController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the job title ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="JobTitle")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(string $id, Request $request)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $model = $em->getRepository(JobTitle::class)->findOneById($id);

        if (!$model) {
            $translator = $this->container->get('translator');
            throw new NotFoundHttpException($translator->trans('Job title not found.'));
        }

        $this->isGranted(JobPermissions::PERMISSION_JOB_TITLE_UPDATE, $model);

        $form = $this->createForm(JobTitleType::class, $model, [
            'method' => 'PUT',
        ]);

        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new JobTitleEditCommand($model)
            )
        );
    }
}
