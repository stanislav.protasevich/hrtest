<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Monitor;


use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractController;

class PingController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('pong', Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}