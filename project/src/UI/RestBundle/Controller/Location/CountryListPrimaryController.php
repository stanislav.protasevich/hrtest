<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Location;

use Infrastructure\LocationBundle\Command\CountryListPrimaryCommand;
use Infrastructure\LocationBundle\Rbac\LocationPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CountryListPrimaryController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LocationPermissions::PERMISSION_COUNTRY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the country array list."
     * )
     * @SWG\Tag(name="Location")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CountryListPrimaryCommand()
            )
        );
    }
}