<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Location;

use Infrastructure\LocationBundle\Command\CityListPrimaryCommand;
use Infrastructure\LocationBundle\Rbac\LocationPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CityListPrimaryController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LocationPermissions::PERMISSION_CITY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the city list array."
     * )
     * @SWG\Tag(name="Location")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CityListPrimaryCommand()
            )
        );
    }
}