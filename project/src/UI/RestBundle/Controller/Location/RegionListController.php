<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Location;

use Infrastructure\LocationBundle\Command\RegionListCommand;
use Infrastructure\LocationBundle\Rbac\LocationPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class RegionListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LocationPermissions::PERMISSION_REGION_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the region list."
     * )
     * @SWG\Parameter(
     *     name="RegionSearch[id][]",
     *     in="query",
     *     type="string",
     *     description="The region ID"
     * )
     * @SWG\Tag(name="Location")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new RegionListCommand($request->get('RegionSearch', []))
            )
        );
    }
}