<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller;


use Domain\User\Entity\User;
use League\Tactician\CommandBus;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestAbstractBusController extends AbstractBusController
{
    public function __construct(ContainerInterface $container, CommandBus $bus, CommandBus $queryBus)
    {
        if (strtolower(kernel()->getEnvironment()) !== 'dev') {
            throw new \LogicException('Controller can run only in "test" environment.');
        }

        parent::__construct($container, $bus, $queryBus);
    }

    public function init()
    {
    }

    protected function isGranted($attributes, $subject = null, ?string $message = null)
    {
    }
    
    protected function getUser(): User
    {
        $reflection = new \ReflectionClass(User::class);

        /** @var User $user */
        $user = $reflection->newInstance();

        $reflectionIdProp = $reflection->getProperty('id');
        $reflectionIdProp->setAccessible(true);
        $reflectionIdProp->setValue($user, Uuid::uuid4());

        return $user;
    }
}
