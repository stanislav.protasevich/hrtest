<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\User;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\UserBundle\Command\UserDismissalCommand;
use Infrastructure\UserBundle\Factory\Form\UserDismissalType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserDismissalController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_DISMISSAL, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        if ($user->getStatus() === User::STATUS_DISMISSAL) {
            //TODO
//            throw FormException::withMessages([
//                $this->translate('User "%name%" already dismissed.', [
//                    '%name%' => $user->getProfile()->getFullName()
//                ]),
//            ]);
        }

        $form = $this->createForm(UserDismissalType::class, $user->getEmployee());
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new UserDismissalCommand($user)
            )
        );
    }
}
