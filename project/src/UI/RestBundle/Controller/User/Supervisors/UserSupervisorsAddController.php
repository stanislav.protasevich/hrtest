<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Supervisors;


use Infrastructure\UserBundle\Command\Supervisor\UserAddSupervisorCommand;
use Infrastructure\UserBundle\Factory\Form\UserSupervisorType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserSupervisorsAddController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user supervisors."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_SUPERVISOR_ADD, $user);

        $command = new UserAddSupervisorCommand($user);

        $form = $this->createForm(UserSupervisorType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle($command)
        );
    }
}
