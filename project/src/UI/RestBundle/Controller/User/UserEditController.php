<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace UI\RestBundle\Controller\User;


use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Service\Gate;
use Infrastructure\UserBundle\Command\UserEditCommand;
use Infrastructure\UserBundle\Factory\Form\UserType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Infrastructure\UserBundle\Service\HobbyService;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserEditController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user.",
     *     @Model(type=User::class, groups={"all"})
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     description="The unique email",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The user status",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled","email_bounce","email_confirm"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     description="The user password",
     *     required=true,
     *     minimum="6",
     *     maximum="16",
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        if (Gate::cannot(UserPermissions::PERMISSION_USER_SELF_EDIT, ['model' => $user])) {
            $this->isGranted(UserPermissions::PERMISSION_USER_EDIT, $user);
        }

        /** @var HobbyService $hobbyService */
        $hobbyService = $this->get(HobbyService::class);
        $hobbyService->createHobbyFromRequest($request);

        $form = $this->createForm(UserType::class, $user);

        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new UserEditCommand($user)
            )
        );
    }
}
