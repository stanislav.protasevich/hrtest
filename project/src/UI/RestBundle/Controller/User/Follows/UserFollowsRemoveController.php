<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Follows;


use Infrastructure\UserBundle\Command\Follow\UserRemoveFollowsCommand;
use Infrastructure\UserBundle\Factory\Form\UserFollowType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserFollowsRemoveController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user followers."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_FOLLOW_REMOVE, $user);

        $command = new UserRemoveFollowsCommand($user);

        $form = $this->createForm(UserFollowType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle($command)
        );
    }
}
