<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace UI\RestBundle\Controller\User;


use Domain\User\Entity\User;
use Infrastructure\UserBundle\Command\UserCreateCommand;
use Infrastructure\UserBundle\Factory\Form\UserGeneralCreateType;
use Infrastructure\UserBundle\Factory\Form\UserType;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class UserCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the user ID."
     * )
     * @SWG\Parameter(
     *     name="email",
     *     in="body",
     *     description="The unique email",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The user status",
     *     required=false,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"active","disabled","email_bounce","email_confirm"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     description="The user password",
     *     required=true,
     *     minimum="6",
     *     maximum="16",
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $command = new UserCreateCommand();

        $form = $this->createForm(UserGeneralCreateType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                $command
            ),
            Response::HTTP_CREATED
        );
    }
}