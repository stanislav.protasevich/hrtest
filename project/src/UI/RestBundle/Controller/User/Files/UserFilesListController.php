<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Files;

use Infrastructure\UserBundle\Command\Files\UserFilesListCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserFilesListController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user files list."
     * )
     * @SWG\Parameter(
     *     name="UserFilesSearch[name]",
     *     in="query",
     *     type="string",
     *     description="The user files name."
     * )
     * @SWG\Parameter(
     *     name="UserFilesSearch[createdAt]",
     *     in="query",
     *     type="string",
     *     description="The user files created date."
     * )
     * @SWG\Parameter(
     *     name="UserFilesSearch[size]",
     *     in="query",
     *     type="string",
     *     description="The user files size."
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *          type="string",
     *          enum={"name", "createdAt", "size"}
     *     ),
     *     collectionFormat="multi",
     *     description="The user files sort by params, etc. &sort=id,name,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_FILES_LIST, $user);

        $command = new UserFilesListCommand($request->get('UserFilesSearch', []));

        $command->setUser($user);

        return $this->render(
            $this->handle($command)
        );
    }
}
