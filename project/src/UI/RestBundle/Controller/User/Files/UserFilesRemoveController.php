<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Files;

use Domain\Archive\Entity\File;
use Infrastructure\UserBundle\Command\Files\UserFilesRemoveCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class UserFilesRemoveController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $fileId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, string $fileId, Request $request)
    {
        $user = $this->findOrFail($id);

        $file = $this->getEm()->getRepository(File::class)->find($fileId);

        if (is_null($file)) {
            throw new NotFoundHttpException($this->translate('File not found.'));
        }

        $this->isGranted(UserPermissions::PERMISSION_USER_FILES_REMOVE, $file);

        return $this->render(
            $this->handle(
                new UserFilesRemoveCommand($file)
            )
        );
    }
}
