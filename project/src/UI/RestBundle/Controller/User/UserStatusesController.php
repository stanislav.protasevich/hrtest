<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace UI\RestBundle\Controller\User;

use Domain\User\Entity\User;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserStatusesController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_LIST_PRIMARY, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the users statuses array.",
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            User::statuses()
        );
    }
}