<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Alert;


use Infrastructure\UserBundle\Command\Alert\UserAlertHistoryCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserAlertHistoryController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user alert history."
     * )
     * @SWG\Parameter(
     *          name="AlertHistorySearch[user]",
     *          in="query",
     *          type="string",
     *          description="The user who create alert."
     * )
     * @SWG\Parameter(
     *          name="AlertHistorySearch[createdAt]",
     *          in="query",
     *          type="string",
     *          description="The alert created at."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_ALERT_HISTORY, $user);

        $command = new UserAlertHistoryCommand($request->get('AlertHistorySearch', []));

        $command->setAlertedUser($user);

        return $this->render(
            $this->handle($command)
        );
    }
}
