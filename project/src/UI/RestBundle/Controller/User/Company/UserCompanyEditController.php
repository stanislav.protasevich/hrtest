<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Company;


use Domain\User\Entity\User;
use Infrastructure\UserBundle\Command\Company\UserEditCompanyCommand;
use Infrastructure\UserBundle\Factory\Form\UserCompanyType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserCompanyEditController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user.",
     *     @Model(type=User::class, groups={"all"})
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_COMPANY_EDIT, $user);
        
        $form = $this->createForm(UserCompanyType::class, $user);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new UserEditCompanyCommand($user)
            )
        );
    }
}
