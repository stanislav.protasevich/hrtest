<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Performance;

use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormAddCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Helpers\UserPerformanceHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Infrastructure\UserBundle\Service\UserPerformanceService;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormVo;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class UserPerformanceFormAddController extends AbstractBusController
{
    use UserHelpersTrait;
    use UserPerformanceHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the user performance form object."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $performanceId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, string $performanceId, Request $request)
    {
        $user = $this->findOrFail($id);

        $performance = $this->findOrNotFoundPerformance($user, $performanceId);

        $author = $request->get('author');

        $this->isGranted(
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_ADD,
            compact('user', 'performance', 'author')
        );

        /* @var $performanceService UserPerformanceService */
        $performanceService = $this->container->get(UserPerformanceService::class);
        
        $model = new UserPerformanceFormVo();

        $formType = $performanceService->getFormTemplate($performance->getType(), $author);

        $form = $this->createForm($formType, $model);
        $this->processForm($request, $form);

        $model->type = $performance->getType();
        $model->employee = $user;
        $model->author = $author;
        $model->performance = $performance;

        return $this->render(
            $this->handle(
                new UserPerformanceFormAddCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}
