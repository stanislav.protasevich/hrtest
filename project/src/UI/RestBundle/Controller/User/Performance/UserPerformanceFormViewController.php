<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Performance;


use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormViewCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Helpers\UserPerformanceHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserPerformanceFormViewController extends AbstractBusController
{
    use UserHelpersTrait;
    use UserPerformanceHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user performance reviews form."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $performanceId
     * @param string $formId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, string $performanceId, string $formId, Request $request)
    {
        $user = $this->findOrFail($id);

        $performance = $this->findOrNotFoundPerformance($user, $performanceId);

        $performanceForm = $this->findOrNotFoundPerformanceForm($formId, $performance);

        $this->isGranted(
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_VIEW,
            compact('user', 'performance', 'performanceForm')
        );

        return $this->render(
            $this->handle(
                new UserPerformanceFormViewCommand($performanceForm)
            )
        );
    }
}
