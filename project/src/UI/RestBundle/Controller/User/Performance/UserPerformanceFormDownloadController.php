<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\User\Performance;

use Domain\Archive\Entity\File;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormDownloadCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Helpers\UserPerformanceHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormDownloadVo;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use UI\RestBundle\Controller\AbstractBusController;

class UserPerformanceFormDownloadController extends AbstractBusController
{
    use UserHelpersTrait;
    use UserPerformanceHelpersTrait;

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the user performance form object."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $performanceId
     * @param string $formId
     * @param Request $request
     * @return \Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse
     */
    public function __invoke(string $id, string $performanceId, string $formId, Request $request)
    {
        $user = $this->findOrFail($id);
        $performance = $this->findOrNotFoundPerformance($user, $performanceId);
        $performanceForm = $this->findOrNotFoundPerformanceForm($formId, $performance);

        $this->isGranted(
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_DOWNLOAD,
            compact('user', 'performance', 'performanceForm')
        );

        /** @var UserPerformanceFormDownloadVo $data */
        $data = $this->handle(
            new UserPerformanceFormDownloadCommand($user, $performance, $performanceForm)
        );

        $template = $data->getTemplate();
        $form = $data->getForm();
        $filename = $data->getFilename().'.pdf';

        $html = $this->renderView('@User/performance/performance.pdf.html.twig', compact('template', 'form'));

        $pdf = $this->get('knp_snappy.pdf');

        $pdf->setOption('page-size', 'A4');
        $pdf->setOption('footer-font-size', '10');
//        $pdf->setOption('orientation', 'landscape');
        $pdf->setOption('margin-bottom', '8mm');
        $pdf->setOption('margin-top', '8mm');
        $pdf->setOption('margin-right', '8mm');
        $pdf->setOption('margin-left', '8mm');

//        $pdf->setOption('header-html', $this->renderView('@User/performance/performance_header.html.twig'));
//        $pdf->setOption('footer-html', $this->renderView('@User/performance/performance_footer.html.twig'));
        $pdf->setOption(
            'footer-center',
            sprintf('generated by %s at %s', $this->getUser()->getProfile()->getFullName(), date('d.m.Y H:i'))
        );
        $pdf->setOption('footer-right', '[page]/[topage]');

//        $pdf->setOption('disable-javascript', true);
//        $pdf->setOption('no-background', true);

        return new PdfResponse(
            $pdf->getOutputFromHtml($html),
            $filename,
            File::MIME_TYPE_PDF,
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            Response::HTTP_OK,
            [
                'Access-Control-Expose-Headers' => 'Content-Disposition',
            ]
        );
    }
}
