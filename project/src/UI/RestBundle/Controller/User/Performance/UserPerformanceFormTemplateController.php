<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Performance;


use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormTemplateCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Helpers\UserPerformanceFormTemplate;
use Infrastructure\UserBundle\Helpers\UserPerformanceHelpersTrait;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserPerformanceFormTemplateController extends AbstractBusController
{
    use UserHelpersTrait;
    use UserPerformanceHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the performance form template."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $performanceId
     * @param string $name
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, string $performanceId, string $name, Request $request)
    {
        $user = $this->findOrFail($id);
        $performance = $this->findOrNotFoundPerformance(
            $user,
            $performanceId
        );

        $author = $request->get('author', null);
//
//        /* @var $performanceService UserPerformanceService */
//        $performanceService = $this->container->get(UserPerformanceService::class);
//
//        $formType = $performanceService->getFormTemplate($performance->getType(), $author);
//
//        $form = $this->createForm($formType, new UserPerformanceFormVo());

        return $this->render(
            $this->handle(
                new UserPerformanceFormTemplateCommand(
                    UserPerformanceFormTemplate::content($name, $author),
                    $user
                )
            )
        );
    }
}
