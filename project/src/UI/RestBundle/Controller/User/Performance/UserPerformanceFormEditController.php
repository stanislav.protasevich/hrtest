<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Performance;


use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormEditCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Helpers\UserPerformanceHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Infrastructure\UserBundle\Service\UserPerformanceService;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormVo;
use Infrastructure\UserBundle\ValueObject\UserPerformanceQuestionnaireFormVo;
use Infrastructure\UserBundle\ValueObject\UserPerformanceValuationFormVo;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserPerformanceFormEditController extends AbstractBusController
{
    use UserHelpersTrait;
    use UserPerformanceHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the user performance form object."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param string $performanceId
     * @param string $formId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, string $performanceId, string $formId, Request $request)
    {
        $user = $this->findOrFail($id);

        $performance = $this->findOrNotFoundPerformance($user, $performanceId);

        $performanceForm = $this->findOrNotFoundPerformanceForm($formId, $performance);

        $this->isGranted(
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_EDIT,
            compact('user', 'performance', 'performanceForm')
        );

        /* @var $performanceService UserPerformanceService */
        $performanceService = $this->container->get(UserPerformanceService::class);

        $formType = $performanceService->getFormTemplate($performanceForm->getType(), $performanceForm->getAuthor());

        $model = new UserPerformanceFormVo();

        $data = $performanceForm->getData();

        $valuation = new UserPerformanceValuationFormVo();
        $valuation->setBatchAttributes($data['valuation']);

        $model->valuation = $valuation;

        if (isset($data['questionnaire'])) {
            $questionnaire = new UserPerformanceQuestionnaireFormVo();
            $questionnaire->setBatchAttributes($data['questionnaire']);
            $model->questionnaire = $questionnaire;
        }

        $form = $this->createForm($formType, $model);
        $this->processForm($request, $form);

        $model->type = $performanceForm->getType();
        $model->employee = $performanceForm->getEmployee();
        $model->author = $performanceForm->getAuthor();
        $model->performance = $performance;
        $model->form = $performanceForm;

        return $this->render(
            $this->handle(
                new UserPerformanceFormEditCommand($model)
            )
        );
    }
}
