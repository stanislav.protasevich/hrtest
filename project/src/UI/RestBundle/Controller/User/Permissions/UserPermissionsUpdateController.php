<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\Permissions;


use Infrastructure\UserBundle\Command\Permissions\UserPermissionsUpdateCommand;
use Infrastructure\UserBundle\Factory\Form\UserPermissionsType;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class UserPermissionsUpdateController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the true."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_PERMISSIONS_UPDATE, $user);

        $command = new UserPermissionsUpdateCommand($user);

        $form = $this->createForm(UserPermissionsType::class, $command);
        $this->processForm($request, $form);
        
        return $this->render(
            $this->handle($command),
            Response::HTTP_CREATED
        );
    }
}
