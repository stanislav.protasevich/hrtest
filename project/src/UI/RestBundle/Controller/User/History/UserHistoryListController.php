<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\User\History;

use Infrastructure\UserBundle\Command\History\UserHistoryListCommand;
use Infrastructure\UserBundle\Helpers\UserHelpersTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserHistoryListController extends AbstractBusController
{
    use UserHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the followers list."
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[name]",
     *     in="query",
     *     type="string",
     *     description="The user status"
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[startDate]",
     *     in="query",
     *     type="string",
     *     description="The leaves start"
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[endDate]",
     *     in="query",
     *     type="string",
     *     description="The leaves end"
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[status]",
     *     in="query",
     *     type="string",
     *     description="The leaves end"
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[duration]",
     *     in="query",
     *     type="string",
     *     description="The leaves duration"
     * )
     * @SWG\Parameter(
     *     name="UserHistorySearch[reason]",
     *     in="query",
     *     type="string",
     *     description="The leaves reason"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"name", "timeOffType", "reason", "start", "end", "status"}
     *     ),
     *     collectionFormat="multi",
     *     description="The company sort by params, etc. &sort=id,name,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $user = $this->findOrFail($id);

        $this->isGranted(UserPermissions::PERMISSION_USER_HISTORY_LIST, $user);

        $command = new UserHistoryListCommand($request->get('UserHistorySearch', []));

        $command->setUser($user->getId());

        return $this->render(
            $this->handle($command)
        );
    }
}
