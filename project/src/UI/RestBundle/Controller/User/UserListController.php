<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace UI\RestBundle\Controller\User;

use Domain\User\Entity\User;
use Infrastructure\UserBundle\Command\UserSearchCommand;
use Infrastructure\UserBundle\Rbac\UserPermissions;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class UserListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(UserPermissions::PERMISSION_USER_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the users list.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class, groups={"all"}))
     *     )
     * )
     * @SWG\Parameter(
     *          name="UserSearch[status]",
     *          in="query",
     *          type="string",
     *          description="The user status"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[id]",
     *          in="query",
     *          type="string",
     *          description="The user ID"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[name]",
     *          in="query",
     *          type="string",
     *          description="The user first name, middle name, surname"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[city]",
     *          in="query",
     *          type="string",
     *          description="The user city ID"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[company]",
     *          in="query",
     *          type="string",
     *          description="The user company ID"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[jobTitle]",
     *          in="query",
     *          type="string",
     *          description="The user job title ID"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[atWork]",
     *          in="query",
     *          type="string",
     *          description="The user atWork period"
     * )
     * @SWG\Parameter(
     *          name="UserSearch[birthday]",
     *          in="query",
     *          type="string",
     *          description="The user birthday period"
     * )
     * @SWG\Parameter(
     *          name="sort",
     *          in="query",
     *          type="array",
     *          @SWG\Items(
     *             type="string",
     *             enum={"status", "id", "name", "city", "company", "jobTitle"}
     *          ),
     *          collectionFormat="multi",
     *          description="The user sort by params, etc. &sort=id,name,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="User")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new UserSearchCommand($request->get('UserSearch', []))
            )
        );
    }
}
