<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Leaves;

use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\LeavesBundle\Command\LeavesUserStatsCommand;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;


class LeavesUserStatsTimeOffTypeController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leaves stats by user."
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param string $typeOffTypeId
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(string $typeOffTypeId, Request $request)
    {
        $model = $this->getEm()->getRepository(TimeOffType::class)->findOneById($typeOffTypeId);

        if (!$model) {
            throw new NotFoundHttpException($this->translate('Time off type not found.'));
        }

        return $this->render(
            $this->handle(
                new LeavesUserStatsCommand($this->getUser(), $model)
            )
        );
    }
}