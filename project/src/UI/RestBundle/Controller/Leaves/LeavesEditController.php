<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Leaves;

use Infrastructure\LeavesBundle\Command\LeavesRequestEditCommand;
use Infrastructure\LeavesBundle\Factory\Form\LeaveRequestEditType;
use Infrastructure\LeavesBundle\Helpers\LeavesHelpersTrait;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesEditController extends AbstractBusController
{
    use LeavesHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leave."
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param string $id
     * @param Request $request
     * @return Response
     */
    public function __invoke(string $id, Request $request)
    {
        $leaveRequest = $this->findOrNotFoundUser($id);

        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_EDIT, $leaveRequest);

        $oldLeaveRequest = clone $leaveRequest;

        $form = $this->createForm(LeaveRequestEditType::class, $leaveRequest, [
            'isAdministrator' => Gate::can(RbacPermissions::ROLE_ADMINISTRATOR, ['model' => $leaveRequest]),
        ]);
        $form->remove('timeOffType');
        $this->processForm($request, $form);

        $command = new LeavesRequestEditCommand(
            $leaveRequest->getUser(),
            $leaveRequest->getTimeOffType(),
            $leaveRequest->getStartDate(),
            $leaveRequest->getEndDate()
        );

        $command->setLeaveRequest($leaveRequest);
        $command->setOldLeaveRequest($oldLeaveRequest);
        $command->setSubstituteUser($leaveRequest->getSubstituteUser());
        $command->setReason($leaveRequest->getReason());

        return $this->render(
            $this->handle($command)
        );
    }
}
