<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Leaves;


use Domain\Leaves\Entity\LeaveRequests;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesRequestStatusesController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leaves statuses list."
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            LeaveRequests::statuses()
        );
    }
}