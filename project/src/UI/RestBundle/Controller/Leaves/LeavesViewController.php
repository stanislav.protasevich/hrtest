<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Leaves;


use Infrastructure\LeavesBundle\Command\LeavesRequestViewCommand;
use Infrastructure\LeavesBundle\Helpers\LeavesHelpersTrait;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesViewController extends AbstractBusController
{
    use LeavesHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leaves list."
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $leaveRequest = $this->findOrNotFoundUser($id);

        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_VIEW, $leaveRequest);

        return $this->render(
            $this->handle(
                new LeavesRequestViewCommand($leaveRequest)
            )
        );
    }
}