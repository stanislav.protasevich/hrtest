<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Leaves;

use Domain\Leaves\Entity\LeaveRequests;
use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Infrastructure\LeavesBundle\Factory\Form\LeaveRequestType;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leave id."
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param Request $request
     * @return Response
     *
     */
    public function __invoke(Request $request)
    {
        $leave = new LeaveRequests();
        $leave->setStatus(LeaveRequests::STATUS_PENDING);

        $form = $this->createForm(LeaveRequestType::class, $leave);

        $this->processForm($request, $form);

        $isAppointed = $leave->getTarget() === LeaveRequests::TARGET_APPOINTED;

        if ($isAppointed) {
            $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_CREATE_APPOINTED, $leave);
        }

        $command = new LeavesRequestCommand(
            $isAppointed ? $leave->getUser() : $this->getUser(),
            $leave->getTimeOffType(),
            $leave->getStartDate(),
            $leave->getEndDate()
        );

        $command->setTarget($leave->getTarget());
        $command->setSubstituteUser($leave->getSubstituteUser());
        $command->setReason($leave->getReason());

        return $this->render(
            $this->handle(
                $command
            ),
            Response::HTTP_CREATED
        );
    }
}