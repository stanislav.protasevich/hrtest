<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\Leaves;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Infrastructure\LeavesBundle\Command\LeaveSupervisorVoteCommand;
use Infrastructure\LeavesBundle\Factory\Form\LeaveRequestSupervisorType;
use Infrastructure\LeavesBundle\Helpers\LeavesHelpersTrait;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesSupervisorVoteController extends AbstractBusController
{
    use LeavesHelpersTrait;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns bool."
     * )
     * @SWG\Parameter(
     *     name="status",
     *     in="body",
     *     description="The vote status",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"approved", "denied"}
     *     )
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function __invoke(string $id, Request $request)
    {
        $leaveRequest = $this->findOrNotFoundUser($id);
        if ($leaveRequest->getStatus() !== LeaveRequests::STATUS_PENDING) {
            throw new BadRequestHttpException($this->translate('Leave request all ready closed.'));
        }

        $request->request->set('leaveRequests', $id);

        $leaveVote = $this->getEm()->getRepository(LeaveRequestSupervisor::class)->findOneBy([
            'user' => $this->getUser(),
            'leaveRequests' => $leaveRequest,
        ]);

        if (!Gate::can(RbacPermissions::ROLE_ADMINISTRATOR)) {

            if (!$leaveVote) {
                $this->throwAccessDeniedException(LeavesPermissions::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE, $leaveRequest);
            }

            $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE, $leaveVote);
        }

        if (!$leaveVote && Gate::can(LeavesPermissions::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE)) {
            $leaveVote = new LeaveRequestSupervisor();
            $leaveVote->setUser($this->getUser());
            $leaveVote->setLeaveRequests($leaveRequest);
            $leaveVote->setCreatedAt(new \DateTime());
            $leaveVote->setUpdatedAt(new \DateTime());
        }

        if (!$leaveVote) {
            throw new NotFoundHttpException($this->translate('You can not vote for this leave request.'));
        }

        $form = $this->createForm(LeaveRequestSupervisorType::class, $leaveVote);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new LeaveSupervisorVoteCommand($leaveVote)
            )
        );
    }
}
