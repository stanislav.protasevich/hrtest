<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Leaves;


use Infrastructure\LeavesBundle\Command\LeavesListCommand;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class LeavesListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(LeavesPermissions::PERMISSION_LEAVES_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns leaves list."
     * )
     * @SWG\Parameter(
     *     name="LeavesSearch[id]",
     *     in="query",
     *     type="string",
     *     description="The leaves ID"
     * )
     * @SWG\Parameter(
     *     name="LeavesSearch[timeOffType]",
     *     in="query",
     *     type="string",
     *     description="The leaves time off type"
     * )
     * @SWG\Parameter(
     *     name="LeavesSearch[startDate]",
     *     in="query",
     *     type="string",
     *     description="The leaves start"
     * )
     * @SWG\Parameter(
     *     name="LeavesSearch[endDate]",
     *     in="query",
     *     type="string",
     *     description="The leaves end"
     * )
     * @SWG\Parameter(
     *     name="LeavesSearch[status]",
     *     in="query",
     *     type="string",
     *     description="The leaves end"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"name", "timeOffType", "reason", "startDate", "end", "status"}
     *     ),
     *     collectionFormat="multi",
     *     description="The company sort by params, etc. &sort=id,name,-status"
     * )
     * @SWG\Tag(name="Leaves")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function __invoke(Request $request)
    {
        $command = new LeavesListCommand(
            $request->get('LeavesSearch', [])
        );

        return $this->render(
            $this->handle($command)
        );
    }
}