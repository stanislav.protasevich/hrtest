<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\TimeOff;


use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeDeleteCommand;
use Infrastructure\TimeOffBundle\Rbac\TimeOffTypePermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

class TimeOffTypeDeleteController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the time off type."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $model = $this->getEm()->getRepository(TimeOffType::class)->find($id);

        if (!$model) {
            throw new NotFoundHttpException($this->translate('Time off type not found.'));
        }

        $this->isGranted(TimeOffTypePermissions::PERMISSION_TIME_OFF_TYPE_DELETE, $model);

        return $this->render(
            $this->handle(
                new TimeOffTypeDeleteCommand($model)
            )
        );
    }
}
