<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;


use Domain\TimeOff\Entity\Policy;
use Infrastructure\TimeOffBundle\Command\PolicyCreateCommand;
use Infrastructure\TimeOffBundle\Factory\Form\PolicyType;
use Infrastructure\TimeOffBundle\Rbac\PolicyPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class PolicyCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(PolicyPermissions::PERMISSION_POLICY_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the policy."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $policy = new Policy();
        $form = $this->createForm(PolicyType::class, $policy);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new PolicyCreateCommand($policy)
            ),
            Response::HTTP_CREATED
        );
    }
}