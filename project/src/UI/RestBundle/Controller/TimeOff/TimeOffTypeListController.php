<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;


use Infrastructure\TimeOffBundle\Command\TimeOffTypeSearchCommand;
use Infrastructure\TimeOffBundle\Rbac\TimeOffTypePermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class TimeOffTypeListController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(TimeOffTypePermissions::PERMISSION_TIME_OFF_TYPE_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the time off type."
     * )
     * @SWG\Parameter(
     *     name="TimeOffTypesSearch[id]",
     *     in="query",
     *     type="string",
     *     description="The time off type id."
     * )
     * @SWG\Parameter(
     *     name="TimeOffTypesSearch[name]",
     *     in="query",
     *     type="string",
     *     description="The time off type name."
     * )
     * @SWG\Parameter(
     *     name="TimeOffTypesSearch[description]",
     *     in="query",
     *     type="string",
     *     description="The description of type off list."
     * )
     * @SWG\Parameter(
     *     name="TimeOffTypesSearch[status]",
     *     in="query",
     *     type="string",
     *     description="The status."
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="array",
     *     @SWG\Items(
     *         type="string",
     *         enum={"status", "id", "name", "description", "paid", "color"}
     *     ),
     *     collectionFormat="multi",
     *     description="The user sort by params, etc. &sort=id,name,-status"
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new TimeOffTypeSearchCommand($request->get('TimeOffTypesSearch', []))
            )
        );
    }
}