<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;


use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeCreateCommand;
use Infrastructure\TimeOffBundle\Factory\Form\TimeOffTypeInput;
use Infrastructure\TimeOffBundle\Rbac\TimeOffTypePermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class TimeOffTypeCreateController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(TimeOffTypePermissions::PERMISSION_TIME_OFF_TYPE_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the time off type."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="trackTime",
     *     in="body",
     *     description="How track time",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"days","hours"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="showInCalendar",
     *     in="body",
     *     description="Show in calendar",
     *     required=false,
     *     @SWG\Schema(
     *       type="boolean"
     *     )
     * )
     * @SWG\Parameter(
     *     name="calendarColor",
     *     in="body",
     *     description="Color on calendar in HEX",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="calendarColor",
     *     in="body",
     *     description="The description",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="paid",
     *     in="body",
     *     description="Is paid",
     *     required=false,
     *     @SWG\Schema(
     *       type="boolean"
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $model = new TimeOffType();
        $form = $this->createForm(TimeOffTypeInput::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new TimeOffTypeCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}