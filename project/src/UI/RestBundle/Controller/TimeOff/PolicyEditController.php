<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;


use Infrastructure\TimeOffBundle\Command\PolicyEditCommand;
use Infrastructure\TimeOffBundle\Factory\Form\PolicyType;
use Infrastructure\TimeOffBundle\Helpers\PolicyHelpersTrait;
use Infrastructure\TimeOffBundle\Rbac\PolicyPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class PolicyEditController extends AbstractBusController
{
    use PolicyHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the policy."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $policy = $this->findOrFail($id);

        $this->isGranted(PolicyPermissions::PERMISSION_POLICY_UPDATE, $policy);

        $form = $this->createForm(PolicyType::class, $policy);
        $form->add('timeOffType', null, ['disabled' => true]);
        $form->add('type', null, ['disabled' => true]);

        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new PolicyEditCommand($policy)
            )
        );
    }
}
