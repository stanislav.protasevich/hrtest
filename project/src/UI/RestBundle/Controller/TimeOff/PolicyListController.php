<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;


use Infrastructure\TimeOffBundle\Command\PolicySearchCommand;
use Infrastructure\TimeOffBundle\Rbac\PolicyPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class PolicyListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(PolicyPermissions::PERMISSION_POLICY_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the policy."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new PolicySearchCommand($request->get('PolicySearch', []))
            )
        );
    }
}