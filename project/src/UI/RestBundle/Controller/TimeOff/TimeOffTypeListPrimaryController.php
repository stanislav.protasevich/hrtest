<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace UI\RestBundle\Controller\TimeOff;

use Infrastructure\TimeOffBundle\Command\TimeOffTypePrimaryCommand;
use Infrastructure\TimeOffBundle\Rbac\TimeOffTypePermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class TimeOffTypeListPrimaryController extends AbstractBusController
{
    public function init()
    {
        $this->isGranted(TimeOffTypePermissions::PERMISSION_TIME_OFF_TYPE_LIST_PRIMARY, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the time off type array."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="TimeOff")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new TimeOffTypePrimaryCommand()
            )
        );
    }
}