<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Calendar;


use Infrastructure\CalendarBundle\Command\CalendarListCommand;
use Infrastructure\CalendarBundle\Rbac\CalendarPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CalendarListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CalendarPermissions::PERMISSION_CALENDAR_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the calendar list."
     * )
     * @SWG\Parameter(
     *     name="CalendarSearch[date]",
     *     in="query",
     *     type="string",
     *     description="The calendar date."
     * )
     * @SWG\Parameter(
     *     name="CalendarSearch[text]",
     *     in="query",
     *     type="string",
     *     description="The calendar search by title and description."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Calendar")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new CalendarListCommand($request->get('CalendarSearch', []))
            )
        );
    }
}
