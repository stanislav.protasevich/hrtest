<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Calendar;

use Infrastructure\CalendarBundle\Command\CalendarDeleteCommand;
use Infrastructure\CalendarBundle\Helpers\CalendarHelpersTrait;
use Infrastructure\CalendarBundle\Rbac\CalendarPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CalendarDeleteController extends AbstractBusController
{
    use CalendarHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the bool."
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Calendar")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $calendar = $this->findOrFail($id);

        $this->isGranted(CalendarPermissions::PERMISSION_CALENDAR_DELETE, $calendar);

        return $this->render(
            $this->handle(
                new CalendarDeleteCommand($calendar)
            )
        );
    }
}
