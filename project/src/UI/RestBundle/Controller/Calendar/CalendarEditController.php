<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Calendar;

use Infrastructure\CalendarBundle\Command\CalendarEditCommand;
use Infrastructure\CalendarBundle\Factory\Form\CalendarType;
use Infrastructure\CalendarBundle\Helpers\CalendarHelpersTrait;
use Infrastructure\CalendarBundle\Rbac\CalendarPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class CalendarEditController extends AbstractBusController
{
    use CalendarHelpersTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the calendar view."
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The calendar date title",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="description",
     *     in="body",
     *     description="The calendar date description",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="repeat",
     *     in="body",
     *     description="The calendar date repeat",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"never", "everyday", "weekly", "quarterly", "monthly", "yearly"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Calendar")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(string $id, Request $request)
    {
        $calendar = $this->findOrFail($id);

        $this->isGranted(CalendarPermissions::PERMISSION_CALENDAR_EDIT, $calendar);

        $form = $this->createForm(CalendarType::class, $calendar);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new CalendarEditCommand($calendar)
            )
        );
    }
}
