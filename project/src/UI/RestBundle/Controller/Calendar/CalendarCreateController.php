<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Calendar;


use Domain\Calendar\Entity\Calendar;
use Infrastructure\CalendarBundle\Command\CalendarCreateCommand;
use Infrastructure\CalendarBundle\Factory\Form\CalendarType;
use Infrastructure\CalendarBundle\Rbac\CalendarPermissions;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;

class CalendarCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(CalendarPermissions::PERMISSION_CALENDAR_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the company ID."
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="body",
     *     description="The calendar date title",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="description",
     *     in="body",
     *     description="The calendar date description",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="repeat",
     *     in="body",
     *     description="The calendar date repeat",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"never", "everyday", "weekly", "quarterly", "monthly", "yearly"}
     *     )
     * )
     * @Security(name="Bearer")
     * @SWG\Tag(name="Calendar")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $model = new Calendar();

        $form = $this->createForm(CalendarType::class, $model);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new CalendarCreateCommand($model)
            ),
            Response::HTTP_CREATED
        );
    }
}
