<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Oauth;

use Infrastructure\OauthBundle\Command\Oauth2DeactivateCommand;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class Oauth2DeactivateController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns true"
     * )
     * @SWG\Parameter(
     *     name="provider",
     *     in="query",
     *     type="string",
     *     description="The oauth2 provider name",
     *     required=true
     * )
     * @SWG\Tag(name="Oauth")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $provider = $data['provider'] ?? null;

        $command = new Oauth2DeactivateCommand($provider, null, null);
        $command->userAgent = $request->server->get('HTTP_USER_AGENT');
        $command->ip = $request->getClientIp();
        
        return $this->render(
            $this->handle($command)
        );
    }
}