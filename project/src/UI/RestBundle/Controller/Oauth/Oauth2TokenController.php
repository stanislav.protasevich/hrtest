<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace UI\RestBundle\Controller\Oauth;


use Infrastructure\OauthBundle\Command\Oauth2TokenCommand;
use Infrastructure\OauthBundle\Factory\Form\Oauth2TokenType;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class Oauth2TokenController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns access token, refresh_token, user info"
     * )
     * @SWG\Parameter(
     *     name="grantType",
     *     in="body",
     *     description="The grant type",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *       enum={"password","refresh_token"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="clientId",
     *     in="body",
     *     description="The client ID",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *     )
     * )
     * @SWG\Parameter(
     *     name="clientSecret",
     *     in="body",
     *     description="The client secret",
     *     required=true,
     *     @SWG\Schema(
     *       type="string",
     *     )
     * )
     * @SWG\Parameter(
     *     name="username",
     *     in="body",
     *     description="The email",
     *     required=false,
     *     @SWG\Schema(
     *       type="email",
     *       example="test@email.com"
     *     )
     * )
     * @SWG\Parameter(
     *     name="password",
     *     in="body",
     *     description="The password",
     *     required=false,
     *     @SWG\Schema(
     *       type="password",
     *       example="secret",
     *       minLength=6,
     *       maxLength=16
     *     )
     * )
     * @SWG\Parameter(
     *     name="refresh_token",
     *     in="body",
     *     description="The refresh token",
     *     required=false,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Tag(name="Auth")
     *
     * @RateLimit(limit=10, period=360)
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $command = new Oauth2TokenCommand();
        $form = $this->createForm(Oauth2TokenType::class, $command);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                $command
            )
        );
    }
}