<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Oauth;

use Infrastructure\OauthBundle\Command\Oauth2Command;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;

class Oauth2LoginController extends AbstractBusController
{
    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns access token and user info"
     * )
     *
     * @SWG\Parameter(
     *     name="provider",
     *     in="query",
     *     type="string",
     *     description="The oauth2 provider name",
     *     required=true
     * )
     *
     * @SWG\Parameter(
     *     name="code",
     *     in="query",
     *     type="string",
     *     description="The access token from oauth2 server"
     * )
     *
     * @SWG\Parameter(
     *     name="state",
     *     in="query",
     *     type="string",
     *     description="The security state"
     * )
     * @SWG\Tag(name="Oauth")
     * @RateLimit(limit=10, period=360)
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        $provider = $request->get('provider') ?? null;
        $code = $request->get('code') ?? null;
        $state = $request->get('state') ?? null;

        $command = new Oauth2Command($provider, $code, $state);
        $command->userAgent = $request->server->get('HTTP_USER_AGENT');
        $command->ip = $request->getClientIp();

        return $this->render(
            $this->handle($command)
        );
    }
}