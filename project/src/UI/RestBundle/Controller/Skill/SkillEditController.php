<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Skill;


use Domain\User\Entity\Skill;
use Infrastructure\UserBundle\Command\Skill\SkillEditCommand;
use Infrastructure\UserBundle\Factory\Form\SkillType;
use Infrastructure\UserBundle\Rbac\SkillPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class SkillEditController extends AbstractBusController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the skill ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="sortOrder",
     *     in="body",
     *     description="The sort order",
     *     required=false,
     *     @SWG\Schema(
     *       type="integer"
     *     )
     * )
     * @SWG\Tag(name="Skill")
     *
     * @param string $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(string $id, Request $request)
    {
        $skill = $this->getEm()->getRepository(Skill::class)->findOneById($id);

        if (is_null($skill)) {
            throw new NotFoundHttpException($this->translate('Skill not found.'));
        }

        $this->isGranted(SkillPermissions::PERMISSION_SKILL_EDIT, $skill);

        $form = $this->createForm(SkillType::class, $skill, [
            'method' => 'PUT',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new SkillEditCommand($skill)
            )
        );
    }
}