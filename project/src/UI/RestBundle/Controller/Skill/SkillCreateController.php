<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Skill;


use Domain\User\Entity\Skill;
use Infrastructure\UserBundle\Command\Skill\SkillCreateCommand;
use Infrastructure\UserBundle\Factory\Form\SkillType;
use Infrastructure\UserBundle\Rbac\SkillPermissions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class SkillCreateController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(SkillPermissions::PERMISSION_SKILL_CREATE, self::class);
    }

    /**
     * @SWG\Response(
     *     response=201,
     *     description="Returns the skill ID."
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="body",
     *     description="The unique name",
     *     required=true,
     *     @SWG\Schema(
     *       type="string"
     *     )
     * )
     * @SWG\Parameter(
     *     name="sortOrder",
     *     in="body",
     *     description="The sort order",
     *     required=false,
     *     @SWG\Schema(
     *       type="integer"
     *     )
     * )
     * @SWG\Tag(name="Skill")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function __invoke(Request $request)
    {
        $skill = new Skill();
        $form = $this->createForm(SkillType::class, $skill, [
            'method' => 'POST',
        ]);
        $this->processForm($request, $form);

        return $this->render(
            $this->handle(
                new SkillCreateCommand($skill)
            ),
            Response::HTTP_CREATED
        );
    }
}