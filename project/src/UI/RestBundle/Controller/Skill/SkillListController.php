<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace UI\RestBundle\Controller\Skill;


use Infrastructure\UserBundle\Command\Skill\SkillListCommand;
use Infrastructure\UserBundle\Rbac\SkillPermissions;
use Symfony\Component\HttpFoundation\Request;
use UI\RestBundle\Controller\AbstractBusController;
use Swagger\Annotations as SWG;

class SkillListController extends AbstractBusController
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->isGranted(SkillPermissions::PERMISSION_SKILL_LIST, self::class);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the skills list."
     * )
     * @SWG\Tag(name="Skill")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(Request $request)
    {
        return $this->render(
            $this->handle(
                new SkillListCommand()
            )
        );
    }
}