<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog;


use Domain\Blog\Resource\BlogPostResource;
use Infrastructure\BlogBundle\Command\BlogPostViewCommand;

class BlogPostViewHandler implements BlogPostViewHandlerInterface
{
    public function __invoke(BlogPostViewCommand $command)
    {
        return new BlogPostResource(
            $command->getBlogPost()
        );
    }
}
