<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Category;


use Domain\Blog\Resource\BlogCategoryListResource;
use Infrastructure\BlogBundle\Command\Category\BlogCategoryListCommand;
use Infrastructure\BlogBundle\Service\BlogCategorySearchService;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlogCategoryListHandler implements BlogCategoryListHandlerInterface
{
    /**
     * @var BlogCategorySearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, BlogCategorySearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(BlogCategoryListCommand $command)
    {
        $qb = $this->service->search($command);
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_blog_category_list');

        return BlogCategoryListResource::collection($paginated);
    }
}
