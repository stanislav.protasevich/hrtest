<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Category;


use Domain\Blog\Resource\BlogCategoryResource;
use Infrastructure\BlogBundle\Command\Category\BlogCategoryEditCommand;
use Infrastructure\BlogBundle\Event\BlogCategoryEvent;
use Infrastructure\BlogBundle\Service\BlogCategoryService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogCategoryEditHandler implements BlogCategoryEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var BlogCategoryService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, BlogCategoryService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogCategoryEditCommand $command)
    {
        $this->dispatcher->dispatch(BlogCategoryEvent::EVENT_BEFORE_INSERT);

        $category = $this->service->edit($command->getCategory());

        $this->dispatcher->dispatch(BlogCategoryEvent::EVENT_AFTER_INSERT,
            new BlogCategoryEvent($category)
        );

        return new BlogCategoryResource($category);
    }
}
