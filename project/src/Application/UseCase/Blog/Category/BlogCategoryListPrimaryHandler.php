<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Category;


use Infrastructure\BlogBundle\Command\Category\BlogCategoryListPrimaryCommand;
use Infrastructure\BlogBundle\Service\BlogCategoryService;

class BlogCategoryListPrimaryHandler implements BlogCategoryListPrimaryHandlerInterface
{
    /**
     * @var BlogCategoryService
     */
    private $service;

    public function __construct(BlogCategoryService $service)
    {
        $this->service = $service;
    }

    public function __invoke(BlogCategoryListPrimaryCommand $command)
    {
        return $this->service->getPrimary();
    }
}
