<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Category;


use Domain\Blog\Resource\BlogCategoryResource;
use Infrastructure\BlogBundle\Command\Category\BlogCategoryViewCommand;

class BlogCategoryViewHandler implements BlogCategoryViewHandlerInterface
{
    public function __invoke(BlogCategoryViewCommand $command)
    {
        return new BlogCategoryResource(
            $command->getCategory()
        );
    }
}
