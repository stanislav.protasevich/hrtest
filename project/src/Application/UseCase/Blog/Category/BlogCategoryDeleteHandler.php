<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Category;


use Infrastructure\BlogBundle\Command\Category\BlogCategoryDeleteCommand;
use Infrastructure\BlogBundle\Event\BlogCategoryEvent;
use Infrastructure\BlogBundle\Service\BlogCategoryService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogCategoryDeleteHandler implements BlogCategoryDeleteHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var BlogCategoryService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, BlogCategoryService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogCategoryDeleteCommand $command)
    {
        $this->dispatcher->dispatch(BlogCategoryEvent::EVENT_BEFORE_DELETE);

        $category = $command->getCategory();

        $this->service->delete($category);

        $this->dispatcher->dispatch(BlogCategoryEvent::EVENT_AFTER_DELETE,
            new BlogCategoryEvent($category)
        );

        return true;
    }
}
