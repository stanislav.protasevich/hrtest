<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog;


use Domain\Blog\Resource\BlogPostResource;
use Infrastructure\BlogBundle\Command\BlogPostEditCommand;
use Infrastructure\BlogBundle\Event\BlogPostEvent;
use Infrastructure\BlogBundle\Service\BlogService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostEditHandler implements BlogPostEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var BlogService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, BlogService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogPostEditCommand $command)
    {
        $this->dispatcher->dispatch(BlogPostEvent::EVENT_BEFORE_UPDATE);

        $blogPost = $this->service->edit($command->getBlogPost());

        $this->dispatcher->dispatch(BlogPostEvent::EVENT_AFTER_UPDATE,
            new BlogPostEvent($blogPost)
        );

        return new BlogPostResource($blogPost);
    }
}
