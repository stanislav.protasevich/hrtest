<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog;


use Infrastructure\BlogBundle\Command\BlogPostCreateCommand;
use Infrastructure\BlogBundle\Event\BlogPostEvent;
use Infrastructure\BlogBundle\Service\BlogService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostCreateHandler implements BlogPostCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var BlogService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, BlogService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogPostCreateCommand $command)
    {
        $this->dispatcher->dispatch(BlogPostEvent::EVENT_BEFORE_INSERT);

        $blogPost = $this->service->create($command->getBlogPost());

        $this->dispatcher->dispatch(BlogPostEvent::EVENT_AFTER_INSERT,
            new BlogPostEvent($blogPost)
        );

        return [
            'id' => $blogPost->getId()
        ];
    }
}
