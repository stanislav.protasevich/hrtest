<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog;


use Domain\Blog\Resource\BlogPostListResource;
use Infrastructure\BlogBundle\Command\BlogPostListCommand;
use Infrastructure\BlogBundle\Service\BlogSearchService;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlogPostListHandler
{
    /**
     * @var BlogSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, BlogSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(BlogPostListCommand $command)
    {
        $qb = $this->service->search($command);
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_blog_list');

        return BlogPostListResource::collection($paginated);
    }
}
