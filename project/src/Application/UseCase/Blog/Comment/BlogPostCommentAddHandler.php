<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Comment;


use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentAddCommand;
use Infrastructure\BlogBundle\Event\BlogPostCommentEvent;
use Infrastructure\BlogBundle\Service\BlogCommentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostCommentAddHandler implements BlogPostCommentAddHandlerInterface
{
    /**
     * @var BlogCommentService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, BlogCommentService $service)
    {
        $this->service = $service;
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogPostCommentAddCommand $command)
    {
        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_BEFORE_INSERT);

        $comment = $this->service->add($command->getComment());

        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_AFTER_INSERT,
            new BlogPostCommentEvent($comment)
        );

        return [
            'id' => $comment->getId()
        ];
    }
}
