<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Comment;


use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentRemoveCommand;
use Infrastructure\BlogBundle\Event\BlogPostCommentEvent;
use Infrastructure\BlogBundle\Service\BlogCommentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostCommentRemoveHandler implements BlogPostCommentRemoveHandlerInterface
{
    /**
     * @var BlogCommentService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, BlogCommentService $service)
    {
        $this->service = $service;
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogPostCommentRemoveCommand $command)
    {
        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_BEFORE_DELETE);

        $comment = $command->getComment();

        $this->service->delete($comment);

        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_AFTER_DELETE,
            new BlogPostCommentEvent($comment)
        );

        return true;
    }
}
