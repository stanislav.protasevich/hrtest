<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Comment;


use Domain\Blog\Resource\BlogPostCommentResource;
use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentEditCommand;
use Infrastructure\BlogBundle\Event\BlogPostCommentEvent;
use Infrastructure\BlogBundle\Service\BlogCommentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BlogPostCommentEditHandler implements BlogPostCommentEditHandlerInterface
{
    /**
     * @var BlogCommentService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, BlogCommentService $service)
    {
        $this->service = $service;
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(BlogPostCommentEditCommand $command)
    {
        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_BEFORE_UPDATE);

        $comment = $this->service->edit($command->getComment());

        $this->dispatcher->dispatch(BlogPostCommentEvent::EVENT_AFTER_UPDATE,
            new BlogPostCommentEvent($comment)
        );

        return new BlogPostCommentResource($comment);
    }
}
