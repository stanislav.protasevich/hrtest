<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Comment;


use Domain\Blog\Resource\BlogPostCommentResource;
use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentPostListCommand;
use Infrastructure\BlogBundle\Service\BlogCommentSearchService;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BlogPostCommentPostListHandler implements BlogPostCommentPostListHandlerInterface
{
    /**
     * @var BlogCommentSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, BlogCommentSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(BlogPostCommentPostListCommand $command)
    {
        $qb = $this->service->search($command);
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_blog_list');

        return BlogPostCommentResource::collection($paginated);
    }
}
