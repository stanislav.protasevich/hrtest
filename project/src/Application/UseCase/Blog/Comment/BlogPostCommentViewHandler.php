<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Blog\Comment;


use Domain\Blog\Resource\BlogPostCommentResource;
use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentViewCommand;

class BlogPostCommentViewHandler implements BlogPostCommentViewHandlerInterface
{
    /**
     * @param BlogPostCommentViewCommand $command
     * @return BlogPostCommentResource
     */
    public function __invoke(BlogPostCommentViewCommand $command)
    {
        return new BlogPostCommentResource(
            $command->getComment()
        );
    }
}
