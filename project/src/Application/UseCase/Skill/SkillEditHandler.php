<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Skill;


use Infrastructure\UserBundle\Command\Skill\SkillEditCommand;
use Infrastructure\UserBundle\Service\SkillService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SkillEditHandler implements SkillEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var SkillService
     */
    private $service;

    public function __construct(ContainerInterface $container, SkillService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param SkillEditCommand $command
     * @return \Domain\User\Entity\Skill
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(SkillEditCommand $command)
    {
        return $this->service->edit($command);
    }
}