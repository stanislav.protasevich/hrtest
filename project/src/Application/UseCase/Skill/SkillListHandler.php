<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Skill;


use Domain\User\Resource\SkillResource;
use Infrastructure\UserBundle\Command\Skill\SkillListCommand;
use Infrastructure\UserBundle\Service\SkillService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SkillListHandler implements SkillListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var SkillService
     */
    private $service;

    public function __construct(ContainerInterface $container, SkillService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param SkillListCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource|void
     */
    public function __invoke(SkillListCommand $command)
    {
        return SkillResource::collection($this->service->getAll());
    }
}