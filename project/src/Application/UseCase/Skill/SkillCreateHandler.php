<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Skill;


use Infrastructure\UserBundle\Command\Skill\SkillCreateCommand;
use Infrastructure\UserBundle\Service\SkillService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SkillCreateHandler implements SkillCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var SkillService
     */
    private $service;

    public function __construct(ContainerInterface $container, SkillService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param SkillCreateCommand $command
     * @return \Domain\User\Entity\Skill
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(SkillCreateCommand $command)
    {
        return $this->service->create($command);
    }
}