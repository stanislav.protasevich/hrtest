<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Oauth2;

use Doctrine\ORM\EntityManager;
use Domain\Auth\Resource\LoginResource;
use Domain\User\Entity\ConnectedAccountProvider;
use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Service\JwtTokenService;
use Infrastructure\OauthBundle\Client\OAuth2;
use Infrastructure\OauthBundle\Command\Oauth2Command;
use Infrastructure\OauthBundle\Event\Oauth2Event;
use Infrastructure\OauthBundle\Service\Oauth2Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Oauth2Handler implements Oauth2HandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var JwtTokenService
     */
    private $tokenService;
    /**
     * @var Oauth2Service
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        JwtTokenService $tokenService,
        Oauth2Service $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->tokenService = $tokenService;
        $this->service = $service;
    }

    /**
     * @param Oauth2Command $command
     * @return string|array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function __invoke(Oauth2Command $command)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        /* @var $provider ConnectedAccountProvider */
        $provider = $em->getRepository(ConnectedAccountProvider::class)->findOneByName($command->getProvider());

        /* @var $client OAuth2 */
        $client = $this->container->get('oath2_client_' . $provider->getName());
        $client->setProvider($provider);

        if ($command->getCode()) {

            $this->dispatcher->dispatch(Oauth2Event::EVENT_BEFORE_TOKEN, new Oauth2Event([
                'provider' => $provider,
                'client' => $client,
            ]));

            $token = $client->fetchAccessToken($command->getCode(), $command->getState());

            $this->dispatcher->dispatch(Oauth2Event::EVENT_AFTER_TOKEN, new Oauth2Event([
                'provider' => $provider,
                'client' => $client,
                'token' => $token,
            ]));

            $client->setToken($token);

            if (!empty($token)) {

                $this->dispatcher->dispatch(Oauth2Event::EVENT_BEFORE_LOGIN, new Oauth2Event([
                    'provider' => $provider,
                    'client' => $client,
                    'token' => $token,
                ]));

                /* @var $tokenStorage TokenStorage */
                $tokenStorage = $this->container->get('security.token_storage');
                /* @var $user User */
                $user = $tokenStorage->getToken() ? $tokenStorage->getToken()->getUser() : null;
                if (!$user instanceof User) {
                    $info = $client->getUserAttributes();

                    $emails = array_filter($info['emails'], function ($item) {
                        return $item['type'] === 'account';
                    });

                    if (empty($emails)) {
                        throw new BadRequestHttpException('Can not check credentials for this account.');
                    }

                    $user = $em->getRepository(User::class)->loadByEmail($emails[0]['value']);
                    if (!$user) {
                        throw new BadRequestHttpException('Account does not exist.');
                    }
                }

                $account = $this->service->connect($user, $provider, $token['access_token']);

                $accessToken = $this->tokenService->create($user->getId());

                $this->dispatcher->dispatch(Oauth2Event::EVENT_AFTER_LOGIN, new Oauth2Event([
                    'user' => $user,
                    'provider' => $provider,
                    'client' => $client,
                    'token' => $token,
                    'accessToken' => $accessToken,
                    'account' => $account,
                ]));

                return new LoginResource($user, $accessToken);
            } else {
                $this->dispatcher->dispatch(Oauth2Event::EVENT_FAILED_LOGIN, new Oauth2Event([
                    'provider' => $provider,
                    'client' => $client,
                    'token' => $token,
                ]));

                throw new BadRequestHttpException('Invalid credentials.');
            }
        }

        return $client->buildAuthUrl();
    }
}
