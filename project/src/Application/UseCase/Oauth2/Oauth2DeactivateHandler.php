<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Oauth2;

use Infrastructure\OauthBundle\Command\Oauth2DeactivateCommand;
use Infrastructure\OauthBundle\Service\Oauth2Service;
use Infrastructure\UserBundle\Event\UserConnectedAccountEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Oauth2DeactivateHandler implements Oauth2DeactivateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var Oauth2Service
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        Oauth2Service $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param Oauth2DeactivateCommand $command
     * @return string|array
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function __invoke(Oauth2DeactivateCommand $command)
    {
        $this->dispatcher->dispatch(UserConnectedAccountEvent::EVENT_BEFORE_DELETE);

        $account = $this->service->deactivate($command);

        $this->dispatcher->dispatch(UserConnectedAccountEvent::EVENT_AFTER_DELETE,
            new UserConnectedAccountEvent([
                'model' => $account
            ])
        );

        return 'true';
    }
}