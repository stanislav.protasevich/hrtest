<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Oauth2;


use Domain\User\Resource\UserResource;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\OauthBundle\Command\Oauth2TokenCommand;
use Infrastructure\OauthBundle\Exception\Oauth2TokenException;
use Infrastructure\OauthBundle\Lib\OAuth2;
use OAuth2\IOAuth2Storage;
use OAuth2\OAuth2ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Oauth2TokenHandler implements Oauth2TokenHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var OAuth2
     */
    private $OAuth2;

    /**
     * @var Request
     */
    private $request;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var IOAuth2Storage
     */
    private $auth2Storage;

    public function __construct(ContainerInterface $container, OAuth2 $OAuth2, IOAuth2Storage $auth2Storage, ValidatorInterface $validator)
    {
        $this->container = $container;
        $this->OAuth2 = $OAuth2;
        $this->request = $container->get('request_stack')->getCurrentRequest();
        $this->validator = $validator;
        $this->auth2Storage = $auth2Storage;
    }

    /**
     * @param Oauth2TokenCommand $command
     * @return array
     * @throws OAuth2ServerException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(Oauth2TokenCommand $command)
    {
        $client = $this->auth2Storage->getClient($command->getClientId());

        $translator = $this->container->get('translator');

        if (!$client || $this->auth2Storage->checkClientCredentials($client, $command->getClientSecret()) === false) {
            throw FormException::withMessages([
                'clientId' => $translator->trans('The client credentials are invalid.'),
            ]);
        }

        if (!$this->auth2Storage->checkRestrictedGrantType($client, $command->getGrantType())) {
            throw FormException::withMessages([
                'grantType' => $translator->trans('The grant type is unauthorized for this client_id.'),
            ]);
        }

        $stored = [];
        switch ($command->getGrantType()) {
            case OAuth2::GRANT_TYPE_USER_CREDENTIALS:
                $errors = $this->validator->validate($command, null, ['secondStep']);

                if ($errors->count() !== 0) {
                    throw FormException::withMessages([
                        'username' => $translator->trans($translator->trans('Incorrect email or password.')),
                    ]);
                }

                // returns: true || ['scope' => scope]
                $stored = $this->OAuth2->getGrantAccessTokenUserCredentials($client, $command);
                break;

            case OAuth2::GRANT_TYPE_REFRESH_TOKEN:
                // returns ['data' => data, 'scope' => scope]
                $stored = $this->OAuth2->getGrantAccessTokenRefreshToken($client, $command);
                break;
        }

        $expire = $this->container->getParameter('auth.token.ttl');

        $stored += [
            'scope' => $this->OAuth2->getVariable(OAuth2::CONFIG_SUPPORTED_SCOPES, null),
            'data' => null,
            'access_token_lifetime' => $expire ?: $this->OAuth2->getVariable(OAuth2::CONFIG_ACCESS_LIFETIME),
            'issue_refresh_token' => true,
            'refresh_token_lifetime' => $this->OAuth2->getVariable(OAuth2::CONFIG_REFRESH_LIFETIME)
        ];

        $scope = $command->getScope();
        if ($command->getScope()) {
            // Check scope, if provided
            if (!isset($stored["scope"]) || !$this->OAuth2->getCheckScope($command->getScope(), $stored["scope"])) {
                throw new OAuth2ServerException(Response::HTTP_BAD_REQUEST, OAuth2::ERROR_INVALID_SCOPE, 'An unsupported scope was requested.');
            }
            $scope = $command->getScope();
        }

        try {
            $result = $this->OAuth2->createAccessToken(
                $client,
                $stored['data'],
                $scope,
                $stored['access_token_lifetime'],
                $stored['issue_refresh_token'],
                $stored['refresh_token_lifetime']
            );

            $result['info'] = (new UserResource($this->OAuth2->getUser()))->toArray();

            return $result;
        } catch (OAuth2ServerException $exception) {
            throw new Oauth2TokenException((int)$exception->getHttpCode(), $exception->getDescription());
        }
    }
}
