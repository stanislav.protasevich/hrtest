<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Job;


use Domain\Job\Resource\JobTitleListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\JobBundle\Command\JobTitleSearchCommand;
use Infrastructure\JobBundle\Service\JobTitleSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobTitleListHandler implements JobTitleListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var JobTitleSearchService
     */
    private $service;

    public function __construct(ContainerInterface $container, JobTitleSearchService $service) {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param JobTitleSearchCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(JobTitleSearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_job_title_list');

        return JobTitleListResource::collection($paginated);
    }
}