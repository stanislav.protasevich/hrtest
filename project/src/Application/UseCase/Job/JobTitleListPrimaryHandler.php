<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Job;


use Infrastructure\JobBundle\Command\JobTitleListPrimaryCommand;
use Infrastructure\JobBundle\Service\JobTitleService;

class JobTitleListPrimaryHandler implements JobTitleListPrimaryHandlerInterface
{
    /**
     * @var JobTitleService
     */
    private $service;

    public function __construct(JobTitleService $service)
    {
        $this->service = $service;
    }

    public function __invoke(JobTitleListPrimaryCommand $command)
    {
        return $this->service->getAllJobTitles();
    }
}