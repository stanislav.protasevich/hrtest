<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Job;


use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\JobBundle\Command\JobTitleDeleteCommand;
use Infrastructure\JobBundle\Service\JobTitleService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class JobTitleDeleteHandler implements JobTitleDeleteHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var JobTitleService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        JobTitleService $service
    ) {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param JobTitleDeleteCommand $command
     * @return int
     */
    public function __invoke(JobTitleDeleteCommand $command)
    {
        try {
            $this->service->delete($command->getJobTitle());

            return true;
        } catch (ForeignKeyConstraintViolationException $exception) {
            throw FormException::withMessages(['Job title has active links. Remove all links.']);
        }
    }
}