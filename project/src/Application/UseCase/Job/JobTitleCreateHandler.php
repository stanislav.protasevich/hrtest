<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Job;


use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Command\JobTitleCreateCommand;
use Infrastructure\JobBundle\Event\JobTitleEvent;
use Infrastructure\JobBundle\Service\JobTitleService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class JobTitleCreateHandler implements JobTitleCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var JobTitleService
     */
    private $service;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, JobTitleService $service)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param JobTitleCreateCommand $command
     * @return int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(JobTitleCreateCommand $command)
    {
        $this->dispatcher->dispatch(JobTitleEvent::EVENT_BEFORE_INSERT);

        $jobTitle = $this->service->create($command);

        $this->dispatcher->dispatch(JobTitleEvent::EVENT_AFTER_INSERT,
            new JobTitleEvent(compact('jobTitle'))
        );

        return [
            'id' => $jobTitle->getId()
        ];
    }
}
