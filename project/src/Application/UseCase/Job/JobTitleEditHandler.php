<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Job;


use Infrastructure\JobBundle\Command\JobTitleEditCommand;
use Infrastructure\JobBundle\Event\JobTitleEvent;
use Infrastructure\JobBundle\Service\JobTitleService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class JobTitleEditHandler implements JobTitleEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var JobTitleService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        JobTitleService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param JobTitleEditCommand $command
     * @return array|int
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(JobTitleEditCommand $command)
    {
        $this->dispatcher->dispatch(JobTitleEvent::EVENT_BEFORE_UPDATE);

        $jobTitle = $this->service->edit($command);

        $this->dispatcher->dispatch(JobTitleEvent::EVENT_AFTER_UPDATE,
            new JobTitleEvent(compact('jobTitle'))
        );

        return [
            'id' => $jobTitle->getId()
        ];
    }
}
