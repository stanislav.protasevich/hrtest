<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemAssignRemoveCommand;
use Infrastructure\RbacBundle\Event\AuthItemAssignEvent;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RbacAssignRemoveHandler implements RbacAssignRemoveHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var AuthItemService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        AuthItemService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    public function __invoke(RbacItemAssignRemoveCommand $command)
    {
        $this->dispatcher->dispatch(AuthItemAssignEvent::EVENT_BEFORE_ASSIGN_REMOVE);

        $item = $this->service->assignRemove($command->getItem(), $command->getAssignment());

        $this->dispatcher->dispatch(AuthItemAssignEvent::EVENT_AFTER_ASSIGN_REMOVE,
            new AuthItemAssignEvent($item)
        );

        return true;
    }
}