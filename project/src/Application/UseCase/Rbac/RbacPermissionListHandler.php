<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacPermissionListCommand;
use Infrastructure\RbacBundle\Service\RbacService;

class RbacPermissionListHandler implements RbacPermissionListHandlerInterface
{
    /**
     * @var RbacService
     */
    private $service;

    public function __construct(RbacService $service)
    {
        $this->service = $service;
    }

    public function __invoke(RbacPermissionListCommand $command)
    {
        $available = array_merge($this->service->getRoles(), $this->service->getPermissions());

        $assigned = [];
        foreach ($this->service->getRoles() as $key => $authItem) {

            $children = [];
            foreach ($this->service->getChildren($authItem['name']) as $child) {
                $children[] = $child;
            }

            $assigned[$key] = $authItem;
            $assigned[$key]['children'] = $children;
        }

        return [
            'roles' => array_values($assigned),
            'permissions' => array_values($available),
        ];
    }
}