<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemRoleDeleteCommand;
use Infrastructure\RbacBundle\Service\AuthItemService;

class RbacItemRoleDeleteHandler implements RbacItemRoleDeleteHandlerInterface
{
    /**
     * @var AuthItemService
     */
    private $service;

    public function __construct(AuthItemService $service)
    {
        $this->service = $service;
    }

    public function __invoke(RbacItemRoleDeleteCommand $command)
    {
        $this->service->delete(
            $command->getItem()
        );

        return true;
    }
}