<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacPermissionUserListCommand;
use Infrastructure\RbacBundle\Service\RbacService;

class RbacPermissionUserListHandler implements RbacPermissionUserListHandlerInterface
{
    /**
     * @var RbacService
     */
    private $service;

    public function __construct(RbacService $service)
    {
        $this->service = $service;
    }

    public function __invoke(RbacPermissionUserListCommand $command)
    {
        $permissions = $this->service->getPermissionsByUser(
            $command->getUser()
        );

        $roles = $this->service->getRolesByUser(
            $command->getUser()
        );

        $childRoles = [];
        foreach ($roles as $role) {
            $childRoles = array_merge($childRoles, $this->service->getChildRoles($role));
        }

        return array_keys(
            array_merge(
                $permissions,
                $roles,
                $childRoles
            )
        );
    }
}
