<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemAssignCreateCommand;
use Infrastructure\RbacBundle\Event\AuthItemAssignEvent;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RbacAssignCreateHandler implements RbacAssignCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var AuthItemService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        AuthItemService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param RbacItemAssignCreateCommand $command
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(RbacItemAssignCreateCommand $command): bool
    {
        $this->dispatcher->dispatch(AuthItemAssignEvent::EVENT_BEFORE_ASSIGN_CREATE);

        $item = $this->service->assignCreate($command->getItem(), $command->getAssignment());

        $this->dispatcher->dispatch(AuthItemAssignEvent::EVENT_AFTER_ASSIGN_CREATE,
            new AuthItemAssignEvent($item)
        );

        return true;
    }
}