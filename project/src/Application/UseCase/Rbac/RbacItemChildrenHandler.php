<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemChildrenCommand;
use Infrastructure\RbacBundle\Event\AuthItemChangeEvent;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RbacItemChildrenHandler implements RbacItemChildrenHandlerInterface
{
    /**
     * @var AuthItemService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(AuthItemService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param RbacItemChildrenCommand $command
     * @return bool|\Domain\Rbac\Entity\AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(RbacItemChildrenCommand $command)
    {
        $this->dispatcher->dispatch(AuthItemChangeEvent::EVENT_BEFORE_AUTH_ITEM_CHANGE);

        $authItem = $this->service->updateChildren($command->getName(), $command->getChildren());

        $this->dispatcher->dispatch(AuthItemChangeEvent::EVENT_AFTER_AUTH_ITEM_CHANGE,
            new AuthItemChangeEvent($authItem)
        );

        return true;
    }
}