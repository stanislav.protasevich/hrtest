<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Rbac;


use Domain\User\Resource\UserSimpleResource;
use Infrastructure\RbacBundle\Command\RbacPermissionUsersListCommand;

class RbacPermissionUsersListHandler implements RbacPermissionUsersListHandlerInterface
{
    public function __invoke(RbacPermissionUsersListCommand $command)
    {
        $item = $command->getItem();

        return UserSimpleResource::collection($item->getAssignment()->getValues());
    }
}