<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacItemRoleCreateCommand;
use Infrastructure\RbacBundle\Service\AuthItemService;

class RbacItemRoleCreateHandler implements RbacItemRoleCreateHandlerInterface
{
    /**
     * @var AuthItemService
     */
    private $service;

    public function __construct(AuthItemService $service)
    {
        $this->service = $service;
    }

    public function __invoke(RbacItemRoleCreateCommand $command)
    {
        $this->service->createRole($command->getAuthItem());

        return true;
    }
}