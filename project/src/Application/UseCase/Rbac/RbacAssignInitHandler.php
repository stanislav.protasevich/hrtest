<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Rbac;


use Infrastructure\RbacBundle\Command\RbacAssignInitCommand;
use Infrastructure\RbacBundle\Event\AuthItemInitEvent;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class RbacAssignInitHandler implements RbacAssignInitHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var AuthItemService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        AuthItemService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param RbacAssignInitCommand $command
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(RbacAssignInitCommand $command): array
    {
        $this->dispatcher->dispatch(AuthItemInitEvent::EVENT_BEFORE_INIT);

        $items = $this->service->append(
            $command->getItems(),
            $command->isClear(),
            $command->getRules()
        );

        $this->dispatcher->dispatch(AuthItemInitEvent::EVENT_AFTER_INIT,
            new AuthItemInitEvent($items)
        );

        return $items;
    }
}