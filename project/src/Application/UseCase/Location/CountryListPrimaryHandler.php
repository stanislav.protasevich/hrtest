<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Location;


use Infrastructure\LocationBundle\Command\CountryListPrimaryCommand;
use Infrastructure\LocationBundle\Service\LocationService;

class CountryListPrimaryHandler implements CountryListPrimaryHandlerInterface
{
    /**
     * @var LocationService
     */
    private $service;

    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CountryListPrimaryCommand $command)
    {
        return $this->service->getCountryPrimaryAll();
    }
}
