<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Location;


use Domain\Location\Resource\RegionResource;
use Infrastructure\LocationBundle\Command\RegionListCommand;
use Infrastructure\LocationBundle\Service\LocationService;

class RegionListHandler implements RegionListHandlerInterface
{
    /**
     * @var LocationService
     */
    private $service;

    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    public function __invoke(RegionListCommand $command)
    {
        return RegionResource::collection($this->service->getRegionAll($command));
    }
}