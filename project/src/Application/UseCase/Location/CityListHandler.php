<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Location;


use Domain\Location\Resource\CityResource;
use Infrastructure\LocationBundle\Command\CityListCommand;
use Infrastructure\LocationBundle\Service\LocationService;

class CityListHandler implements CityListHandlerInterface
{
    /**
     * @var LocationService
     */
    private $service;

    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CityListCommand $command)
    {
        return CityResource::collection($this->service->getCityAll());
    }
}