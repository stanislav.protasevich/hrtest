<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Location;


use Domain\Location\Resource\CountryResource;
use Infrastructure\LocationBundle\Command\CountryListCommand;
use Infrastructure\LocationBundle\Service\LocationService;

class CountryListHandler implements CountryListHandlerInterface
{
    /**
     * @var LocationService
     */
    private $service;

    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CountryListCommand $command)
    {
        return CountryResource::collection($this->service->getCountryAll($command));
    }
}