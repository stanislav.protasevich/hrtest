<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Location;


use Infrastructure\LocationBundle\Command\CityListPrimaryCommand;
use Infrastructure\LocationBundle\Service\LocationService;

class CityListPrimaryHandler implements CityListPrimaryHandlerInterface
{
    /**
     * @var LocationService
     */
    private $service;

    public function __construct(LocationService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CityListPrimaryCommand $command)
    {
        return $this->service->getCityPrimaryAll();
    }
}