<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Calendar;


use Domain\Calendar\Resource\CalendarResource;
use Infrastructure\CalendarBundle\Command\CalendarEditCommand;
use Infrastructure\CalendarBundle\Event\CalendarEvent;
use Infrastructure\CalendarBundle\Service\CalendarService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CalendarEditHandler implements CalendarEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CalendarService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, CalendarService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(CalendarEditCommand $command)
    {
        $this->dispatcher->dispatch(CalendarEvent::EVENT_BEFORE_UPDATE);

        $calendar = $this->service->edit($command->getCalendar());

        $this->dispatcher->dispatch(CalendarEvent::EVENT_AFTER_UPDATE,
            new CalendarEvent($calendar)
        );

        return new CalendarResource($calendar);
    }
}
