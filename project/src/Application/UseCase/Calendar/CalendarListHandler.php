<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Calendar;


use Domain\Calendar\Resource\CalendarResource;
use Infrastructure\CalendarBundle\Command\CalendarListCommand;
use Infrastructure\CalendarBundle\Service\CalendarSearchService;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CalendarListHandler implements CalendarListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CalendarSearchService
     */
    private $service;

    public function __construct(ContainerInterface $container, CalendarSearchService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(CalendarListCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_calendar.list');

        return CalendarResource::collection($paginated);
    }
}
