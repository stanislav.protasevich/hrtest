<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Calendar;


use Infrastructure\CalendarBundle\Command\CalendarCreateCommand;
use Infrastructure\CalendarBundle\Event\CalendarEvent;
use Infrastructure\CalendarBundle\Service\CalendarService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CalendarCreateHandler implements CalendarCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CalendarService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, CalendarService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(CalendarCreateCommand $command)
    {
        $this->dispatcher->dispatch(CalendarEvent::EVENT_BEFORE_INSERT);

        $calendar = $this->service->create($command->getCalendar());

        $this->dispatcher->dispatch(CalendarEvent::EVENT_AFTER_INSERT,
            new CalendarEvent($calendar)
        );

        return [
            'id' => $calendar->getId()
        ];
    }
}
