<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Calendar;


use Domain\Calendar\Resource\CalendarResource;
use Infrastructure\CalendarBundle\Command\CalendarViewCommand;

class CalendarViewHandler implements CalendarViewHandlerInterface
{
    public function __invoke(CalendarViewCommand $command)
    {
        return new CalendarResource(
            $command->getCalendar()
        );
    }
}
