<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Calendar;


use Infrastructure\CalendarBundle\Command\CalendarDeleteCommand;
use Infrastructure\CalendarBundle\Event\CalendarEvent;
use Infrastructure\CalendarBundle\Service\CalendarService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CalendarDeleteHandler implements CalendarDeleteHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CalendarService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, CalendarService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(CalendarDeleteCommand $command)
    {
        $this->dispatcher->dispatch(CalendarEvent::EVENT_BEFORE_DELETE);

        $calendar = $command->getCalendar();

        $this->service->delete($calendar);

        $this->dispatcher->dispatch(CalendarEvent::EVENT_AFTER_DELETE,
            new CalendarEvent($calendar)
        );

        return true;
    }
}
