<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\TimeOff;


use Infrastructure\TimeOffBundle\Command\TimeOffTypePrimaryCommand;
use Infrastructure\TimeOffBundle\Service\TimeOffService;

class TimeOffTypeListPrimaryHandler implements TimeOffTypeListPrimaryHandlerInterface
{
    /**
     * @var TimeOffService
     */
    private $service;

    public function __construct(TimeOffService $service)
    {
        $this->service = $service;
    }

    public function __invoke(TimeOffTypePrimaryCommand $command): array
    {
        return $this->service->getPrimaryAll();
    }
}