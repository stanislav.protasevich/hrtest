<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\TimeOff;

use Infrastructure\TimeOffBundle\Command\PolicyCreateCommand;
use Infrastructure\TimeOffBundle\Event\PolicyEvent;
use Infrastructure\TimeOffBundle\Service\PolicyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PolicyCreateHandler implements PolicyCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var PolicyService
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        PolicyService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param PolicyCreateCommand $command
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(PolicyCreateCommand $command)
    {
        $this->dispatcher->dispatch(PolicyEvent::EVENT_BEFORE_INSERT);

        $policy = $this->service->create($command->getPolicy());

        $this->dispatcher->dispatch(PolicyEvent::EVENT_AFTER_INSERT,
            new PolicyEvent($policy)
        );

        return [
            'id' => $policy->getId()
        ];
    }
}
