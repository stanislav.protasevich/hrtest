<?php

declare(strict_types=1);

namespace Application\UseCase\TimeOff;

use Infrastructure\TimeOffBundle\Command\TimeOffTypeCreateCommand;
use Infrastructure\TimeOffBundle\Event\TimeOffEvent;
use Infrastructure\TimeOffBundle\Service\TimeOffService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TimeOffTypeCreateHandler implements TimeOffTypeCreateHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var TimeOffService
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        TimeOffService $service
    ) {
        $this->dispatcher = $dispatcher;
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param TimeOffTypeCreateCommand $command
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(TimeOffTypeCreateCommand $command)
    {
        $this->dispatcher->dispatch(TimeOffEvent::EVENT_BEFORE_INSERT);

        $timeOffType = $this->service->create($command);

        $this->dispatcher->dispatch(TimeOffEvent::EVENT_AFTER_INSERT,
            new TimeOffEvent(compact('timeOffType'))
        );

        return [
            'id' => $timeOffType->getId()
        ];
    }
}
