<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\TimeOff;


use Domain\TimeOff\Resource\PolicyViewResource;
use Infrastructure\TimeOffBundle\Command\PolicyViewCommand;

class PolicyViewHandler implements PolicyViewHandlerInterface
{
    /**
     * @param PolicyViewCommand $command
     * @return PolicyViewResource
     */
    public function __invoke(PolicyViewCommand $command)
    {
        return new PolicyViewResource($command->getPolicy());
    }
}