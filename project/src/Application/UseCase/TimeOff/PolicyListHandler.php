<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\TimeOff;


use Domain\TimeOff\Resource\PolicyListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\TimeOffBundle\Command\PolicySearchCommand;
use Infrastructure\TimeOffBundle\Service\PolicySearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PolicyListHandler implements PolicyListHandlerInterface
{
    /**
     * @var PolicySearchService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container, PolicySearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    /**
     * @param PolicySearchCommand $command
     * @return \Infrastructure\CommonBundle\Pagination\PaginatedCollection|\Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(PolicySearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_timeoff_policy_list');

        return PolicyListResource::collection($paginated);
    }
}
