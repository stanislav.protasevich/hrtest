<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\TimeOff;


use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeDeleteCommand;
use Infrastructure\TimeOffBundle\Service\TimeOffService;

class TimeOffTypeDeleteHandler implements TimeOffTypeDeleteHandlerInterface
{
    /**
     * @var TimeOffService
     */
    private $service;

    public function __construct(TimeOffService $service)
    {
        $this->service = $service;
    }

    public function __invoke(TimeOffTypeDeleteCommand $command)
    {
        try {
            $this->service->delete($command->getTimeOffType());

            return true;
        } catch (ForeignKeyConstraintViolationException $exception) {
            throw FormException::withMessages(['Time off type has active links. Remove all links.']);
        }
    }
}