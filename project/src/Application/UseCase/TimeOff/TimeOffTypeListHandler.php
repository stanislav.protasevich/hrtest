<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\TimeOff;


use Domain\TimeOff\Resource\TimeOffTypeResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeSearchCommand;
use Infrastructure\TimeOffBundle\Service\TimeOffSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TimeOffTypeListHandler implements TimeOffTypeListHandlerInterface
{
    /**
     * @var TimeOffSearchService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container, TimeOffSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    /**
     * @param TimeOffTypeSearchCommand $command
     * @return \Infrastructure\CommonBundle\Pagination\PaginatedCollection|\Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(TimeOffTypeSearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_timeoff_type_list');

        return TimeOffTypeResource::collection($paginated);
    }
}