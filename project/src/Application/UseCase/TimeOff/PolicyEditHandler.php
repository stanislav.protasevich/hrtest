<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\TimeOff;

use Infrastructure\TimeOffBundle\Command\PolicyEditCommand;
use Infrastructure\TimeOffBundle\Event\PolicyEvent;
use Infrastructure\TimeOffBundle\Service\PolicyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PolicyEditHandler implements PolicyEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var PolicyService
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        PolicyService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param PolicyEditCommand $command
     * @return array|string
     * @throws \Exception
     */
    public function __invoke(PolicyEditCommand $command)
    {
        $this->dispatcher->dispatch(PolicyEvent::EVENT_BEFORE_INSERT);

        $policy = $this->service->edit($command);

        $this->dispatcher->dispatch(PolicyEvent::EVENT_AFTER_INSERT,
            new PolicyEvent($policy)
        );

        return [
            'id' => $policy->getId()
        ];
    }
}
