<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\TimeOff;


use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\TimeOffBundle\Command\PolicyDeleteCommand;
use Infrastructure\TimeOffBundle\Event\PolicyEvent;
use Infrastructure\TimeOffBundle\Service\PolicyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class PolicyDeleteHandler implements PolicyDeleteHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var PolicyService
     */
    private $service;

    function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, PolicyService $service)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    public function __invoke(PolicyDeleteCommand $command)
    {
        $policy = $command->getPolicy();
        
        try {
            $this->dispatcher->dispatch(PolicyEvent::EVENT_BEFORE_DELETE);

            $this->service->delete($policy);

            $this->dispatcher->dispatch(PolicyEvent::EVENT_AFTER_DELETE,
                new PolicyEvent($policy)
            );

            return true;
        } catch (ForeignKeyConstraintViolationException $exception) {
            throw FormException::withMessages(
                [sprintf('Policy id: %s has active links. Remove all links.', $policy->getId())]
            );
        }
    }
}
