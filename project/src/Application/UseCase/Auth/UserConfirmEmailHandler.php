<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Auth;

use Domain\User\Entity\User;
use Domain\User\Event\UserConfirmEmail;
use Infrastructure\AuthBundle\Command\ConfirmEmailCommand;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserConfirmEmailHandler implements UserConfirmEmailHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var UserService
     */
    private $userService;

    function __construct(
        EventDispatcherInterface $dispatcher,
        ContainerInterface $container,
        UserService $userService
    )
    {
        $this->dispatcher = $dispatcher;
        $this->container = $container;
        $this->userService = $userService;
    }

    /**
     * @param ConfirmEmailCommand $command
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function __invoke(ConfirmEmailCommand $command)
    {
        $this->dispatcher->dispatch(UserConfirmEmail::EVENT_BEFORE_CONFIRM);

        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $em->getConnection()->beginTransaction();

        try {
            /* @var $user User */
            $user = $em->getRepository(User::class)->loadByResetToken($command->token);

            $this->userService->resetPassword($user, (string)$command->password);
            $this->userService->setStatusActive($user);

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        $this->dispatcher->dispatch(UserConfirmEmail::EVENT_AFTER_CONFIRM, new UserConfirmEmail($user));

        return [
            'email' => $user->getEmail(),
        ];
    }
}
