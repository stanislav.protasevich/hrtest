<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Auth;

use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Command\UserPasswordResetCommand;
use Infrastructure\AuthBundle\Service\TokenService;
use Infrastructure\UserBundle\Event\UserPasswordResetEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserPasswordResetHandler implements UserPasswordResetHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var TokenService
     */
    private $tokenService;

    function __construct(
        EventDispatcherInterface $dispatcher,
        ContainerInterface $container,
        UserService $userService,
        TokenService $tokenService
    )
    {
        $this->dispatcher = $dispatcher;
        $this->container = $container;
        $this->userService = $userService;
        $this->tokenService = $tokenService;
    }

    /**
     * @param UserPasswordResetCommand $command
     * @return array
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(UserPasswordResetCommand $command)
    {
        $this->dispatcher->dispatch(UserPasswordResetEvent::EVENT_BEFORE_REQUEST);

        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $em->getConnection()->beginTransaction();

        try {
            /* @var $user User */
            $user = $em->getRepository(User::class)->loadByResetToken($command->token);

            $this->userService->resetPassword($user, $command->password);

            $this->tokenService->removeAll($user);

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        $this->dispatcher->dispatch(UserPasswordResetEvent::EVENT_AFTER_REQUEST, new UserPasswordResetEvent([
            'user' => $user,
            'command' => $command,
        ]));

        return [
            'email' => $user->getEmail(),
        ];
    }
}