<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Auth;

use Doctrine\ORM\EntityManagerInterface;
use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Command\UserPasswordResetRequestCommand;
use Infrastructure\AuthBundle\Helpers\PasswordResetTokenHelper;
use Infrastructure\UserBundle\Event\UserPasswordResetRequestEvent;
use Infrastructure\UserBundle\Message\PasswordResetEmailNotification;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class UserPasswordResetRequestHandler implements UserPasswordResetRequestHandlerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var MessageBusInterface
     */
    private $bus;

    function __construct(EntityManagerInterface $em, EventDispatcherInterface $dispatcher, ContainerInterface $container, MessageBusInterface $bus)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
        $this->bus = $bus;
    }

    /**
     * @param UserPasswordResetRequestCommand $command
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function __invoke(UserPasswordResetRequestCommand $command)
    {
        $this->dispatcher->dispatch(UserPasswordResetRequestEvent::EVENT_BEFORE_REQUEST);

        /* @var $repository UserRepository */
        $repository = $this->em->getRepository(User::class);

        /* @var $user User */
        $user = $repository->loadByEmail($command->email);

        $token = PasswordResetTokenHelper::generateResetToken();

        $user->setPasswordResetToken($token);
        $this->em->persist($user);
        $this->em->flush();

        $this->dispatcher->dispatch(UserPasswordResetRequestEvent::EVENT_AFTER_REQUEST, new UserPasswordResetRequestEvent([
            'user' => $user
        ]));

        $this->bus->dispatch(
            new PasswordResetEmailNotification($user)
        );

        return true;
    }
}
