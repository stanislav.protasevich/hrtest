<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Auth;

use Doctrine\ORM\EntityManagerInterface;
use Infrastructure\AuthBundle\Command\logoutCommand;
use Infrastructure\AuthBundle\Service\TokenService;

/**
 * @property EntityManagerInterface $em
 */
class UserLogoutHandler implements UserLogoutHandlerInterface
{
    /**
     * @var TokenService
     */
    private $service;

    function __construct(TokenService $service)
    {
        $this->service = $service;
    }

    /**
     * @param logoutCommand $command
     * @return bool
     */
    public function __invoke(logoutCommand $command)
    {
        $this->service->removeToken($command->access_token);

        return true;
    }
}