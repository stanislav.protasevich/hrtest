<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Auth;


use Doctrine\ORM\EntityManager;
use Domain\Auth\Resource\LoginResource;
use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Command\LoginCommand;
use Infrastructure\AuthBundle\Event\UserSignIn;
use Infrastructure\AuthBundle\Service\JwtTokenService;
use Infrastructure\UserBundle\Exception\UserInvalidLoginException;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserLoginHandler implements UserLoginHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var JwtTokenService
     */
    private $tokenService;

    function __construct(
        ValidatorInterface $validator,
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        JwtTokenService $tokenService
    ) {
        $this->dispatcher = $dispatcher;
        $this->container = $container;
        $this->validator = $validator;
        $this->tokenService = $tokenService;
    }

    /**
     * @param LoginCommand $command
     * @return array|string
     * @throws UserInvalidLoginException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function __invoke(LoginCommand $command)
    {
        $this->dispatcher->dispatch(UserSignIn::EVENT_BEFORE_LOGIN);

        $errors = $this->validator->validate($command, null, ['secondStep']);

        if ($errors->count() !== 0) {
            $this->dispatcher->dispatch(UserSignIn::EVENT_FAILED_LOGIN, new UserSignIn([
                'command' => $command,
                'errors' => $errors,
            ]));

            $translator = $this->container->get('translator');

            throw new UserInvalidLoginException($translator->trans('Incorrect email or password.'),
                Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        /* @var $repository UserRepository */
        $repository = $em->getRepository(User::class);

        /* @var $user User */
        $user = $repository->loadByEmail($command->email);

        $token = $this->tokenService->create($user->getId());

        $this->dispatcher->dispatch(UserSignIn::EVENT_AFTER_LOGIN, new UserSignIn(compact('user')));

        return new LoginResource($user, $token);
    }
}
