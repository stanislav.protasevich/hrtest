<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Domain\User\Resource\UserPerformanceListResource;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceListCommand;
use Infrastructure\UserBundle\Service\UserPerformanceService;

class UserPerformanceListHandler implements UserPerformanceListHandlerInterface
{
    /**
     * @var UserPerformanceService
     */
    private $service;

    public function __construct(UserPerformanceService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserPerformanceListCommand $command)
    {
        return UserPerformanceListResource::collection(
            $this->service->getAllByUser(
                $command->getUser()
            )
        );
    }
}