<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\User\Performance;


use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormDownloadCommand;
use Infrastructure\UserBundle\Helpers\UserPerformanceFormTemplate;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormDownloadVo;

class UserPerformanceFormDownloadHandler implements UserPerformanceFormDownloadHandlerInterface
{
    /**
     * @var UserPerformanceFormTemplateHandlerInterface
     */
    private $formTemplateHandler;

    public function __construct(UserPerformanceFormTemplateHandlerInterface $formTemplateHandler)
    {
        $this->formTemplateHandler = $formTemplateHandler;
    }

    public function __invoke(UserPerformanceFormDownloadCommand $command)
    {
        $form = $command->getForm();

        $user = $command->getUser();

        $template = UserPerformanceFormTemplate::content($form->getType(), $form->getAuthor());

        $filename = $user->getProfile()->getFullName().' '.$form->getType().' '.$form->getAuthor().' '.$form->getCompletionAt()->format('d.m.Y');

        return new UserPerformanceFormDownloadVo($template, $form, $user, $filename);
    }
}