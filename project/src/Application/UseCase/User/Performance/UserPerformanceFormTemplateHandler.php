<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Domain\User\Entity\User;
use Infrastructure\JobBundle\Service\JobTitleService;
use Infrastructure\LocationBundle\Service\LocationService;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormTemplateCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;

class UserPerformanceFormTemplateHandler implements UserPerformanceFormTemplateHandlerInterface
{
    private const TYPE_INPUT = 'input';
    private const TYPE_STRING = 'string';
    private const TYPE_SELECT = 'select';
    protected $map = [
        'jobTitle' => 'populateJobTitle',
        'name' => 'populateName',
        'supervisor' => 'populateSupervisor',
        'city' => 'populateCity',
    ];
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(UserPerformanceFormTemplateCommand $command)
    {
        $template = $command->getTemplate();

        foreach ($template['general']['rows'] as $key => &$row) {
            foreach ($row as $rowKey => &$rowItem) {

                if ($callback = $this->map[$rowItem['name']] ?? null) {
                    $rowItem = $this->$callback($rowItem, $command);
                }
            }
        }

        return $template;
    }

    protected function populateJobTitle(array $rowItem, UserPerformanceFormTemplateCommand $command): array
    {
        /** @var JobTitleService $jobTitleService */
        $jobTitleService = $this->container->get(JobTitleService::class);

        $jobTitles = $jobTitleService->findAll(Query::HYDRATE_ARRAY);

        $rowItem = $this->populateOptionsRow($rowItem, $jobTitles);

        $rowItem['value'] = $command->getUser()->getMainEmployeeCompany() ?
            $command->getUser()->getMainEmployeeCompany()->getJobTitle()->getId() :
            null;
        $rowItem['type'] = self::TYPE_SELECT;
        $rowItem['disabled'] = false;

        return $rowItem;
    }

    /**
     * @param array $rowItem
     * @param $jobTitles
     * @return array
     */
    protected function populateOptionsRow(array $rowItem, $jobTitles): array
    {
        $rowItem['options'] = [];

        foreach ($jobTitles as $jobTitle) {
            $rowItem['options'][] = [
                'label' => $jobTitle['name'],
                'value' => $jobTitle['id'],
            ];
        }

        return $rowItem;
    }

    protected function populateName(array $rowItem, UserPerformanceFormTemplateCommand $command): array
    {
        $rowItem['value'] = $command->getUser()->getProfile()->getFullName();
        $rowItem['type'] = self::TYPE_INPUT;
        $rowItem['disabled'] = true;

        return $rowItem;
    }

    protected function populateSupervisor(array $rowItem, UserPerformanceFormTemplateCommand $command): array
    {
        /** @var User $supervisor */
        $supervisor = $command->getUser()->getSupervisors()->first();

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $users = $em->getRepository(User::class)->findAllActive();

        $rowItem['options'] = [];

        foreach ($users as $user) {
            $rowItem['options'][] = [
                'label' => $user->getProfile()->getFullName(),
                'value' => $user->getId(),
            ];
        }

        $rowItem['value'] = $supervisor ? $supervisor->getId() : null;
        $rowItem['type'] = self::TYPE_SELECT;
        $rowItem['disabled'] = false;

        return $rowItem;
    }

    protected function populateCity(array $rowItem, UserPerformanceFormTemplateCommand $command): array
    {
        /** @var LocationService $locationService */
        $locationService = $this->container->get(LocationService::class);

        $locations = $locationService->getCityAll(Query::HYDRATE_ARRAY);

        $rowItem = $this->populateOptionsRow($rowItem, $locations);

        if ($command->getUser()->getMainEmployeeCompany() && $command->getUser()->getMainEmployeeCompany()->getCompany()->getAddress()) {
            $rowItem['value'] = $command->getUser()->getMainEmployeeCompany()->getCompany()->getAddress()->getCity()->getId();
        } else {
            $rowItem['value'] = null;
        }

        $rowItem['type'] = self::TYPE_SELECT;
        $rowItem['disabled'] = false;

        return $rowItem;
    }
}