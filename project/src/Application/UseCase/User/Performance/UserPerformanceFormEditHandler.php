<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Domain\User\Resource\UserPerformanceFormResource;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormEditCommand;
use Infrastructure\UserBundle\Service\UserPerformanceFormService;

class UserPerformanceFormEditHandler implements UserPerformanceFormEditHandlerInterface
{
    /**
     * @var UserPerformanceFormService
     */
    private $service;

    public function __construct(UserPerformanceFormService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserPerformanceFormEditCommand $command)
    {
        $form = $this->service->updateForm($command->getUserPerformanceForm());

        return new UserPerformanceFormResource($form);
    }
}