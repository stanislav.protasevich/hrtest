<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Domain\User\Resource\UserPerformanceFormResource;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormAddCommand;
use Infrastructure\UserBundle\Service\UserPerformanceFormService;

class UserPerformanceFormAddHandler implements UserPerformanceFormAddHandlerInterface
{
    /**
     * @var UserPerformanceFormService
     */
    private $service;

    public function __construct(UserPerformanceFormService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserPerformanceFormAddCommand $command)
    {
        $form = $this->service->createForm($command->getUserPerformanceForm());

        return new UserPerformanceFormResource($form);
    }
}