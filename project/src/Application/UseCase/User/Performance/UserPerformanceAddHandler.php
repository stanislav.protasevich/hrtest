<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Domain\User\Resource\UserPerformanceListResource;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceAddCommand;
use Infrastructure\UserBundle\Service\UserPerformanceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserPerformanceAddHandler implements UserPerformanceAddHandlerInterface
{
    /**
     * @var UserPerformanceService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserPerformanceService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(UserPerformanceAddCommand $command)
    {
        $performance = $this->service->create($command->getUser());

        return new UserPerformanceListResource($performance);
    }
}