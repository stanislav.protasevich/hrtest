<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Infrastructure\UserBundle\Command\Performance\UserPerformanceOutdatedStatusCommand;
use Infrastructure\UserBundle\Service\UserPerformanceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserPerformanceOutdatedStatusHandler implements UserPerformanceOutdatedStatusHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserPerformanceService
     */
    private $service;

    public function __construct(ContainerInterface $container, UserPerformanceService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param UserPerformanceOutdatedStatusCommand $command
     * @return \Domain\User\Entity\UserPerformance[]
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(UserPerformanceOutdatedStatusCommand $command)
    {
        return $this->service->changeOutdatedStatus();
    }
}