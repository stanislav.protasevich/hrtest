<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Performance;


use Domain\User\Resource\UserPerformanceFormResource;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceFormViewCommand;

class UserPerformanceFormViewHandler implements UserPerformanceFormViewHandlerInterface
{
    public function __invoke(UserPerformanceFormViewCommand $command)
    {
        return new UserPerformanceFormResource($command->getForm());
    }
}