<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Supervisor;


use Domain\User\Resource\UserSupervisorsResource;
use Infrastructure\UserBundle\Command\Supervisor\UserRemoveSupervisorCommand;
use Infrastructure\UserBundle\Event\UserSupervisorEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserRemoveSupervisorHandler implements UserRemoveSupervisorHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserRemoveSupervisorCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(UserRemoveSupervisorCommand $command)
    {
        $this->dispatcher->dispatch(UserSupervisorEvent::EVENT_BEFORE_REMOVE);

        $user = $this->service->removeSupervisors($command->getUser(), $command->getSupervisors());

        $this->dispatcher->dispatch(UserSupervisorEvent::EVENT_AFTER_REMOVE,
            new UserSupervisorEvent($user)
        );

        return UserSupervisorsResource::collection(
            $user->getSupervisors()->getValues()
        );
    }
}
