<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\User\Supervisor;


use Domain\User\Resource\UserSimpleResource;
use Infrastructure\UserBundle\Command\Supervisor\UserSupervisorCommand;

class UserSupervisorHandler implements UserSupervisorHandlerInterface
{
    public function __invoke(UserSupervisorCommand $command)
    {
        $user = $command->getUser();
        $supervisor = $user->getSupervisor();
        
        return $supervisor ? new  UserSimpleResource($supervisor) : null;
    }
}