<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Supervisor;


use Domain\User\Resource\UserSupervisorsResource;
use Infrastructure\UserBundle\Command\Supervisor\UserViewSupervisorCommand;

class UserViewSupervisorHandler
{
    public function __invoke(UserViewSupervisorCommand $command)
    {
        return UserSupervisorsResource::collection(
            $command->getUser()->getSupervisors()->getValues()
        );
    }
}