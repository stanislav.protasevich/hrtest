<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Supervisor;

use Domain\User\Resource\UserSupervisorsResource;
use Infrastructure\UserBundle\Command\Supervisor\UserAddSupervisorCommand;
use Infrastructure\UserBundle\Event\UserSupervisorEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserAddSupervisorHandler implements UserAddSupervisorHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserAddSupervisorCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(UserAddSupervisorCommand $command)
    {
        $this->dispatcher->dispatch(UserSupervisorEvent::EVENT_BEFORE_ADD);

        $user = $this->service->addSupervisors($command->getUser(), $command->getSupervisors());

        $this->dispatcher->dispatch(UserSupervisorEvent::EVENT_AFTER_ADD,
            new UserSupervisorEvent($user)
        );

        return UserSupervisorsResource::collection(
            $user->getSupervisors()->getValues()
        );
    }
}
