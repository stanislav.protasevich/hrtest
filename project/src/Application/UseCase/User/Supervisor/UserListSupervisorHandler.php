<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Supervisor;


use Domain\User\Resource\UserSupervisorsResource;
use Infrastructure\UserBundle\Command\Supervisor\UserListSupervisorCommand;
use Infrastructure\UserBundle\Service\UserSupervisorsSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserListSupervisorHandler implements UserListSupervisorHandlerInterface
{
    /**
     * @var UserSupervisorsSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserSupervisorsSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(UserListSupervisorCommand $command)
    {
        $results = $this->service->search($command);

        return UserSupervisorsResource::collection($results);
    }
}