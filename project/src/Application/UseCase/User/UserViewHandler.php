<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Application\UseCase\User;

use Domain\User\Resource\UserResource;
use Domain\User\Resource\UserViewResource;
use Infrastructure\UserBundle\Command\UserViewCommand;

class UserViewHandler implements UserViewHandlerInterface
{
    /**
     * @param UserViewCommand $command
     * @return UserResource
     */
    public function __invoke(UserViewCommand $command)
    {
        return new UserViewResource($command->getUser());
    }
}