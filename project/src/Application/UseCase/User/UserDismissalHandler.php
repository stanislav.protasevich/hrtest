<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\User;


use Infrastructure\UserBundle\Command\UserDismissalCommand;
use Infrastructure\UserBundle\Event\UserDismissalEvent;
use Infrastructure\UserBundle\Service\UserDismissalService;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserDismissalHandler implements UserDismissalHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, UserDismissalService $service, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(UserDismissalCommand $command)
    {
        $user = $command->getUser();

        $this->dispatcher->dispatch(UserDismissalEvent::EVENT_BEFORE_DISMISSAL, new UserDismissalEvent($user));

        $this->service->dismissal($user);

        $this->dispatcher->dispatch(UserDismissalEvent::EVENT_AFTER_DISMISSAL, new UserDismissalEvent($user));

        return true;
    }
}
