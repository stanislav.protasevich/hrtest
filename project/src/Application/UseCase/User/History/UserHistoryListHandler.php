<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\History;


use Domain\User\Resource\UserHistoryListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\UserBundle\Command\History\UserHistoryListCommand;
use Infrastructure\UserBundle\Service\UserHistorySearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserHistoryListHandler implements UserHistoryListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserHistorySearchService
     */
    private $service;

    public function __construct(ContainerInterface $container, UserHistorySearchService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(UserHistoryListCommand $command)
    {
        $qb = $this->service->search($command);
        
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container
            ->get('pagination_factory')
            ->createCollection($qb, 'api_user_list_history', ['id' => $command->getUser()]);

        return UserHistoryListResource::collection($paginated);
    }
}
