<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\User\Alert;


use Infrastructure\CommonBundle\Helpers\Json;
use Infrastructure\UserBundle\Command\Alert\UserAlertRefreshCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserAlertRefreshHandler implements UserAlertRefreshHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(UserAlertRefreshCommand $command)
    {
        $body = Json::encode([
            'service' => 'refresh',
            'userId' => $command->getUser()->getId(),
        ]);

        $this->container->get('old_sound_rabbit_mq.user_alert_queue_refresh_producer')->publish($body);
    }
}