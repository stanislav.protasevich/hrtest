<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Alert;


use Domain\User\Resource\UserAlertResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\UserBundle\Command\Alert\UserAlertHistoryCommand;
use Infrastructure\UserBundle\Service\UserAlertSearchService;
use Infrastructure\UserBundle\Service\UserAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserAlertHistoryHandler implements UserAlertHistoryHandlerInterface
{
    /**
     * @var UserAlertService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserAlertSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(UserAlertHistoryCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')
            ->createCollection($qb, 'api_user_view_alert_history', [
                'id' => $command->getAlertedUser()->getId(),
            ]);

        return UserAlertResource::collection($paginated);
    }
}
