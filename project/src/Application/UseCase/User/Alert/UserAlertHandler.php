<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Alert;


use Domain\User\Entity\User;
use Domain\User\Resource\UserAlertResource;
use Infrastructure\CommonBundle\Helpers\Json;
use Infrastructure\UserBundle\Command\Alert\UserAlertCommand;
use Infrastructure\UserBundle\Service\UserAlertService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserAlertHandler implements UserAlertHandlerInterface
{
    /**
     * @var UserAlertService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(UserAlertService $service, EventDispatcherInterface $dispatcher, ContainerInterface $container)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    public function __invoke(UserAlertCommand $command)
    {
        $alert = $this->service->create($command);

        $body = Json::encode(
            (new UserAlertResource($alert))->toArray()
        );

        $this->container->get('old_sound_rabbit_mq.user_alert_queue_producer')->publish($body);

        return true;
    }

    protected function userInfo(User $user): array
    {
        return [
            'id' => $user->getId(),
            'profile' => [
                'fullName' => $user->getProfile()->getFirstName(),
                'avatar' => $user->getProfile()->getAvatarUrl(),
            ],
        ];
    }
}
