<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\User;


use Infrastructure\UserBundle\Command\UserListPrimaryCommand;
use Infrastructure\UserBundle\Service\UserService;

class UserListPrimaryHandler implements UserListPrimaryHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserListPrimaryCommand $command)
    {
        return $this->service->getPrimaryAll();
    }
}