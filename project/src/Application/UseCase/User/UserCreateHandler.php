<?php

declare(strict_types=1);

namespace Application\UseCase\User;


use Infrastructure\UserBundle\Command\UserCreateCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserCreateHandler implements UserCreateHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserCreateCommand $command
     * @return int|string
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(UserCreateCommand $command)
    {
        $this->dispatcher->dispatch(UserEvent::EVENT_BEFORE_INSERT);

        $user = $this->service->create($command);

        $this->dispatcher->dispatch(UserEvent::EVENT_AFTER_INSERT,
            new UserEvent($user)
        );

        return [
            'id' => $user->getId()
        ];
    }
}
