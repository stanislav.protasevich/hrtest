<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Follows;


use Domain\User\Resource\UserFollowersResource;
use Infrastructure\UserBundle\Command\Follow\UserListFollowsCommand;
use Infrastructure\UserBundle\Service\UserFollowSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserListFollowsHandler implements UserListFollowsHandlerInterface
{
    /**
     * @var UserFollowSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserFollowSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(UserListFollowsCommand $command)
    {
        $results = $this->service->search($command);

        return UserFollowersResource::collection($results);
    }
}