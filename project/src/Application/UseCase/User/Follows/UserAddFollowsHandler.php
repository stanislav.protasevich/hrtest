<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Follows;


use Domain\User\Resource\UserFollowersResource;
use Infrastructure\UserBundle\Command\Follow\UserAddFollowsCommand;
use Infrastructure\UserBundle\Event\UserFollowEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserAddFollowsHandler implements UserAddFollowsHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserAddFollowsCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     * @throws \Exception
     */
    public function __invoke(UserAddFollowsCommand $command)
    {
        $this->dispatcher->dispatch(UserFollowEvent::EVENT_BEFORE_ADD);

        $user = $this->service->addFollows($command->getUser(), $command->getFollowers());

        $this->dispatcher->dispatch(UserFollowEvent::EVENT_AFTER_ADD,
            new UserFollowEvent($user)
        );

        return UserFollowersResource::collection(
            $user->getFollowers()->getValues()
        );
    }
}
