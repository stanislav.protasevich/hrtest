<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Follows;


use Domain\User\Resource\UserFollowersResource;
use Infrastructure\UserBundle\Command\Follow\UserRemoveFollowsCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Event\UserFollowEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserRemoveFollowsHandler implements UserRemoveFollowsHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserRemoveFollowsCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(UserRemoveFollowsCommand $command)
    {
        $this->dispatcher->dispatch(UserFollowEvent::EVENT_BEFORE_REMOVE);

        $user = $this->service->removeFollows($command->getUser(), $command->getFollowers());

        $this->dispatcher->dispatch(UserFollowEvent::EVENT_AFTER_REMOVE,
            new UserFollowEvent($user)
        );

        return UserFollowersResource::collection(
            $user->getFollowers()->getValues()
        );
    }
}
