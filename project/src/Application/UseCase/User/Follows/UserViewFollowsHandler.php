<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Follows;


use Domain\User\Resource\UserFollowersResource;
use Infrastructure\UserBundle\Command\Follow\UserViewFollowsCommand;

class UserViewFollowsHandler implements UserViewFollowsHandlerInterface
{
    public function __invoke(UserViewFollowsCommand $command)
    {
        return UserFollowersResource::collection(
            $command->getUser()->getFollowers()->getValues()
        );
    }
}