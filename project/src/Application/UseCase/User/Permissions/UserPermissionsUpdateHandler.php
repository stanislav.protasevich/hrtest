<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Permissions;


use Infrastructure\UserBundle\Command\Permissions\UserPermissionsUpdateCommand;
use Infrastructure\UserBundle\Event\UserPermissionsEvent;
use Infrastructure\UserBundle\Service\UserRbacService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserPermissionsUpdateHandler implements UserPermissionsUpdateHandlerInterface
{
    /**
     * @var UserRbacService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(UserRbacService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(UserPermissionsUpdateCommand $command)
    {
        $this->dispatcher->dispatch(UserPermissionsEvent::EVENT_BEFORE_UPDATE);

        $user = $this->service->updateRbacItem($command->getUser(), $command->getItems());

        $this->dispatcher->dispatch(UserPermissionsEvent::EVENT_AFTER_UPDATE,
            new UserPermissionsEvent($user)
        );

        return true;
    }
}