<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Permissions;


use Infrastructure\RbacBundle\Service\RbacService;
use Infrastructure\UserBundle\Command\Permissions\UserPermissionsListCommand;

class UserPermissionsListHandler implements UserPermissionsListHandlerInterface
{
    /**
     * @var RbacService
     */
    private $service;

    public function __construct(RbacService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserPermissionsListCommand $command)
    {
//        $available = [];
//
//        $this->service->getRoles();
//        foreach (array_keys($this->service->getRoles()) as $name) {
//            $available[$name] = AuthItem::TYPE_ROLE;
//        }
//
//
//        foreach (array_keys($this->service->getPermissions()) as $name) {
//            if ($name[0] != '/') {
//                $available[$name] = AuthItem::TYPE_PERMISSION;
//            }
//        }

        $available = array_merge($this->service->getRoles(), $this->service->getPermissions());

        $assigned = [];
        foreach ($this->service->getAssignments($command->getUser()) as $item) {
            $assigned[$item->getName()] = $available[$item->getName()];

            unset($available[$item->getName()]);
        }
        
        return [
            'available' => array_values($available),
            'assigned' => array_values($assigned),
        ];
    }
}