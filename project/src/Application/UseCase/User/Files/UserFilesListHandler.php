<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Files;


use Domain\Archive\Resource\FileEntityResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\UserBundle\Command\Files\UserFilesListCommand;
use Infrastructure\UserBundle\Service\UserFilesSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFilesListHandler implements UserFilesListHandlerInterface
{
    /**
     * @var UserFilesSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserFilesSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(UserFilesListCommand $command)
    {
        $user = $command->getUser();

        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container
            ->get('pagination_factory')
            ->createCollection($qb, 'api_user_list_files', ['id' => $user->getId()]);
        
        return FileEntityResource::collection($paginated);
    }
}
