<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Files;


use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Infrastructure\UserBundle\Command\Files\UserFilesRemoveCommand;

class UserFilesRemoveHandler implements UserFilesRemoveHandlerInterface
{
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(ArchiveFileService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserFilesRemoveCommand $command)
    {
        $this->service->remove($command->getFile());

        return true;
    }
}