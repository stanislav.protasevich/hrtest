<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Files;


use Domain\Archive\Resource\FileEntityResource;
use Infrastructure\ArchiveBundle\Command\ArchiveFileAttachCommand;
use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Infrastructure\UserBundle\Command\Files\UserFilesAddCommand;

class UserFilesAddHandler implements UserFilesAddHandlerInterface
{
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(ArchiveFileService $service)
    {
        $this->service = $service;
    }

    public function __invoke(UserFilesAddCommand $command)
    {
//        $files = [];

        $uploaded = $command->getFile();
//
//        if (is_array($uploaded)) {
//            foreach ($uploaded as $file) {
//                $dto = new ArchiveFileAttachCommand($file, $command->getUser());
//                $files[] = $this->service->attach($dto);
//            }
//        } else {
//        }

        $dto = new ArchiveFileAttachCommand($uploaded, $command->getUser());
        $files = $this->service->attach($dto);

//        $dto->setTitle(
//            $command->getTitle()
//        );

        return new FileEntityResource($files);
    }
}