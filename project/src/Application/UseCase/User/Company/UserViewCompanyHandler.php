<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Company;


use Domain\User\Resource\UserViewCompanyResource;
use Infrastructure\UserBundle\Command\Company\UserViewCompanyCommand;

class UserViewCompanyHandler implements UserViewCompanyHandlerInterface
{
    public function __invoke(UserViewCompanyCommand $command)
    {
        return new UserViewCompanyResource($command->getUser());
    }
}
