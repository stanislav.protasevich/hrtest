<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Company;


use Domain\User\Resource\UserViewCompanyResource;
use Domain\User\Resource\UserViewResource;
use Infrastructure\UserBundle\Command\Company\UserEditCompanyCommand;
use Infrastructure\UserBundle\Event\UserCompanyEvent;
use Infrastructure\UserBundle\Service\UserCompanyService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserEditCompanyHandler implements UserEditCompanyHandlerInterface
{
    /**
     * @var UserCompanyService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserCompanyService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserEditCompanyCommand $command
     * @return UserViewCompanyResource|UserViewResource
     * @throws \Exception
     */
    public function __invoke(UserEditCompanyCommand $command)
    {
        $this->dispatcher->dispatch(UserCompanyEvent::EVENT_BEFORE);

        $user = $this->service->updateCompany($command->getUser());

        $this->dispatcher->dispatch(UserCompanyEvent::EVENT_AFTER,
            new UserCompanyEvent($user)
        );

        return new UserViewCompanyResource($user);
    }
}
