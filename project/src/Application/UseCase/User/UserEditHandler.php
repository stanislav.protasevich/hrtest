<?php

declare(strict_types=1);

namespace Application\UseCase\User;


use Domain\User\Entity\User;
use Domain\User\Resource\UserViewResource;
use Infrastructure\UserBundle\Command\UserEditCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserEditHandler implements UserEditHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserEditCommand $command
     * @return UserViewResource|User
     * @throws \Exception
     */
    public function __invoke(UserEditCommand $command)
    {
        $this->dispatcher->dispatch(UserEvent::EVENT_BEFORE_UPDATE);

        $user = $this->service->update($command->getUser());

        $this->dispatcher->dispatch(UserEvent::EVENT_AFTER_UPDATE,
            new UserEvent($user)
        );

        return new UserViewResource($user);
    }
}