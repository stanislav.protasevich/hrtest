<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User;


use Domain\User\Resource\UserSupervisorsResource;
use Infrastructure\UserBundle\Command\Supervisor\UserAddSupervisorCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserEditSupervisorsHandler implements UserEditSupervisorsHandlerInterface
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    function __construct(UserService $service, EventDispatcherInterface $dispatcher)
    {
        $this->service = $service;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserAddSupervisorCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     * @throws \Exception
     */
    public function __invoke(UserAddSupervisorCommand $command)
    {
        $this->dispatcher->dispatch(UserEvent::EVENT_BEFORE_UPDATE);

        $user = $this->service->updateSupervisors($command->getUser());

        $this->dispatcher->dispatch(UserEvent::EVENT_AFTER_UPDATE,
            new UserEvent($user)
        );

        return UserSupervisorsResource::collection(
            $user->getSupervisors()->getValues()
        );
    }
}