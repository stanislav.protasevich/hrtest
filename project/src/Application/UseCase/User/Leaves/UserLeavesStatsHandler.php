<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User\Leaves;


use Domain\TimeOff\Entity\TimeOffType;
use Domain\TimeOff\Resource\PolicyResource;
use Domain\TimeOff\Resource\TimeOffTypeResource;
use Infrastructure\LeavesBundle\Service\dto\LeaveUserStatsDto;
use Infrastructure\LeavesBundle\Service\LeavesSearchService;
use Infrastructure\LeavesBundle\Service\LeaveStatsService;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeSearchCommand;
use Infrastructure\TimeOffBundle\Service\TimeOffSearchService;
use Infrastructure\UserBundle\Command\Leaves\UserLeavesStatsCommand;

class UserLeavesStatsHandler implements UserLeavesStatsHandlerInterface
{
    /**
     * @var LeavesSearchService
     */
    private $searchService;
    /**
     * @var LeaveStatsService
     */
    private $statsService;

    public function __construct(TimeOffSearchService $searchService, LeaveStatsService $statsService)
    {
        $this->searchService = $searchService;
        $this->statsService = $statsService;
    }

    /**
     * @param UserLeavesStatsCommand $command
     * @return array
     * @throws \Exception
     */
    public function __invoke(UserLeavesStatsCommand $command)
    {
        $searchCommand = new TimeOffTypeSearchCommand();
        $searchCommand->status = TimeOffType::STATUS_ACTIVE;

        $timeOffTypes = $this->searchService->search($searchCommand)->getQuery()->getResult();

        $result = [];

        foreach ($timeOffTypes as $timeOff) {

            /** @var LeaveUserStatsDto $res */
            $res = $this->statsService->userStat($command->getUser(), $timeOff);

            $result[] = [
                'timeOffType' => (new TimeOffTypeResource($timeOff))->toArray(),
                'left' => $res->left,
                'days' => $res->days,
                'total' => $res->total,
                'period' => $res->period,
                'policy' => (new PolicyResource($res->policy))->toArray(),
            ];
        }

        return $result;
    }
}
