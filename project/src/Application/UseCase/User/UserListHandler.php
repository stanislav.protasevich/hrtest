<?php

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\User;

use Domain\User\Resource\UserListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\UserBundle\Command\UserSearchCommand;
use Infrastructure\UserBundle\Service\UserSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserListHandler implements UserListHandlerInterface
{
    /**
     * @var UserSearchService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container, UserSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    /**
     * @param UserSearchCommand $command
     * @return \Infrastructure\CommonBundle\Pagination\PaginatedCollection|\Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(UserSearchCommand $command)
    {
        $qb = $this->service->search($command);
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_user_list');

        return UserListResource::collection($paginated);
    }
}
