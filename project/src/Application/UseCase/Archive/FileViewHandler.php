<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Archive;


use Infrastructure\ArchiveBundle\Command\ArchiveFileViewCommand;
use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileViewHandler implements FileViewHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(ContainerInterface $container, ArchiveFileService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(ArchiveFileViewCommand $command)
    {
        return $this->service->viewOrDownloadFile($command->getFile());
    }
}