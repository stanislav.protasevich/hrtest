<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Archive;

use Infrastructure\ArchiveBundle\Command\ArchiveFileUserAttachCommand;
use Infrastructure\ArchiveBundle\Event\ArchiveFileEvent;
use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FileUserAttachHandler implements FileUserAttachHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        ArchiveFileService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    public function __invoke(ArchiveFileUserAttachCommand $command)
    {
        $this->dispatcher->dispatch(ArchiveFileEvent::EVENT_BEFORE_INSERT);

        $file = $this->service->userAttach($command);

        $this->dispatcher->dispatch(ArchiveFileEvent::EVENT_AFTER_INSERT,
            new ArchiveFileEvent($file)
        );

        return [
            'id' => $file->getId()
        ];
    }
}
