<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Archive;


use Infrastructure\ArchiveBundle\Command\ArchiveFileAttachCommand;
use Infrastructure\ArchiveBundle\Event\ArchiveFileEvent;
use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FileAttachHandler implements FileAttachHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        ArchiveFileService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    public function __invoke(ArchiveFileAttachCommand $command)
    {
        $this->dispatcher->dispatch(ArchiveFileEvent::EVENT_BEFORE_INSERT);

        $file = $this->service->attach($command);

        $this->dispatcher->dispatch(ArchiveFileEvent::EVENT_AFTER_INSERT,
            new ArchiveFileEvent($file)
        );

        return [
            'id' => $file->getId()
        ];
    }
}
