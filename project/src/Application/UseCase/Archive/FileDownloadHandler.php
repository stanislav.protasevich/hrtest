<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Archive;


use Infrastructure\ArchiveBundle\Command\ArchiveFileDownloadCommand;
use Infrastructure\ArchiveBundle\Service\ArchiveFileService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileDownloadHandler implements FileDownloadHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ArchiveFileService
     */
    private $service;

    public function __construct(ContainerInterface $container, ArchiveFileService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(ArchiveFileDownloadCommand $command)
    {
        return $this->service->viewOrDownloadFile($command->getFile(), true);
    }
}