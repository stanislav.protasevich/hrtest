<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company;


use Infrastructure\CompanyBundle\Command\CompanyAddressPrimaryCommand;
use Infrastructure\CompanyBundle\Service\CompanyAddressService;

class CompanyAddressPrimaryHandler implements CompanyAddressPrimaryHandlerInterface
{
    /**
     * @var CompanyAddressService
     */
    private $service;

    public function __construct(CompanyAddressService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyAddressPrimaryCommand $command)
    {
        return $this->service->primaryList();
    }
}
