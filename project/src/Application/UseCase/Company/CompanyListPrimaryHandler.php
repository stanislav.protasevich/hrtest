<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Company;


use Infrastructure\CompanyBundle\Command\CompanyListPrimaryCommand;
use Infrastructure\CompanyBundle\Service\CompanyService;

class CompanyListPrimaryHandler implements CompanyListPrimaryHandlerInterface
{
    /**
     * @var CompanyService
     */
    private $service;

    public function __construct(CompanyService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyListPrimaryCommand $command)
    {
        return $this->service->getAllCompanies();
    }
}