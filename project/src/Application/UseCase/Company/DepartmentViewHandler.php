<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company;


use Domain\Company\Resource\DepartmentDetailResource;
use Infrastructure\CompanyBundle\Command\DepartmentViewCommand;

class DepartmentViewHandler implements DepartmentViewHandlerInterface
{
    /**
     * @param DepartmentViewCommand $command
     * @return DepartmentDetailResource
     */
    public function __invoke(DepartmentViewCommand $command)
    {
        return new DepartmentDetailResource($command->getDepartment());
    }
}