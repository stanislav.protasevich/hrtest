<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company\Recruiters;


use Infrastructure\CompanyBundle\Command\Recruiters\CompanyViewRecruitersRemoveCommand;
use Infrastructure\CompanyBundle\Service\CompanyRecruiterService;

class CompanyViewRecruitersRemoveHandler implements CompanyViewRecruitersRemoveHandlerInterface
{
    /**
     * @var CompanyRecruiterService
     */
    private $service;

    public function __construct(CompanyRecruiterService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyViewRecruitersRemoveCommand $command)
    {
        $this->service->removeRecruiter($command->getCompany(), $command->getRecruiter());

        return true;
    }
}