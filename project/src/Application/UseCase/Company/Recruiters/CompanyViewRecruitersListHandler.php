<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company\Recruiters;


use Domain\Company\Resource\CompanyRecruitersListResource;
use Infrastructure\CompanyBundle\Command\Recruiters\CompanyViewRecruitersListCommand;

class CompanyViewRecruitersListHandler implements CompanyViewRecruitersListHandlerInterface
{
    public function __invoke(CompanyViewRecruitersListCommand $command)
    {
        return CompanyRecruitersListResource::collection(
            $command->getCompany()->getRecruiters()->getValues()
        );
    }
}