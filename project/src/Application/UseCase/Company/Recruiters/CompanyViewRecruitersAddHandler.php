<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company\Recruiters;


use Infrastructure\CompanyBundle\Command\Recruiters\CompanyViewRecruitersAddCommand;
use Infrastructure\CompanyBundle\Service\CompanyRecruiterService;

class CompanyViewRecruitersAddHandler implements CompanyViewRecruitersAddHandlerInterface
{
    /**
     * @var CompanyRecruiterService
     */
    private $service;

    public function __construct(CompanyRecruiterService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyViewRecruitersAddCommand $command)
    {
        $this->service->addRecruiter($command->getCompany(), $command->getRecruiter());

        return true;
    }
}