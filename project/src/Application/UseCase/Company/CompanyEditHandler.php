<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company;

use Domain\Company\Resource\CompanyViewResource;
use Infrastructure\CompanyBundle\Command\CompanyEditCommand;
use Infrastructure\CompanyBundle\Event\CompanyEvent;
use Infrastructure\CompanyBundle\Service\CompanyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompanyEditHandler implements CompanyEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var CompanyService
     */
    private $service;

    function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher, CompanyService $service)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param CompanyEditCommand $command
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(CompanyEditCommand $command)
    {
        $this->dispatcher->dispatch(CompanyEvent::EVENT_BEFORE_UPDATE);

        $company = $this->service->edit($command);

        $this->dispatcher->dispatch(CompanyEvent::EVENT_AFTER_UPDATE,
            new CompanyEvent($company)
        );

        return new CompanyViewResource($command->getCompany());
    }
}
