<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company;

use Domain\Company\Entity\Company;
use Domain\Company\Resource\CompanyViewResource;
use Infrastructure\CompanyBundle\Command\CompanyViewCommand;

class CompanyViewHandler implements CompanyViewHandlerInterface
{
    /**
     * @param CompanyViewCommand $command
     * @return Company|null
     */
    public function __invoke(CompanyViewCommand $command)
    {
        return new CompanyViewResource($command->getCompany());
    }
}