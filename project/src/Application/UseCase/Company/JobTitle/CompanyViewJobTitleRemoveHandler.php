<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company\JobTitle;


use Infrastructure\CompanyBundle\Command\JobTitle\CompanyViewJobTitleRemoveCommand;
use Infrastructure\CompanyBundle\Service\CompanyJobTitleService;

class CompanyViewJobTitleRemoveHandler implements CompanyViewJobTitleRemoveHandlerInterface
{
    /**
     * @var CompanyJobTitleService
     */
    private $service;

    public function __construct(CompanyJobTitleService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyViewJobTitleRemoveCommand $command)
    {
        $this->service->removeJobTitle($command->getCompany(), $command->getJobTitle());
        
        return true;
    }
}