<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company\JobTitle;


use Domain\Company\Resource\CompanyJobTitleListResource;
use Infrastructure\CompanyBundle\Command\JobTitle\CompanyViewJobTitleListCommand;

class CompanyViewJobTitleListHandler implements CompanyViewJobTitleListHandlerInterface
{
    /**
     * @param CompanyViewJobTitleListCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(CompanyViewJobTitleListCommand $command)
    {
        return CompanyJobTitleListResource::collection(
            $command->getCompany()->getJobTitles()->getValues()
        );
    }
}