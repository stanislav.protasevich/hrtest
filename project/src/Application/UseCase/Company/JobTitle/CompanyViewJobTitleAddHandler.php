<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company\JobTitle;


use Infrastructure\CompanyBundle\Command\JobTitle\CompanyViewJobTitleAddCommand;
use Infrastructure\CompanyBundle\Service\CompanyJobTitleService;

class CompanyViewJobTitleAddHandler implements CompanyViewJobTitleAddHandlerInterface
{
    /**
     * @var CompanyJobTitleService
     */
    private $service;

    public function __construct(CompanyJobTitleService $service)
    {
        $this->service = $service;
    }

    public function __invoke(CompanyViewJobTitleAddCommand $command)
    {
        $this->service->addJobTitle($command->getCompany(), $command->getJobTitle());

        return true;
    }
}