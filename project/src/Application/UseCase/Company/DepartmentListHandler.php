<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company;


use Domain\Company\Resource\DepartmentListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\CompanyBundle\Command\DepartmentSearchCommand;
use Infrastructure\CompanyBundle\Service\DepartmentSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DepartmentListHandler implements DepartmentListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var DepartmentSearchService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        DepartmentSearchService $service
    ) {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param DepartmentSearchCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(DepartmentSearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_company_department_list');

        return DepartmentListResource::collection($paginated);
    }
}