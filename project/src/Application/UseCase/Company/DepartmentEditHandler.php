<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Company;


use Infrastructure\CompanyBundle\Command\DepartmentCreateCommand;
use Infrastructure\CompanyBundle\Event\DepartmentEvent;
use Infrastructure\CompanyBundle\Service\DepartmentService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DepartmentEditHandler implements DepartmentEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var DepartmentService
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        DepartmentService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param DepartmentCreateCommand $command
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(DepartmentCreateCommand $command)
    {
        $this->dispatcher->dispatch(DepartmentEvent::EVENT_BEFORE_UPDATE);

        $department = $this->service->edit($command);

        $this->dispatcher->dispatch(DepartmentEvent::EVENT_AFTER_UPDATE,
            new DepartmentEvent(compact('department'))
        );

        return [
            'id' => $department->getId()
        ];
    }
}
