<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);


namespace Application\UseCase\Company;

use Domain\Company\Resource\CompanyListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\CompanyBundle\Command\CompanySearchCommand;
use Infrastructure\CompanyBundle\Service\CompanySearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CompanyListHandler implements CompanyListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CompanySearchService
     */
    private $service;

    public function __construct(ContainerInterface $container, CompanySearchService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * @param CompanySearchCommand $command
     * @return \Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(CompanySearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_company_list');

        return CompanyListResource::collection($paginated);
    }
}