<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Application\UseCase\Company;

use Infrastructure\CompanyBundle\Command\CompanyCreateCommand;
use Infrastructure\CompanyBundle\Event\CompanyEvent;
use Infrastructure\CompanyBundle\Service\CompanyService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompanyCreateHandler implements CompanyCreateHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var CompanyService
     */
    private $service;

    function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        CompanyService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param CompanyCreateCommand $command
     * @return array|string
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(CompanyCreateCommand $command)
    {
        $this->dispatcher->dispatch(CompanyEvent::EVENT_BEFORE_INSERT);

        $company = $this->service->create($command);

        $this->dispatcher->dispatch(CompanyEvent::EVENT_AFTER_INSERT,
            new CompanyEvent($company)
        );

        return [
            'id' => $company->getId()
        ];
    }
}
