<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Employee;


use Infrastructure\EmployeeBundle\Command\EmployeeCountriesPrimaryCommand;
use Infrastructure\EmployeeBundle\Service\EmployeeService;

class EmployeeCountriesPrimaryHandler implements EmployeeCountriesPrimaryHandlerInterface
{
    /**
     * @var EmployeeService
     */
    private $service;

    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    public function __invoke(EmployeeCountriesPrimaryCommand $command)
    {
        return $this->service->getCountriesPrimaryAll();
    }
}