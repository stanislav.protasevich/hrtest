<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Employee;


use Domain\Employee\Resource\CareerHistoryResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\EmployeeBundle\Command\CareerHistorySearchCommand;
use Infrastructure\EmployeeBundle\Service\CareerHistorySearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CareerHistoryHandler implements CareerHistoryHandlerInterface
{
    /**
     * @var CareerHistorySearchService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    function __construct(ContainerInterface $container, CareerHistorySearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    /**
     * @param CareerHistorySearchCommand $command
     * @return \Infrastructure\CommonBundle\Pagination\PaginatedCollection|\Infrastructure\CommonBundle\Resources\Json\JsonResource
     */
    public function __invoke(CareerHistorySearchCommand $command)
    {
        $qb = $this->service->search($command);

        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_employee_career_history');

        return CareerHistoryResource::collection($paginated);
    }
}
