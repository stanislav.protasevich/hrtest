<?php

declare(strict_types=1);

namespace Application\UseCase\Employee;


use Domain\Employee\Entity\EmployeeCompany;
use Domain\Employee\Factory\EmployeeAddFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeAddHandler implements EmployeeAddHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EmployeeAddFactoryInterface
     */
    private $factory;

    function __construct(ContainerInterface $container, EmployeeAddFactoryInterface $factory)
    {
        $this->container = $container;
        $this->factory = $factory;
    }

    /**
     * @param EmployeeCompany $employeeCompany
     * @return EmployeeCompany
     */
    public function __invoke(EmployeeCompany $employeeCompany)
    {
        return $this->factory->save($employeeCompany);
    }
}