<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Employee;


use Domain\Employee\Resource\EmployeeCompanyResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\EmployeeBundle\Command\EmployeeCompanyListCommand;
use Infrastructure\EmployeeBundle\Service\EmployeeCompanySearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmployeeCompanyListHandler implements EmployeeCompanyListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EmployeeCompanySearchService
     */
    private $service;

    public function __construct(ContainerInterface $container, EmployeeCompanySearchService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(EmployeeCompanyListCommand $command)
    {
        $qb = $this->service->search($command);
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_employee_company_list');

        return EmployeeCompanyResource::collection($paginated);
    }
}
