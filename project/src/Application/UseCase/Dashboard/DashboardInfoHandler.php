<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Dashboard;


use Domain\Dashboard\Resources\DashboardInfoResource;
use Infrastructure\DashboardBundle\Command\DashboardInfoCommand;

class DashboardInfoHandler implements DashboardInfoHandlerInterface
{
    public function __invoke(DashboardInfoCommand $command)
    {
        return new DashboardInfoResource(
            $command->getUser()
        );
    }
}
