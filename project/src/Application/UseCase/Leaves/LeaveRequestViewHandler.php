<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Leaves;


use Domain\Leaves\Resources\LeavesViewResource;
use Infrastructure\LeavesBundle\Command\LeavesRequestViewCommand;

class LeaveRequestViewHandler implements LeaveRequestViewHandlerInterface
{
    public function __invoke(LeavesRequestViewCommand $command)
    {
        return new LeavesViewResource($command->getLeaveRequests());
    }
}