<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Leaves;


use Infrastructure\LeavesBundle\Command\LeavesRequestEditCommand;
use Infrastructure\LeavesBundle\Event\LeaveRequestEvent;
use Infrastructure\LeavesBundle\Service\LeaveRequestsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class LeaveRequestEditHandler implements LeaveRequestEditHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var LeaveRequestsService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        LeaveRequestsService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param LeavesRequestEditCommand $command
     * @return array|string
     * @throws \Exception
     */
    public function __invoke(LeavesRequestEditCommand $command)
    {
        $this->dispatcher->dispatch(LeaveRequestEvent::EVENT_BEFORE_UPDATE);

        $leaveRequest = $this->service->update($command);

        $this->dispatcher->dispatch(LeaveRequestEvent::EVENT_AFTER_UPDATE,
            new LeaveRequestEvent($leaveRequest)
        );

        return [
            'id' => $leaveRequest->getId()
        ];
    }
}
