<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Leaves;


use Domain\Leaves\Resources\LeavesListResource;
use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\LeavesBundle\Command\LeavesListCommand;
use Infrastructure\LeavesBundle\Service\LeavesSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LeavesListHandler implements LeavesListHandlerInterface
{
    /**
     * @var LeavesSearchService
     */
    private $service;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, LeavesSearchService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    public function __invoke(LeavesListCommand $command)
    {
        $qb = $this->service->search($command);
        
        /* @var $paginated PaginatedCollection */
        $paginated = $this->container->get('pagination_factory')->createCollection($qb, 'api_leaves_list');

        return LeavesListResource::collection($paginated);
    }
}