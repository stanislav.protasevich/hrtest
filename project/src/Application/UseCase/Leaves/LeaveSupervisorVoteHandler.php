<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\UseCase\Leaves;


use Infrastructure\LeavesBundle\Command\LeaveSupervisorVoteCommand;
use Infrastructure\LeavesBundle\Event\LeaveSupervisorVoteEvent;
use Infrastructure\LeavesBundle\Service\LeaveSupervisorVoteService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class LeaveSupervisorVoteHandler implements LeaveSupervisorVoteHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var LeaveSupervisorVoteService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        LeaveSupervisorVoteService $service
    )
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param LeaveSupervisorVoteCommand $command
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function __invoke(LeaveSupervisorVoteCommand $command)
    {
        $this->dispatcher->dispatch(LeaveSupervisorVoteEvent::EVENT_BEFORE_UPDATE);

        $leaveRequestSupervisor = $this->service->vote($command);

        $this->dispatcher->dispatch(LeaveSupervisorVoteEvent::EVENT_AFTER_UPDATE,
            new LeaveSupervisorVoteEvent($leaveRequestSupervisor)
        );

        return true;
    }
}