<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Leaves;


use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Infrastructure\LeavesBundle\Event\LeaveRequestEvent;
use Infrastructure\LeavesBundle\Service\LeaveRequestsService;
use Infrastructure\TimeOffBundle\Helpers\PolicyHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class LeaveRequestHandler implements LeaveRequestHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var LeaveRequestsService
     */
    private $service;

    public function __construct(
        ContainerInterface $container,
        EventDispatcherInterface $dispatcher,
        LeaveRequestsService $service
    ) {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
        $this->service = $service;
    }

    /**
     * @param LeavesRequestCommand $command
     * @return array|string
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function __invoke(LeavesRequestCommand $command)
    {
        $this->dispatcher->dispatch(LeaveRequestEvent::EVENT_BEFORE_INSERT);

        $policy = PolicyHelper::userPolicy($command->getTimeOffType(), $command->getUser());

        $command->setPolicy($policy);
        $leaveRequest = $this->service->create($command);

        $this->dispatcher->dispatch(LeaveRequestEvent::EVENT_AFTER_INSERT,
            new LeaveRequestEvent($leaveRequest)
        );

        return [
            'id' => $leaveRequest->getId()
        ];
    }
}
