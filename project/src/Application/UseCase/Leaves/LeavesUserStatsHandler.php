<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\Leaves;


use Domain\Leaves\Resources\LeavesUserStatsResource;
use Infrastructure\LeavesBundle\Command\LeavesUserStatsCommand;
use Infrastructure\LeavesBundle\Service\LeaveStatsService;

class LeavesUserStatsHandler implements LeavesUserStatsHandlerInterface
{
    /**
     * @var LeaveStatsService
     */
    private $service;

    public function __construct(LeaveStatsService $service)
    {
        $this->service = $service;
    }

    public function __invoke(LeavesUserStatsCommand $command)
    {
        $result = $this->service->userStat(
            $command->getUser(),
            $command->getTimeOffType()
        );

        return new LeavesUserStatsResource($result);
    }
}
