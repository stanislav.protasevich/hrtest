<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\UseCase\System;


use Domain\System\Resource\SystemCurrencyResource;
use Infrastructure\SystemBundle\Command\CurrencyListCommand;
use Infrastructure\SystemBundle\Service\SystemCurrencyService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CurrencyListHandler implements CurrencyListHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var SystemCurrencyService
     */
    private $service;

    function __construct(ContainerInterface $container, SystemCurrencyService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    public function __invoke(CurrencyListCommand $command)
    {
        return SystemCurrencyResource::collection($this->service->getAll());
    }
}