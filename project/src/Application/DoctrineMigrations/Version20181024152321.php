<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\Migrations;

use Carbon\Carbon;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Domain\Leaves\Entity\Holiday;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181024152321 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $holidays = [
            [
                'name' => '1 January',
                'month' => 1,
                'day' => 1,
            ],
            [
                'name' => '2 January',
                'month' => 1,
                'day' => 2,
            ],
            [
                'name' => '7 January',
                'month' => 1,
                'day' => 7,
            ],
            [
                'name' => '8 January',
                'month' => 1,
                'day' => 8,
            ]
        ];

        foreach ($holidays as $item) {
            $holiday = new Holiday();
            $holiday->setName($item['name']);
            $holiday->setDate(
                Carbon::create(null, $item['month'], $item['day'])
            );

            $em->persist($holiday);
        }

        $em->flush();
    }

    public function down(Schema $schema): void
    {
    }
}