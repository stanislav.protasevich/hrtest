<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Infrastructure\TimeOffBundle\Rbac\PolicyPermissions;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190128092259 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var AuthItemService $service */
        $service = $this->get(AuthItemService::class);

        foreach ((new PolicyPermissions())->getPermissions() as $item) {
            $service->createFromAnonItem($item);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
