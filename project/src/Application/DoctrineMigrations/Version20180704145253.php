<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Domain\User\Entity\ConnectedAccountProvider;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180704145253 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $connected_account_provider = new ConnectedAccountProvider();
        $connected_account_provider->setName('google');
        $connected_account_provider->setSortOrder(1);
        $connected_account_provider->setStatus(ConnectedAccountProvider::STATUS_ACTIVE);
        $connected_account_provider->setOptions([
            'client_id' => '112876747111-kacmbmr3u70b5s9bpei39u39fd5a62qe.apps.googleusercontent.com',
            'client_secret' => 'batSTy4MQ6ZlE0BJFPBsdj6w',
        ]);

        $em->persist($connected_account_provider);
        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }
}
