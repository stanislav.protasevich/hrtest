<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\AuthItemService;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190109102527 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var AuthItemService $service */
        $service = $this->get(AuthItemService::class);

        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, RbacPermissions::ROLE_USER);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, RbacPermissions::ROLE_RECRUITER);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, RbacPermissions::ROLE_HR);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, RbacPermissions::ROLE_HR_HEAD);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, RbacPermissions::ROLE_PROJECT_HEAD);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, LeavesPermissions::PERMISSION_LEAVES_GRANT_ALL);
        $service->addChildrenByString(RbacPermissions::ROLE_ADMINISTRATOR, LeavesPermissions::PERMISSION_LEAVES_LIST_ALL);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
