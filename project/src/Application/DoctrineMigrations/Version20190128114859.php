<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\BlogBundle\Rbac\Rules\BlogCommentEditRule;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\RbacBundle\Service\AuthItemService;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190128114859 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var AuthItemService $service */
        $service = $this->get(AuthItemService::class);

        $service->createRule('BlogCommentEditRule', BlogCommentEditRule::class);

        foreach ((new BlogPermissions())->getPermissions() as $item) {
            $service->createFromAnonItem($item);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
