<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Application\Migrations;


use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Domain\Rbac\Entity\AuthRule;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\UserBundle\Rbac\Rules\UserViewRule;


class Version20181217160652 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $now = new \DateTime();

        $rule = new AuthRule();
        $rule->setName('UserViewRule');
        $rule->setData(UserViewRule::class);
        $rule->setCreatedAt($now);
        $rule->setUpdatedAt($now);

        $em->persist($rule);
        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
