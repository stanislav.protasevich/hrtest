<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Domain\User\Entity\UserField;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181213113107 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $em = $this->getEm();

        $list = [
            [
                'id' => UserField::GROUP_CONTACT_EMAIL,
                'group' => UserField::GROUP_CONTACT,
                'sortOrder' => 1,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_EMAIL
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_CONTACT_PHONE,
                'group' => UserField::GROUP_CONTACT,
                'sortOrder' => 2,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_PHONE
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_CONTACT_SKYPE,
                'group' => UserField::GROUP_CONTACT,
                'sortOrder' => 3,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_SOCIAL
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_CONTACT_SECOND_PHONE,
                'group' => UserField::GROUP_CONTACT,
                'sortOrder' => 4,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_PHONE
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_CONTACT_TELEGRAM,
                'group' => UserField::GROUP_CONTACT,
                'sortOrder' => 5,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_PHONE
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_SOCIAL_LINKEDIN,
                'group' => UserField::GROUP_SOCIAL,
                'sortOrder' => 6,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_SOCIAL
                ],
                'matchLength' => 30,
                'required' => true,
            ],
            [
                'id' => UserField::GROUP_SOCIAL_FACEBOOK,
                'group' => UserField::GROUP_SOCIAL,
                'sortOrder' => 7,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_SOCIAL
                ],
                'matchLength' => 30,
                'required' => false,
            ],
            [
                'id' => UserField::GROUP_SOCIAL_INSTAGRAM,
                'group' => UserField::GROUP_SOCIAL,
                'sortOrder' => 8,
                'matchType' => UserField::MATCH_TYPE_REGEX,
                'matchParams' => [
                    "regex" => UserField::REGEX_SOCIAL
                ],
                'matchLength' => 30,
                'required' => false,
            ],
        ];

        foreach ($list as $item) {
            $field = new UserField();
            $field->setId($item['id']);
            $field->setGroup($item['group']);
            $field->setSortOrder($item['sortOrder']);
            $field->setMatchType($item['matchType']);
            $field->setMatchParams($item['matchParams']);
            $field->setMatchLength($item['matchLength']);
            $field->setRequired($item['required']);

            $em->persist($field);
            $em->flush();
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
