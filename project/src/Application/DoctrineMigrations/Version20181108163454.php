<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Domain\Rbac\Entity\AuthItem;
use Domain\Rbac\Entity\AuthRule;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\UserBundle\Rbac\Rules\PerformanceRule;
use Infrastructure\UserBundle\Rbac\UserPermissions;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181108163454 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $em = $this->getEm();

        $now = new \DateTime();

        $rule = new AuthRule();
        $rule->setName('PerformanceRule');
        $rule->setData(PerformanceRule::class);
        $rule->setCreatedAt($now);
        $rule->setUpdatedAt($now);


        $permissions = [
            UserPermissions::PERMISSION_USER_PERFORMANCE_LIST,
            UserPermissions::PERMISSION_USER_PERFORMANCE_ADD,
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_ADD,
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_EDIT,
            UserPermissions::PERMISSION_USER_PERFORMANCE_FORM_VIEW,
        ];

        $em->persist($rule);

        foreach ($permissions as $permission) {
            $authItem = $this->getEm()->getRepository(AuthItem::class)->find($permission);
            $authItem->setRule($rule);
            $authItem->setUpdatedAt($now);
            $em->persist($authItem);
        }

        $em->flush();
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }
}
