<?php declare(strict_types=1);

namespace Application\Migrations;

use Carbon\Carbon;
use Doctrine\DBAL\Migrations\Version;
use Doctrine\DBAL\Schema\Schema;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\CompanyAddress;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Entity\JobTitle;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Domain\System\Entity\Currency;
use Domain\User\Entity\User;
use Domain\User\Entity\UserEmployee;
use Domain\User\Entity\UserProfile;
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\CommonBundle\Helpers\NumberHelper;
use Infrastructure\EmployeeBundle\Service\CareerHistoryService;
use Infrastructure\UserBundle\Command\UserCreateCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Helpers\UserProfileHelper;
use Infrastructure\UserBundle\Service\UserPerformanceService;
use Infrastructure\UserBundle\Service\UserService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181224150601 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $projectsFile = file(__DIR__ . '/sql/projects.sql');

        $projects = [];

        $em = $this->getEm();

        $now = new \DateTime();

        $address = new CompanyAddress();
        $address->setCountry($em->getRepository(Country::class)->findOneBy([]));
        $address->setRegion($em->getRepository(Region::class)->findOneBy([]));
        $address->setCity($em->getRepository(City::class)->findOneBy([]));
        $address->setType(CompanyAddress::TYPE_ACTUAL);
        $address->setStreet('');

        foreach ($projectsFile as $file) {
            if (preg_match('/\((\d+), (.*?)\)/', $file, $matches)) {

                $company = new Company();
                $company->setName($this->replaceQuotes($matches[2]));
                $company->setStatus(Company::STATUS_ACTIVE);
                $company->setCreatedAt($now);
                $company->setUpdatedAt($now);

                $address = new CompanyAddress();
                $address->setCountry($em->getRepository(Country::class)->findOneBy([]));
                $address->setRegion($em->getRepository(Region::class)->findOneBy([]));
                $address->setCity($em->getRepository(City::class)->findOneBy([]));
                $address->setType(CompanyAddress::TYPE_ACTUAL);
                $address->setStreet('');

                $company->setAddress($address);

                $em->persist($address);
                $em->persist($company);

                $projects[$matches[1]] = $company;
            }
        }
        $em->flush();

        $employeesFile = file(__DIR__ . '/sql/employees.sql');

        $employees = [];

        foreach ($employeesFile as $file) {
            if (preg_match('/\((\d+), (.*?), (\d+)\)/', $file, $matches)) {
                $employees[$matches[1]] = $matches;
            }
        }

        $groupsFile = file(__DIR__ . '/sql/groups.sql');

        $groups = [];
        foreach ($groupsFile as $file) {
            if (preg_match('/\((\d+), (\d+), (.*?)\)/', $file, $matches)) {
                $groups[$matches[1]] = [
                    'project_id' => $matches[2],
                    'name' => $matches[3],
                ];
            }
        }

        $employeeGroupsFile = file(__DIR__ . '/sql/employee_groups.sql');

        $currency = $em->getRepository(Currency::class)->findOneBy([]);

        /** @var UserService $userService */
        $userService = $this->get(UserService::class);
        /** @var UserPerformanceService $userPerformanceService */
        $userPerformanceService = $this->get(UserPerformanceService::class);
        /** @var CareerHistoryService $careerHistoryService */
        $careerHistoryService = $this->get(CareerHistoryService::class);

        $admin = $em->getRepository(User::class)->findOneBy([]);

        $userService->setUser($admin);
        $userPerformanceService->setUser($em->getRepository(User::class)->findOneBy([]));
        $careerHistoryService->setUser($em->getRepository(User::class)->findOneBy([]));

        /** @var EventDispatcherInterface $event */
        $event = $this->get('eventDispatcher');

        foreach ($employeeGroupsFile as $file) {
            if (preg_match('/\((\d+), (\d+), (\d+), (.*?), (\d+)\)/', $file, $matches)) {

                /** @var User $userData */
                $userData = $employees[$matches[3]] ?? null;

                $group = $groups[$matches[2]] ?? null;
                $projectId = $group['project_id'];

                /** @var Company $company */
                $company = $projects[$projectId] ?? null;

                /////
                $command = new UserCreateCommand();

                $profile = new UserProfile();

                $names = explode(' ', $this->replaceQuotes($userData[2]));
                $names = array_values(array_filter($names));

                $profile->setPhone('');
                $profile->setMaritalStatus(false);

                $profile->setFirstName($this->trim($names[1]));
                $profile->setSurname($this->trim($names[0]));

                $profile->setGender(UserProfileHelper::getGender($profile->getFirstName()));
                $profile->setBirthday(new \DateTime());
                $profile->setPhone(NumberHelper::randomNumber(10));

                if (count($names) === 3) {
                    $profile->setMiddleName($this->trim($names[2]));
                }

                $command->profile = $profile;

                $employee = new UserEmployee();
                $employee->setCompanyAddress($em->getRepository(CompanyAddress::class)->findOneBy([]));
                $employee->setJobOfferDate(Carbon::now()->subDays(rand(100, 300)));

                $command->employee = $employee;

                $jobTitle = $this->createOrGetJobTitle($this->replaceQuotes($matches[4]));

                $employeeCompany = new EmployeeCompany();
                $employeeCompany->setCompany($company);
                $employeeCompany->setJobTitle($jobTitle);
                $employeeCompany->setJobTitle($jobTitle);
                $employeeCompany->setIsMain(true);
                $employeeCompany->setCreatedAt($now);
                $employeeCompany->setUpdatedAt($now);
                $employeeCompany->setStatus(EmployeeCompany::STATUS_EMPLOYED);
                $employeeCompany->setSalary(0);
                $employeeCompany->setCurrency($currency);
                $employeeCompany->setWorkType(EmployeeCompany::WORK_TYPE_FULL_TIME);
                $employeeCompany->setStartAt($now);

                $command->employeeCompany = $employeeCompany;
                $command->recruiters = [
                    $admin
                ];

                $user = $userService->create($command);

                $event->dispatch(UserEvent::EVENT_AFTER_INSERT,
                    new UserEvent($user)
                );
            } else {
                prnx($file);
            }
        }
    }

    protected function replaceQuotes(string $str): string
    {
        return $this->trim(
            str_replace("'", '', $str)
        );
    }

    protected function trim(string $str)
    {
        return trim($str);
    }

    protected function createOrGetJobTitle(string $name): JobTitle
    {
        $em = $this->getEm();
        $jobTitle = $em->getRepository(JobTitle::class)->findOneBy(['name' => $name]);

        if (!$jobTitle) {
            $jobTitle = new JobTitle();
            $jobTitle->setName($name);
            $jobTitle->setStatus(JobTitle::STATUS_ACTIVE);
            $jobTitle->setCreatedAt(new \DateTime());
            $jobTitle->setUpdatedAt(new \DateTime());

            $em->persist($jobTitle);
            $em->flush();
        }

        return $jobTitle;
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }
}
