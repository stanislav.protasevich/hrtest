<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Infrastructure\ArchiveBundle\Rbac\{ArchivePermissions, CanSeeEditOwnArchiveRbacRule};
use Infrastructure\CommonBundle\Doctrine\Migrations\AbstractMigration;
use Infrastructure\CompanyBundle\Rbac\{CompanyEditViewOwnRbacRule, CompanyPermissions};
use Infrastructure\EmployeeBundle\Rbac\CareerHistoryPermissions;
use Infrastructure\JobBundle\Rbac\JobPermissions;
use Infrastructure\LeavesBundle\Rbac\{LeaveEditViewOwnRbacRule, LeavesPermissions};
use Infrastructure\LocationBundle\Rbac\LocationPermissions;
use Infrastructure\RbacBundle\{Rbac\RbacPermissions, Service\AuthItemService};
use Infrastructure\SystemBundle\Rbac\CurrencyPermissions;
use Infrastructure\TimeOffBundle\Rbac\{PolicyPermissions, TimeOffTypePermissions};
use Infrastructure\UserBundle\Rbac\{SkillPermissions, UserPermissions};
use Infrastructure\UserBundle\Rbac\Rules\{PerformanceAddRule, PerformanceEditRule, PerformanceRule, PerformanceViewRule, UserSelfEditRule, UserSelfHistoryRule, UserViewRule};


/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218081343 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $permissions = [
            (new RbacPermissions())->getPermissions(),
            (new UserPermissions())->getPermissions(),
            (new ArchivePermissions())->getPermissions(),
            (new CompanyPermissions())->getPermissions(),
            (new CareerHistoryPermissions())->getPermissions(),
            (new JobPermissions())->getPermissions(),
            (new LeavesPermissions())->getPermissions(),
            (new LocationPermissions())->getPermissions(),
            (new CurrencyPermissions())->getPermissions(),
            (new PolicyPermissions())->getPermissions(),
            (new TimeOffTypePermissions())->getPermissions(),
            (new SkillPermissions())->getPermissions()
        ];

        /** @var AuthItemService $service */
        $service = $this->get(AuthItemService::class);

        $service->createRule('CanSeeEditOwnArchiveRbacRule', CanSeeEditOwnArchiveRbacRule::class);

        $service->createRule('CompanyEditViewOwnRbacRule', CompanyEditViewOwnRbacRule::class);

        $service->createRule('LeaveEditViewOwnRbacRule', LeaveEditViewOwnRbacRule::class);

        $service->createRule('PerformanceRule', PerformanceRule::class);
        $service->createRule('PerformanceViewRule', PerformanceViewRule::class);
        $service->createRule('PerformanceAddRule', PerformanceAddRule::class);
        $service->createRule('PerformanceEditRule', PerformanceEditRule::class);

        $service->createRule('UserViewRule', UserViewRule::class);
        $service->createRule('UserSelfEditRule', UserSelfEditRule::class);
        $service->createRule('UserSelfHistoryRule', UserSelfHistoryRule::class);

        foreach ($permissions as $permission) {

            foreach ($permission as $item) {
                $service->createFromAnonItem($item);
            }
            
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
