<?php
if (!function_exists('app')) {
    function app(string $name)
    {
        return container()->get($name);
    }
}

if (!function_exists('container')) {
    function container(): \Symfony\Component\DependencyInjection\ContainerInterface
    {
        return kernel()->getContainer();
    }
}

if (!function_exists('em')) {
    function em(): \Doctrine\ORM\EntityManager
    {
        return app('doctrine.orm.default_entity_manager');
    }
}

if (!function_exists('kernel')) {
    function kernel(): \Symfony\Component\HttpKernel\Kernel
    {
        return \Application\Kernel::getInstance();
    }
}

if (!function_exists('event')) {
    function event($eventName, \Symfony\Component\EventDispatcher\Event $event = null)
    {
        /* @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = app('event_dispatcher');
        $dispatcher->dispatch($eventName, $event);
    }
}

if (!function_exists('getParameter')) {
    function getParameter(string $name)
    {
        return container()->getParameter($name);
    }
}

if (!function_exists('view')) {
    function view($content = null, int $statusCode = null, array $headers = [])
    {
        return response($content, $statusCode, $headers);
    }
}

if (!function_exists('trans')) {
    function trans(string $phrase)
    {
        return app('translator')->trans($phrase);
    }
}

if (!function_exists('root_path')) {
    function root_path()
    {
        $path = getParameter('kernel.root_dir');

        return realpath($path . '/../../');
    }
}

if (!function_exists('response')) {
    /**
     * Return a new response from the application.
     *
     * @param  string|array $content
     * @param  int $status
     * @param  array $headers
     * @return \Symfony\Component\HttpFoundation\Response|\Infrastructure\CommonBundle\Http\ResponseFactory
     */
    function response($content = '', $status = 200, array $headers = [])
    {
        $factory = new \Infrastructure\CommonBundle\Http\ResponseFactory();

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->make($content, $status, $headers);
    }
}
