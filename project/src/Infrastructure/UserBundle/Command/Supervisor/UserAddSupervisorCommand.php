<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Supervisor;


use Doctrine\Common\Collections\ArrayCollection;
use Domain\User\Entity\User;

class UserAddSupervisorCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var ArrayCollection
     */
    private $supervisors;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->supervisors = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Add supervisor.
     *
     * @param \Domain\User\Entity\User $supervisor
     *
     * @return UserAddSupervisorCommand
     */
    public function addSupervisor(User $supervisor)
    {
        $this->supervisors[] = $supervisor;

        return $this;
    }

    /**
     * Remove supervisor.
     *
     * @param \Domain\User\Entity\User $supervisor
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSupervisor(User $supervisor)
    {
        return $this->supervisors->removeElement($supervisor);
    }

    /**
     * Get supervisors.
     *
     * @return ArrayCollection
     */
    public function getSupervisors()
    {
        return $this->supervisors;
    }
}