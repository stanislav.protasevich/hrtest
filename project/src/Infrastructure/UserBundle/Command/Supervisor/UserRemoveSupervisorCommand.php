<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Supervisor;

class UserRemoveSupervisorCommand extends UserAddSupervisorCommand
{

}