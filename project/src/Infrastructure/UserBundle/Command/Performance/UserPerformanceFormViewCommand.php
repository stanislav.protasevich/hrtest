<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Performance;


use Domain\User\Entity\UserPerformanceForm;

class UserPerformanceFormViewCommand
{
    /**
     * @var UserPerformanceForm
     */
    private $form;

    public function __construct(UserPerformanceForm $form)
    {
        $this->form = $form;
    }

    /**
     * @return UserPerformanceForm
     */
    public function getForm(): UserPerformanceForm
    {
        return $this->form;
    }
}