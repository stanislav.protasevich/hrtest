<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Performance;


use Domain\User\Entity\User;
use Symfony\Component\Form\Form;

class UserPerformanceFormTemplateCommand
{
    /**
     * @var array
     */
    private $template;
    /**
     * @var User
     */
    private $user;
    /**
     * @var Form
     */
    private $form;

    public function __construct(array $template, User $user)
    {
        $this->template = $template;
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function getTemplate(): array
    {
        return $this->template;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }

    /**
     * @param Form $form
     */
    public function setForm(Form $form): void
    {
        $this->form = $form;
    }
}