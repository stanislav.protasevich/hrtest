<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Command\Performance;


use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;

class UserPerformanceFormDownloadCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var UserPerformance
     */
    private $performance;
    /**
     * @var UserPerformanceForm
     */
    private $form;

    public function __construct(User $user, UserPerformance $performance, UserPerformanceForm $form)
    {
        $this->user = $user;
        $this->performance = $performance;
        $this->form = $form;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return UserPerformance
     */
    public function getPerformance(): UserPerformance
    {
        return $this->performance;
    }

    /**
     * @return UserPerformanceForm
     */
    public function getForm(): UserPerformanceForm
    {
        return $this->form;
    }
}