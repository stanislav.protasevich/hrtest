<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Performance;


use Infrastructure\UserBundle\ValueObject\UserPerformanceFormVo;

class UserPerformanceFormAddCommand
{
    /**
     * @var UserPerformanceFormVo
     */
    private $userPerformanceForm;

    public function __construct(UserPerformanceFormVo $userPerformanceForm)
    {
        $this->userPerformanceForm = $userPerformanceForm;
    }

    /**
     * @return UserPerformanceFormVo
     */
    public function getUserPerformanceForm(): UserPerformanceFormVo
    {
        return $this->userPerformanceForm;
    }
}