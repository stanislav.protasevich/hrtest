<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\History;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;

class UserHistoryListCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $startDate;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $endDate;
    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $status;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $duration;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $reason;

    /**
     * @var string
     */
    private $user;

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser(string $user): void
    {
        $this->user = $user;
    }
}