<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Company;

use Infrastructure\UserBundle\Command\UserViewCommand;

class UserViewCompanyCommand extends UserViewCommand
{
}