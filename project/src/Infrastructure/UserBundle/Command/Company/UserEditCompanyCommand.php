<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Company;

use Domain\User\Entity\User;
use Infrastructure\UserBundle\Command\UserCommand;

class UserEditCompanyCommand extends UserCommand
{
    /**
     * @var array
     */
    private $oldCompanies;
    /**
     * @var array
     */
    private $companies;

    public function __construct(User $user, array $oldCompanies = [], array $companies = [])
    {
        parent::__construct($user);
        $this->oldCompanies = $oldCompanies;
        $this->companies = $companies;
    }

    /**
     * @return array
     */
    public function getOldCompanies(): array
    {
        return $this->oldCompanies;
    }

    /**
     * @return array
     */
    public function getCompanies(): array
    {
        return $this->companies;
    }
}
