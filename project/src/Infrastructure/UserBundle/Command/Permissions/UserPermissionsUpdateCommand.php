<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Permissions;


use Doctrine\Common\Collections\ArrayCollection;
use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;

class UserPermissionsUpdateCommand
{
    /**
     * @var ArrayCollection|AuthItem[]
     */
    private $items;

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return ArrayCollection|AuthItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add add.
     *
     * @param AuthItem|null $item
     * @return UserPermissionsUpdateCommand
     */
    public function addItem(?AuthItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item.
     *
     * @param AuthItem|null $item
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeItem(?AuthItem $item)
    {
        return $this->items->removeElement($item);
    }


    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}