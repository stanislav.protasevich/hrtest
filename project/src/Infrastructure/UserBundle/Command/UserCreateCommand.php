<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command;

use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Domain\User\Entity\UserEmployee;
use Domain\User\Entity\UserProfile;

class UserCreateCommand
{
    /**
     * @var User[]
     */
    public $recruiters = [];
    /**
     * @var UserProfile
     */
    public $profile;
    /**
     * @var UserEmployee
     */
    public $employee;
    /**
     * @var EmployeeCompany
     */
    public $employeeCompany;

    /**
     * @var string
     */
    public $status = User::STATUS_DRAFT;

    /**
     * @var string
     */
    public $password;
}
