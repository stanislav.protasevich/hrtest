<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Alert;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;

class UserAlertHistoryCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $user;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $createdAt;

    /**
     * @var User
     */
    private $alertedUser;

    /**
     * @return User
     */
    public function getAlertedUser(): User
    {
        return $this->alertedUser;
    }

    /**
     * @param User $alertedUser
     */
    public function setAlertedUser(User $alertedUser): void
    {
        $this->alertedUser = $alertedUser;
    }
}