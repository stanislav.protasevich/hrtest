<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Alert;

use Domain\User\Entity\User;

class UserAlertCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $contentType;
    /**
     * @var string
     */
    private $contentId;
    /**
     * @var string
     */
    private $action;
    /**
     * @var User
     */
    private $alertedUser;
    /**
     * @var object
     */
    private $meta;
    /**
     * @var string
     */
    private $message;

    public function __construct(User $user, string $contentType, string $contentId, string $action, User $alertedUser, string $message, $meta = null)
    {
        $this->user = $user;
        $this->contentType = $contentType;
        $this->contentId = $contentId;
        $this->action = $action;
        $this->alertedUser = $alertedUser;
        $this->meta = $meta;
        $this->message = $message;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }

    /**
     * @return string
     */
    public function getContentId(): string
    {
        return $this->contentId;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @return User
     */
    public function getAlertedUser(): User
    {
        return $this->alertedUser;
    }

    /**
     * @return object
     */
    public function getMeta(): ?object
    {
        return $this->meta;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
}
