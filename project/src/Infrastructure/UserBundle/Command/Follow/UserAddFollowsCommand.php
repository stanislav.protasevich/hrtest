<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Follow;


use Doctrine\Common\Collections\ArrayCollection;
use Domain\User\Entity\User;

class UserAddFollowsCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var ArrayCollection
     */
    private $followers;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->followers = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Add follower.
     *
     * @param \Domain\User\Entity\User $follower
     *
     * @return UserAddFollowsCommand
     */
    public function addFollower(?User $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower.
     *
     * @param \Domain\User\Entity\User $follower
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFollower(?User $follower)
    {
        return $this->followers->removeElement($follower);
    }

    /**
     * Get followers.
     *
     * @return \Doctrine\Common\Collections\Collection|User[]
     */
    public function getFollowers()
    {
        return $this->followers;
    }
}