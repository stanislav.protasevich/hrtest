<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Follow;


use Domain\User\Entity\User;

class UserListFollowsCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var null|string
     */
    private $name;

    public function __construct(User $user, ?string $name)
    {
        $this->user = $user;
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
}