<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class UserSearchCommand
{
    use ObjectebleTrait;

    /**
     * @var int|int[]
     */
    public $id;

    /**
     * @var string|string[]
     */
    public $status;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string|string[]
     */
    public $city;

    /**
     * @var string|string[]
     */
    public $company;

    /**
     * @var string|string[]
     */
    public $jobTitle;

    /**
     * @var string|string[]
     */
    public $atWork;

    /**
     * @var string|string[]
     */
    public $birthday;
}
