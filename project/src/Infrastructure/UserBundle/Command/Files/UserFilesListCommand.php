<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Command\Files;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;

class UserFilesListCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $createdAt;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $size;

    /**
     * @var User
     */
    private $user;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }
}