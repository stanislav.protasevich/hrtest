<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserFollowEvent extends Event
{
    public const EVENT_BEFORE_ADD = 'beforeUserFollowEventAdd';
    public const EVENT_AFTER_ADD = 'afterUserFollowEventAdd';

    public const EVENT_BEFORE_REMOVE = 'beforeUserFollowEventRemove';
    public const EVENT_AFTER_REMOVE = 'afterUserFollowEventRemove';

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
