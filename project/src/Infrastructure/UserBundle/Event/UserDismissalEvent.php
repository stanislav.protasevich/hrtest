<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserDismissalEvent extends Event
{
    public const EVENT_BEFORE_DISMISSAL = 'beforeUserDismissalEvent';
    public const EVENT_AFTER_DISMISSAL = 'afterUserDismissalEvent';
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}