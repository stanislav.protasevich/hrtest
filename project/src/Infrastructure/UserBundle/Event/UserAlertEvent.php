<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\UserAlert;
use Symfony\Component\EventDispatcher\Event;

class UserAlertEvent extends Event
{
    public const EVENT_ALERT = 'userAlert';

    /**
     * @var UserAlert
     */
    private $alert;

    public function __construct(UserAlert $alert)
    {
        $this->alert = $alert;
    }
}