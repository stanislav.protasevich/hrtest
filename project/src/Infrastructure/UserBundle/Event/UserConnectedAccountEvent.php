<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class UserConnectedAccountEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'userConnectedAccountEventBeforeInsert';
    public const EVENT_AFTER_INSERT = 'userConnectedAccountEventAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'userConnectedAccountEventBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'userConnectedAccountEventAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'userConnectedAccountEventBeforeDelete';
    public const EVENT_AFTER_DELETE = 'userConnectedAccountEventAfterDelete';

    public const EVENT_BEFORE_FIND = 'userConnectedAccountEventBeforeFind';
    public const EVENT_AFTER_FIND = 'userConnectedAccountEventAfterFind';

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}