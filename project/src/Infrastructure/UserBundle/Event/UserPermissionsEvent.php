<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Event;

use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserPermissionsEvent extends Event
{
    public const EVENT_BEFORE_UPDATE = 'beforeUserPermissionsEventUpdate';
    public const EVENT_AFTER_UPDATE = 'afterUserPermissionsEventUpdate';

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}