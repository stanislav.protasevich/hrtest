<?php declare(strict_types=1);

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserCompanyEvent extends Event
{
    public const EVENT_BEFORE = 'beforeUserCompanyEvent';
    public const EVENT_AFTER = 'afterUserCompanyEvent';

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
