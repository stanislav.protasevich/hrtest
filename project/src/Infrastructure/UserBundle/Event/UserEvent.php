<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class UserEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeUserEventInsert';
    public const EVENT_AFTER_INSERT = 'afterUserEventInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeUserEventUpdate';
    public const EVENT_AFTER_UPDATE = 'afterUserEventUpdate';

    public const EVENT_BEFORE_DELETE = 'beforeUserEventDelete';
    public const EVENT_AFTER_DELETE = 'afterUserEventDelete';

    public const EVENT_BEFORE_FIND = 'beforeUserEventFind';
    public const EVENT_AFTER_FIND = 'afterUserEventFind';
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
