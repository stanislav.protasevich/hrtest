<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Event;


use Domain\User\Entity\UserPerformance;
use Symfony\Component\EventDispatcher\Event;

class UserPerformanceEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeUserPerformanceEventInsert';
    public const EVENT_AFTER_INSERT = 'afterUserPerformanceEventInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeUserPerformanceEventUpdate';
    public const EVENT_AFTER_UPDATE = 'afterUserPerformanceEventUpdate';
    /**
     * @var UserPerformance
     */
    private $performance;

    public function __construct(UserPerformance $performance)
    {
        $this->performance = $performance;
    }

    /**
     * @return UserPerformance
     */
    public function getPerformance(): UserPerformance
    {
        return $this->performance;
    }
}