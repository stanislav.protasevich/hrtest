<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class UserPasswordResetRequestEvent extends Event
{
    public const EVENT_ERROR_VALIDATION = 'userPasswordResetRequestErrorValidation';

    public const EVENT_BEFORE_REQUEST = 'userPasswordResetRequestBeforeRequest';
    public const EVENT_AFTER_REQUEST = 'userPasswordResetRequestAfterRequest';

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}