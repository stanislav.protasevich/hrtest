<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Console;

use Infrastructure\CommonBundle\Console\AbstractBusCommand;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceOutdatedStatusCommand as Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserPerformanceOutdatedStatusCommand extends AbstractBusCommand
{
    protected static $defaultName = 'user:performance:outdated-status';

    protected function configure()
    {
        $this->setDescription('Change outdated performance status.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $result = $this->handle(
            new Command()
        );

        $count = count($result);

        $io->success("Change status in {$count} performance reviews");
    }
}