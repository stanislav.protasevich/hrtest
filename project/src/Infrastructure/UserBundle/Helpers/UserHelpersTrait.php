<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Helpers;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @property AbstractBusController $this
 */
trait UserHelpersTrait
{
    public function findOrFail(string $id): User
    {
        $user = $this->getEm()->getRepository(User::class)->findOneByUuid($id);

        if (is_null($user)) {
            throw new ModelNotFoundException(User::class, $id);
        }

        return $user;
    }
}
