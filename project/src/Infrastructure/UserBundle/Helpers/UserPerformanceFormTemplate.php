<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Helpers;


use Infrastructure\CommonBundle\Helpers\Json;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserPerformanceFormTemplate
{
    public static function content(string $name, string $author): array
    {
        return Json::decode(
            file_get_contents(
                self::path($name, $author)
            )
        );
    }

    public static function path(string $name, string $author): string
    {
        $path = root_path().'/src/Infrastructure/UserBundle/templates/pe/'.$name.'_'.$author.'.json';

        if (!file_exists($path)) {
            throw new NotFoundHttpException('Template not found.');
        }

        return $path;
    }
}