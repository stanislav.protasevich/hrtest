<?php
declare(strict_types=1);

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Helpers;


use Domain\User\Entity\UserProfile;

class UserProfileHelper
{
    public static function getGender(string $name): string
    {
        return in_array(mb_strtolower(mb_substr($name, -1)), ['а', 'я']) ? UserProfile::GENDER_WOMAN : UserProfile::GENDER_MAN;
    }
}
