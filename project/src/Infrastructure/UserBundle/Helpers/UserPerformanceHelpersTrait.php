<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Helpers;


use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @property AbstractBusController $this
 */
trait UserPerformanceHelpersTrait
{
    public function findOrNotFoundPerformance(User $user, string $performanceId): UserPerformance
    {
        $performance = $this->getEm()->getRepository(UserPerformance::class)->findOneBy([
            'user' => $user,
            'id' => $performanceId,
        ]);

        if (is_null($performance)) {
            throw new NotFoundHttpException($this->translate('Performance not found.'));
        }

        return $performance;
    }

    public function findOrNotFoundPerformanceForm(string $formId, UserPerformance $performance): UserPerformanceForm
    {
        $performanceForm = $this->getEm()->getRepository(UserPerformanceForm::class)->findOneBy([
            'id' => $formId,
            'performance' => $performance,
        ]);

        if (is_null($performanceForm)) {
            throw new NotFoundHttpException($this->translate('Performance form not found.'));
        }

        return $performanceForm;
    }
}