<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPerformanceForm|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPerformanceForm|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPerformanceForm[]    findAll()
 * @method UserPerformanceForm[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPerformanceFormRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPerformanceForm::class);
    }

    public function findAllQueryBuilder($alias = 'form'): QueryBuilder
    {
        return $this->createQueryBuilder($alias);
    }

    public function findUnique(string $type, string $author, UserPerformance $performance)
    {
        return $this->createQueryBuilder('form')
            ->where('form.type = :type AND form.author = :author AND form.performance = :performance')
            ->setParameter('type', $type)
            ->setParameter('author', $author)
            ->setParameter('performance', $performance)
            ->getQuery()
            ->getResult();
    }

    public function findByPerformanceAuthorEmployee(UserPerformance $performance, string $author, User $employee)
    {
        return $this->createQueryBuilder('form')
            ->where('form.author = :author AND form.performance = :performance AND form.employee = :employee')
            ->setParameter('author', $author)
            ->setParameter('performance', $performance)
            ->setParameter('employee', $employee)
            ->getQuery()
            ->getOneOrNullResult();
    }
}