<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */


namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Domain\User\Entity\User;
use Domain\User\Entity\UserAlert;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserAlert|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserAlert|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserAlert[]    findAll()
 * @method UserAlert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserAlertRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserAlert::class);
    }

    public function findAllByUserQuery(User $user): QueryBuilder
    {
        $qb = $this
            ->findAllQueryBuilder('alert')
            ->select(['alert', 'leavesRequest'])
            ->leftJoin('alert.leavesRequest', 'leavesRequest')
            ->where('alert.alertedUser = :alertedUser')
            ->setParameter('alertedUser', $user)
            ->orderBy('alert.read', 'ASC')
            ->orderBy('alert.createdAt', 'DESC');

        return $qb;
    }

    public function findAllQueryBuilder($alias = 'alert')
    {
        return $this->createQueryBuilder($alias);
    }
}