<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPerformance|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPerformance|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPerformance[]    findAll()
 * @method UserPerformance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPerformanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPerformance::class);
    }

    public function findAllQueryBuilder($alias = 'performance')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param User $user
     * @return UserPerformance[]
     */
    public function findAllByUser(User $user): array
    {
        return $this->createQueryBuilder('performance')
            ->where('performance.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTime|null $date
     * @return UserPerformance[]
     */
    public function findAllOutdated(?\DateTime $date = null)
    {
        if (!$date) {
            $date = new \DateTime();
        }

        return $this->createQueryBuilder('performance')
            ->where('performance.date < :date AND performance.status = :status')
            ->setParameter('date', $date)
            ->setParameter('status', UserPerformance::STATUS_PENDING)
            ->getQuery()
            ->getResult();
    }
}