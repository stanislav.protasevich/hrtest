<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */


namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\Skill;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Skill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skill[]    findAll()
 * @method Skill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    /**
     * @param string $skillId
     * @param null $hydrationMode
     * @return Skill|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $skillId, $hydrationMode = null): ?Skill
    {
        return $this->createQueryBuilder('skill')
            ->select('skill')
            ->where('skill.id = :id')
            ->setParameter('id', $skillId)
            ->getQuery()
            ->getOneOrNullResult($hydrationMode);
    }

    public function findAllQueryBuilder($alias = 'skill')
    {
        return $this->createQueryBuilder($alias);
    }
}