<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */


namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\UserField;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserField|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserField|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserField[]    findAll()
 * @method UserField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserField::class);
    }

    public function findAllQueryBuilder($alias = 'userField')
    {
        return $this->createQueryBuilder($alias);
    }
}