<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\UserEmployee;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserEmployee|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserEmployee|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserEmployee[]    findAll()
 * @method UserEmployee[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserEmployeeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserEmployee::class);
    }
}
