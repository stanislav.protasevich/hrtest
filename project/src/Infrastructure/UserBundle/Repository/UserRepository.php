<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $userId
     * @param null $hydrationMode
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByUuid(string $userId, $hydrationMode = null)
    {
        return $this->createQueryBuilder('user')
            ->select('user')
            ->where('user.id = :id')
            ->setParameter('id', $userId)
            ->getQuery()
            ->getOneOrNullResult($hydrationMode);
    }

    /**
     * @param string $userId
     * @return User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneActiveById(string $userId)
    {
        return $this->createQueryBuilder('user')
            ->select('user')
            ->where('user.id = :id AND user.status = :status')
            ->setParameter('id', $userId)
            ->setParameter('status', User::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Loads the user for the given username.
     *
     * This method must return null if the user is not found.
     *
     * @param string $username The username
     *
     * @return UserInterface|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email AND u.status = :status')
            ->setParameter('email', $username)
            ->setParameter('status', User::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $email
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadByEmail(string $email)
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email AND u.status = :status')
            ->setParameter('email', $email)
            ->setParameter('status', User::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $token
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadByResetToken(string $token): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.passwordResetToken = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param string $token
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePasswordResetToken(User $user, string $token)
    {
        $user->setPasswordResetToken($token);

        $this->_em->persist($user);
        $this->_em->flush();

        return $user;
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder()
            ->where('user.status = :status')
            ->setParameter('status', User::STATUS_ACTIVE);
    }

    /**
     * @param int $hydrationMode
     * @return User[]
     */
    public function findAllActive($hydrationMode = Query::HYDRATE_OBJECT): array
    {
        return $this->queryFindAllActive()->getQuery()->getResult($hydrationMode);
    }

    public function findAllQueryBuilder($alias = 'user')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('user')
            ->select(['user.id as value', 'CONCAT(profile.firstName, \' \', profile.surname) as label'])
            ->leftJoin('user.profile', 'profile')
            ->getQuery()
            ->getArrayResult();
    }
}