<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\ConnectedAccountProvider;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ConnectedAccountProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConnectedAccountProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConnectedAccountProvider[]    findAll()
 * @method ConnectedAccountProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConnectedAccountProviderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConnectedAccountProvider::class);
    }

    /**
     * @param string $name
     * @return null|ConnectedAccountProviderRepository
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByName(string $name): ?ConnectedAccountProvider
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder()
            ->where('provider.status = :status')
            ->setParameter('status', ConnectedAccountProvider::STATUS_ACTIVE);
    }

    public function findAllQueryBuilder($alias = 'provider')
    {
        return $this->createQueryBuilder($alias);
    }
}
