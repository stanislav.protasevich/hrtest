<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\ConnectedAccountProvider;
use Domain\User\Entity\User;
use Domain\User\Entity\UserConnectedAccount;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserConnectedAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserConnectedAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserConnectedAccount[]    findAll()
 * @method UserConnectedAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserConnectedAccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserConnectedAccount::class);
    }

    /**
     * @param User $user
     * @param ConnectedAccountProvider $provider
     * @return null|UserConnectedAccount
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByUserAndProvider(User $user, ConnectedAccountProvider $provider): ?UserConnectedAccount
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->andWhere('u.connectedAccountProvider = :connected_account_provider')
            ->setParameter('user', $user)
            ->setParameter('connected_account_provider', $provider)
            ->getQuery()
            ->getOneOrNullResult();
    }
}