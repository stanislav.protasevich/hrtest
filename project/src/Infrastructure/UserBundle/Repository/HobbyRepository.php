<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\User\Entity\Hobby;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Hobby|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hobby|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hobby[]    findAll()
 * @method Hobby[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HobbyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Hobby::class);
    }

    /**
     * @param string $hobbyId
     * @param null $hydrationMode
     * @return Hobby|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $hobbyId, $hydrationMode = null): ?Hobby
    {
        return $this->createQueryBuilder('hobby')
            ->select('hobby')
            ->where('hobby.id = :id')
            ->setParameter('id', $hobbyId)
            ->getQuery()
            ->getOneOrNullResult($hydrationMode);
    }

    public function findPrimaryList()
    {
        $result = $this->findAllQueryBuilder()
            ->select('hobby.id')
            ->orderBy('hobby.sortOrder', "DESC")
            ->getQuery()
            ->getScalarResult();

        return array_map('current', $result);
    }

    public function findAllQueryBuilder($alias = 'hobby')
    {
        return $this->createQueryBuilder($alias);
    }
}