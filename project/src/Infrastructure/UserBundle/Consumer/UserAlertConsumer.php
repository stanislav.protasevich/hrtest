<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class UserAlertConsumer implements ConsumerInterface
{
    /**
     * @param AMQPMessage $message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $message)
    {
//        return ConsumerInterface::MSG_SINGLE_NACK_REQUEUE;

//        /** @var AMQPLazyConnection $connection */
//        $connection = $this->container->get('old_sound_rabbit_mq.user_alert_queue_consumer');
//
//        /** @var AMQPChannel $channel */
//        $channel = $connection->channel();
////
//        $consumerTag = 'consumer_' . getmypid();
//        $channel->basic_consume(AMQPHelper::QUEUE_NOTIFICATIONS, $consumerTag, false, false, false, false, function ($message) {
//
//            /** @var AMQPChannel $channel */
//            $channel = $message->delivery_info['channel'];
//            $channel->basic_ack($message->delivery_info['delivery_tag']);
//        });

//        while (\count($channel->callbacks)) {
//            $channel->wait();
//        }
    }
}