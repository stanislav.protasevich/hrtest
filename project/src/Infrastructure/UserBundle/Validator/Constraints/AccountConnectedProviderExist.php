<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class AccountConnectedProviderExist extends Constraint
{
    public $message = 'Provider "{{ name }}" does not linked with your account.';

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}