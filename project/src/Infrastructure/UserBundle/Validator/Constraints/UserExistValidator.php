<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\UserBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\{
    Constraint, ConstraintValidator
};
use Domain\User\Entity\User;

class UserExistValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @return void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate($value, Constraint $constraint)
    {
        $errors = $this->context->getViolations()->count();
        if (empty($errors)) {
            /* @var $user User */
            $user = $this->em->getRepository(User::class)->loadById((int)$value);

            if (is_null($user)) {
                $this->context->buildViolation($this->translator->trans($constraint->message))
                    ->addViolation();
            }
        }
    }
}