<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use Domain\User\Entity\UserField;
use Domain\User\Entity\UserFieldValue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Form;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class UserFieldValueConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint|UserFieldValueConstraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UserFieldValueConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UserFieldValueConstraint');
        }

        $value = trim((string)$value);

        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        /* @var $form Form */
        $form = $this->context->getRoot();

        /* @var $fields ArrayCollection */
        $fields = $form->get('fieldValues')->getData();

        /* @var $fieldValue UserFieldValue */
        $fieldValue = $fields->filter(function (UserFieldValue $item) use ($value) {
            return $item->getValue() === $value;
        })->first();

        if (!is_object($fieldValue)) {
            $this->addError($constraint, $translator);
            return;
        }

        /* @var $field UserField */
        $field = $fieldValue->getField();

        if (!$field) {
            $this->addError($constraint, $translator);
            return;
        }

        $matchParams = $field->getMatchParams();
        $matchLength = $field->getMatchLength();

        if ($matchLength && strlen($value) > $matchLength) {
            $this->context->buildViolation($translator->trans($constraint->messageLength))
                ->setParameter('{{ length }}', $matchLength)
                ->addViolation();
            return;
        }

        switch ($field->getMatchType()) {

            case UserField::MATCH_TYPE_REGEX:
                $pattern = "/{$matchParams['regex']}/";

                if (!preg_match($pattern, $value, $matches)) {
                    $this->addError($constraint, $translator);
                }

                break;
        }
    }

    /**
     * @param Constraint $constraint
     * @param $translator
     */
    public function addError(Constraint $constraint, $translator): void
    {
        $this->context->buildViolation($translator->trans($constraint->message))->addViolation();
    }
}