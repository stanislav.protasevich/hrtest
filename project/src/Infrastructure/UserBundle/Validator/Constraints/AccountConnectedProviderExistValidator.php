<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\UserBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\{
    Constraint, ConstraintValidator
};
use Domain\User\Entity\ConnectedAccountProvider;
use Domain\User\Entity\User;
use Domain\User\Entity\UserConnectedAccount;

class AccountConnectedProviderExistValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @return void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate($value, Constraint $constraint)
    {
        $errors = $this->context->getViolations()->count();

        if (empty($errors)) {
            /* @var $provider ConnectedAccountProvider */
            $provider = $this->em->getRepository(ConnectedAccountProvider::class)->findOneByName((string)$value);
            /* @var $user User */
            $user = $this->tokenStorage->getToken()->getUser();

            /* @var $account ConnectedAccountProviderExist */
            $account = $this->em->getRepository(UserConnectedAccount::class)
                ->findOneByUserIdAndProviderId($user->getId(), $provider->getId());

            if (is_null($account)) {
                $this->context->buildViolation($this->translator->trans($constraint->message))
                    ->setParameter('{{ name }}', $provider->getName())
                    ->addViolation();
            }
        }
    }
}