<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Message;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Message\EmailNotification;
use Infrastructure\UserBundle\Service\UserService;

class InviteEmailNotification extends EmailNotification
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this
            ->subject('Please confirm email for HR Admin.')
            ->to($this->user->getEmail(), $this->user->getProfile()->getFullName())
            ->view('@User/emails/invite.html.twig', [
                'token' => $this->user->getPasswordResetToken()
            ]);
    }

    /**
     * @param \Exception $exception
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onFail(\Exception $exception)
    {
        /** @var UserService $service */
        $service = app(UserService::class);
        $service->setStatusEmailBounce($this->user);
    }
}
