<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Message;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Message\EmailNotification;

class PasswordResetEmailNotification extends EmailNotification
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this
            ->subject('Password reset request.')
            ->to($this->user->getEmail(), $this->user->getProfile()->getFullName())
            ->view('@User/emails/password-reset-request.html.twig', [
                'token' => $this->user->getPasswordResetToken()
            ]);
    }
}
