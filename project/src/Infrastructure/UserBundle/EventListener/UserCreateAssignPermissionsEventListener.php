<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\AuthItemService;
use Infrastructure\RbacBundle\Service\RbacService;
use Infrastructure\UserBundle\Event\UserEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreateAssignPermissionsEventListener implements EventSubscriberInterface
{
    /**
     * @var RbacService
     */
    private $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, AuthItemService $service)
    {
        $this->service = $service;
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::EVENT_AFTER_INSERT => 'onAfterInsert',
        ];
    }

    /**
     * @param UserEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAfterInsert(UserEvent $event)
    {
        $model = $event->getUser();

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $authItemUser = $em->getRepository(AuthItem::class)->findOneBy([
            'type' => AuthItem::TYPE_ROLE,
            'name' => RbacPermissions::ROLE_USER
        ]);

        $this->service->assignCreate($authItemUser, [$model]);
    }
}