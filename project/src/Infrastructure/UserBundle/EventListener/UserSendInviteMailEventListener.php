<?php declare(strict_types=1);

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\EventListener;


use Doctrine\ORM\EntityManager;
use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Helpers\PasswordResetTokenHelper;
use Infrastructure\UserBundle\Event\UserEvent;
use Infrastructure\UserBundle\Message\InviteEmailNotification;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class UserSendInviteMailEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(ContainerInterface $container, MessageBusInterface $bus)
    {
        $this->container = $container;
        $this->bus = $bus;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::EVENT_AFTER_UPDATE => 'onAfterUpdate',
        ];
    }

    /**
     * @param UserEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAfterUpdate(UserEvent $event)
    {
        $user = $event->getUser();
        if (in_array($user->getStatus(), [User::STATUS_ACTIVE, User::STATUS_EMAIL_CONFIRM]) && $user->isDirty('email')) {
            /* @var $em EntityManager */
            $em = $this->container->get('doctrine.orm.entity_manager');

            $user->setStatus(User::STATUS_EMAIL_CONFIRM);
            $user->setPasswordResetToken(PasswordResetTokenHelper::generateResetToken());

            $em->persist($user);
            $em->flush();

            $this->bus->dispatch(
                new InviteEmailNotification($user)
            );
        }
    }
}
