<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Domain\User\Entity\EmergencyContact;
use Infrastructure\UserBundle\Event\UserEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreateAddEmergencyContactEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::EVENT_AFTER_INSERT => 'onAfterInsert',
        ];
    }

    /**
     * @param UserEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAfterInsert(UserEvent $event)
    {
        $user = $event->getUser();

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $now = new \DateTime();

        $emergencyContact = new EmergencyContact();
        $emergencyContact->setName('');
        $emergencyContact->setContactType(EmergencyContact::CONTACT_TYPE_PHONE);
        $emergencyContact->setContact('');
        $emergencyContact->setSortOrder(1);
        $emergencyContact->setCreatedAt($now);
        $emergencyContact->setUpdatedAt($now);

        $user->addEmergencyContact($emergencyContact);

        $em->persist($user);
        $em->flush();
    }
}
