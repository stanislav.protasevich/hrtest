<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\EventListener;


use Infrastructure\UserBundle\Event\UserPermissionsEvent;
use Predis\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserPermissionsClearCacheEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserPermissionsEvent::EVENT_AFTER_UPDATE => 'onAfterChange',
        ];
    }

    public function onAfterChange(UserPermissionsEvent $event)
    {
        $user = $event->getUser();
        $client = $this->container->hasParameter('rbac.client') ? $this->container->getParameter('rbac.client') : 'default';
        /* @var $redisClient Client */
        $redisClient = $this->container->get('snc_redis.' . $client);

        $cacheKey = 'rbac' . '-' . $user->getId();

        if ($redisClient->get($cacheKey)) {
            $redisClient->set($cacheKey, null, 0, 0);
        }
    }
}