<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\EventListener;

use Application\UseCase\User\Performance\UserPerformanceAddHandler;
use Application\UseCase\User\Performance\UserPerformanceAddHandlerInterface;
use Infrastructure\UserBundle\Command\Performance\UserPerformanceAddCommand;
use Infrastructure\UserBundle\Event\UserEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreatePeOnCreateEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserPerformanceAddHandler
     */
    private $handler;

    public function __construct(
        ContainerInterface $container,
        UserPerformanceAddHandlerInterface $handler
    )
    {
        $this->container = $container;
        $this->handler = $handler;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::EVENT_AFTER_INSERT => 'onAfterInsert',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onAfterInsert(UserEvent $event)
    {
        ($this->handler)(new UserPerformanceAddCommand($event->getUser()));
    }
}