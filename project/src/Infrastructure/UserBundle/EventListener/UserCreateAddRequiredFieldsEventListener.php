<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Domain\User\Entity\UserField;
use Domain\User\Entity\UserFieldValue;
use Infrastructure\UserBundle\Event\UserEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreateAddRequiredFieldsEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserEvent::EVENT_AFTER_INSERT => 'onAfterInsert',
        ];
    }

    /**
     * @param UserEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAfterInsert(UserEvent $event)
    {
        $user = $event->getUser();

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $linkedinField = new UserFieldValue();
        $linkedinField->setValue('');
        $linkedinField->setField(
            $em->getRepository(UserField::class)->find(UserField::GROUP_SOCIAL_LINKEDIN)
        );
        $user->addFieldValue($linkedinField);

        $em->persist($user);
        $em->flush();
    }
}
