<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory;


use Application\UseCase\User\Alert\UserAlertHandlerInterface;
use Domain\User\Entity\User;
use Infrastructure\UserBundle\Command\Alert\UserAlertCommand;

class UserAlertFactory
{
    public static function create(User $user, string $contentType, string $contentId, string $action, User $alertedUser, string $text, $meta = null)
    {
        /** @var UserAlertHandlerInterface $handler */
        $handler = app(UserAlertHandlerInterface::class);

        $command = new UserAlertCommand($user, $contentType, $contentId, $action, $alertedUser, $text, $meta);

        ($handler)($command);
    }
}
