<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory;


use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Domain\User\Entity\UserProfile;
use Domain\User\Factory\UserFactoryInterface;
use Infrastructure\AuthBundle\Helpers\PasswordResetTokenHelper;
use Infrastructure\CommonBundle\Service\FileUploader;
use Infrastructure\EmployeeBundle\Event\EmployeeCompanyEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory implements UserFactoryInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $encoder)
    {
        $this->container = $container;
        $this->encoder = $encoder;
    }

    /**
     * @param User $user
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(User $user)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $em->getConnection()->beginTransaction();

        try {
            $this->setPassword($user);

            $user->setAccessToken('');
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());
            $token = PasswordResetTokenHelper::generateResetToken();
            $user->setPasswordResetToken($token);

            $em->persist($user);
            $em->persist($user->getProfile());

            $this->createCompanyEmployee($user, $em);

            $em->flush();

            $profile = $user->getProfile();
            $this->setProfileAvatar($user, $profile, $em);

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     */
    protected function setPassword(User $user): void
    {
        $password = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPasswordHash($password);
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws \Doctrine\ORM\ORMException
     */
    protected function createCompanyEmployee(User $user, EntityManager $em): void
    {
        $creator = $this->container->get('security.token_storage')->getToken()->getUser();

        event(EmployeeCompanyEvent::EVENT_BEFORE_INSERT);

        $items = [];
        foreach ($user->getEmployeeCompanies() as $employeeCompany) {
            $employeeCompany->setCreatedBy($creator);
            $employeeCompany->setUpdatedBy($creator);
            $user->addEmployeeCompany($employeeCompany);
            $em->persist($employeeCompany);

            $items[] = $employeeCompany;
        }

        event(EmployeeCompanyEvent::EVENT_AFTER_INSERT, new EmployeeCompanyEvent([
            'model' => $items,
        ]));
    }

    /**
     * @param User $user
     * @param $profile
     * @param $em
     * @throws \Exception
     */
    protected function setProfileAvatar(User $user, UserProfile $profile, EntityManager $em): void
    {
        $avatar = $profile->getAvatar();

        if ($avatar instanceof File) {
            /* @var $uploader FileUploader */
            $uploader = $this->container->get(FileUploader::class);

            $path = "user/{$user->getId()}/avatars";
            $uploader->setPath($path);
            $filename = $uploader->upload($avatar);

            $profile->setAvatar($filename);
            $em->persist($profile);
            $em->flush();
        }
    }

    /**
     * @param User $user
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function update(User $user)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        if ($user->getPassword()) {
            $this->setPassword($user);
        }

        $profile = $user->getProfile();
        $this->setProfileAvatar($user, $profile, $em);

        $this->updateCompanyEmployee($user, $em);

        $em->persist($profile);
        $em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param EntityManager $em
     * @throws \Doctrine\ORM\ORMException
     */
    protected function updateCompanyEmployee(User $user, EntityManager $em): void
    {
        $creator = $this->container->get('security.token_storage')->getToken()->getUser();

        event(EmployeeCompanyEvent::EVENT_BEFORE_UPDATE);

        $items = [];

        foreach ($user->getEmployeeCompanies() as $employeeCompany) {
            /* @var $employeeCompany EmployeeCompany */
            if (!$employeeCompany->getCreatedBy()) {
                $employeeCompany->setCreatedBy($creator);
            }

            if (!$employeeCompany->getCreatedAt()) {
                $employeeCompany->setCreatedAt(new \DateTime());
            }

            $employeeCompany->setUpdatedBy($creator);
            $employeeCompany->setUpdatedAt(new \DateTime());

            $user->addEmployeeCompany($employeeCompany);

            $em->persist($employeeCompany);

            $items[] = $employeeCompany;
        }

        event(EmployeeCompanyEvent::EVENT_AFTER_UPDATE, new EmployeeCompanyEvent([
            'model' => $items,
        ]));
    }
}