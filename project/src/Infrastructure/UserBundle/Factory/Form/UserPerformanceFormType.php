<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Infrastructure\CommonBundle\Validator\Constraints\MatrixConstraint;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['attributes'] as $key => $attribute) {
            $builder->add(
                $attribute['name'],
                $this->resolveFieldType($attribute['type']),
                $this->resolveOptions($attribute['options'])
            );
        }
    }

    public function resolveFieldType(?string $needle)
    {
        $type = null;
        switch ($needle) {
            case 'text':
                $type = TextType::class;
                break;

            case 'date':
            case 'datetime':
                $type = DateTimeType::class;
                break;

            case 'collection':
                $type = CollectionType::class;
                break;

            case 'entity':
                $type = EntityType::class;
                break;
        }

        return $type;
    }

    public function resolveOptions(array $data): array
    {
        $options = [];

        foreach ($data as $key => $item) {
            if ($key === 'validators') {
                $options['constraints'] = [];

                foreach ($data['validators'] as $validator) {
                    $options['constraints'] = $this->resolveValidations($validator);
                }
            } else {
                $options[$key] = $item;
            }
        }

        return $options;
    }

    public function resolveValidations(string $param)
    {
        $validators = explode('|', $param);

        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $constraints = [];
        foreach ($validators as $validator) {
            $formattedParams = explode(':', $validator);

            switch ($formattedParams[0]) {
                case 'required':

                    if (!isset($constraints['required'])) {
                        $constraints['required'] = new NotBlank();
                    }

                    break;

                case 'max':
                case 'min':
                    if (!isset($constraints['length'])) {
                        $constraints['length'] = new Length([
                            $formattedParams[0] => $formattedParams[1]
                        ]);
                    } else {
                        $propertyAccessor->setValue($constraints['length'], $formattedParams[0], $formattedParams[1]);
                    }

                    break;

                case 'matrix':
                    $constraints['matrix'] = new MatrixConstraint([
                        'payload' => $param
                    ]);
                    break;
            }
        }

        return $constraints;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'attributes' => null,
        ]);
    }
}