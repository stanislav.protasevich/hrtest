<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\Company\Entity\CompanyAddress;
use Domain\User\Entity\UserEmployee;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserEmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyAddress', EntityType::class, [
                'disabled' => Gate::cannot(RbacPermissions::ROLE_RECRUITER),
                'class' => CompanyAddress::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('jobOfferDate', DateType::class, [
                'disabled' => Gate::cannot(RbacPermissions::ROLE_RECRUITER),
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserEmployee::class,
            'csrf_protection' => false,
        ]);
    }
}