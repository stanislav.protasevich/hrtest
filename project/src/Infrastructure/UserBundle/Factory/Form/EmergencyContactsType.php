<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form;


use Domain\User\Entity\EmergencyContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmergencyContactsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 2
                    ])
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => EmergencyContact::TYPES,
//                'constraints' => [
//                    new NotBlank(),
//                ],
            ])
            ->add('contactType', ChoiceType::class, [
                'choices' => EmergencyContact::CONTACT_TYPES,
//                'constraints' => [
//                    new NotBlank(),
//                ],
            ])
            ->add('contact', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('sortOrder', IntegerType::class, [
                'empty_data' => "1"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmergencyContact::class,
            'csrf_protection' => false,
        ]);
    }
}