<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\CollectionLimitAttribute;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\EmployeeBundle\Factory\Form\EmployeeCompanyType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserGeneralType_old extends AbstractType
{
    protected const SCENARIO_CREATE = 'create';
    protected const SCENARIO_UPDATE = 'update';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profile', UserProfileType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('children', CollectionType::class, [
                'entry_type' => UserChildrenType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->add('emergencyContacts', CollectionType::class, [
                'entry_type' => EmergencyContactsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->add('fieldValues', CollectionType::class, [
                'entry_type' => UserFieldValueType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $form->add('address', UserAddressType::class, [
                    'disabled' => !isset($data['address']),
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();

                $groups = ['Default'];

                if ($method === Request::METHOD_PUT) {
                    $groups[] = static::SCENARIO_UPDATE;
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = static::SCENARIO_CREATE;
                }

                return $groups;
            },
        ]);
    }
}