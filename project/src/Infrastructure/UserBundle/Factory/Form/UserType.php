<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;


use Domain\User\Entity\Hobby;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Infrastructure\UserBundle\Factory\Form\Partials\UserGeneralTrait;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    use UserGeneralTrait;

    public const SCENARIO_CREATE = 'create';
    public const SCENARIO_UPDATE = 'update';
    public const SCENARIO_ACTIVE_USER = 'active';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'disabled' => Gate::cannot(RbacPermissions::ROLE_HR),
                'choices' => User::STATUSES_CHANGED,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('email', TextType::class, [
                'disabled' => Gate::cannot(RbacPermissions::ROLE_HR),
                'constraints' => [
                    new NotBlank([
                        'groups' => static::SCENARIO_ACTIVE_USER,
                    ]),
                    new Email(),
                ],
            ])
            ->add('password', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 6,
                        'max' => 16,
                    ]),
                ],
            ])
            ->add('children', CollectionType::class, [
                'entry_type' => UserChildrenType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->add('hobbies', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => Hobby::class,
                ],
            ])
            ->add('emergencyContacts', CollectionType::class, [
                'entry_type' => EmergencyContactsType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ])
            ->add('fieldValues', CollectionType::class, [
                'entry_type' => UserFieldValueType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
            ]);

        $this->buildGeneral($builder);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();
                /** @var User $data */
                $data = $form->getData();

                $groups = ['Default'];

                if ($method === Request::METHOD_PUT) {
                    $groups[] = static::SCENARIO_UPDATE;
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = static::SCENARIO_CREATE;
                }

                if ($data->getStatus() && $data->getStatus() !== User::STATUS_DRAFT) {
                    $groups[] = static::SCENARIO_ACTIVE_USER;
                }

                return $groups;
            },
        ]);
    }
}