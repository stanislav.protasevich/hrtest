<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\CollectionLimitAttribute;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\EmployeeBundle\Factory\Form\EmployeeCompanyType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('employeeCompanies', CollectionType::class, [
                'entry_type' => EmployeeCompanyType::class,
                'allow_add' => true,
                'allow_delete' => false,
                'error_bubbling' => false,
                'by_reference' => false,
                'constraints' => [
                    new NotNullCollection(),
//                    new UniqueCollections([
//                        'attributes' => ['jobTitle'],
//                    ]),
                    new CollectionLimitAttribute([
                        'attribute' => 'isMain',
                        'value' => true,
                        'limit' => 1,
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }
}
