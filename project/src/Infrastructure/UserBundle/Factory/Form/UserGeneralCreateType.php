<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Infrastructure\EmployeeBundle\Factory\Form\EmployeeCompanyType;
use Infrastructure\UserBundle\Factory\Form\Partials\UserGeneralTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserGeneralCreateType extends AbstractType
{
    use UserGeneralTrait;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->buildGeneral($builder);

        $builder
            ->add('employeeCompany', EmployeeCompanyType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}