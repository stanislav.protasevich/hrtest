<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form\Partials;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Infrastructure\RbacBundle\Validator\Constraints\AuthItemAssignedRoleConstraint;
use Infrastructure\UserBundle\Factory\Form\UserEmployeeType;
use Infrastructure\UserBundle\Factory\Form\UserProfileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

trait UserGeneralTrait
{
    public function buildGeneral(FormBuilderInterface $builder)
    {
        $builder
            ->add('profile', UserProfileType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('employee', UserEmployeeType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('recruiters', CollectionType::class, [
                'disabled' => Gate::cannot(RbacPermissions::ROLE_RECRUITER),
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => User::class,
                ],
                'constraints' => [
                    new NotBlank(),
                    new NotNullCollection(),
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                    new AuthItemAssignedRoleConstraint([
                        'role' => RbacPermissions::ROLE_RECRUITER,
                    ]),
                ],
            ]);
    }
}