<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\User\Entity\UserField;
use Domain\User\Entity\UserFieldValue;
use Infrastructure\UserBundle\Validator\Constraints\UserFieldValueConstraint;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;


class UserFieldValueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('field', EntityType::class, [
                'class' => UserField::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('value', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new UserFieldValueConstraint(),
                ]
            ])->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserFieldValue::class,
            'csrf_protection' => false,
        ]);
    }
}

