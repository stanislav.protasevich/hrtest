<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;


use Domain\User\Entity\Skill;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\CollectionLimitAttribute;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\EmployeeBundle\Factory\Form\EmployeeCompanyType;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserTypeCopy extends UserGeneralType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => User::STATUSES,
            ])
            ->add('password', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 6,
                        'max' => 16,
                    ]),
                ],
            ])
            ->add('employeeCompanies', CollectionType::class, [
                'entry_type' => EmployeeCompanyType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'constraints' => [
                    new NotNullCollection(),
                    new UniqueCollections([
                        'attributes' => ['jobTitle'],
                    ]),
                    new CollectionLimitAttribute([
                        'attribute' => 'isMain',
                        'value' => true,
                        'limit' => 1,
                    ])
                ],
            ])
            ->add('skills', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => Skill::class,
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ])
            ->add('supervisors', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => User::class,
                    'query_builder' => function (UserRepository $r) {
                        return $r->queryFindAllActive();
                    },
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ]);
        parent::buildForm($builder, $options);
    }
}