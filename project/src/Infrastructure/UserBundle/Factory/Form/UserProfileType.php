<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\User\Entity\UserProfile;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Infrastructure\CommonBundle\Form\Type\BooleanType;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 2
                    ])
                ],
            ])
            ->add('middleName', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 2
                    ])
                ],
            ])
            ->add('surname', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 2
                    ])
                ],
            ])
            ->add('birthday', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => UserProfile::GENDERS,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
//            ->add('maritalStatus', ChoiceType::class, [
//                'choices' => UserProfile::MARITAL_STATUSES,
//            ])
            ->add('maritalStatus', BooleanType::class)
            ->add('marriageAnniversary', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('phone', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length([
                        'min' => 5,
                        'max' => 30
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9\\-\\(\\)\\/\\+\\s]*$/'
                    ])
                ],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $avatar = $data['avatar'] ?? null;

                $skipAvatar = !$avatar || StringHelper::isBase64File($avatar) === false;

                $form->add('avatar', Base64EncodedFileType::class, [
                    'strict_decode' => false,
                    'disabled' => $skipAvatar,
                    'validation_groups' => $skipAvatar ? false : ['Default'],
                    'constraints' => [
                        new File([
                            'maxSize' => '10M',
                            'mimeTypes' => \Domain\Archive\Entity\File::MIME_TYPE_IMAGES,
                        ])
                    ]
                ]);

            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserProfile::class,
            'csrf_protection' => false,
        ]);
    }
}