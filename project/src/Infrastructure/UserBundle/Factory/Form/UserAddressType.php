<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Domain\User\Entity\UserAddress;
use Infrastructure\CommonBundle\Validator\Constraints\Latitude;
use Infrastructure\CommonBundle\Validator\Constraints\Longitude;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => UserAddress::ADDRESS_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('street', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('latitude', TextType::class, [
                'constraints' => [
                    new Latitude(),
                ],
            ])
            ->add('longitude', TextType::class, [
                'constraints' => [
                    new Longitude(),
                ],
            ])
            ->add('building', TextType::class, [
                'constraints' => [
                    new Length([
                        'max' => 10,
                    ]),
                ],
            ])
            ->add('room', TextType::class, [
                'constraints' => [
                    new Length([
                        'max' => 10,
                    ]),
                ],
            ])
            ->add('postCode', TextType::class, [
                'constraints' => [
                    new Length([
                        'max' => 10,
                    ]),
                ],
            ])
            ->add('sortOrder', IntegerType::class, [
                'empty_data' => "1",
            ])
            ->add('country', EntityType::class, [
                'class' => Country::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('region', EntityType::class, [
                'class' => Region::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserAddress::class,
            'csrf_protection' => false,
        ]);
    }
}