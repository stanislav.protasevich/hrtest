<?php

declare(strict_types=1);

namespace Infrastructure\UserBundle\Factory\Form;

use Domain\Archive\Entity\File as FileEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'constraints' => [
                    new NotBlank(),
                    new File([
                        'maxSize' => '50M',
                        'mimeTypes' => array_merge(
                            FileEntity::MIME_TYPE_DOCS,
                            FileEntity::MIME_TYPE_IMAGES
                        ),
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}