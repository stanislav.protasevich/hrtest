<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\Second\Employee;

use Infrastructure\UserBundle\Factory\Form\Performance\First\Employee\UserPerformanceFirstEmployeeValuationFormType;

class UserPerformanceSecondEmployeeValuationFormType extends UserPerformanceFirstEmployeeValuationFormType
{

}