<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\Second\Head;


use Infrastructure\UserBundle\Factory\Form\Performance\First\Head\UserPerformanceFirstHeadValuationFormType;

class UserPerformanceSecondHeadValuationFormType extends UserPerformanceFirstHeadValuationFormType
{

}