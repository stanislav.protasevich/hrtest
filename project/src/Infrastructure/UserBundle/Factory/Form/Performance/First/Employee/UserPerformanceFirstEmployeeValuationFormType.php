<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\First\Employee;

use Infrastructure\CommonBundle\Validator\Constraints\MatrixConstraint;
use Infrastructure\UserBundle\ValueObject\UserPerformanceValuationFormVo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceFirstEmployeeValuationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comments', TextType::class, [
                'constraints' => [
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'comments',
                            'description' => 'Please provide any additional comments or feedback that you would like to share. '
                        ],
                    ]),
                ],
            ])
            ->add('jobKnowledge', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'jobKnowledge',
                            'description' => '1. Job Knowledge/Functional and Technical Skills
                                                • Possessing and using skills required for the position.   
                                                • Achiving the required level of knowledge and skills in position-related areas.
                                                • Keeping up to date in all relevant knowledge and skills areas to meet job requirements.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('productivity', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'productivity',
                            'description' => '2. Productivity and Quality of Work
                                                • Consistently performing at a superior level, setting an example for others. Producing outstanding quantity of work while giving the proper attention to quality.
                                                • Recognizing and taking initiative to resolve problems.
                                                • Ability to work under pressure and learn from previous mistakes.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('speedWork', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'speedWork',
                            'description' => '3. Speed of Work
                                                • Ability to work at quick rates of speed, while producing accurate outcomes.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('timeManagement', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'timeManagement',
                            'description' => '4. Time Management
                                                • Ability to self-manage.
                                                • Prioritization of multiple activities and assignments effectively.
                                                • Planning consistently on a long and short term basis.
                                                • Defining goals and monitors progress.
                                                • Meeting commitments and deadlines consistently.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('professionalAttitude', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'professionalAttitude',
                            'description' => '5. Professional Attitude
                                                • Effective work under pressure or in a crisis situation.     
                                                • Working beyond normal expectations when workload and deadlines require it.    
                                                • Punctual for work, meetings, appointments and reporting; when absent from work, informs the supervisor, and insures work assignments are completed. 
                                                • Taking project decisions based on the understanding of the product, market and customer/users needs.
                                                • Involving others team members as needed to ensure the quality and commitment of the decision.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('decisionMaking', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'decisionMaking',
                            'description' => '6. Decision Making/Problem Solving Ability
                                        • Forecasting and identifying problems and their priorities, evaluating alternatives.
                                        • Choosing the appropriate action by evaluating options and considering implications in a timely manner.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('collaborativeWorking', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'collaborativeWorking',
                            'description' => '7. Collaborative Working Relationships  
                                        • Positive cooperation as a team member with members of the own unit  and other units.
                                        • Negotiating with respect, resolving conflicts, and reaching agreements.
                                        • Effective work with individuals with diverse backgrounds and abilities.
                                        • Creating collaborative and positive atmosphere in the team.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('interpersonalCommunication', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'interpersonalCommunication',
                            'description' => '8. Interpersonal Communication      
                                            • Clear expressing ideas and information in both verbal and written form.
                                            • Building and maintaining effective working relationships.
                                            • Ability to act as a liaison between management and staff. '
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('vision', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'vision',
                            'description' => '10. Vision/Creativity
                                                • Bringing new ideas and ability to implement them.
                                                • Find the time and resources to develop and implement those ideas that are worth acting upon finding.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPerformanceValuationFormVo::class,
            'csrf_protection' => false,
        ]);
    }
}