<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\First\Employee;


use Infrastructure\UserBundle\Factory\Form\Performance\General\UserPerformanceFormType;
use Infrastructure\UserBundle\Factory\Form\Performance\General\UserPerformanceQuestionnaireFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceFirstEmployeeFormType extends UserPerformanceFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('valuation', UserPerformanceFirstEmployeeValuationFormType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('questionnaire', UserPerformanceQuestionnaireFormType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
                'count' => 10,
            ]);
    }
}