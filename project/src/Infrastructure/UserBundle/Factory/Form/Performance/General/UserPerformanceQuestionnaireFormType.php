<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\General;


use Infrastructure\CommonBundle\Validator\Constraints\MatrixConstraint;
use Infrastructure\UserBundle\ValueObject\UserPerformanceQuestionnaireFormVo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceQuestionnaireFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $count = $options['count'] ?? 1;

        for ($i = 1; $i <= $count; $i++) {
            $builder
                ->add('question_' . $i, TextType::class, [
                    'constraints' => [
                        new NotBlank(),
                        new MatrixConstraint([
                            'payload' => [
                                'attribute' => 'question_' . $i,
                            ],
                        ]),
                    ],
                ]);
        };
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPerformanceQuestionnaireFormVo::class,
            'csrf_protection' => false,
        ]);

        $resolver->setRequired(['count']);
    }
}