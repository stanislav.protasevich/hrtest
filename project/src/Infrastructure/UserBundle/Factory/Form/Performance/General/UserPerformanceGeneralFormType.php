<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\General;


use Domain\Job\Entity\JobTitle;
use Domain\Location\Entity\City;
use Domain\User\Entity\User;
use Infrastructure\JobBundle\Repository\JobTitleRepository;
use Infrastructure\UserBundle\Repository\UserRepository;
use Infrastructure\UserBundle\ValueObject\UserPerformanceGeneralFormVo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceGeneralFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('jobTitle', EntityType::class, [
                'class' => JobTitle::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (JobTitleRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('city', EntityType::class, [
                'class' => City::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('supervisor', EntityType::class, [
                'class' => User::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (UserRepository $repository) {
                    return $repository->queryFindAllActive();
                }
            ])
            ->add('completionAt', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('startAt', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('endAt', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('meetingAt', DateType::class, [
                'widget' => 'single_text',
                'empty_data' => new \DateTime(),
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPerformanceGeneralFormVo::class,
            'csrf_protection' => false,
        ]);
    }

}