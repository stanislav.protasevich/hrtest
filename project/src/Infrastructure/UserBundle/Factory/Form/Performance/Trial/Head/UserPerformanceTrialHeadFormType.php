<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\Trial\Head;


use Infrastructure\UserBundle\Factory\Form\Performance\General\UserPerformanceFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceTrialHeadFormType extends UserPerformanceFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('valuation', UserPerformanceTrialHeadValuationFormType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ]);
    }
}