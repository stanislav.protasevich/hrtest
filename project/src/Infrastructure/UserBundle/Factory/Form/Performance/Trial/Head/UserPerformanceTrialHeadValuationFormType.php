<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Factory\Form\Performance\Trial\Head;

use Infrastructure\CommonBundle\Validator\Constraints\MatrixConstraint;
use Infrastructure\UserBundle\ValueObject\UserPerformanceValuationFormVo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserPerformanceTrialHeadValuationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comments', TextType::class, [
                'constraints' => [
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'comments',
                            'description' => 'Please provide any additional comments or feedback that you would like to share. '
                        ],
                    ]),
                ],
            ])
            ->add('jobKnowledge', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'jobKnowledge',
                            'description' => '1. Job Knowledge/Functional and Technical Skills
                                                • Possessing and using skills required for the position.   
                                                • Achiving the required level of knowledge and skills in position-related areas.
                                                • Keeping up to date in all relevant knowledge and skills areas to meet job requirements.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('productivity', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'productivity',
                            'description' => '2. Productivity and Quality of Work
                                                • Consistently performing at a superior level, setting an example for others. Producing outstanding quantity of work while giving the proper attention to quality.
                                                • Recognizing and taking initiative to resolve problems.
                                                • Ability to work under pressure and learn from previous mistakes.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('speedWork', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'speedWork',
                            'description' => '3. Speed of Work
                                                • Ability to work at quick rates of speed, while producing accurate outcomes.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('timeManagement', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'timeManagement',
                            'description' => '4. Time Management
                                                • Ability to self-manage.
                                                • Prioritization of multiple activities and assignments effectively.
                                                • Planning consistently on a long and short term basis.
                                                • Defining goals and monitors progress.
                                                • Meeting commitments and deadlines consistently.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('professionalAttitude', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'professionalAttitude',
                            'description' => '5. Professional Attitude
                                                • Effective work under pressure or in a crisis situation.     
                                                • Working beyond normal expectations when workload and deadlines require it.    
                                                • Punctual for work, meetings, appointments and reporting; when absent from work, informs the supervisor, and insures work assignments are completed. 
                                                • Taking project decisions based on the understanding of the product, market and customer/users needs.
                                                • Involving others team members as needed to ensure the quality and commitment of the decision.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('leadership', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'leadership',
                            'description' => '6.  Leadership
                                                • Inspiration and direction team members to higher levels of performance.
                                                • Accepting accountability for group performance, identifying and resolving issues and problems areas.
                                                • Establishing and monitoring department standards/policies.
                                                • Providing the staff with information and ensure understanding of the overall company goals / decisions and culture.
                                                • Creating a climate in which people want to do their best.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('employeesDevelopment', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'employeesDevelopment',
                            'description' => '7. Employees Development                                                                                        • Encouraging and mentoring staff for growth and providing training opportunities.
                                                • Setting clear objectives and measures.
                                                • Providing the necessary information and resources for staff to be effective.        
                                                • Working with employees to reinforce effective efforts and progress or improve performance.       
                                                • Conducting performance review on time.                                                            '
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('valuingDiversity', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'valuingDiversity',
                            'description' => '8.  Valuing Diversity                                                                                     • Effective work with individuals of diverse styles, abilities, backgrounds, and motivations.
                                                • Attention to all details and aspects of a job or process to ensuring a complete, high quality output.
                                                • Demonstrating a positive attitude and maintaining constructive interpersonal relationships under stress.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('decisionMaking', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'decisionMaking',
                            'description' => '9. Decision Making/Problem Solving Ability
                                                • Forecasting and identifying problems and their priorities, evaluating alternatives.
                                                • Choosing the appropriate action by evaluating options and considering implications in a timely manner.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ])
            ->add('vision', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new MatrixConstraint([
                        'payload' => [
                            'attribute' => 'vision',
                            'description' => '10. Vision/Creativity
                                                • Bringing new ideas and ability to implement them.
                                                • Find the time and resources to develop and implement those ideas that are worth acting upon finding.'
                        ],
                        'size' => 3,
                        'requiredMin' => 1,
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPerformanceValuationFormVo::class,
            'csrf_protection' => false,
        ]);
    }
}