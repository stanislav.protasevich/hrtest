<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Exception;

use Throwable;

/**
 * Class UserNotFoundException
 *
 * @package Leos\Domain\User\Exception
 */
class UserInvalidLoginException extends \Exception
{

    protected $statusCode;

    public function __construct(string $message = "", int $statusCode, int $code = 0, Throwable $previous = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
