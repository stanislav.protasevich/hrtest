<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Exception;


use Symfony\Component\HttpFoundation\Response;

class UserInvalidStatusException extends \Exception
{
    protected $statusCode;

    public function __construct(string $message = "", int $statusCode = Response::HTTP_BAD_REQUEST, int $code = 0, \Throwable $previous = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
