<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class SkillPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_SKILL_CREATE = 'skillCreate';
    public const PERMISSION_SKILL_EDIT = 'skillEdit';
    public const PERMISSION_SKILL_LIST = 'skillList';

    public const PERMISSION_SKILLS = [
        self::PERMISSION_SKILL_CREATE,
        self::PERMISSION_SKILL_EDIT,
        self::PERMISSION_SKILL_LIST,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_SKILL_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_SKILL_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_SKILL_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}