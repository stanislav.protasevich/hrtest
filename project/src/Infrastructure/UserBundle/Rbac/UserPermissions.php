<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class UserPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_USER_CREATE = 'userCreate';
    public const PERMISSION_USER_EDIT = 'userEdit';
    public const PERMISSION_USER_LIST = 'userList';
    public const PERMISSION_USER_LIST_PRIMARY = 'userListPrimary';
    public const PERMISSION_USER_VIEW = 'userView';
    public const PERMISSION_USER_STATUSES_PRIMARY = 'userStatusesPrimary';
    public const PERMISSION_USER_DISMISSAL = 'userDismissal';
    public const PERMISSION_USER_COMPANY_VIEW = 'userCompanyView';
    public const PERMISSION_USER_COMPANY_EDIT = 'userCompanyEdit';
    public const PERMISSION_USER_COMPANY_SALARY_VIEW = 'userCompanySalaryView';
    public const PERMISSION_USER_FOLLOW_ADD = 'userFollowAdd';
    public const PERMISSION_USER_FOLLOW_REMOVE = 'userFollowRemove';
    public const PERMISSION_USER_FOLLOW_LIST = 'userFollowList';
    public const PERMISSION_USER_FOLLOW_VIEW = 'userFollowView';
    public const PERMISSION_USER_SUPERVISOR_ADD = 'userSupervisorAdd';
    public const PERMISSION_USER_SUPERVISOR_REMOVE = 'userSupervisorRemove';
    public const PERMISSION_USER_SUPERVISOR_VIEW = 'userSupervisorView';
    public const PERMISSION_USER_SUPERVISOR_LIST = 'userSupervisorList';
    public const PERMISSION_USER_HISTORY_LIST = 'userHistoryList';
    public const PERMISSION_USER_FILES_ADD = 'userFilesAdd';
    public const PERMISSION_USER_FILES_REMOVE = 'userFilesRemove';
    public const PERMISSION_USER_FILES_LIST = 'userFilesList';
    public const PERMISSION_USER_PERFORMANCE_LIST = 'userPerformanceList';
    public const PERMISSION_USER_PERFORMANCE_ADD = 'userPerformanceAdd';
    public const PERMISSION_USER_PERFORMANCE_GRANT_ALL = 'userPerformanceGrantAll';
    public const PERMISSION_USER_PERFORMANCE_FORM_ADD = 'userPerformanceFormAdd';
    public const PERMISSION_USER_PERFORMANCE_FORM_EDIT = 'userPerformanceFormEdit';
    public const PERMISSION_USER_PERFORMANCE_FORM_VIEW = 'userPerformanceFormView';
    public const PERMISSION_USER_PERFORMANCE_FORM_DOWNLOAD = 'userPerformanceFormDownload';
    public const PERMISSION_USER_PERMISSIONS_LIST = 'userPermissionsList';
    public const PERMISSION_USER_PERMISSIONS_UPDATE = 'userPermissionsUpdate';
    public const PERMISSION_USER_LEAVES_STATS = 'userLeavesStats';
    public const PERMISSION_USER_ALERT_HISTORY = 'userAlertHistory';

    public const PERMISSION_USER_HOBBY_LIST = 'userHobbyList';
    public const PERMISSION_USER_SELF_EDIT = 'userSelfEdit';

    public const PERMISSION_USER = [
        self::PERMISSION_USER_CREATE,
        self::PERMISSION_USER_EDIT,
        self::PERMISSION_USER_LIST,
        self::PERMISSION_USER_VIEW,
        self::PERMISSION_USER_COMPANY_VIEW,
        self::PERMISSION_USER_COMPANY_EDIT,
        self::PERMISSION_USER_FOLLOW_ADD,
        self::PERMISSION_USER_FOLLOW_REMOVE,
        self::PERMISSION_USER_FOLLOW_VIEW,
        self::PERMISSION_USER_FOLLOW_LIST,
        self::PERMISSION_USER_SUPERVISOR_ADD,
        self::PERMISSION_USER_SUPERVISOR_REMOVE,
        self::PERMISSION_USER_SUPERVISOR_VIEW,
        self::PERMISSION_USER_SUPERVISOR_LIST,
        self::PERMISSION_USER_HISTORY_LIST,
        self::PERMISSION_USER_FILES_ADD,
        self::PERMISSION_USER_FILES_REMOVE,
        self::PERMISSION_USER_FILES_LIST,
        self::PERMISSION_USER_PERFORMANCE_LIST,
        self::PERMISSION_USER_PERFORMANCE_ADD,
        self::PERMISSION_USER_PERFORMANCE_FORM_ADD,
        self::PERMISSION_USER_PERFORMANCE_FORM_EDIT,
        self::PERMISSION_USER_PERFORMANCE_FORM_VIEW,
        self::PERMISSION_USER_PERMISSIONS_LIST,
        self::PERMISSION_USER_PERMISSIONS_UPDATE,
        self::PERMISSION_USER_LEAVES_STATS,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_USER_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_USER_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_USER_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_LIST_PRIMARY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'UserViewRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_COMPANY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_COMPANY_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FOLLOW_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FOLLOW_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FOLLOW_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FOLLOW_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_SUPERVISOR_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_SUPERVISOR_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_SUPERVISOR_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_SUPERVISOR_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_HISTORY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'UserSelfHistoryRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_FILES_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FILES_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_FILES_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'PerformanceRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'PerformanceRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_FORM_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'PerformanceAddRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_FORM_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'PerformanceEditRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_FORM_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'PerformanceViewRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_FORM_DOWNLOAD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'PerformanceViewRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERMISSIONS_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERMISSIONS_UPDATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_LEAVES_STATS,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_ALERT_HISTORY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_DISMISSAL,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR_HEAD,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_HOBBY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_STATUSES_PRIMARY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_SELF_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'UserSelfEditRule'
            ),
            new RbacItemCommand(self::PERMISSION_USER_COMPANY_SALARY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_USER_PERFORMANCE_GRANT_ALL,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
        ];
    }
}
