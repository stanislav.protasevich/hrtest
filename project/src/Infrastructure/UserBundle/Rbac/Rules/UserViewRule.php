<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac\Rules;


use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;

class UserViewRule implements RbacRuleInterface
{
    public function execute(User $user, $item, $params)
    {
        if (Gate::can(RbacPermissions::ROLE_ADMINISTRATOR) || Gate::can(RbacPermissions::ROLE_RECRUITER)) {
            return true;
        }

        /** @var User $owner */
        $owner = $params['model'] ?? null;

        if ($owner && $user->getId() === $owner->getId()) {
            return true;
        }

        return false;
    }
}