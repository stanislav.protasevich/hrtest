<?php
declare(strict_types=1);

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Rbac\Rules\helpers;


use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Infrastructure\UserBundle\Rbac\UserPermissions;

trait PerformanceHelperTrait
{
    protected function canAccess()
    {
        if (
            Gate::can(RbacPermissions::ROLE_ADMINISTRATOR) ||
            Gate::can(UserPermissions::PERMISSION_USER_PERFORMANCE_GRANT_ALL)
        ) {
            return true;
        }
    }

    protected function supervisorVerification(User $owner, User $user)
    {
        $supervisor = $owner->getSupervisor();
        if (!$supervisor) { // если нет супервайзера
            return false;
        }

        if ($user->getId() === $supervisor->getId()) { // если это супервайрез юзера
            return true;
        }
    }

    /**
     * @param User $owner
     */
    protected function isOwner(User $owner): void
    {
        if (!$owner) {
            throw new \LogicException('Owner user not found.');
        }
    }


    public function validateAuthorAccess(string $author, UserPerformance $performance, User $owner, User $user)
    {
        switch ($author) {
            case UserPerformanceForm::AUTHOR_HEAD:
                if ($owner->getSupervisor()->getId() == $user->getId()) {
                    return true;
                }

                break;

            case UserPerformanceForm::AUTHOR_EMPLOYEE:

                if ($performance->getUser()->getId() === $user->getId()) {
                    return true;
                }

                break;

            default:
                return false;
        }
    }
}