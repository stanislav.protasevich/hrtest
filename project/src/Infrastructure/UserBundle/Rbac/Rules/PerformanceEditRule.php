<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac\Rules;

use Domain\User\Entity\{User, UserPerformance, UserPerformanceForm};
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\UserBundle\Rbac\Rules\helpers\PerformanceHelperTrait;

class PerformanceEditRule implements RbacRuleInterface
{
    use PerformanceHelperTrait;

    public function execute(User $user, $item, $params)
    {
        $this->canAccess();

        /** @var User $owner */
        /** @var UserPerformance $performance */
        /** @var UserPerformanceForm $performanceForm */
        list('user' => $owner, 'performance' => $performance, 'performanceForm' => $performanceForm) = $params['model'];

        $this->isOwner($owner);

        if ($this->validateAuthorAccess($performanceForm->getAuthor(), $performance, $owner, $user)) {
            return true;
        }

        return false;
    }
}