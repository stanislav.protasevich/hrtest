<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac\Rules;

use Domain\User\Entity\{User, UserPerformance};
use Infrastructure\RbacBundle\{Contracts\RbacRuleInterface};
use Infrastructure\UserBundle\Rbac\{Rules\helpers\PerformanceHelperTrait};

class PerformanceAddRule implements RbacRuleInterface
{
    use PerformanceHelperTrait;

    public function execute(User $user, $item, $params)
    {
        $this->canAccess();

        /** @var User $owner */
        /** @var UserPerformance $performance */
        /** @var string $author */
        list('user' => $owner, 'performance' => $performance, 'author' => $author) = $params['model'];

        if ($this->validateAuthorAccess($author, $performance, $owner, $user)) {
            return true;
        }

        return false;
    }
}