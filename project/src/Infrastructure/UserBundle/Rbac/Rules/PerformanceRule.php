<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac\Rules;

use Domain\User\Entity\{User, UserPerformance, UserPerformanceForm};
use Infrastructure\RbacBundle\{Contracts\RbacRuleInterface, Rbac\RbacPermissions, Service\Gate};
use Infrastructure\UserBundle\Rbac\Rules\helpers\PerformanceHelperTrait;
use Infrastructure\UserBundle\Rbac\UserPermissions;

class PerformanceRule implements RbacRuleInterface
{
    use PerformanceHelperTrait;

    public function execute(User $user, $item, $params)
    {
        if (
            Gate::can(RbacPermissions::ROLE_ADMINISTRATOR) ||
            Gate::can(UserPermissions::PERMISSION_USER_PERFORMANCE_GRANT_ALL)
        ) {
            return true;
        }

        /** @var User $owner */
        /** @var UserPerformance $performance */
        /** @var UserPerformanceForm $performanceForm */
        list('user' => $owner, 'performance' => $performance, 'performanceForm' => $performanceForm) = $params['model'];

        if (!$owner) {
            throw new \LogicException('Owner user not found.');
        }
        
        if ($owner->getId() === $user->getId()) {

            if ($performanceForm->getEmployee()->getId() === $user->getId()) { // если это владелец формы
                return true;
            }

        } else {
            $this->supervisorVerification($owner, $user);
        }

        return false;
    }
}