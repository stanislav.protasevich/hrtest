<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Rbac\Rules;


use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;

class UserSelfEditRule implements RbacRuleInterface
{
    public function execute(User $user, $item, $params)
    {
        /** @var User $owner */
        $owner = $params['model'] ?? null;

        if ($owner && $user->getId() === $owner->getId()) {
            return true;
        }

        return false;
    }
}