<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Bus\Middleware;

use Domain\User\Entity\User;
use Infrastructure\UserBundle\Exception\UserInvalidStatusException;
use League\Tactician\Middleware;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class EventUserActiveMiddleware implements Middleware
{
    use ControllerTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     * @throws UserInvalidStatusException
     */
    public function execute($command, callable $next)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user) {
            return $next($command);
        }

        $status = $user->getStatus();

        /** @var RequestStack $requestStack */
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $route = $request->get('_route');

        $allowedRoutes = $this->container->getParameter('allowedDraftRoutes');

        if ($status === User::STATUS_ACTIVE) {
            return $next($command);
        } elseif ($status === User::STATUS_DRAFT && !in_array($route, $allowedRoutes)) {
            throw new UserInvalidStatusException('Please fill your account to continue.');
        } elseif ($status === User::STATUS_DRAFT) {
            return $next($command);
        }

        throw new UserInvalidStatusException('Contact your HR manager to activate your account.', Response::HTTP_BAD_REQUEST);
    }
}
