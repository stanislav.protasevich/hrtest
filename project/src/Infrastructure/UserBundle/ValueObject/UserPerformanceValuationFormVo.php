<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\ValueObject;

use Infrastructure\CommonBundle\Factory\DynamicObject;

/**
 * @property $jobKnowledge array
 * @property $productivity array
 * @property $speedWork array
 * @property $timeManagement array
 * @property $professionalAttitude array
 * @property $leadership array
 * @property $employeesDevelopment array
 * @property $valuingDiversity array
 * @property $decisionMaking array
 * @property $vision array
 */
class UserPerformanceValuationFormVo extends DynamicObject
{

}