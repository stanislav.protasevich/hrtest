<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\ValueObject;


use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformanceForm;

class UserPerformanceFormDownloadVo
{
    /**
     * @var array
     */
    private $template;
    /**
     * @var UserPerformanceForm
     */
    private $form;
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $filename;

    public function __construct(array $template, UserPerformanceForm $form, User $user, string $filename)
    {
        $this->template = $template;
        $this->form = $form;
        $this->user = $user;
        $this->filename = $filename;
    }

    /**
     * @return array
     */
    public function getTemplate(): array
    {
        return $this->template;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return UserPerformanceForm
     */
    public function getForm(): UserPerformanceForm
    {
        return $this->form;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }
}