<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\ValueObject;

use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\CommonBundle\Factory\DynamicObject;

/**
 * @property string $type
 * @property User $employee
 * @property string $author
 * @property UserPerformance $performance
 * @property UserPerformanceGeneralFormVo $general
 * @property UserPerformanceValuationFormVo $valuation
 * @property UserPerformanceQuestionnaireFormVo $questionnaire
 * @property UserPerformanceForm $form
 */
class UserPerformanceFormVo extends DynamicObject
{
    /**
     * @var string
     */
    public $type;
    /**
     * @var User
     */
    public $employee;
    /**
     * @var string
     */
    public $author;
    /**
     * @var UserPerformance
     */
    public $performance;
    /**
     * @var UserPerformanceForm
     */
    public $form;
}