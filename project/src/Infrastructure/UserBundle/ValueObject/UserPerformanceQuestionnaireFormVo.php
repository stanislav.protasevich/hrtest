<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\ValueObject;


use Infrastructure\CommonBundle\Factory\DynamicObject;

/**
 * @property string $question_1
 * @property string $question_2
 * @property string $question_3
 * @property string $question_4
 * @property string $question_5
 * @property string $question_6
 * @property string $question_7
 * @property string $question_8
 * @property string $question_9
 * @property string $question_10
 */
class UserPerformanceQuestionnaireFormVo extends DynamicObject
{

}