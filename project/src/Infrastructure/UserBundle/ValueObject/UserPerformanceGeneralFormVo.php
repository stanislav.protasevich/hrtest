<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\ValueObject;

use Domain\Job\Entity\JobTitle;
use Domain\Location\Entity\City;
use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\CommonBundle\Factory\DynamicObject;

class UserPerformanceGeneralFormVo extends DynamicObject
{
    /**
     * @var JobTitle
     */
    public $jobTitle;
    /**
     * @var City
     */
    public $city;
    /**
     * @var \DateTime
     */
    public $completionAt;
    /**
     * @var \DateTime
     */
    public $startAt;
    /**
     * @var \DateTime
     */
    public $endAt;
    /**
     * @var \DateTime
     */
    public $meetingAt;
    /**
     * @var UserPerformanceForm
     */
    public $form;
    /**
     * @var User
     */
    public $supervisor;
}