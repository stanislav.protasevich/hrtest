<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\UserBundle\Service;


use Domain\User\Entity\Hobby;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Infrastructure\CommonBundle\Service\BaseService;
use Symfony\Component\HttpFoundation\Request;

class HobbyService extends BaseService
{
    public function createHobbyFromRequest(Request $request)
    {
        $hobbiesArray = $request->get('hobbies', []);
        $created = [];

        if (!empty($hobbiesArray) && !ArrayHelper::isAssociative($hobbiesArray)) {
            $prepared = array_filter(
                array_unique(
                    $hobbiesArray
                )
            );

            if (!empty($prepared)) {
                $request->request->set('hobbies', $hobbiesArray);
                $created = $this->createFromArray($prepared);
            }
        }

        return $created;
    }

    public function createFromArray(array $hobbies)
    {
        $created = [];

        foreach ($hobbies as $hobby) {
            $created[] = $this->create($hobby);
        }

        return $created;
    }

    public function create(string $hobby): Hobby
    {
        $entityHobby = $this->getHobby($hobby);

        if (!$entityHobby) {
            $entityHobby = new Hobby();
            $entityHobby->setId($hobby);
            $entityHobby->setSortOrder(1);
            $this->getEm()->persist($entityHobby);
            $this->getEm()->flush();
        }

        return $entityHobby;
    }

    public function getHobby(string $hobby): ?Hobby
    {
        return $this->getEm()->getRepository(Hobby::class)->find($hobby);
    }
}
