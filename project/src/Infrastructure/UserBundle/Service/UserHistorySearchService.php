<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\LeavesBundle\Command\LeavesListCommand;
use Infrastructure\LeavesBundle\Service\LeavesBasicSearchService;
use Infrastructure\UserBundle\Command\History\UserHistoryListCommand;

class UserHistorySearchService extends FilterSortService
{
    protected $sortMapping = [
        'name' => 'timeOffType.name',
        'reason' => 'leaves.reason',
        'duration' => 'leaves.days',
    ];

    public function search(UserHistoryListCommand $command): QueryBuilder
    {
        $historyCommand = new LeavesListCommand([
            'startDate' => $command->startDate,
            'endDate' => $command->endDate,
            'status' => $command->status,
            'user' => $command->getUser(),
            'id' => $command->id,
        ]);

        /* @var $leaveService LeavesBasicSearchService */
        $leaveService = $this->container->get(LeavesBasicSearchService::class);

        $qb = $leaveService->search($historyCommand);

        $qb
            ->addSelect('timeOffType')
            ->leftJoin('leaves.timeOffType', 'timeOffType')
            ->andWhere('leaves.user = :user')
            ->setParameter('user', $command->getUser());

        if ($command->name && $timeOffType = $this->paramsFromString($command->name)) {
            $qb->andWhere('timeOffType.id IN (:timeOffType)')
                ->setParameter('timeOffType', $timeOffType);
        }

        if ($command->reason) {
            $qb->andHaving('leaves.reason LIKE :reason')
                ->setParameter('reason', $this->makeLikeParam((string)$command->reason));
        }

        if ($command->duration) {
            $qb->andHaving('leaves.days = :duration')
                ->setParameter('duration', (int)$command->duration);
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping, 'leaves.createdAt', 'DESC');

        return $qb;
    }
}
