<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Company\Entity\Company;
use Domain\Job\Entity\JobTitle;
use Domain\Location\Entity\City;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\UserBundle\Command\UserSearchCommand;

class UserSearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'user.id',
        'email' => 'user.email',
        'status' => 'user.status',
        'name' => ['profile.firstName', 'profile.middleName', 'profile.surname'],
        'city' => 'city.name',
        'company' => 'company.name',
        'jobTitle' => 'jobTitle.name',
    ];

    public function search(UserSearchCommand $command): QueryBuilder
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(User::class)->findAllQueryBuilder();

        $qb
            ->addSelect(['user', 'profile'])
            ->addSelect(['address', 'city'])
            ->addSelect(['employeeCompanies', 'company', 'jobTitle'])
            ->leftJoin('user.profile', 'profile')
            ->leftJoin('user.address', 'address')
            ->leftJoin('user.employeeCompanies', 'employeeCompanies')
            ->leftJoin('address.city', 'city')
            ->leftJoin('employeeCompanies.company', 'company')
            ->leftJoin('employeeCompanies.jobTitle', 'jobTitle')
            ->groupBy('user.id');

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('user.status IN (:status)')
                ->setParameter('status', $status);
        }

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('user.id IN (:id)')
                ->setParameter('id', $id);
        }

        if ($command->name) {
            $qb->andHaving('profile.firstName LIKE :name OR profile.middleName LIKE :name OR profile.surname LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$command->name));
        }

        if ($command->city && $city = $this->paramsFromString($command->city)) {
            $qb->andHaving('city IN (:city)')
                ->setParameter('city', $this->getModel(City::class, $city));
        }

        if ($command->company && $company = $this->paramsFromString($command->company)) {
            $qb->andHaving('company IN (:company)')
                ->setParameter('company', $this->getModel(Company::class, $company));
        }

        if ($command->jobTitle && $jobTitle = $this->paramsFromString($command->jobTitle)) {
            $qb->andHaving('jobTitle IN (:jobTitle)')
                ->setParameter('jobTitle', $this->getModel(JobTitle::class, $jobTitle));
        }

        if ($command->atWork && $period = $this->periodFromString($command->atWork)) {
            $qb->andHaving('employeeCompanies.startAt BETWEEN :start AND :end')
                ->setParameter('start', $period[0])
                ->setParameter('end', $period[1]);
        }

        if ($command->birthday && $birthday = $this->periodFromString($command->birthday)) {
            [$start, $end] = $birthday;
            $qb
                ->andHaving('DATE_FORMAT(profile.birthday,\'%m-%d\') BETWEEN :start AND :end')
                ->setParameter('start', $start->format('m-d'))
                ->setParameter('end', $end->format('m-d'));
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

//        return $qb->getQuery()->getResult();
        return $qb;
    }
}
