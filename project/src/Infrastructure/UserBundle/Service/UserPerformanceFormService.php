<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Domain\User\Entity\UserPerformanceForm;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormVo;

class UserPerformanceFormService extends BaseService
{
    public function createForm(UserPerformanceFormVo $vo): UserPerformanceForm
    {
        $em = $this->getEm();

        $form = new UserPerformanceForm();
        $form->setType($vo->type);
        $form->setPerformance($vo->performance);
        $form->setAuthor($vo->author);
        $form->setEmployee($vo->employee);

        $this->populateAttributes($vo, $form);

        $vo->descriptions = $vo->descriptions->toArray();

        $now = new \DateTime();
        $form->setCreatedAt($now);
        $form->setCreatedBy($this->getUser());

        $em->persist($form);
        $em->flush();

        return $form;
    }

    /**
     * @param UserPerformanceFormVo $vo
     * @param $form
     */
    protected function populateAttributes(UserPerformanceFormVo $vo, UserPerformanceForm $form): void
    {
        $now = new \DateTime();
        $form->setJobTitle($vo->general->jobTitle);
        $form->setCity($vo->general->city);
        $form->setSupervisor($vo->general->supervisor);
        $form->setCompletionAt($vo->general->completionAt);
        $form->setStartAt($vo->general->startAt);
        $form->setEndAt($vo->general->endAt);
        $form->setMeetingAt($vo->general->meetingAt);

        $data = [
            'valuation' => $vo->valuation->getAttributes(),
        ];

        if ($vo->questionnaire) {
            $data['questionnaire'] = $vo->valuation->getAttributes();
        }

        $form->setData($data);

        $form->setUpdatedAt($now);
        $form->setUpdatedBy($this->getUser());
    }

    public function updateForm(UserPerformanceFormVo $vo): UserPerformanceForm
    {
        $em = $this->getEm();

        $form = $vo->form;
        $this->populateAttributes($vo, $form);

        $vo->descriptions = $vo->descriptions->toArray();

        $em->persist($form);
        $em->flush();

        return $form;
    }
}