<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Carbon\Carbon;
use Domain\User\Entity\UserAlert;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\UserBundle\Command\Alert\UserAlertCommand;
use Ramsey\Uuid\Uuid;

class UserAlertService extends BaseService
{
    public function create(UserAlertCommand $command): UserAlert
    {
        $em = $this->getEm();

        $id = (Uuid::uuid4())->toString();

        $qb = new \Doctrine\DBAL\Query\QueryBuilder($em->getConnection());
        $qb
            ->insert('u_user_alert')
            ->values([
                'id' => ':id',
                'user_id' => ':user_id',
                'content_type' => ':content_type',
                'content_id' => ':content_id',
                'action' => ':action',
                'created_at' => ':created_at',
                'message' => ':message',
                'view_at' => ':view_at',
                'alerted_user_id' => ':alerted_user_id',
                '`read`' => ':read',
            ])
            ->setParameter(':id', $id)
            ->setParameter(':user_id', $command->getUser()->getId())
            ->setParameter(':content_type', $command->getContentType())
            ->setParameter(':content_id', $command->getContentId())
            ->setParameter(':action', $command->getAction())
            ->setParameter(':message', $command->getMessage())
            ->setParameter(':created_at', Carbon::now())
            ->setParameter(':view_at', null)
            ->setParameter(':alerted_user_id', $command->getAlertedUser()->getId())
            ->setParameter(':read', 0)
            ->execute();

//        $model = new UserAlert();
//        $model->setUser($command->getUser());
//        $model->setContentType($command->getContentType());
//        $model->setContentId($command->getContentId());
//        $model->setAction($command->getAction());
//        $model->setCreatedAt(new \DateTime());
//        $model->setAlertedUser($command->getAlertedUser());
//        $model->setRead(false);
//        $model->setViewAt(null);
//
//        $em->persist($model);
//        $em->flush();

        return $em->getRepository(UserAlert::class)->find($id);
    }
}