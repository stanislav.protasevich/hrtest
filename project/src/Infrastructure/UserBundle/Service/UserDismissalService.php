<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Service;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class UserDismissalService extends BaseService
{
    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function dismissal(User $user): void
    {
        $em = $this->getEm();

        $user->setStatus(User::STATUS_DISMISSAL);

        $em->persist($user);
        $em->flush();
    }
}
