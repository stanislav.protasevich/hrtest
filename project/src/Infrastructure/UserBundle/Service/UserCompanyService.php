<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\EmployeeBundle\Event\CareerHistoryEvent;

class UserCompanyService extends BaseService
{
    /**
     * @param User $user
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateCompany(User $user)
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $count = $user->getEmployeeCompanies()->count();
            $isMainExist = false;

            foreach ($user->getEmployeeCompanies() as $employeeCompany) {
                $employeeCompany->setUpdatedAt($this->now());
                $employeeCompany->setUpdatedBy($this->getUser());

                if (!$employeeCompany->getCreatedBy()) {
                    $employeeCompany->setCreatedBy($this->getUser());
                }

                if ($count === 1) {
                    $employeeCompany->setIsMain(true);
                }

                if ($count === 1 || $isMainExist === false && $employeeCompany->getStatus() === true) {
                    $isMainExist = true;
                }

                if ($employeeCompany->getStatus() === EmployeeCompany::STATUS_DISMISSED) {
                    $user->removeEmployeeCompany($employeeCompany);
                    $this->event(EmployeeCompany::STATUS_DISMISSED, new CareerHistoryEvent($employeeCompany));
                    continue;
                }

                /* @var $employeeCompany EmployeeCompany */
                if (!$employeeCompany->getCreatedAt()) {
                    $employeeCompany->setCreatedAt($this->now());

                    if ($employeeCompany->getStatus() === EmployeeCompany::STATUS_EMPLOYED) {
                        $this->event(EmployeeCompany::STATUS_EMPLOYED, new CareerHistoryEvent($employeeCompany));
                    }

                    if ($employeeCompany->getStatus() === EmployeeCompany::STATUS_MATERNITY_LEAVE) {
                        $this->event(EmployeeCompany::STATUS_MATERNITY_LEAVE, new CareerHistoryEvent($employeeCompany));
                    }
                }

                $em->persist($employeeCompany);
            }

            if ($isMainExist === false) {
                /** @var EmployeeCompany $firstCompany */
                $firstCompany = $user->getEmployeeCompanies()->last();
                $firstCompany->setIsMain(true);
                $em->persist($firstCompany);
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function dismissal(User $user): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            foreach ($user->getEmployeeCompanies() as $employeeCompany) {
                $employeeCompany->setUpdatedAt($this->now());
                $employeeCompany->setUpdatedBy($this->getUser());

                $employeeCompany->setEndAt($this->now());
                $employeeCompany->setStatus(EmployeeCompany::STATUS_DISMISSED);
                $em->persist($employeeCompany);
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }
}
