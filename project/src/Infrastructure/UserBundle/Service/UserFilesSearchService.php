<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Domain\Archive\Entity\File;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\UserBundle\Command\Files\UserFilesListCommand;

class UserFilesSearchService extends FilterSortService
{
    protected $sortMapping = [
        'name' => 'file.title',
        'size' => 'file.size',
        'createdAt' => 'file.createdAt',
    ];

    public function search(UserFilesListCommand $command)
    {
        $qb = $this->getEm()->getRepository(File::class)->findAllQueryBuilder();

        $qb
            ->where('file.user = :user')
            ->setParameter('user', $command->getUser());

        if ($command->name) {
            $qb->andWhere('file.title LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$command->name));
        }
        
        if ($command->createdAt && $period = $this->periodFromString($command->createdAt)) {
            $qb->andHaving('file.createdAt BETWEEN :start AND :end')
                ->setParameter('start', $period[0])
                ->setParameter('end', $period[1]);
        }

        if ($command->size) {
            $qb->andWhere('file.size = :size')
                ->setParameter('size', $command->size * 1000000);
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
