<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class UserRbacService extends BaseService
{
    /**
     * @param User $user
     * @param array $collection
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function updateRbacItem(User $user, array $collection): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {

            $assigned = $user->getPermissions();

            foreach ($assigned as $item) {
                $item->removeAssignment($user);
                $em->persist($item);
            }

            foreach ($collection as $permission) {
                $user->addPermission($permission);
                $em->persist($user);
            }

            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     * @param AuthItem $authItem
     * @return bool
     */
    protected function isExist(User $user, AuthItem $authItem): bool
    {
        $assignments = $authItem->getAssignment();

        $exist = $assignments->exists(function ($index, User $assignmentUser) use ($user) {
            return $assignmentUser->getId() === $user->getId();
        });
        return $exist;
    }
}