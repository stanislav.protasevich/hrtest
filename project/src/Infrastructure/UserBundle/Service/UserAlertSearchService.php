<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\User\Entity\User;
use Domain\User\Entity\UserAlert;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\UserBundle\Command\Alert\UserAlertHistoryCommand;

class UserAlertSearchService extends FilterSortService
{
    /**
     * @param UserAlertHistoryCommand $command
     * @return QueryBuilder
     */
    public function search(UserAlertHistoryCommand $command): QueryBuilder
    {
        $qb = $this->getEm()->getRepository(UserAlert::class)->findAllQueryBuilder();

        $qb
            ->select(['alert'])
            ->where('alert.alertedUser = :alertedUser')
            ->setParameter('alertedUser', $command->getAlertedUser())
            ->orderBy('alert.read', 'ASC')
            ->orderBy('alert.createdAt', 'DESC');

        if ($command->user && $user = $this->paramsFromString($command->user)) {
            $qb->andWhere('alert.user IN (:user)')
                ->setParameter('user', $this->getModel(User::class, $user));
        }

        if ($command->createdAt && $period = $this->periodFromString($command->createdAt)) {
            $qb->andWhere('alert.createdAt BETWEEN :start AND :end')
                ->setParameter('start', $period[0])
                ->setParameter('end', $period[1]);
        }

        return $qb;
    }
}
