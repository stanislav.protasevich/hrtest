<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Domain\User\Entity\UserProfile;
use Infrastructure\AuthBundle\Helpers\PasswordResetTokenHelper;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\CommonBundle\Service\FileUploader;
use Infrastructure\EmployeeBundle\Service\EmployeeService;
use Infrastructure\UserBundle\Command\UserCreateCommand;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Workflow\Workflow;

class UserService extends BaseService
{
    /**
     * @param UserCreateCommand $command
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(UserCreateCommand $command): User
    {
        $user = new User();

        $user->setPassword($command->password ?: uniqid());
        $this->setPassword($user);

        $user->setStatus($command->status);
        $user->setPasswordResetToken(PasswordResetTokenHelper::generateResetToken());

        $employeeCompany = new EmployeeCompany();
        $employeeCompany->setCompany($command->employeeCompany->getCompany());
        $employeeCompany->setJobTitle($command->employeeCompany->getJobTitle());
        $employeeCompany->setIsMain(true);
        $employeeCompany->setSalary($command->employeeCompany->getSalary());
        $employeeCompany->setSalaryIncrease($command->employeeCompany->getSalaryIncrease());
        $employeeCompany->setCurrency($command->employeeCompany->getCurrency());
        $employeeCompany->setWorkType($command->employeeCompany->getWorkType());
        $employeeCompany->setStatus($command->employeeCompany->getStatus());
        $employeeCompany->setProbationPeriod($command->employeeCompany->getProbationPeriod());
        $employeeCompany->setStartAt($command->employeeCompany->getStartAt());
        $employeeCompany->setCreatedAt($this->now());
        $employeeCompany->setUpdatedAt($this->now());
        $employeeCompany->setCreatedBy($this->getUser());
        $employeeCompany->setUpdatedBy($this->getUser());

        $user->addEmployeeCompany($employeeCompany);

        $user->setProfile($command->profile);
        $user->setEmployee($command->employee);

        foreach ($command->recruiters as $recruiter) {
            $user->addRecruiter($recruiter);
        }

        $this->update($user);

        return $user;
    }

    /**
     * @param User $user
     */
    protected function setPassword(User $user): void
    {
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $user->getNewPassword());
        $user->setPasswordHash($password);
    }

    /**
     * @param User $user
     * @return User|null
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function update(User $user)
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            if ($user->getNewPassword()) {
                $this->setPassword($user);
            }

            $profile = $user->getProfile();
            $user->setUpdatedAt(new \DateTime());

            $em->persist($profile);
            $em->persist($user->getEmployee());

            if ($address = $user->getAddress()) {
                $em->persist($address);
            }

            foreach ($user->getEmergencyContacts() as $contact) {
                if (!$contact->getCreatedAt()) {
                    $contact->setCreatedAt(new \DateTime());
                }

                $contact->setUpdatedAt(new \DateTime());

                $em->persist($contact);
            }

            $em->persist($user);
            $em->flush();

            $this->setProfileAvatar($profile);

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param UserProfile $profile
     * @throws \Exception
     */
    public function setProfileAvatar(UserProfile $profile): void
    {
        $avatar = $profile->getAvatar();

        if ($avatar instanceof File) {
            /* @var $uploader FileUploader */
            $uploader = $this->container->get(FileUploader::class);

            $path = $this->getAbsolutePath($profile->getUser()->getId());
            $uploader->setPath($path);
            $filename = $uploader->upload($avatar);

            $profile->setAvatar($filename);
        }
    }

    public function getAbsolutePath($id)
    {
        return "user/{$id}/avatars";
    }

    /**
     * @param User $user
     * @param ArrayCollection|User[] $follows
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function addFollows(User $user, ArrayCollection $follows): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $followersArray = $user->getFollowers()->getValues();

            foreach ($follows as $follow) {
                if (in_array($follow, $followersArray) === false) {
                    $user->addFollower($follow);
                }
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     * @param ArrayCollection $supervisors
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function addSupervisors(User $user, ArrayCollection $supervisors): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $supervisorsArray = $user->getSupervisors()->getValues();

            foreach ($supervisors as $supervisor) {
                if (in_array($supervisor, $supervisorsArray) === false) {
                    $user->addSupervisor($supervisor);
                }
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     * @param ArrayCollection|User[] $follows
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function removeFollows(User $user, ArrayCollection $follows): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $followersArray = $user->getFollowers()->getValues();

            foreach ($follows as $follow) {
                if (in_array($follow, $followersArray)) {
                    $user->removeFollower($follow);
                }
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    /**
     * @param User $user
     * @param ArrayCollection|User[] $supervisors
     * @return User
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function removeSupervisors(User $user, ArrayCollection $supervisors): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {

            $supervisorsArray = $user->getSupervisors()->getValues();

            foreach ($supervisors as $supervisor) {
                if (in_array($supervisor, $supervisorsArray)) {
                    $user->removeSupervisor($supervisor);
                }
            }

            $em->persist($user);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    public function updateSupervisors(User $user): User
    {
        $em = $this->getEm();

        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param string $password
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resetPassword(User $user, string $password): User
    {
        $em = $this->getEm();

        $hash = $this->container->get('security.password_encoder')->encodePassword($user, $password);

        $user->setPasswordHash($hash);
        $user->setPasswordResetToken(null);
        $user->setSalt();
        $user->setUpdatedAt(new \DateTime());

        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setStatusEmailBounce(User $user)
    {
        $em = $this->getEm();

        $user->setStatus(User::STATUS_EMAIL_BOUNCE);

        $em->persist($user);
        $em->flush();
//        $this->changeStatusWorkflow($user, 'bad_email');
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setStatusActive(User $user)
    {
        $this->changeStatusWorkflow($user, 'registered');
    }

    /**
     * @param User $user
     * @param string $step
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function changeStatusWorkflow(User $user, string $step)
    {
        $em = $this->getEm();

        /* @var $workflows Workflow */
        $workflow = $this->container->get('workflow.user');
        $workflow->apply($user, $step);

        $em->persist($user);
        $em->flush();
    }

    /**
     * @return User[]
     */
    public function getPrimaryAll(): array
    {
        return $this->getEm()->getRepository(User::class)->findPrimaryAll();
    }

    protected function getEmployeeService(): EmployeeService
    {
        return $this->container->get(EmployeeService::class);
    }
}
