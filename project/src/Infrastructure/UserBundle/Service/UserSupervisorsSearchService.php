<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Database\LikeQueryHelpers;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\UserBundle\Command\Supervisor\UserListSupervisorCommand;

class UserSupervisorsSearchService extends BaseService
{
    use LikeQueryHelpers;

    public function search(UserListSupervisorCommand $command): array
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(User::class)->findAllQueryBuilder();

        $user = $command->getUser();
        $supervisors = $user->getSupervisors()->getValues();
        $supervisors[] = $user;

        $qb
            ->andWhere('user NOT IN (:users)')
            ->andWhere('user.status = :status')
            ->setParameter('users', $supervisors)
            ->setParameter('status', User::STATUS_ACTIVE);

        $name = $command->getName();

        if ($name && is_string($name)) {
            $qb
                ->select('user, profile')
                ->leftJoin('user.profile', 'profile')
                ->andHaving('profile.firstName LIKE :name OR profile.middleName LIKE :name OR profile.surname LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$name));
        }

        return $qb->getQuery()->getResult();
    }
}