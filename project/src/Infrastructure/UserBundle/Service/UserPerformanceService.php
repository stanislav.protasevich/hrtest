<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\PersistentCollection;
use Domain\User\Entity\User;
use Domain\User\Entity\UserPerformance;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\UserBundle\Factory\Form\Performance\First\Employee\UserPerformanceFirstEmployeeFormType;
use Infrastructure\UserBundle\Factory\Form\Performance\First\Head\UserPerformanceFirstHeadFormType;
use Infrastructure\UserBundle\Factory\Form\Performance\Second\Employee\UserPerformanceSecondEmployeeFormType;
use Infrastructure\UserBundle\Factory\Form\Performance\Second\Head\UserPerformanceSecondHeadFormType;
use Infrastructure\UserBundle\Factory\Form\Performance\Trial\Head\UserPerformanceTrialHeadFormType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserPerformanceService extends BaseService
{
    public function create(User $user): User
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $now = new \DateTime();

            $trial = new UserPerformance();
            $trial->setType(UserPerformance::TYPE_TRIAL);
            $trial->setUser($user);
            $trial->setStatus(UserPerformance::STATUS_PENDING);
            $trial->setCreatedAt($now);
            $trial->setUpdatedAt($now);
            $trial->setCreatedBy($this->getUser());
            $trial->setUpdatedBy($this->getUser());
            $trial->setDate(
                (new \DateTime())->add(new \DateInterval(UserPerformance::PERIOD_TRIAL))
            );

            $em->persist($trial);

            $first = new UserPerformance();
            $first->setUser($user);
            $first->setType(UserPerformance::TYPE_FIRST_PE);
            $first->setStatus(UserPerformance::STATUS_PENDING);
            $first->setCreatedAt($now);
            $first->setUpdatedAt($now);
            $first->setCreatedBy($this->getUser());
            $first->setUpdatedBy($this->getUser());
            $first->setDate(
                (new \DateTime())->add(new \DateInterval(UserPerformance::PERIOD_FIRST))
            );

            $em->persist($first);

            $second = new UserPerformance();
            $second->setUser($user);
            $second->setType(UserPerformance::TYPE_SECOND_PE);
            $second->setStatus(UserPerformance::STATUS_PENDING);
            $second->setCreatedAt($now);
            $second->setUpdatedAt($now);
            $second->setCreatedBy($this->getUser());
            $second->setUpdatedBy($this->getUser());
            $second->setDate(
                (new \DateTime())->add(new \DateInterval(UserPerformance::PERIOD_SECOND))
            );

            $em->persist($second);

            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $user;
    }

    public function getLastClosedByUser(PersistentCollection $collection): ?UserPerformance
    {
        $item = $collection->filter(function (UserPerformance $performance) {
            return $performance->getStatus() === UserPerformance::STATUS_COMPLETED;
        })->last();

        return $item ?: null;
    }

    public function getAllByUser(User $user)
    {
        return $this->getEm()->getRepository(UserPerformance::class)->findAllByUser($user);
    }

    /**
     * @param string $status
     * @return UserPerformance[]
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changeOutdatedStatus(string $status = UserPerformance::STATUS_OVERDUE)
    {
        $em = $this->getEm();
        $performances = $em->getRepository(UserPerformance::class)->findAllOutdated();

        foreach ($performances as $performance) {
            $performance->setStatus($status);
            $performance->setUpdatedAt(new \DateTime());
            $em->persist($performance);
        }

        $em->flush();

        return $performances;
    }

    public function getFormTemplate(string $type, ?string $author)
    {
        $needle = $type . '_' . $author;

        $formType = null;

        switch (strtolower($needle)) {
            case 'trial_head':
                $formType = UserPerformanceTrialHeadFormType::class;
                break;

            case 'first_employee':
                $formType = UserPerformanceFirstEmployeeFormType::class;
                break;

            case 'first_head':
                $formType = UserPerformanceFirstHeadFormType::class;
                break;

            case 'second_head':
                $formType = UserPerformanceSecondHeadFormType::class;
                break;

            case 'second_employee':
                $formType = UserPerformanceSecondEmployeeFormType::class;
                break;
        }

        if (!$formType) {
            throw new NotFoundHttpException(sprintf('Form type: %s not found.', $formType));
        }

        return $formType;
    }
}