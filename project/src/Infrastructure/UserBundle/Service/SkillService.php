<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\UserBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\User\Entity\Skill;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\UserBundle\Command\Skill\SkillCreateCommand;
use Infrastructure\UserBundle\Command\Skill\SkillEditCommand;

class SkillService extends BaseService
{
    /**
     * @param SkillCreateCommand $command
     * @return Skill
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(SkillCreateCommand $command): Skill
    {
        return $this->update($command->getSkill());
    }

    /**
     * @param Skill $skill
     * @return Skill
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function update(Skill $skill): Skill
    {
        $em = $this->getEm();

        $em->persist($skill);
        $em->flush();

        return $skill;
    }

    /**
     * @param SkillEditCommand $command
     * @return Skill
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(SkillEditCommand $command): Skill
    {
        return $this->update($command->getSkill());
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $em = $this->getEm();

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(Skill::class)->findAllQueryBuilder();

        return $qb->getQuery()->getResult();
    }
}