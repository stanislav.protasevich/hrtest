<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\User\Entity\Skill;
use Domain\User\Entity\User;
use Faker\Factory;
use Faker\Provider\Lorem;
use Faker\Provider\Person;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserSkillLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();

        $faker = Factory::create();
        $faker->addProvider(new Person($faker));
        $faker->addProvider(new Lorem($faker));

        $skills = [];
        for ($i = 0; $i < 20; $i++) {
            $skill = new Skill();
            $skill->setName($faker->word);
            $skill->setSortOrder($i);

            $manager->persist($skill);

            $skills[] = $skill;
        }
        $manager->flush();


        foreach ($users as $user) {

            for ($i = 0; $i <= rand(0, count($skills)); $i++) {
                $user->addSkill($skills[$i]);
            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserLoad::class,
        ];
    }
}