<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Employee\Entity\CareerHistory;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;

class CareerHistoryLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();
        
        foreach ($users as $user) {

            if (!empty($user->getEmployeeCompanies()->getValues())) {

                /* @var $employeeCompanies EmployeeCompany[] */
                $employeeCompanies = $user->getEmployeeCompanies();

                foreach ($employeeCompanies as $employeeCompany) {

                    $job = $employeeCompany->getJobTitle();

                    $history = new CareerHistory();
                    $history->setType(CareerHistory::TYPE_EMPLOYED);
                    $history->setCompany($employeeCompany->getCompany());
                    $history->setUser($user);
                    $history->setJobTitle($job);

                    $history->setCreatedAt(new \DateTime());
                    $history->setUpdatedAt(new \DateTime());
                    $history->setCreatedBy($user);
                    $history->setUpdatedBy($user);

                    $manager->persist($history);
                }
            }

        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EmployeeCompanyLoad::class,
        ];
    }
}
