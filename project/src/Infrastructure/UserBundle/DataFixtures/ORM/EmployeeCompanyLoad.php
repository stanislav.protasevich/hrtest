<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Entity\JobTitle;
use Domain\System\Entity\Currency;
use Domain\User\Entity\User;
use Infrastructure\CompanyBundle\DataFixtures\ORM\CompanyLoad;
use Infrastructure\JobBundle\DataFixtures\ORM\JobLoad;
use Infrastructure\SystemBundle\DataFixtures\ORM\CurrencyLoad;

class EmployeeCompanyLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();
        $companies = $manager->getRepository(Company::class)->findAll();
        $departments = $manager->getRepository(Department::class)->findAll();
        $currency = $manager->getRepository(Currency::class)->findAll();
        $companiesCount = count($companies);

        $jobTitles = $manager->getRepository(JobTitle::class)->findAll();
        $jobTitlesCount = count($jobTitles);

        $departmentsCount = count($departments) - 1;

        $workTypes = EmployeeCompany::WORK_TYPES;
        $workTypesCount = count($workTypes) - 1;

        foreach ($users as $user) {
            for ($i = 0; $i < rand(1, 3); $i++) {
                $model = new EmployeeCompany();
                $model->setUser($user);

                $model->setSalary(rand(10, 5000));
                $model->setCurrency($currency[0]);
                $model->setWorkType($workTypes[rand(0, $workTypesCount)]);

                $rndCompany = $this->rnd($companiesCount);
                $model->setCompany($companies[$rndCompany]);

                $rndJobTitles = $this->rnd($jobTitlesCount);
                $model->setJobTitle($jobTitles[$rndJobTitles]);

                $model->setIsMain($i === 0 ? true : false);

//                $model->setJobTitle($jobTitles[$rndJobTitle]);

                $departmentIndex = rand(0, $departmentsCount);
                $department = $departments[$departmentIndex];

                $model->setDepartment($department);
                $model->setCreatedBy($user);
                $model->setUpdatedBy($user);
                $model->setCreatedAt(new \DateTime());
                $model->setUpdatedAt(new \DateTime());

                $manager->persist($model);
            }
        }

        $manager->flush();
    }

    protected function rnd(int $counts)
    {
        if ($counts === 1) {
            return 0;
        }

        $count = rand(0, $counts - 1);

        return $count;
    }


    public function getDependencies()
    {
        return [
            UserLoad::class,
            CompanyLoad::class,
            JobLoad::class,
            CurrencyLoad::class,
        ];
    }
}