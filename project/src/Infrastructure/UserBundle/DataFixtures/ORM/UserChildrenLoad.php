<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\User\Entity\User;
use Domain\User\Entity\UserChildren;
use Domain\User\Entity\UserProfile;
use Faker\Factory;
use Faker\Provider\Person;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserChildrenLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();
        $faker = Factory::create();
        $faker->addProvider(new Person($faker));

        foreach ($users as $user) {

            for ($i = 0; $i <= rand(1, 5); $i++) {
                $child = new UserChildren();
                $child->setName($faker->firstName);
                $child->setBirthday($faker->dateTime);
                $child->setGender(rand(0, 1) ? UserProfile::GENDER_MAN : UserProfile::GENDER_WOMAN);
                $child->setSortOrder($i);
                $child->setCreatedAt($faker->dateTime);
                $child->setUpdatedAt($faker->dateTime);

                $manager->persist($child);
                $user->addChild($child);
            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserLoad::class,
        ];
    }
}