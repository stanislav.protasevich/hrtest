<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\User\Entity\User;
use Domain\User\Entity\UserProfile;
use Faker\Factory;
use Faker\Generator;
use Faker\Provider\en_US\Person;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserLoad extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(User::class);

        $faker = Factory::create();
        $faker->addProvider(new Person($faker));
        $faker->addProvider(new \Application\Vendor\Faker\Provider\Image($faker));

        if (!$userRepo->findBy(['email' => 'admin@admin.com'])) {
            $this->createUser($manager, 'admin@admin.com', $faker, User::STATUS_ACTIVE, 2);
        }

        for ($i = 1; $i < 50; $i++) {

            $email = $faker->email;

            if ($userRepo->findBy(['email' => $email])) {
                continue;
            }

            $this->createUser($manager, $email, $faker, rand(0, 1) ? User::STATUS_ACTIVE : User::STATUS_DISABLED, $i);
        }

    }

    /**
     * @param ObjectManager $manager
     * @param $email
     * @param Generator $faker Factory
     * @param string $status
     * @param int $i
     * @return void
     * @throws \Exception
     */
    protected function createUser(ObjectManager $manager, $email, Generator $faker, string $status, int $i)
    {
        $user = new User();

        $password = $this->encoder->encodePassword($user, '123456');
        $user->setPasswordHash($password);

        $user->setEmail($email);
        $user->setStatus($status);
        $user->setCreatedAt(new \DateTime());
        $user->setUpdatedAt(new \DateTime());

        $profile = new UserProfile();
        $profile->setFirstName($faker->firstName);
        $profile->setSurname($faker->lastName);
        $profile->setPhone($faker->phoneNumber);
        $profile->setMaritalStatus(rand(0, 1));
        $profile->setMarriageAnniversary(new \DateTime($faker->date));
        $profile->setGender($i % 2 === 0 ? UserProfile::GENDER_MAN : UserProfile::GENDER_WOMAN);
        $profile->setBirthday($faker->dateTime);
        $profile->setAvatar($faker->imageUrl(640, 480, 'people'));

        $manager->persist($profile);
        $user->setProfile($profile);

        $manager->persist($user);
        
        $manager->flush();
    }
}
