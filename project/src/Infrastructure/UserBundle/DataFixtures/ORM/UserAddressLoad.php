<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Location\Entity\City;
use Domain\User\Entity\User;
use Domain\User\Entity\UserAddress;
use Faker\Factory;
use Faker\Provider\Person;
use Infrastructure\LocationBundle\DataFixtures\ORM\LocationLoad;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserAddressLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();
        $faker = Factory::create();
        $faker->addProvider(new Person($faker));

        /* @var $cities City[] */
        $cities = $manager->getRepository(City::class)->findAll();

        $cityCount = count($cities) - 1;

        foreach ($users as $key => $user) {

            $cityIndex = rand(0, $cityCount);
            /* @var $city City */
            $city = $cities[$cityIndex];

            $address = new UserAddress();
            $address->setCity($city);
            $address->setRegion($city->getRegion());
            $address->setCountry($city->getCountry());
            $address->setType(rand(0, 1) ? UserAddress::ADDRESS_TYPE_ACTUAL : UserAddress::ADDRESS_TYPE_POST);
            $address->setSortOrder($key);
            
            $address->setPostCode($faker->postcode);
            $address->setStreet($faker->streetName);
            $address->setBuilding($faker->buildingNumber);
            $address->setLatitude($faker->latitude);
            $address->setLongitude($faker->longitude);

            $address->setRoom(rand(1, 1000));

            $manager->persist($address);

            $user->setAddress($address);
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserLoad::class,
            LocationLoad::class,
        ];
    }
}