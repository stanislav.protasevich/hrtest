<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\UserBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\User\Entity\User;
use Faker\Factory;
use Faker\Provider\en_US\Person;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserRequitersLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container, UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $users = $manager->getRepository(User::class)->findAll();

        $faker = Factory::create();
        $faker->addProvider(new Person($faker));

        $count = count($users);

        foreach ($users as $i => $user) {

            for ($i = 0; $i <= rand(1, 3); $i++) {

                /** @var User $rec */
                $rec = $users[rand(0, $count - 1)];

                if ($rec->getId() !== $user->getId() &&
                    !$user->getRecruiters()->exists(function ($index, User $owner) use ($rec) {
                        return $owner->getId() === $rec->getId();
                    })) {
                    $user->addRecruiter($rec);
                }

            }

            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserLoad::class
        ];
    }
}
