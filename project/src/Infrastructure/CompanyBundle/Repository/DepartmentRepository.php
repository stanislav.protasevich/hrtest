<?php

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Department|null find($id, $lockMode = null, $lockVersion = null)
 * @method Department|null findOneBy(array $criteria, array $orderBy = null)
 * @method Department[]    findAll()
 * @method Department[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DepartmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Department::class);
    }

    /**
     * @param string $id
     * @return Department|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?Department
    {
        return $this->createQueryBuilder('d')
            ->where('d.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllActiveById(array $ids): array
    {
        return $this->createQueryBuilder('d')
            ->where('d.id IN (:id)')
            ->setParameter('id', $ids)
            ->getQuery()
            ->getResult();
    }

    public function findAllActiveByCompany(?Company $company): array
    {
        return $this->queryFindAllActiveByCompany($company)->getQuery()->getResult();
    }

    public function queryFindAllActiveByCompany(?Company $company)
    {
        return $this->findAllQueryBuilder()
            ->select('department, company')
            ->join('department.company', 'company')
            ->andWhere('company = :company')
            ->setParameter(':company', $company);
    }

    public function findAllQueryBuilder($alias = 'department')
    {
        return $this->createQueryBuilder($alias);
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder('d')
            ->where('d.status = :status')
            ->setParameter('status', Department::STATUS_ACTIVE);
    }

}
