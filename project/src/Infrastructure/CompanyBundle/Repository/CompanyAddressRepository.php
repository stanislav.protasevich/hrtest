<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\CompanyAddress;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CompanyAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyAddress[]    findAll()
 * @method CompanyAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyAddressRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CompanyAddress::class);
    }

    public function getAllAddresses(): array
    {
        return $this->getAddressQuery()
            ->getQuery()
            ->getArrayResult();
    }

    public function getAddressQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('address')
            ->select('address.postCode as postCode, ' .
                'country.name as countryName, ' .
                'region.name as regionName, ' .
                'city.name as cityName, ' .
                'address.street, address.building, address.room, address.id')
            ->addSelect('company.name as companyName')
            ->where('company.status = :status')
            ->setParameter('status', Company::STATUS_ACTIVE)
            ->leftJoin('address.country', 'country')
            ->leftJoin('address.company', 'company')
            ->leftJoin('address.region', 'region')
            ->leftJoin('address.city', 'city');
    }

    public function getAddress(string $id): ?CompanyAddress
    {
        return $this->getAddressQuery()
            ->where('address.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
