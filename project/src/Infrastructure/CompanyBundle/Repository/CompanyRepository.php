<?php

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Company\Entity\Company;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findAll()
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Company::class);
    }

    /**
     * @param string $id
     * @return Company|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?Company
    {
        return $this->createQueryBuilder('c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $id
     * @return Company|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneActiveById(?string $id): ?Company
    {
        return $this->queryFindAllActive()
            ->andWhere('company.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder()
            ->where('company.status = :status')
            ->setParameter('status', Company::STATUS_ACTIVE);
    }

    public function findAllQueryBuilder($alias = 'company')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param string $name
     * @return Company|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadByName(string $name): ?Company
    {
        return $this->createQueryBuilder('c')
            ->where('c.name = :name AND c.status = :status')
            ->setParameter('name', $name)
            ->setParameter('status', Company::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('company')
            ->select(['company.id as value', 'company.name as label'])
            ->getQuery()
            ->getArrayResult();
    }
}
