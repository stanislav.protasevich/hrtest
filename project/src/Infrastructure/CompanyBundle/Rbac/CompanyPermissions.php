<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class CompanyPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_COMPANY_CREATE = 'companyCreate';
    public const PERMISSION_COMPANY_EDIT = 'companyEdit';
    public const PERMISSION_COMPANY_LIST = 'companyList';
    public const PERMISSION_COMPANY_LIST_PRIMARY = 'companyListPrimary';
    public const PERMISSION_COMPANY_VIEW = 'companyView';
    public const PERMISSION_COMPANY_VIEW_JOBTITLE_LIST = 'companyViewJobtitleList';
    public const PERMISSION_COMPANY_VIEW_JOBTITLE_ADD = 'companyViewJobtitleAdd';
    public const PERMISSION_COMPANY_VIEW_JOBTITLE_REMOVE = 'companyViewJobtitleRemove';
    public const PERMISSION_COMPANY_VIEW_RECRUITERS_LIST = 'companyViewRecruitersList';
    public const PERMISSION_COMPANY_VIEW_RECRUITERS_ADD = 'companyViewRecruitersAdd';
    public const PERMISSION_COMPANY_VIEW_RECRUITERS_REMOVE = 'companyViewRecruitersRemove';

    public const PERMISSION_COMPANIES = [
        self::PERMISSION_COMPANY_CREATE,
        self::PERMISSION_COMPANY_EDIT,
        self::PERMISSION_COMPANY_LIST,
        self::PERMISSION_COMPANY_VIEW,
        self::PERMISSION_COMPANY_VIEW_JOBTITLE_LIST,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_COMPANY_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_COMPANY_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'CompanyEditViewOwnRule'
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_LIST_PRIMARY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'CompanyEditViewOwnRule'
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_JOBTITLE_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_JOBTITLE_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_JOBTITLE_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_RECRUITERS_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_RECRUITERS_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_COMPANY_VIEW_RECRUITERS_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}