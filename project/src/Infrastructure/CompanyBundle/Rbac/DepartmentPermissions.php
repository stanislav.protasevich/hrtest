<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class DepartmentPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_DEPARTMENT_CREATE = 'departmentCreate';
    public const PERMISSION_DEPARTMENT_EDIT = 'departmentEdit';
    public const PERMISSION_DEPARTMENT_LIST = 'departmentList';
    public const PERMISSION_DEPARTMENT_VIEW = 'departmentView';

    public const PERMISSION_COMPANIES = [
        self::PERMISSION_DEPARTMENT_CREATE,
        self::PERMISSION_DEPARTMENT_EDIT,
        self::PERMISSION_DEPARTMENT_LIST,
        self::PERMISSION_DEPARTMENT_VIEW,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_DEPARTMENT_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_DEPARTMENT_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_DEPARTMENT_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_DEPARTMENT_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}