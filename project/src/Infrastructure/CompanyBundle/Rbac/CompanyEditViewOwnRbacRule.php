<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Rbac;


use Domain\Company\Entity\Company;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;

class CompanyEditViewOwnRbacRule implements RbacRuleInterface
{
    public function execute(User $user, $item, $params)
    {
        if (Gate::can(RbacPermissions::ROLE_ADMINISTRATOR)) {
            return true;
        }

        /* @var $company Company */
        $company = $params['model'] ?? null;

        if (!$company) {
            throw new \Exception('Company model does not exist in rule: '.self::class);
        }

        if ($user !== $company->getCreatedBy()) {
            return false;
        }
    }
}