<?php

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class DepartmentEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'departmentBeforeInsert';
    public const EVENT_AFTER_INSERT = 'departmentAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'departmentBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'departmentAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'departmentBeforeDelete';
    public const EVENT_AFTER_DELETE = 'departmentAfterDelete';

    public const EVENT_BEFORE_FIND = 'departmentBeforeFind';
    public const EVENT_AFTER_FIND = 'departmentAfterFind';

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}