<?php

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Event;

use Domain\Company\Entity\Company;
use Symfony\Component\EventDispatcher\Event;

class CompanyEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'companyBeforeInsert';
    public const EVENT_AFTER_INSERT = 'companyAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'companyBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'companyAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'companyBeforeDelete';
    public const EVENT_AFTER_DELETE = 'companyAfterDelete';

    public const EVENT_BEFORE_FIND = 'companyBeforeFind';
    public const EVENT_AFTER_FIND = 'companyAfterFind';
    /**
     * @var Company
     */
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }
}
