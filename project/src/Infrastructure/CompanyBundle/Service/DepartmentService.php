<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;


use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Domain\Company\Entity\Department;
use Domain\User\Entity\User;
use Infrastructure\CompanyBundle\Command\DepartmentCreateCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DepartmentService
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $_em;
    /**
     * @var User
     */
    private $_user;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param DepartmentCreateCommand $command
     * @return Department
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(DepartmentCreateCommand $command): Department
    {
        $department = $command->getDepartment();

        $em = $this->getEm();

        $department->setCreatedBy($this->getUser());
        $department->setUpdatedBy($this->getUser());

        $em->persist($department);
        $em->flush();

        return $department;
    }

    public function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->container->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    protected function getUser(): User
    {
        if (!$this->_user) {
            $this->_user = $this->container->get('security.token_storage')->getToken()->getUser();
        }

        return $this->_user;
    }

    /**
     * @param DepartmentCreateCommand $command
     * @return Department
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(DepartmentCreateCommand $command): Department
    {
        $department = $command->getDepartment();

        $em = $this->getEm();

        $department->setUpdatedBy($this->getUser());
        $department->setUpdatedAt(new \DateTime());

        $em->persist($department);
        $em->flush();

        return $department;
    }
}