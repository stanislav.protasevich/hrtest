<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;


use Domain\Company\Entity\CompanyAddress;
use Infrastructure\CommonBundle\Service\BaseService;

class CompanyAddressService extends BaseService
{
    public function primaryList(): array
    {
        return array_map(function (array $item) {

            $id = $item['id'];
            $company = $item['companyName'];

            unset($item['id'], $item['companyName']);

            return [
                'value' => $id,
                'label' => sprintf('%s: %s', $company, implode(', ', array_filter($item)))
            ];
        }, $this->getAllAddresses());
    }

    public function getAllAddresses(): array
    {
        return $this->getEm()->getRepository(CompanyAddress::class)->getAllAddresses();
    }
}
