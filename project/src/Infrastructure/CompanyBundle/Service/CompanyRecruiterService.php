<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Service;


use Domain\Company\Entity\Company;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class CompanyRecruiterService extends BaseService
{
    public function addRecruiter(Company $company, User $recruiter): Company
    {
        if (!$this->isExist($company, $recruiter)) {
            $em = $this->getEm();
            $company->addRecruiter($recruiter);

            $em->persist($company);
            $em->flush();
        }

        return $company;
    }

    /**
     * @param Company $company
     * @param User $recruiter
     * @return bool
     */
    protected function isExist(Company $company, User $recruiter): bool
    {
        $exist = $company->getRecruiters()->exists(function ($index, User $title) use ($recruiter) {
            return $title->getId() === $recruiter->getId();
        });

        return $exist;
    }

    public function removeRecruiter(Company $company, User $recruiter): Company
    {
        if ($this->isExist($company, $recruiter)) {
            $em = $this->getEm();
            $company->removeRecruiter($recruiter);

            $em->persist($company);
            $em->flush();
        }

        return $company;
    }
}