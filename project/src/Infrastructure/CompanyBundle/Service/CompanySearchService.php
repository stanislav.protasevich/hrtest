<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;

use Doctrine\ORM\QueryBuilder;
use Domain\Company\Entity\Company;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\CompanyBundle\Command\CompanySearchCommand;

class CompanySearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'company.id',
        'status' => 'company.status',
        'name' => 'company.name',
        'country' => 'country.name',
        'city' => 'city.name',
        'employeesCount' => 'employeesCount',
    ];

    public function search(CompanySearchCommand $command): QueryBuilder
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(Company::class)->findAllQueryBuilder();

        $qb
            ->select('company')
            ->addSelect(['address', 'country', 'city'])
            ->addSelect(['employees', 'COUNT(employees) as employeesCount'])
            ->leftJoin('company.address', 'address')
            ->leftJoin('company.employees', 'employees')
            ->leftJoin('address.country', 'country')
            ->leftJoin('address.city', 'city')
            ->addGroupBy('company');

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('company.status IN (:status)')
                ->setParameter('status', $status);
        }

        if ($command->name) {
            $qb->andWhere('company.name LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$command->name));
        }

        if ($command->address) {
            $qb->andHaving('address.street LIKE :address OR address.building LIKE :address '.
                'OR address.room LIKE :address OR address.postCode LIKE :address')
                ->setParameter('address', $this->makeLikeParam((string)$command->address));
        }

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('company.id IN (:id)')
                ->setParameter('id', $id);
        }

        if ($command->country && $country = $this->paramsFromString($command->country)) {
            $qb->andHaving('country IN (:country)')
                ->setParameter('country', $this->getModel(Country::class, $country));
        }

        if ($command->city && $city = $this->paramsFromString($command->city)) {
            $qb->andHaving('city IN (:city)')
                ->setParameter('city', $this->getModel(City::class, $city));
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}