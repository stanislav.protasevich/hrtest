<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;


use Doctrine\ORM\PersistentCollection;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\CommonBundle\Service\FileUploader;
use Infrastructure\CompanyBundle\Command\CompanyCreateCommand;
use Infrastructure\CompanyBundle\Command\CompanyEditCommand;
use Symfony\Component\HttpFoundation\File\File;

class CompanyService extends BaseService
{
    /**
     * @param CompanyCreateCommand $command
     * @return Company
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(CompanyCreateCommand $command): Company
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $company = $command->getCompany();

            $em = $this->getEm();

            $company->setCreatedBy($this->getUser());
            $company->setUpdatedBy($this->getUser());
            $company->setCreatedAt($this->now());
            $company->setUpdatedAt($this->now());

            $this->processDepartments($company, $company->getDepartments());

            $em->persist($company);
            $em->flush();

            $this->setImage($company);

            $em->persist($company->getAddress());

            $em->persist($company);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $company;
    }

    /**
     * @param Company $company
     * @param PersistentCollection $departments
     * @throws \Doctrine\ORM\ORMException
     */
    public function processDepartments(Company $company, $departments)
    {
        $em = $this->getEm();

        foreach ($departments->getValues() as $department) {
            /* @var $department Department */
            $company->addDepartment($department);

            $em->persist($department);
        }
    }

    /**
     * @param Company $company
     * @throws \Exception
     */
    public function setImage(Company $company): void
    {
        $image = $company->getImage();

        if ($image instanceof File) {
            /* @var $uploader FileUploader */
            $uploader = $this->container->get(FileUploader::class);

            $uploader->setPath($company->getRelativeLogoPath());

            $filename = $uploader->upload($image);

            $company->setImage($filename);
        }
    }

    /**
     * @param CompanyEditCommand $command
     * @return Company
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(CompanyEditCommand $command)
    {
        $em = $this->getEm();

        $company = $command->getCompany();
        $company->setUpdatedAt($this->now());
        $company->setUpdatedBy($this->getUser());

        $this->setImage($company);

//        $this->processDepartments($company, $company->getDepartments());

        $em->persist($company);
        $em->flush();

        return $company;
    }

    public function getAllCompanies(): array
    {
        return $this->getEm()->getRepository(Company::class)->findPrimaryAll();
    }
}
