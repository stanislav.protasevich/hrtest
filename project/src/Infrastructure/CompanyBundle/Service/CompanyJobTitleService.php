<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;


use Domain\Company\Entity\Company;
use Domain\Job\Entity\JobTitle;
use Infrastructure\CommonBundle\Service\BaseService;

class CompanyJobTitleService extends BaseService
{
    public function addJobTitle(Company $company, JobTitle $jobTitle): Company
    {
        if (!$this->isExist($company, $jobTitle)) {
            $em = $this->getEm();
            $company->addJobTitle($jobTitle);

            $em->persist($company);
            $em->flush();
        }

        return $company;
    }

    /**
     * @param Company $company
     * @param JobTitle $jobTitle
     * @return bool
     */
    protected function isExist(Company $company, JobTitle $jobTitle): bool
    {
        $exist = $company->getJobTitles()->exists(function ($index, JobTitle $title) use ($jobTitle) {
            return $title->getId() === $jobTitle->getId();
        });
        return $exist;
    }

    public function removeJobTitle(Company $company, JobTitle $jobTitle): Company
    {
        if ($this->isExist($company, $jobTitle)) {
            $em = $this->getEm();
            $company->removeJobTitle($jobTitle);

            $em->persist($company);
            $em->flush();
        }

        return $company;
    }
}