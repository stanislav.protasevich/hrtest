<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Domain\Company\Entity\Department;
use Infrastructure\CompanyBundle\Command\DepartmentSearchCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DepartmentSearchService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function search(DepartmentSearchCommand $command)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(Department::class)->findAllQueryBuilder();

        if ($command->name) {
            $qb->andWhere('department.name LIKE :name')->setParameter('name', $command->name);
        }

        if ($command->status) {
            $qb->andWhere('department.status = :status')->setParameter('status', $command->status);
        }

        if ($command->id && !empty($command->id)) {
            $qb->andWhere('department.id IN (:id)')->setParameter('id', (array)$command->id);
        }

        return $qb;
    }
}