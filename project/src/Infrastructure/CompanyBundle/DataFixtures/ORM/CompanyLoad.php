<?php

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\DataFixtures\ORM;

use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Company\Entity\Company;
use Domain\User\Entity\User;
use Faker\Factory;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\UserBundle\DataFixtures\ORM\UserLoad;

class CompanyLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(User::class);
        $companyRepo = $manager->getRepository(Company::class);

        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new \Application\Vendor\Faker\Provider\Image($faker));

        for ($i = 0; $i < 10; $i++) {

            $name = $faker->company;

            if ($companyRepo->findBy(['name' => $name])) {
                continue;
            }

            $user = $userRepo->findAll();

            $company = new Company();
            $company->setName($name);
            $company->setCreatedAt(new \DateTime());
            $company->setUpdatedAt(new \DateTime());
            $company->setCreatedBy($user[0]);
            $company->setUpdatedBy($user[0]);
            $company->setStatus(random_int(0, 1) ? Company::STATUS_ACTIVE : Company::STATUS_DISABLED);

            $manager->persist($company);
            $manager->flush();

            $path = "./public/storage/company/{$company->getId()}/logos";
            FileHelper::createDirectory($path);

            $file = $faker->image($path, 640, 480, null, false);
            $company->setImage($file);

            $manager->persist($company);
            $manager->flush();
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserLoad::class,
        );
    }
}