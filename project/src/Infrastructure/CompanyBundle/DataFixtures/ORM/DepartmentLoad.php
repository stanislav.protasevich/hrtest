<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\DataFixtures\ORM;

use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Company\Entity\Department;
use Domain\User\Entity\User;
use Faker\Factory;
use Faker\Provider\Address;

class DepartmentLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $faker->addProvider(new Address($faker));

        $user = $manager->getRepository(User::class)->findOneBy([]);

        for ($i = 0; $i <= 15; $i++) {
            $model = new Department();
            $model->setName($faker->name);
            $model->setStatus(Department::STATUS_ACTIVE);

            $model->setSortOrder($i);

            $model->setCreatedAt(new \DateTime());
            $model->setUpdatedAt(new \DateTime());
            $model->setCreatedBy($user);
            $model->setUpdatedBy($user);

            $manager->persist($model);
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            CompanyLoad::class
        ];
    }
}