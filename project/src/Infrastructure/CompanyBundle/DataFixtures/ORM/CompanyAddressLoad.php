<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\CompanyAddress;
use Domain\User\Entity\User;
use Faker\Factory;
use Infrastructure\LocationBundle\DataFixtures\ORM\LocationLoad;
use Infrastructure\UserBundle\DataFixtures\ORM\UserLoad;

class CompanyAddressLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(User::class);
        $companyRepo = $manager->getRepository(Company::class);

        $faker = Factory::create();
        $faker->addProvider(new \Faker\Provider\en_US\Company($faker));

        foreach ($companyRepo->findAll() as $company) {

        }


        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserLoad::class,
            CompanyLoad::class,
            LocationLoad::class,
        );
    }
}