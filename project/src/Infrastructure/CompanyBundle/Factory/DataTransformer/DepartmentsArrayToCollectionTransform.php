<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Factory\DataTransformer;


use Doctrine\ORM\PersistentCollection;
use Domain\Company\Entity\Department;
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DepartmentsArrayToCollectionTransform implements DataTransformerInterface
{
//    /**
//     * @var DepartmentRepository
//     */
//    private $repository;
//
//    public function __construct(DepartmentRepository $repository)
//    {
//        $this->repository = $repository;
//    }

    /**
     * @param mixed $value The value in the original representation
     * @return mixed The value in the transformed representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function transform($value)
    {
        return $value;
    }

    /**
     * @param mixed $value The value in the transformed representation
     * @return mixed The value in the original representation
     * @throws TransformationFailedException when the transformation fails
     */
    public function reverseTransform($value)
    {
        if ('' === $value || null === $value) {
            return [];
        }
        prnx($value);
        if ($value instanceof PersistentCollection) {
            $value = $value->getValues();
//
//            $ids = array_map(function (Department $department) {
//                return $department->getId();
//            }, $value->getValues());
//
//            prnx($ids);
        }

        $ids = array_filter(array_unique($value), function ($id) {
            return !is_null($id);
        });

        return $this->repository->findAllActiveById($ids);
    }
}