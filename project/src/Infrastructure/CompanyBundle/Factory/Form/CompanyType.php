<?php

declare(strict_types=1);

namespace Infrastructure\CompanyBundle\Factory\Form;


use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Validator\Constraints\AuthItemAssignedRoleConstraint;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => Company::STATUSES,
                'empty_data' => (string)Company::STATUS_ACTIVE
            ])
            ->add('address', CompanyAddressType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('hr', EntityType::class, [
                'class' => User::class,
                'constraints' => [
                    new NotBlank(),
                    new AuthItemAssignedRoleConstraint([
                        'role' => RbacPermissions::ROLE_HR,
                    ]),
                ],
                'query_builder' => function (UserRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('departments', CollectionType::class, [
//                'data_class' => CompanyDepartment::class,
//                'prototype_data' => CompanyDepartment::class,
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
////                'delete_empty' => true,
                'entry_options' => [
                    'class' => Department::class,
                    'query_builder' => function (DepartmentRepository $r) {
                        return $r->createQueryBuilder('d')
                            ->where('d.status = :status')
                            ->setParameter('status', Department::STATUS_ACTIVE);
                    },
                ],
//                'constraints' => [
//                    new UniqueCollections([
//                        'attributes' => ['id'],
//                    ]),
//                ]
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $image = $data['image'] ?? null;

                $form->add('image', Base64EncodedFileType::class, [
                    'disabled' => StringHelper::isBase64File($image) === false
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
            'csrf_protection' => false,
        ]);
    }
}
