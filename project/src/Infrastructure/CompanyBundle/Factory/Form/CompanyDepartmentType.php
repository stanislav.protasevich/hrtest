<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Factory\Form;

use Infrastructure\CompanyBundle\Factory\DataTransformer\DepartmentsArrayToCollectionTransform;
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyDepartmentType extends AbstractType
{
//    /**
//     * @var DepartmentRepository
//     */
//    private $repository;
//
//    public function __construct(DepartmentRepository $repository)
//    {
//        $this->repository = $repository;
//    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(
            new DepartmentsArrayToCollectionTransform(),
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        prnx($this);
        $resolver
            ->setDefaults([
                'compound' => true,
                'data_class' => null,
                'empty_data' => null,
            ]);
    }
}