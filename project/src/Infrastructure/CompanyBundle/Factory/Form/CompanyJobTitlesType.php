<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Factory\Form;


use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Repository\JobTitleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class CompanyJobTitlesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobTitle', EntityType::class, [
                'class' => JobTitle::class,
                'query_builder' => function (JobTitleRepository $r) {
                    return $r->queryFindAllActive();
                },
                'constraints' => [
                    new NotBlank(),
                ]
            ]);
    }
}