<?php

namespace Infrastructure\CompanyBundle\Consumer;


use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class MailSenderConsumer implements ConsumerInterface
{

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        dump($this);
        echo 'Ну тут типа сообщение пытаюсь отправить: ' . $msg->getBody() . PHP_EOL;
        echo 'Отправлено успешно!...';
    }
}