<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Helpers;


use Domain\Company\Entity\Company;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @property AbstractBusController $this
 */
trait CompanyHelpersTrait
{
    public function findOrFail(string $id): Company
    {
        $company = $this->getEm()->getRepository(Company::class)->findOneById($id);

        if (is_null($company)) {
            throw new ModelNotFoundException(Company::class, $id);
        }

        return $company;
    }
}
