<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Helpers;


use Domain\Company\Entity\CompanyAddress;

class CompanyAddressHelper
{
    public static function address()
    {

    }

    public static function processFields(CompanyAddress $address)
    {
        return [
            'postCode' => $address->getPostCode(),
            'countryName' => $address->getCountry()->getName(),
            'regionName' => $address->getRegion()->getName(),
            'cityName' => $address->getCity()->getName(),
            'street' => $address->getStreet(),
            'building' => $address->getBuilding(),
            'room' => $address->getRoom()
        ];
    }

    public static function addressFromArray(array $item): string
    {
        return implode(', ', array_filter($item));
    }
}