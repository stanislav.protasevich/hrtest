<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Command;


use Doctrine\ORM\EntityManager;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Infrastructure\CommonBundle\Factory\Objecteble;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\HttpFoundation\File\UploadedBase64EncodedFile;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class CompanyCommand extends Objecteble implements ContainerAwareInterface
{
    /**
     * @NotBlank()
     */
    protected $name;

    protected $status;

    protected $image;

    protected $departments;
    /**
     * @var ContainerInterface
     */
    protected $_container;
    /**
     * @var EntityManager
     */
    protected $_em;

    public function __construct(array $values = [])
    {
        $this->status = Company::STATUS_ACTIVE;

        parent::__construct($values);
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('status', new Choice([
            'choices' => Company::STATUSES,
        ]));
    }

    /**
     * @param mixed $departments
     */
    public function setDepartments($departments): void
    {
        if (is_array($departments)) {
            $departments = $this->getEm()->getRepository(Department::class)->findAllActiveById($departments);
        }

        $this->departments = $departments;
    }

    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->_container->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status = null): void
    {
        $this->status = $status;
    }

    public function setImage($image)
    {
        $this->image = new UploadedBase64EncodedFile(
            new Base64EncodedFile($image, true)
        );
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->_container = $container;
    }
}