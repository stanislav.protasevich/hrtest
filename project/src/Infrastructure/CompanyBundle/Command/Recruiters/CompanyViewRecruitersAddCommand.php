<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Command\Recruiters;


use Domain\Company\Entity\Company;
use Domain\User\Entity\User;

class CompanyViewRecruitersAddCommand
{
    /**
     * @var Company
     */
    private $company;
    /**
     * @var User
     */
    private $recruiter;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @return User
     */
    public function getRecruiter(): ?User
    {
        return $this->recruiter;
    }

    /**
     * @param User $recruiter
     */
    public function setRecruiter(User $recruiter): void
    {
        $this->recruiter = $recruiter;
    }
}