<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CompanyBundle\Command\Recruiters;


class CompanyViewRecruitersRemoveCommand extends CompanyViewRecruitersAddCommand
{

}