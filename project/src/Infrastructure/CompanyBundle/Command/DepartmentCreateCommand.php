<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Command;


use Domain\Company\Entity\Department;

class DepartmentCreateCommand
{
    /**
     * @var Department
     */
    private $department;

    /**
     * @return Department
     */
    public function getDepartment(): Department
    {
        return $this->department;
    }

    public function __construct(Department $department)
    {
        $this->department = $department;
    }
}