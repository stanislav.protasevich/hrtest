<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Command\JobTitle;


class CompanyViewJobTitleRemoveCommand extends CompanyViewJobTitleAddCommand
{

}