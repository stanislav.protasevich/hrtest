<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CompanyBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;


class CompanySearchCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $status;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $country;
    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $city;
    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $address;
}