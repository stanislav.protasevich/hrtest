<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Domain\User\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class LeaveSubstituteUserValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param User $value
     * @param Constraint $constraint
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveSubstituteUser) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\LeaveSubstituteUser');
        }

        /* @var $user User */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        if ($value && $user->getId() === $value->getId()) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');
            $this->context
                ->buildViolation($translator->trans($constraint->messageSelf))
                ->addViolation();
        }
    }
}