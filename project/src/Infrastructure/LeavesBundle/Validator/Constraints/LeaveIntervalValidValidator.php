<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;


use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LeaveIntervalValidValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveIntervalValid) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\LeaveIntervalValid');
        }

        /* @var $command LeavesRequestCommand */
        $command = $this->context->getRoot();

        $start = $command->getStartDate();
        $end = $command->getEndDate();

        if ($start > $end) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
//                ->setParameter('{{ start }}', $start->format(\DateTime::ATOM))
//                ->setParameter('{{ end }}', $end->format(\DateTime::ATOM))
                ->addViolation();
        }
    }
}
