<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Domain\Leaves\Entity\LeaveRequests;
use Infrastructure\LeavesBundle\{Command\LeavesRequestCommand, Helpers\DateHelper, Service\LeaveStatsService};
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class LeaveIsInRowConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var LeaveStatsService
     */
    private $statsService;

    /**
     * LeaveIsInRowConstraintValidator constructor.
     * @param ContainerInterface $container
     * @param LeaveStatsService $statsService
     */
    public function __construct(ContainerInterface $container, LeaveStatsService $statsService)
    {
        $this->container = $container;
        $this->statsService = $statsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveIsInRowConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\LeaveIsInRowConstraint');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }

        /* @var $command LeavesRequestCommand */
        $command = $this->context->getRoot();

        if ($command->isExist() && $command->inPeriodIsValid) { // только если уже существует модель (редактирование), проверяем на то валидный ли период
            return;
        }

        $user = $command->getUser();
        $timeOff = $command->getTimeOffType();

        $stat = $this->statsService->userStat($user, $timeOff);

        $policy = $stat->policy;

        if ($policy->getInRow() === true) { // проверка на допуск брать подряд отгул
            return;
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $last = $em->getRepository(LeaveRequests::class)->findLastByUser($user, $timeOff);

        if (!$last) {
            return;
        }

        $lastDate = clone $last->getEndDate();

        $spec = DateHelper::periodDesignator($policy->getInRowType(), $policy->getInRowInterval());
        $endDate = Carbon::instance($command->getEndDate());

        $endDateWithOffset = Carbon::instance(
            $lastDate->add(new \DateInterval($spec))
        );

        if ($endDate < $endDateWithOffset) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation(
                    $translator->trans($constraint->message)
                )
                ->setParameter('{{ interval }}', $endDateWithOffset->diffInDays($endDate))
                ->setParameter('{{ type }}', 'days')
                ->addViolation();
        }
    }
}
