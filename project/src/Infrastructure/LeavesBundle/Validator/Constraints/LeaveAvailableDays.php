<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 */
class LeaveAvailableDays extends Constraint
{
    public $message = 'You used {{ used }} days off. You have another {{ available }} days available.';

    /**
     * Offset in days from today
     *
     * @var integer
     */
    public $offset;

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT, self::PROPERTY_CONSTRAINT
        ];
    }
}