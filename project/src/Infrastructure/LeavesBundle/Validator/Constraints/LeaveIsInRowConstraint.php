<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Проверяет, по политике подряд запросы отсутствия
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 */
class LeaveIsInRowConstraint extends Constraint
{
    public $message = 'You can take next leave after {{ interval }} {{ type }}.';

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
            self::PROPERTY_CONSTRAINT
        ];
    }
}