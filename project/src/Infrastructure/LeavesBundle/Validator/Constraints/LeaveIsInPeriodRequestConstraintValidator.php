<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Validator\Constraints;


use Infrastructure\CommonBundle\Helpers\DateHelper;
use Infrastructure\LeavesBundle\{Command\LeavesRequestCommand, Service\LeaveStatsService};
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class LeaveIsInPeriodRequestConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var LeaveStatsService
     */
    private $statsService;

    /**
     * LeaveIsInPeriodRequestConstraintValidator constructor.
     * @param ContainerInterface $container
     * @param LeaveStatsService $statsService
     */
    public function __construct(ContainerInterface $container, LeaveStatsService $statsService)
    {
        $this->container = $container;
        $this->statsService = $statsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveIsInPeriodRequestConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\LeaveIsInPeriodRequestConstraint');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }

        /* @var $command LeavesRequestCommand */
        $command = $this->context->getRoot();
        $command->inPeriodIsValid = true;

        $getter = 'get' . ucfirst($constraint->field);

        if (is_null($constraint->field) || !method_exists($command, $getter)) {
            throw new \LogicException('Property field must be set.');
        }

        if ($command->isExist()) {
            $oldLeave = $command->getOldLeaveRequest();

            if (
                DateHelper::isEqual($oldLeave->getStartDate(), $command->getStartDate()) &&
                DateHelper::isEqual($oldLeave->getEndDate(), $command->getEndDate())
            ) {
                return;
            }
        }

        $user = $command->getUser();
        $timeOff = $command->getTimeOffType();
        $date = $command->$getter();
        $stat = $this->statsService->userStat($user, $timeOff);

        switch ($constraint->field) {
            case 'startDate':
                $periodStart = new \DateTime($stat->period->start);
                $result = $date < $periodStart;

                if ($result) {
                    /* @var $translator Translator */
                    $translator = $this->container->get('translator');
                    $command->inPeriodIsValid = false;
                    $this->context
                        ->buildViolation($translator->trans($constraint->messageStart))
                        ->setParameter('{{ start }}', $periodStart->format('d.m.Y'))
                        ->addViolation();
                }

                break;

            case 'endDate':
                $periodEnd = new \DateTime($stat->period->end);

                $result = $date > $periodEnd;

                if ($result) {
                    /* @var $translator Translator */
                    $translator = $this->container->get('translator');
                    $command->inPeriodIsValid = false;
                    
                    $this->context
                        ->buildViolation($translator->trans($constraint->messageEnd))
                        ->setParameter('{{ end }}', $periodEnd->format('d.m.Y'))
                        ->addViolation();
                }
                break;
        }
    }
}
