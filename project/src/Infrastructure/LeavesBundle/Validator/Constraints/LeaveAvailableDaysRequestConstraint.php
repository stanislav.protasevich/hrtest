<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Проверяет, доступное количество дней
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 */
class LeaveAvailableDaysRequestConstraint extends Constraint
{
    public $message = 'You can request {{ number }} days.';

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
            self::PROPERTY_CONSTRAINT
        ];
    }
}