<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Infrastructure\LeavesBundle\Service\LeaveStatsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LeaveAvailableDaysRequestConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var LeaveStatsService
     */
    private $statsService;

    public function __construct(ContainerInterface $container, LeaveStatsService $statsService)
    {
        $this->container = $container;
        $this->statsService = $statsService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveAvailableDaysRequestConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\LeaveAvailableDaysRequestConstraint');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }

        /* @var $command LeavesRequestCommand */
        $command = $this->context->getRoot();

        $stat = $this->statsService->userStat(
            $command->getUser(),
            $command->getTimeOffType()
        );

        if ($stat->policy->getMaxCountAtTime() && $command->getDays()->count() > $stat->days) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{{ number }}', $stat->days)
                ->addViolation();
        }
    }
}