<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * Проверяет, входят выбранные даты в активный период начисления отпуска
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 */
class LeaveIsInPeriodRequestConstraint extends Constraint
{
    public $messageStart = 'You can take a vacation from {{ start }}.';
    public $messageEnd = 'You can take a vacation to {{ end }}.';

    public $field;

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
            self::PROPERTY_CONSTRAINT
        ];
    }
}
