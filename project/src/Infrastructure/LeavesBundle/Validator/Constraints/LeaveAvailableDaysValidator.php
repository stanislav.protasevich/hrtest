<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Validator\Constraints;


use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Infrastructure\LeavesBundle\Service\IntervalService\IntervalServiceInterface;
use Infrastructure\LeavesBundle\Service\LeaveRequestsService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LeaveAvailableDaysValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LeaveRequestsService
     */
    private $leaveRequestsService;

    public function __construct(ContainerInterface $container, LeaveRequestsService $leaveRequestsService)
    {
        $this->container = $container;
        $this->leaveRequestsService = $leaveRequestsService;
    }

    /**
     * @param \DateTime $value
     * @param Constraint $constraint
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof LeaveAvailableDays) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\LeaveAvailableDays');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }

        /* @var $command LeavesRequestCommand */
        $command = $this->context->getRoot();

        $interval = $command->getPolicy()->getAccrualSchedule()->getInterval();

        /* @var $service IntervalServiceInterface */
        $service = $this->container->get('leave.interval.'.$interval);
        $service->setAccrualSchedule($command->getPolicy()->getAccrualSchedule());

        $availableHours = $service->getAvailableHours();// доступно часов отпуска

        $period = $service->getActivePeriodInterval();

        // использовано в днях
        $usedDays = $this->leaveRequestsService->getUsedDaysCountByPeriod(
            $service->getUser(),
            $command->getTimeOffType(),
            $period->first(),
            $period->last()
        );

        $requestedDays = $command->getDays()->count();

        $availableCountDays = ($availableHours / 8) - $usedDays;

        if ($requestedDays > $availableCountDays) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{{ used }}', $usedDays)
                ->setParameter('{{ available }}', $availableCountDays < 0 ? 0 : $availableCountDays)
                ->addViolation();
        }
    }
}