<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\EventListener;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\UserAlert;
use Infrastructure\LeavesBundle\Event\LeaveSupervisorVoteEvent;
use Infrastructure\UserBundle\Factory\UserAlertFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LeaveSupervisorVoteEventHandler implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
//            LeaveSupervisorVoteEvent::EVENT_BEFORE_UPDATE => 'onBeforeUpdate',
//            LeaveSupervisorVoteEvent::EVENT_AFTER_UPDATE => 'onAfterUpdate',
        ];
    }

    public function onAfterUpdate(LeaveSupervisorVoteEvent $event)
    {
        $vote = $event->getLeaveRequestSupervisor();

        /** @var LeaveRequests $leave */
        $leave = $vote->getLeaveRequests();

        UserAlertFactory::create(
            $vote->getUser(),
            UserAlert::ALERT_LEAVE_REQUEST,
            $leave->getId(),
            UserAlert::ALERT_ACTION_UPDATE,
            $leave->getUser(),
            sprintf('%s %s your leave request', $vote->getUser()->getProfile()->getFullName(), $vote->getStatus()),
            $leave
        );
    }

    public function onBeforeUpdate()
    {
    }
}
