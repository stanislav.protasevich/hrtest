<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\EventListener;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\User;
use Domain\User\Entity\UserAlert;
use Infrastructure\LeavesBundle\Event\LeaveRequestEvent;
use Infrastructure\UserBundle\Factory\UserAlertFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LeaveRequestUserAlertEventHandler implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            LeaveRequestEvent::EVENT_AFTER_INSERT => 'onLeaveRequest',
            LeaveRequestEvent::EVENT_AFTER_UPDATE => 'onLeaveRequestUpdate',
        ];
    }

    public function onLeaveRequest(LeaveRequestEvent $event)
    {
        $data = function () use ($event) {
            $leave = $event->getLeaveRequests();
            $user = $leave->getUser();

            return sprintf(
                'request leave "%s", %s %s',
                $leave->getTimeOffType()->getName(),
                $leave->getDays(),
                $leave->getTimeOffType()->getTrackTime()
            );
        };

        $this->processAlert($event->getLeaveRequests(), $data);
    }

    /**
     * @param LeaveRequests $leave
     * @param callable $message
     */
    protected function processAlert(LeaveRequests $leave, callable $message): void
    {
        $already = [];

        /** @var User[] $followers */
        $followers = array_merge(
            $leave->getUser()->getSupervisors()->getValues(),
            $leave->getUser()->getFollowers()->getValues()
        );

        foreach ($followers as $follower) {

            if (in_array($follower->getId(), $already)) {
                continue;
            }

            $already[] = $follower->getId();

            UserAlertFactory::create(
                $leave->getUser(),
                UserAlert::ALERT_LEAVE_REQUEST,
                $leave->getId(),
                UserAlert::ALERT_ACTION_CREATE,
                $follower,
                $message(),
                $leave
            );
        }
    }

    public function onLeaveRequestUpdate(LeaveRequestEvent $event)
    {
        $data = function () use ($event) {
            $leave = $event->getLeaveRequests();
            $user = $leave->getUpdatedBy();

            return sprintf(
                'change leave request "%s", %s %s',
                $leave->getTimeOffType()->getName(),
                $leave->getDays(),
                $leave->getTimeOffType()->getTrackTime()
            );
        };

        $this->processAlert($event->getLeaveRequests(), $data);
    }
}
