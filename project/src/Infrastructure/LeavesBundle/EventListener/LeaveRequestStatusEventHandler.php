<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\EventListener;

use Doctrine\Common\Collections\Collection;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Domain\User\Entity\UserAlert;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Infrastructure\LeavesBundle\Event\LeaveSupervisorVoteEvent;
use Infrastructure\LeavesBundle\Service\LeaveRequestsService;
use Infrastructure\UserBundle\Factory\UserAlertFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LeaveRequestStatusEventHandler implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var LeaveRequestsService
     */
    private $service;

    public function __construct(ContainerInterface $container, LeaveRequestsService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            LeaveSupervisorVoteEvent::EVENT_AFTER_UPDATE => 'onAfterUpdate',
        ];
    }

    /**
     * @param LeaveSupervisorVoteEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onAfterUpdate(LeaveSupervisorVoteEvent $event)
    {
        $vote = $event->getLeaveRequestSupervisor();

        $leaveRequest = $vote->getLeaveRequests();

        /* @var $votes Collection|LeaveRequestSupervisor[] */
        $votes = $leaveRequest->getRequestSupervisor();

        $statuses = ArrayHelper::getColumn($votes, 'status');

        $needleCount = $votes->count();
        $countedStatusesArray = array_count_values($statuses);

        $countedApprovedStatus = $countedStatusesArray[LeaveRequestSupervisor::STATUS_APPROVED] ?? 0;
        $countedDeniedStatus = $countedStatusesArray[LeaveRequestSupervisor::STATUS_DENIED] ?? 0;

        if ($countedApprovedStatus === $needleCount) {

            $this->service->approveRequest($leaveRequest);
            $this->alertUser($vote, $leaveRequest, UserAlert::ALERT_ACTION_APPROVE);
        } elseif ($countedDeniedStatus === $needleCount) {

            $this->service->deniedRequest($leaveRequest);
            $this->alertUser($vote, $leaveRequest, UserAlert::ALERT_ACTION_DENIED);
        }
    }

    /**
     * @param LeaveRequestSupervisor $vote
     * @param LeaveRequests $leaveRequest
     * @param string $status
     */
    protected function alertUser(LeaveRequestSupervisor $vote, LeaveRequests $leaveRequest, string $status): void
    {
        $msg = sprintf('Leave request change status to %s', $leaveRequest->getStatus());

        UserAlertFactory::create($vote->getUser(), UserAlert::ALERT_LEAVE_REQUEST, $leaveRequest->getId(), $status, $leaveRequest->getUser(), $msg, $leaveRequest);
    }
}
