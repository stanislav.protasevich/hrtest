<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\EventListener;


use Domain\Leaves\Entity\LeaveRequests;
use Infrastructure\LeavesBundle\Event\LeaveRequestEvent;
use Infrastructure\LeavesBundle\Mail\SupervisorUsersEmail;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LeaveRequestInsertEventHandler implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            LeaveRequestEvent::EVENT_BEFORE_INSERT => 'onBeforeInsert',
            LeaveRequestEvent::EVENT_AFTER_INSERT => 'onAfterInsert',
        ];
    }

    public function onAfterInsert(LeaveRequestEvent $event)
    {
        $leaveRequest = $event->getLeaveRequests();

//        $this->container->get('old_sound_rabbit_mq.leave_request_producer')->publish($leaveRequest->getId());

        $this->notification($leaveRequest);
    }

    protected function notification(LeaveRequests $leaveRequest)
    {
        $user = $leaveRequest->getUser();

        /* @var $mailer SupervisorUsersEmail */
        $mailer = $this->container->get(SupervisorUsersEmail::class);
        $mailer->sendMail($leaveRequest);
    }

    public function onBeforeInsert()
    {
    }
}