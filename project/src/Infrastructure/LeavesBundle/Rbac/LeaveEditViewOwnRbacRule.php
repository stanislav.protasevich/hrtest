<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Rbac;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LeaveEditViewOwnRbacRule implements RbacRuleInterface
{

    public function execute(User $user, $item, $params)
    {
        if (
            Gate::can(RbacPermissions::ROLE_ADMINISTRATOR) ||
            Gate::can(LeavesPermissions::PERMISSION_LEAVES_GRANT_ALL)
        ) {
            return true;
        }

        /* @var $model LeaveRequests */
        $model = $params['model'];

        if ($model->getStatus() === LeaveRequests::STATUS_APPROVED) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'You can`t edit approved request.');
        }

        if ($user->getId() === $model->getUser()->getId()) {
            return true;
        }

        return false;
    }
}
