<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class LeavesPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_LEAVES_CREATE = 'leavesCreate';
    public const PERMISSION_LEAVES_CREATE_APPOINTED = 'leavesCreateAppointed';
    public const PERMISSION_LEAVES_EDIT = 'leavesEdit';
    public const PERMISSION_LEAVES_VIEW = 'leavesView';
    public const PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE = 'leavesRequestSupervisorVote';
    public const PERMISSION_LEAVES_LIST = 'leavesList';
    public const PERMISSION_LEAVES_LIST_ALL = 'leavesListAll';
    public const PERMISSION_LEAVES_GRANT_ALL = 'leavesGrantAll';

    public const PERMISSION_LEAVES = [
        self::PERMISSION_LEAVES_CREATE,
        self::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE,
        self::PERMISSION_LEAVES_LIST,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_LEAVES_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'LeaveEditViewOwnRbacRule'
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_REQUEST_SUPERVISOR_VOTE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_PROJECT_HEAD,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_CREATE_APPOINTED,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_GRANT_ALL,
                AuthItem::TYPE_PERMISSION,
                null
            ),
            new RbacItemCommand(
                self::PERMISSION_LEAVES_LIST_ALL,
                AuthItem::TYPE_PERMISSION,
                null
            ),
        ];
    }
}