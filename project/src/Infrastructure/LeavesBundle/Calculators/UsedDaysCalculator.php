<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Calculators;


use Doctrine\ORM\EntityManager;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;

class UsedDaysCalculator implements LeaveCalculatorInterface
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var TimeOffType
     */
    private $timeOffType;
    /**
     * @var \DateTime
     */
    private $start;
    /**
     * @var \DateTime
     */
    private $end;

    public function __construct(User $user, TimeOffType $timeOffType, \DateTime $start, \DateTime $end)
    {
        $this->user = $user;
        $this->timeOffType = $timeOffType;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function calculate(): int
    {
        $usedDays = $this->getUserDays($this->user, $this->timeOffType, $this->start, $this->end);

        return $usedDays ? (int)$usedDays : 0;
    }

    public function getUserDays(User $user, TimeOffType $timeOffType, \DateTime $start, \DateTime $end): int
    {
        /* @var $em EntityManager */
        $em = app('doctrine.orm.default_entity_manager');

        return $em->getRepository(LeaveRequests::class)
            ->findUsedDaysCountByPeriod($user, $timeOffType, $start, $end);

    }
}
