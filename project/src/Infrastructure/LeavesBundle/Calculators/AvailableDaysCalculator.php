<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Calculators;

use Carbon\CarbonPeriod;

/**
 * Подсчитывает доступное количество дней в текущем периоде
 */
class AvailableDaysCalculator implements LeaveCalculatorInterface
{
    /**
     * @var int
     */
    private $count;
    /**
     * @var CarbonPeriod
     */
    private $period;

    public function __construct(CarbonPeriod $period, int $count = 0)
    {
        $this->count = $count;
        $this->period = $period;
    }

    public function calculate(): int
    {
        $number = 0;

        if ($this->period->count()) {
            $number = $this->count;
        }

        return $number ?: $this->count;
    }
}