<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureLeavesBundle extends Bundle
{

}