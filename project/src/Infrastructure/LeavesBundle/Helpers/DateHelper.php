<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Helpers;


use Carbon\CarbonPeriod;
use Domain\TimeOff\Entity\Policy;

class DateHelper
{
    /**
     * Высчитывает начальный день отсчета со сдвигом
     *
     * @param string $specs
     * @param \DateTime $start
     * @return \DateTime
     * @throws \Exception
     */
    public static function startDateOffset(string $specs, \DateTime $start): \DateTime
    {
        $date = clone $start;

        return $date->add(new \DateInterval($specs));
    }

    /**
     * ISO 8601 интервал на основании типа
     *
     * @param string $type
     * @param int $number
     * @return string
     */
    public static function periodDesignator(string $type, int $number = 1): string
    {
        $intervalSpec = null;
        switch ($type) {
            case Policy::FREQUENCY_TYPE_YEARS:
                $intervalSpec = $number . 'Y';
                break;

            case Policy::FREQUENCY_TYPE_MONTHS:
                $intervalSpec = $number . 'M';
                break;

            case Policy::FREQUENCY_TYPE_WEEKS:
                $intervalSpec = $number . 'W';
                break;

            case Policy::FREQUENCY_TYPE_DAYS:
                $intervalSpec = $number . 'D';
                break;

            case Policy::FREQUENCY_TYPE_QUARTERLY:
                $intervalSpec = $number * 3 . 'M';
        }

        if (!$intervalSpec) {
            throw new \RuntimeException('Period designator invalid:' . $type);
        }

        return 'P' . $intervalSpec;
    }


    /**
     * Период с начальной и конечной даты
     *
     * @param string $spec
     * @param \DateTime $startOriginal
     * @param \DateTime $endOriginal
     * @return CarbonPeriod
     * @throws \Exception
     */
    public static function startEndDatePeriod(string $spec, \DateTime $startOriginal, \DateTime $endOriginal): CarbonPeriod
    {
        $start = clone $startOriginal;
        $end = clone $endOriginal;

        $endDate = static::correctEndDate($start, $end, $spec);
        $endDateClone = clone $endDate;
        $startDate = $endDateClone->sub(new \DateInterval($spec));

        return CarbonPeriod::create($startDate < $start ? $start : $startDate, $spec, $endDate);
    }

    /**
     * Корректирует дату окончания период, чтоб период был полным
     *
     * @param \DateTime $start
     * @param \DateTime $end
     * @param string $spec
     * @return \DateTime
     * @throws \Exception
     */
    public static function correctEndDate(\DateTime $start, \DateTime $end, string $spec): \DateTime
    {
        $date = clone $start;

        while (true) {
            $date->add(new \DateInterval($spec));

            if ($date >= $end) {
                break;
            }
        }

        return $date;
    }

    /**
     * Начальная дата в текущем периоде от крайней даты на основании интервала
     * @param string $spec
     * @param \DateTime $from
     * @return \DateTime
     * @throws \Exception
     */
    public static function currentPeriodStartDate(string $spec, \DateTime $from): \DateTime
    {
        $date = clone $from;
        return $date->sub(new \DateInterval($spec));
    }

    /**
     * Крайняя дата в текущем периоде от начальной даты на основании интервала
     * @param string $spec
     * @param \DateTime $from
     * @param \DateTime $now
     * @return \DateTime
     * @throws \Exception
     */
    public static function currentPeriodEndDate(string $spec, \DateTime $from, \DateTime $now = null): \DateTime
    {
        $date = clone $from;
        $now = $now ?: new \DateTime();

        while ($date < $now) {
            $date->add(new \DateInterval($spec));
        }

        return $date;
    }

    /**
     * Предыдущий период от даты на основании типа интервала
     *
     * @param string $spec
     * @param \DateTime $start
     * @return \DateTime
     * @throws \Exception
     */
    public static function prevDate(string $spec, \DateTime $start): \DateTime
    {
        $date = clone $start;

        return $date->sub(
            new \DateInterval($spec)
        );
    }
}
