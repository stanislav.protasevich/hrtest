<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Helpers;


use Domain\Leaves\Entity\LeaveRequests;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait LeavesHelpersTrait
{
    public function findOrNotFoundUser(string $id): LeaveRequests
    {
        $leaveRequest = $this->getEm()->getRepository(LeaveRequests::class)->find($id);

        if (is_null($leaveRequest)) {
            throw new NotFoundHttpException($this->translate('Leave request not found.'));
        }

        return $leaveRequest;
    }
}