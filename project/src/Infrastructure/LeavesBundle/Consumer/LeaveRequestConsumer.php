<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Consumer;


use Infrastructure\LeavesBundle\Service\LeaveRequestNotifyService;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class LeaveRequestConsumer implements ConsumerInterface
{
    /**
     * @var LeaveRequestNotifyService
     */
    private $service;

    public function __construct(LeaveRequestNotifyService $service)
    {
        $this->service = $service;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->service->notify($msg->getBody());
    }
}