<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service;


use Domain\Employee\Entity\EmployeeCompany;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\LeavesBundle\Calculators\UsedDaysCalculator;
use Infrastructure\LeavesBundle\Helpers\DateHelper;
use Infrastructure\LeavesBundle\Service\dto\LeaveUserStatsDto;
use Infrastructure\TimeOffBundle\Helpers\PolicyHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LeaveStatsService extends BaseService
{
    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @return LeaveUserStatsDto
     * @throws \Exception
     */
    public function userStat(User $user, TimeOffType $timeOffType)
    {
        /* @var $firstEmployeeCompany EmployeeCompany */
        $firstEmployeeCompany = $user->getFirstEmployeeCompany();

        if (!$firstEmployeeCompany instanceof EmployeeCompany) {
            throw new HttpException(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                sprintf('"%s" must assign to any company.', $user->getProfile()->getFullName())
            );
        }

        $startDate = $firstEmployeeCompany->getStartAt() ?: $firstEmployeeCompany->getCreatedAt();

        $policy = PolicyHelper::userPolicy($timeOffType, $user);

        return $this->userStatFromDates($user, $timeOffType, $policy, $startDate);
    }

    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param Policy $policy
     * @param \DateTime $periodStartDate
     * @return LeaveUserStatsDto
     * @throws \Exception
     */
    public function userStatFromDates(User $user, TimeOffType $timeOffType, Policy $policy, \DateTime $periodStartDate): LeaveUserStatsDto
    {
        $frequencyOfUse = $policy->getFrequencyOfUse();
        $frequencyType = $policy->getFrequencyType();

        $spec = DateHelper::periodDesignator($frequencyType, $frequencyOfUse);
        $endDate = DateHelper::currentPeriodEndDate($spec, $periodStartDate);
        $startDate = DateHelper::currentPeriodStartDate($spec, $endDate);

        return $this->process($user, $timeOffType, $policy, $startDate, $endDate);
    }

    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param Policy $policy
     * @param \DateTime $start
     * @param \DateTime $end
     * @return LeaveUserStatsDto
     */
    protected function process(User $user, TimeOffType $timeOffType, Policy $policy, \DateTime $start, \DateTime $end): LeaveUserStatsDto
    {
        $availableDays = $policy->getMaxCountAtTime();
        $usedDays = (new UsedDaysCalculator($user, $timeOffType, $start, $end))->calculate();

        $dto = new LeaveUserStatsDto(
            $policy,
            $timeOffType->getName(),
            $usedDays,
            $availableDays - $usedDays,
            $availableDays ? $policy->getMaxCountAtTime() : 0
        );

        $dto->setPeriod($start, $end);

        return $dto;
    }
}
