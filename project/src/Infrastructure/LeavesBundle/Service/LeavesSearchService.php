<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Infrastructure\LeavesBundle\Command\LeavesListCommand;

class LeavesSearchService extends LeavesBasicSearchService
{
    public function search(LeavesListCommand $command): QueryBuilder
    {
        $qb = parent::search($command);

        return $qb;
    }
}
