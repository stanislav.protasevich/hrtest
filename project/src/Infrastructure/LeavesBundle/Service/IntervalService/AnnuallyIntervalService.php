<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service\IntervalService;

use Domain\TimeOff\Entity\AccrualSchedule;

class AnnuallyIntervalService extends IntervalService implements IntervalServiceInterface
{
    /**
     * Конец рабочего периода
     * @return \DateTime
     * @throws \Exception
     */
    public function getEndPeriod(): \DateTime
    {
        $date = $this->getAccrualSchedule()->getAnnuallyMod();

        $end = \DateTime::createFromFormat(AccrualSchedule::ANNUALLY_MOD_FORMAT, $date);

        $now = new \DateTime();

        if($now >$end) {
            $end->add(new \DateInterval('P1Y'));
        }

        return $end;
    }

    /**
     * @return string
     */
    protected function getHoursCalculateSpec(): string
    {
        return 'P1M';
    }
}