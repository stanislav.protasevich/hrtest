<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service\IntervalService;


use Carbon\CarbonPeriod;
use Domain\Employee\Entity\CareerHistory;
use Domain\TimeOff\Entity\AccrualSchedule;
use Domain\User\Entity\User;
use Infrastructure\EmployeeBundle\Service\CareerHistoryService;
use Infrastructure\LeavesBundle\Helpers\DateHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class IntervalService implements IntervalServiceInterface
{
    /**
     * @var CareerHistory
     */
    protected $_careerHistory;
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var AccrualSchedule
     */
    protected $_accrualSchedule;
    /**
     * @var User
     */
    protected $_user;
    /**
     * @var CarbonPeriod
     */
    protected $_careerPeriodInterval;
    /**
     * @var \DateTime
     */
    protected $_endPeriod;
    /**
     * @var \DateTime
     */
    protected $_careerStart;
    /**
     * @var string
     */
    protected $_intervalSpec;
    /**
     * @var CarbonPeriod
     */
    protected $_activePeriodInterval;
    /**
     * @var \DateTime
     */
    protected $_startPeriod;
    /**
     * @var \DateTime
     */
    protected $_now;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Интервал рабочего периода с начало-конец
     * @return CarbonPeriod
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getCareerPeriodInterval(): CarbonPeriod
    {
        if (!$this->_careerPeriodInterval) {
            $this->_careerPeriodInterval = CarbonPeriod::create($this->getCareerStart(), $this->getEndPeriod());
        }

        return $this->_careerPeriodInterval;
    }

    /**
     * Начало рабочего периода с учетом поправки в правиле
     * @return \DateTime
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function getCareerStart(): \DateTime
    {
        if (!$this->_careerStart) {
            $beginsInterval = $this->getAccrualSchedule()->getBeginsInterval();
            $beginsType = $this->getAccrualSchedule()->getBeginsType();

            $designator = DateHelper::periodDesignator($beginsType);

            $spec = 'P'.$beginsInterval.$designator;

            $date = $this->getDirtyCareerStart();
            $startDate = clone $date;

            $this->_careerStart = $startDate->add(
                new \DateInterval($spec)
            );
        }

        return $this->_careerStart;
    }

    /**
     * @return AccrualSchedule
     */
    public function getAccrualSchedule(): AccrualSchedule
    {
        if (!$this->_accrualSchedule) {
            throw new \RuntimeException('Accrual schedule not set.');
        }

        return $this->_accrualSchedule;
    }

    /**
     * @param AccrualSchedule $accrualSchedule
     */
    public function setAccrualSchedule(AccrualSchedule $accrualSchedule): void
    {
        $this->_accrualSchedule = $accrualSchedule;
    }

    /**
     * Начало карьеры
     *
     * @return \DateTime
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function getDirtyCareerStart(): \DateTime
    {
        return $this->getCareerHistory()->getCreatedAt();
    }

    /**
     * @return CareerHistory
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCareerHistory(): CareerHistory
    {
        if (!$this->_careerHistory) {
            $this->_careerHistory = $this->container->get(CareerHistoryService::class)->getCareerStartDay($this->getUser());
        }

        return $this->_careerHistory;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        if (!$this->_user) {
            $this->_user = $this->container->get('security.token_storage')->getToken()->getUser();
        }

        return $this->_user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->_user = $user;
    }

    /**
     * Конец рабочего периода
     * @return \DateTime
     * @throws \Exception
     */
    public function getEndPeriod(): \DateTime
    {
        prnx(1);
        if (!$this->_endPeriod) {
            $this->_endPeriod = DateHelper::currentPeriodEndDate(
                $this->getIntervalSpec(),
                $this->getCareerStart()
            );
        }

        return $this->_endPeriod;
    }

    public function getIntervalSpec()
    {
        if (!$this->_intervalSpec) {
            $mod = $this->getMod();
            $interval = $this->getAccrualSchedule()->getInterval();
            $designator = DateHelper::periodDesignator($interval);

            $this->_intervalSpec = 'P'.$mod.$designator;
        }

        return $this->_intervalSpec;
    }

    /**
     * Сдвиг
     * @return int
     */
    protected function getMod(): int
    {
        return 1;
    }

    /**
     * Количество доступных часов в текущем периоде за данный момент
     * @return float
     * @throws \Exception
     */
    public function getAvailableHours(): float
    {
        $period = $this->getActivePeriodInterval();

        $period->setDateInterval($this->getHoursCalculateSpec());
        $period->excludeEndDate();

        $hours = $this->getHours();

        $count = 0;
        foreach ($period as $key => $date) {

            if ($date >= $this->getNow()) {
                break;
            }

            $count++;
        }

        $amount = round($hours / $period->count()) * $count;
        
        return $amount >= $this->getMaximumAvailableHours() ? $this->getMaximumAvailableHours() : $amount;
    }

    /**
     * Интервал активного периода
     * @return CarbonPeriod
     * @throws \Exception
     */
    public function getActivePeriodInterval(): CarbonPeriod
    {
        if (!$this->_activePeriodInterval) {
            $this->_activePeriodInterval = CarbonPeriod::create($this->getStartPeriod(), $this->getEndPeriod());
        }

        return $this->_activePeriodInterval;
    }

    /**
     * Начало активного периода
     * @return \DateTime
     * @throws \Exception
     */
    public function getStartPeriod(): \DateTime
    {
        if (!$this->_startPeriod) {
            $this->_startPeriod = DateHelper::prevDate($this->getIntervalSpec(), $this->getEndPeriod());
        }

        return $this->_startPeriod;
    }

    /**
     * @return string
     */
    abstract protected function getHoursCalculateSpec(): string;

    protected function getHours(): float
    {
        return $this->getAccrualSchedule()->getHours();
    }

    /**
     * Текущий момент
     *
     * @return \DateTime
     */
    public function getNow(): \DateTime
    {
        if (!$this->_now) {
            $this->_now = new \DateTime();
        }

        return $this->_now;
    }

    public function setNow(\DateTime $dateTime): void
    {
        $this->_now = $dateTime;
    }

    protected function getMaximumAvailableHours(): float
    {
        return $this->getAccrualSchedule()->getMaximumHours();
    }
}