<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service\IntervalService;


class DailyIntervalService extends IntervalService implements IntervalServiceInterface
{
    /**
     * @return string
     */
    protected function getHoursCalculateSpec(): string
    {
        return 'P1D';
    }

    /**
     * Сдвиг
     * @return int
     */
    protected function getMod(): int
    {
        return $this->getAccrualSchedule()->getAnnuallyDay();
    }
}