<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service\IntervalService;

use Carbon\CarbonPeriod;
use Domain\TimeOff\Entity\AccrualSchedule;

/**
 * @method getAccrualSchedule(): AccrualSchedule
 * @method setAccrualSchedule(AccrualSchedule $accrualSchedule): void
 * @method getCareerHistory(): CareerHistory
 * @method getUser(): User
 * @method getIntervalSpec(): string
 */
interface IntervalServiceInterface
{
    /**
     * Начало рабочего периода
     * @return \DateTime
     */
    public function getCareerStart(): \DateTime;

    /**
     * Начало активного периода
     * @return \DateTime
     */
    public function getStartPeriod(): \DateTime;

    /**
     * Конец активного периода
     * @return \DateTime
     */
    public function getEndPeriod(): \DateTime;

    /**
     * Интервал рабочего периода с начало-конец
     * @return CarbonPeriod
     */
    public function getCareerPeriodInterval(): CarbonPeriod;

    /**
     * Интервал текущего активного периода
     * @return CarbonPeriod
     */
    public function getActivePeriodInterval(): CarbonPeriod;

    /**
     * Количество доступных часов в текущем периоде за данный момент
     * @return float
     */
    public function getAvailableHours(): float;
}