<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service\dto;

class LeaveUserStatsPeriodDto
{
    /**
     * @var string
     */
    public $start;

    /**
     * @var string
     */
    public $end;

    public function __construct(string $start, string $end)
    {
        $this->start = $start;
        $this->end = $end;
    }
}
