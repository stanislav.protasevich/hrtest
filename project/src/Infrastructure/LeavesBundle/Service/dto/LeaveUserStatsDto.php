<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service\dto;


use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;

/**
 * @property TimeOffType $timeOffType
 * @property int $left
 * @property int $days
 * @property int $total
 * @property Policy $policy
 * @property LeaveUserStatsPeriodDto $period
 */
class LeaveUserStatsDto
{
    public $timeOffType;
    public $left = 0;
    public $days = 0;
    public $total = 0;

    /**
     * @var array
     */
    public $period = [];
    /**
     * @var Policy
     */
    public $policy;

    public function __construct(Policy $policy = null, string $timeOffType = null, int $left = 0, int $days = 0, int $total = 0)
    {
        $this->timeOffType = $timeOffType;
        $this->left = $left;
        $this->days = $days;
        $this->total = $total;
        $this->policy = $policy;
    }

    public function setPeriod(\DateTime $start, \DateTime $end)
    {
        $this->period = new LeaveUserStatsPeriodDto($start->format(\DateTime::ATOM), $end->format(\DateTime::ATOM));
    }
}
