<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service;

use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Domain\Leaves\Entity\Leaves;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\LeavesBundle\Command\LeavesRequestCommand;
use Infrastructure\LeavesBundle\Command\LeavesRequestEditCommand;

class LeaveRequestsService extends BaseService
{
    /**
     * @param LeavesRequestCommand $command
     * @return LeaveRequests
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function create(LeavesRequestCommand $command): LeaveRequests
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $userCreator = $this->getUser();
            $user = $command->getUser();

            $now = new \DateTime();

            $leaveRequest = new LeaveRequests();
            $leaveRequest->setPolicy($command->getPolicy());
            $leaveRequest->setTimeOffType($command->getTimeOffType());
            $leaveRequest->setSubstituteUser($command->getSubstituteUser());
            $leaveRequest->setReason($command->getReason());

            $leaveRequest->setTarget($command->getTarget() ? $command->getTarget() : LeaveRequests::TARGET_SELF);

            $leaveRequest->setUser($user);

            $days = $command->getDays();

            $leaveRequest->setStartDate($command->getStartDate());
            $leaveRequest->setEndDate($command->getEndDate());
            $leaveRequest->setDays($days->count());

            $leaveRequest->setStatus(LeaveRequests::STATUS_PENDING);

            $leaveRequest->setCreatedBy($userCreator);
            $leaveRequest->setUpdatedBy($userCreator);
            $leaveRequest->setCreatedAt($now);
            $leaveRequest->setUpdatedAt($now);

            foreach ($days as $day) {
                $leave = new Leaves();
                $leave->setDate($day);
                $leave->setLengthDays(Policy::DEFAULT_LENGTH_DAYS);
                $leave->setLengthHours(Policy::DEFAULT_LENGTH_HOURS);

                $leaveRequest->addLeave($leave);

                $em->persist($leave);
            }

            foreach ($user->getSupervisors() as $supervisor) {
                $leaveRequestSupervisor = new LeaveRequestSupervisor();
                $leaveRequestSupervisor->setUser($supervisor);
                $leaveRequestSupervisor->setStatus(LeaveRequestSupervisor::STATUS_PENDING);

                $leaveRequestSupervisor->setCreatedAt($now);
                $leaveRequestSupervisor->setUpdatedAt($now);

                $leaveRequest->addRequestSupervisor($leaveRequestSupervisor);

                $em->persist($leaveRequest);
            }

            $em->persist($leaveRequest);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $leaveRequest;
    }

    public function update(LeavesRequestEditCommand $command): LeaveRequests
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $user = $this->getUser();

            $now = new \DateTime();

            $leaveRequest = $command->getLeaveRequest();
            $leaveRequest->setSubstituteUser($command->getSubstituteUser());
            $leaveRequest->setReason($command->getReason());
            $leaveRequest->setUser($user);

            $days = $command->getDays();

            $leaveRequest->removeLeaves();

            $leaveRequest->setStartDate($command->getStartDate());
            $leaveRequest->setEndDate($command->getEndDate());
            $leaveRequest->setDays($days->count());

            if ($command->getStatus()) {
                $leaveRequest->setStatus($command->getStatus());
            }

            $leaveRequest->setUpdatedBy($user);
            $leaveRequest->setUpdatedAt($now);

            foreach ($days as $day) {
                $leave = new Leaves();
                $leave->setDate($day);
                $leave->setLengthDays(Policy::DEFAULT_LENGTH_DAYS);
                $leave->setLengthHours(Policy::DEFAULT_LENGTH_HOURS);

                $leaveRequest->addLeave($leave);

                $em->persist($leave);
            }

            $em->persist($leaveRequest);
            $em->flush();

            $em->getConnection()->commit();
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }

        return $leaveRequest;
    }

    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param \DateTime $periodStart
     * @param \DateTime $periodEnd
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getUsedDaysCountByPeriod(
        User $user,
        TimeOffType $timeOffType,
        \DateTime $periodStart,
        \DateTime $periodEnd
    ) {
        return (int)$this->getEm()
            ->getRepository(LeaveRequests::class)
            ->findUsedDaysCountByPeriod($user, $timeOffType, $periodStart, $periodEnd);
    }

    /**
     * @param LeaveRequests $leaveRequests
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function approveRequest(LeaveRequests $leaveRequests)
    {
        $this->setRequestStatus($leaveRequests, LeaveRequests::STATUS_APPROVED);
    }

    /**
     * @param LeaveRequests $leaveRequests
     * @param string $status
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function setRequestStatus(LeaveRequests $leaveRequests, string $status)
    {
        $em = $this->getEm();

        $leaveRequests->setStatus($status);

        $em->persist($leaveRequests);
        $em->flush();
    }

    /**
     * @param LeaveRequests $leaveRequests
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deniedRequest(LeaveRequests $leaveRequests)
    {
        $this->setRequestStatus($leaveRequests, LeaveRequests::STATUS_DENIED);
    }
}