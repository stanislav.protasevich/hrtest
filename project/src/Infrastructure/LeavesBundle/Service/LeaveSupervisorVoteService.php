<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service;

use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\LeavesBundle\Command\LeaveSupervisorVoteCommand;

class LeaveSupervisorVoteService extends BaseService
{
    /**
     * @param LeaveSupervisorVoteCommand $command
     * @return LeaveRequestSupervisor
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function vote(LeaveSupervisorVoteCommand $command): LeaveRequestSupervisor
    {
        $em = $this->getEm();

        $leaveRequestSupervisor = $command->getLeaveRequestSupervisor();

        $date = new \DateTime();
        $leaveRequestSupervisor->setUpdatedAt($date);

        $em->persist($leaveRequestSupervisor);
        $em->flush();

        return $leaveRequestSupervisor;
    }
}