<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Service;

use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\LeavesBundle\Mail\SupervisorUsersEmail;

class LeaveRequestNotifyService extends BaseService
{
    public function notify(string $leaveRequestId)
    {
//        $leaveRequest = $this->getEm()->getRepository(LeaveRequests::class)->find($leaveRequestId);
//
//        $this->sendSupervisorsEmail($leaveRequest);
//        $this->sendSubstituteEmail($leaveRequest->getSubstituteUser());
    }

    public function sendSupervisorsEmail(LeaveRequests $leaveRequest)
    {
        $supervisors = $leaveRequest->getRequestSupervisor()->getValues();

        $emails = array_map(function (LeaveRequestSupervisor $supervisor) {
            return [
                $supervisor->getUser()->getEmail(),
                $supervisor->getUser()->getProfile()->getFullName(),
            ];
        }, $supervisors);

        /* @var $supervisorsEmail SupervisorUsersEmail */
        $supervisorsEmail = $this->getContainer()->get(SupervisorUsersEmail::class);
//        $supervisorsEmail->setTo($emails);
//        $supervisorsEmail->send();
    }

    public function sendSubstituteEmail(User $substitute = null)
    {

    }
}