<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\LeavesBundle\Command\LeavesListCommand;

class LeavesBasicSearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'leaves.id',
        'status' => 'leaves.status',
        'timeOffType' => 'leaves.timeOffType',
        'reason' => 'leaves.reason',
        'startDate' => 'leaves.startDate',
        'endDate' => 'leaves.endDate',
        'user' => 'CONCAT(profile.firstName, \' \', profile.surname)',
    ];

    public function search(LeavesListCommand $command): QueryBuilder
    {
        $qb = $this->getEm()
            ->getRepository(LeaveRequests::class)
            ->findAllQueryBuilder()
            ->select(['leaves', 'user', 'profile'])
            ->leftJoin('leaves.user', 'user')
            ->leftJoin('user.profile', 'profile');

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('leaves.id IN (:id)')
                ->setParameter('id', $id);
        }

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('leaves.status IN (:status)')
                ->setParameter('status', $status);
        }

        if ($command->timeOffType && $timeOffType = $this->paramsFromString($command->timeOffType)) {
            $qb->andWhere('leaves.timeOffType IN (:timeOffType)')
                ->setParameter('timeOffType', $timeOffType);
        }

        if ($command->startDate && $startDate = $this->stringToDate($command->startDate)) {
            $qb->andWhere('leaves.startDate >= :startDate')
                ->setParameter('startDate', $startDate);
        }

        if ($command->endDate && $endDate = $this->stringToDate($command->endDate)) {
            $qb->andWhere('leaves.endDate <= :endDate')
                ->setParameter('endDate', $endDate);
        }

        if ($command->user && $user = $this->paramsFromString($command->user)) {
            $qb->andWhere('leaves.user IN (:user)')
                ->setParameter('user', $this->getModel(User::class, $user));
        }

        if ($command->getGrant() === LeavesListCommand::GRANT_SELF) {
            $qb->andWhere('leaves.user = :user')
                ->setParameter('user', $this->getUser());
        }
        
        $this->sorting($this->getRequest(), $qb, $this->sortMapping, 'leaves.createdAt', 'DESC');

        return $qb;
    }
}
