<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class LeaveRequestsSearchService extends BaseService
{
    public function searchByUserQuery(User $user): QueryBuilder
    {
        return $this->getEm()->getRepository(LeaveRequests::class)->findAllByUserQuery($user);
    }
}