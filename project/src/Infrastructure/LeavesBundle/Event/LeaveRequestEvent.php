<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Event;


use Domain\Leaves\Entity\LeaveRequests;
use Symfony\Component\EventDispatcher\Event;

class LeaveRequestEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeLeaveRequestEventInsert';
    public const EVENT_AFTER_INSERT = 'afterLeaveRequestEventInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeLeaveRequestEventUpdate';
    public const EVENT_AFTER_UPDATE = 'afterLeaveRequestEventUpdate';

    public const EVENT_BEFORE_DELETE = 'beforeLeaveRequestEventDelete';
    public const EVENT_AFTER_DELETE = 'afterLeaveRequestEventDelete';
    /**
     * @var LeaveRequests
     */
    private $leaveRequests;

    public function __construct(LeaveRequests $leaveRequests)
    {

        $this->leaveRequests = $leaveRequests;
    }

    /**
     * @return LeaveRequests
     */
    public function getLeaveRequests(): LeaveRequests
    {
        return $this->leaveRequests;
    }
}