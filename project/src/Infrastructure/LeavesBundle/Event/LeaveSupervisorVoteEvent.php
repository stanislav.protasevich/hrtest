<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Event;


use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Symfony\Component\EventDispatcher\Event;

class LeaveSupervisorVoteEvent extends Event
{
    public const EVENT_BEFORE_UPDATE = 'beforeLeaveSupervisorVoteEventUpdate';
    public const EVENT_AFTER_UPDATE = 'afterLeaveSupervisorVoteEventUpdate';

    /**
     * @var LeaveRequestSupervisor
     */
    private $leaveRequestSupervisor;

    public function __construct(LeaveRequestSupervisor $leaveRequestSupervisor)
    {
        $this->leaveRequestSupervisor = $leaveRequestSupervisor;
    }

    /**
     * @return LeaveRequestSupervisor
     */
    public function getLeaveRequestSupervisor(): LeaveRequestSupervisor
    {
        return $this->leaveRequestSupervisor;
    }
}