<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Command;


use Domain\Leaves\Entity\LeaveRequestSupervisor;

class LeaveSupervisorVoteCommand
{
    /**
     * @var LeaveRequestSupervisor
     */
    private $leaveRequestSupervisor;

    public function __construct(LeaveRequestSupervisor $leaveRequestSupervisor)
    {
        $this->leaveRequestSupervisor = $leaveRequestSupervisor;
    }

    /**
     * @return LeaveRequestSupervisor
     */
    public function getLeaveRequestSupervisor(): LeaveRequestSupervisor
    {
        return $this->leaveRequestSupervisor;
    }
}