<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Command;


use Domain\Leaves\Entity\LeaveRequests;

class LeavesRequestViewCommand
{
    /**
     * @var LeaveRequests
     */
    private $leaveRequests;

    public function __construct(LeaveRequests $leaveRequests)
    {
        $this->leaveRequests = $leaveRequests;
    }

    /**
     * @return LeaveRequests
     */
    public function getLeaveRequests(): LeaveRequests
    {
        return $this->leaveRequests;
    }
}