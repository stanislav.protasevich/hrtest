<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Command;

use Carbon\CarbonPeriod;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Factory\DynamicObject;
use Infrastructure\LeavesBundle\Validator\Constraints\LeaveAvailableDaysRequestConstraint;
use Infrastructure\LeavesBundle\Validator\Constraints\LeaveIntervalValid;
use Infrastructure\LeavesBundle\Validator\Constraints\LeaveIsInPeriodRequestConstraint;
use Infrastructure\LeavesBundle\Validator\Constraints\LeaveIsInRowConstraint;

class LeavesRequestCommand extends DynamicObject
{
    /**
     * @var string
     */
    protected $reason;
    /**
     * @var User
     */
    protected $substituteUser;
    /**
     * @var LeaveRequests
     */
    protected $leaveRequest;
    /**
     * @var LeaveRequests
     */
    protected $oldLeaveRequest;

    /**
     * @var TimeOffType
     */
    private $timeOffType;
    /**
     * @var Policy
     */
    private $policy;

    /**
     * @var \DateTime
     * @LeaveIntervalValid()
     * @LeaveIsInPeriodRequestConstraint(field="startDate")
     * @LeaveIsInRowConstraint()
     * @LeaveAvailableDaysRequestConstraint()
     */
//* @LeaveIntervalValid()
//* @LeaveIsInPeriodRequestConstraint()
//* @LeaveIsInRowConstraint()
//* @LeaveAvailableDaysRequestConstraint()
    private $startDate;

    /**
     * @LeaveIsInPeriodRequestConstraint(field="endDate")
     * @var \DateTime
     */
    private $endDate;
    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $target;

    /**
     * @var bool
     */
    public $inPeriodIsValid;
    /**
     * LeavesRequestCommand constructor.
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(User $user, TimeOffType $timeOffType, \DateTime $startDate, \DateTime $endDate)
    {
        $this->timeOffType = $timeOffType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     */
    public function setReason(?string $reason): void
    {
        $this->reason = $reason;
    }

    /**
     * @return User
     */
    public function getSubstituteUser(): ?User
    {
        return $this->substituteUser;
    }

    /**
     * @param User $substituteUser
     */
    public function setSubstituteUser(?User $substituteUser): void
    {
        $this->substituteUser = $substituteUser;
    }

    /**
     * @return TimeOffType
     */
    public function getTimeOffType(): TimeOffType
    {
        return $this->timeOffType;
    }

    /**
     * @return Policy
     */
    public function getPolicy(): Policy
    {
        return $this->policy;
    }

    /**
     * @param Policy $policy
     */
    public function setPolicy(Policy $policy): void
    {
        $this->policy = $policy;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return clone $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return clone $this->endDate;
    }

    public function getDays(): CarbonPeriod
    {
        return clone CarbonPeriod::create($this->startDate, $this->endDate);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget(string $target): void
    {
        $this->target = $target;
    }

    /**
     * @return LeaveRequests
     */
    public function getOldLeaveRequest(): LeaveRequests
    {
        return $this->oldLeaveRequest;
    }

    /**
     * @param LeaveRequests $oldLeaveRequest
     */
    public function setOldLeaveRequest(LeaveRequests $oldLeaveRequest): void
    {
        $this->oldLeaveRequest = $oldLeaveRequest;
    }

    /**
     * @return LeaveRequests
     */
    public function getLeaveRequest(): LeaveRequests
    {
        return $this->leaveRequest;
    }

    /**
     * @param LeaveRequests $leaveRequest
     */
    public function setLeaveRequest(LeaveRequests $leaveRequest): void
    {
        $this->leaveRequest = $leaveRequest;
    }

    public function isExist(): bool
    {
        return !is_null($this->oldLeaveRequest);
    }
}
