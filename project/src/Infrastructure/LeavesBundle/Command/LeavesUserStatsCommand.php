<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 17.09.18
 * Time: 16:13
 */

namespace Infrastructure\LeavesBundle\Command;


use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;

class LeavesUserStatsCommand
{
    /**
     * @var User
     */
    private $user;
    /**
     * @var TimeOffType
     */
    private $timeOffType;

    public function __construct(User $user, TimeOffType $timeOffType)
    {
        $this->user = $user;
        $this->timeOffType = $timeOffType;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return TimeOffType
     */
    public function getTimeOffType(): TimeOffType
    {
        return $this->timeOffType;
    }
}