<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Infrastructure\LeavesBundle\Rbac\LeavesPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use Symfony\Component\Validator\Constraints as Assert;

class LeavesListCommand
{
    use ObjectebleTrait;

    public const GRANT_ALL = 'all';
    public const GRANT_SELF = 'self';

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $timeOffType;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $user;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $startDate;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $endDate;
    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $status;
    
    /**
     * @return string
     */
    public function getGrant(): string
    {
        return Gate::can(LeavesPermissions::PERMISSION_LEAVES_LIST_ALL) ? static::GRANT_ALL : static::GRANT_SELF;
    }
}