<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Factory\Form;


use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class LeaveRequestSupervisorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, [
                'choices' => LeaveRequestSupervisor::STATUSES_CAN_CHANGE,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('comment', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => [LeaveRequestSupervisor::STATUS_DENIED]
                    ])
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LeaveRequestSupervisor::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $data LeaveRequestSupervisor */
                $data = $form->getData();

                $groups = ['Default'];

                $groups[] = $data->getStatus();

                return $groups;
            },
        ]);
    }
}