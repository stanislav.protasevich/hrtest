<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LeavesBundle\Factory\Form;

use Domain\Leaves\Entity\LeaveRequests;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\DateMin;
use Infrastructure\LeavesBundle\Validator\Constraints\LeaveSubstituteUser;
use Infrastructure\TimeOffBundle\Repository\TimeOffTypeRepository;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class LeaveRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timeOffType', EntityType::class, [
                'class' => TimeOffType::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (TimeOffTypeRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('target', ChoiceType::class, [
                'choices' => LeaveRequests::TARGETS,
                'empty_data' => LeaveRequests::TARGET_SELF,
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $r) {
                    return $r->queryFindAllActive();
                },
                'constraints' => [
                    new NotBlank([
                        'groups' => LeaveRequests::TARGET_APPOINTED,
                    ]),
                ],
            ])
            ->add('reason', TextType::class, [
                'constraints' => [
                    new Length([
                        'min' => 2,
                        'max' => 255,
                    ]),
                ],
            ])
            ->add('substituteUser', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $r) {
                    return $r->queryFindAllActive();
                },
                'constraints' => [
                    new LeaveSubstituteUser(),
                ],
            ])
            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                    new DateMin([
                        'groups' => [LeaveRequests::SCENARIO_CREATE]
                    ]),
                ],
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                    new DateMin([
                        'groups' => [LeaveRequests::SCENARIO_CREATE]
                    ]),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => LeaveRequests::STATUSES,
                'disabled' => !$options['isAdministrator'],
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

//                $timeOffType = $data['timeOffType'] ?? null;
//
//                $form->add('policy', EntityType::class, [
//                    'class' => Policy::class,
//                    'query_builder' => function (PolicyRepository $r) use ($timeOffType) {
//                        return $r->createQueryBuilder('p')
//                            ->where('p.status = :status AND p.timeOffType = :timeOffType')
//                            ->setParameter('timeOffType', $timeOffType)
//                            ->setParameter('status', Policy::STATUS_ACTIVE);
//                    },
//                    'constraints' => [
//                        new NotBlank(),
//                    ],
//                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LeaveRequests::class,
            'csrf_protection' => false,
            'isAdministrator' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $data LeaveRequests */
                $data = $form->getData();

                $groups = ['Default'];

                if ($target = $data->getTarget()) {
                    $groups[] = $target;
                }

                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();

                if ($method === Request::METHOD_PUT) {
                    $groups[] = LeaveRequests::SCENARIO_CREATE;
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = LeaveRequests::SCENARIO_UPDATE;
                }

                return $groups;
            },
        ]);
    }
}