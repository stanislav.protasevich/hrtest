<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Mail;


use Domain\Leaves\Entity\LeaveRequests;
use Domain\User\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SupervisorUsersEmail
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function sendMail(LeaveRequests $leaveRequests)
    {
        /* @var $mailer \Swift_Mailer */
        $mailer = $this->container->get('swiftmailer.mailer.default');
        /* @var $twig \Twig_Environment */
        $twig = $this->container->get('twig');
        /* @var $translator TranslatorInterface */
        $translator = $this->container->get('translator');

        /* @var $supervisors User[] */
        $supervisors = $leaveRequests->getUser()->getSupervisors();

        foreach ($supervisors as $supervisor) {
            $message = (new \Swift_Message(
                $translator->trans('Employee {{ name }} has requested a vacation', [
                    '{{ name }}' => $leaveRequests->getUser()->getProfile()->getFullName(),
                ]))
            )
                ->setFrom('root@localhost.com', 'HR Admin')
                ->setTo($supervisor->getEmail(), $supervisor->getProfile()->getFullName())
                ->setBody(
                    $twig->render('@Leaves/emails/supervisor-notify.html.twig', [
                        'name' => $leaveRequests->getUser()->getProfile()->getFullName(),
                        'days' => $leaveRequests->getDays(),
                        'from' => $leaveRequests->getStartDate()->format('d.m.Y'),
                        'end' => $leaveRequests->getEndDate()->format('d.m.Y'),
                    ]),
                    'text/html'
                );

            $mailer->send($message);
        }
    }
}