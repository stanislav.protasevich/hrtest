<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Leaves\Entity\LeaveRequestSupervisor;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LeaveRequestSupervisor|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaveRequestSupervisor|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaveRequestSupervisor[]    findAll()
 * @method LeaveRequestSupervisor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeaveRequestSupervisorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LeaveRequestSupervisor::class);
    }

    public function findAllQueryBuilder($alias = 'lrsa')
    {
        return $this->createQueryBuilder($alias);
    }
}