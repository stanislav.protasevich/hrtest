<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\Leaves\Entity\Leaves;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Leaves|null find($id, $lockMode = null, $lockVersion = null)
 * @method Leaves|null findOneBy(array $criteria, array $orderBy = null)
 * @method Leaves[]    findAll()
 * @method Leaves[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class LeavesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Leaves::class);
    }

    public function findAllQueryBuilder($alias = 'leaves')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findAllByUser(User $user): array
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user and l.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->getQuery()
            ->getResult();
    }
}