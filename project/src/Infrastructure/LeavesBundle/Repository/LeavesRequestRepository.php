<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LeavesBundle\Repository;

use Carbon\Carbon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Leaves\Entity\LeaveRequests;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LeaveRequests|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeaveRequests|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeaveRequests[]    findAll()
 * @method LeaveRequests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class LeavesRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LeaveRequests::class);
    }

    public function findAllQueryBuilder($alias = 'leaves')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findAllApprovedByUser(User $user): array
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user and l.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->getQuery()
            ->getResult();
    }

    public function findAllByUserQuery(User $user)
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user')
            ->setParameter('user', $user);
    }

    public function findAllByUserTimeOffType(User $user, TimeOffType $timeOffType): array
    {
        return $this->createQueryBuilder('l')
            ->where('l.user = :user and l.status = :status and l.timeOffType = :timeOffType')
            ->setParameter('user', $user)
            ->setParameter('timeOffType', $timeOffType)
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получение утвержденных запросов за период
     *
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param \DateTime $periodStart
     * @param \DateTime $periodEnd
     * @return array
     */
    public function findUserRequestByPeriod(User $user, TimeOffType $timeOffType, \DateTime $periodStart, \DateTime $periodEnd): array
    {
        return $this->createQueryBuilder('lr')
            ->where('lr.user = :user AND lr.status = :status ' .
                'AND lr.timeOffType = :timeOffType AND lr.createdAt >= :periodStart AND lr.createdAt <= :periodEnd')
            ->setParameter('user', $user)
            ->setParameter('timeOffType', $timeOffType)
            ->setParameter('periodStart', $periodStart)
            ->setParameter('periodEnd', $periodEnd)
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получение количества дней по утвержденным запросов за период
     *
     * @param User $user
     * @param TimeOffType $timeOffType
     * @param \DateTime $periodStart
     * @param \DateTime $periodEnd
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findUsedDaysCountByPeriod(User $user, TimeOffType $timeOffType, \DateTime $periodStart, \DateTime $periodEnd): int
    {
        $query = $this->createQueryBuilder('lr')
            ->select('SUM(lr.days)')
            ->where('lr.user = :user AND lr.status = :status AND ' .
                'lr.timeOffType = :timeOffType AND ' .
                'lr.startDate >= :periodStart AND lr.endDate <= :periodEnd')
            ->setParameter('user', $user)
            ->setParameter('timeOffType', $timeOffType)
            ->setParameter('periodStart', Carbon::instance($periodStart)->startOfDay())
            ->setParameter('periodEnd', Carbon::instance($periodEnd)->startOfDay())
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->getQuery();

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @return LeaveRequests|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastAcceptByUser(User $user, TimeOffType $timeOffType): ?LeaveRequests
    {
        $query = $this->createQueryBuilder('lr')
            ->where('lr.user = :user AND lr.status = :status AND lr.timeOffType = :timeOffType')
            ->setParameter('user', $user)
            ->setParameter('timeOffType', $timeOffType)
            ->setParameter('status', LeaveRequests::STATUS_APPROVED)
            ->setMaxResults(1)
            ->orderBy('lr.endDate', 'DESC');

        return $query->getQuery()->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @param TimeOffType $timeOffType
     * @return LeaveRequests|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findLastByUser(User $user, TimeOffType $timeOffType): ?LeaveRequests
    {
        $query = $this->createQueryBuilder('lr')
            ->where('lr.user = :user AND lr.timeOffType = :timeOffType')
            ->setParameter('user', $user)
            ->setParameter('timeOffType', $timeOffType)
            ->setMaxResults(1)
            ->orderBy('lr.endDate', 'DESC');

        return $query->getQuery()->getOneOrNullResult();
    }
}