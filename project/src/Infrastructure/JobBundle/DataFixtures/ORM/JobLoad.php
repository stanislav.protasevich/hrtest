<?php

declare(strict_types=1);

namespace Infrastructure\JobBundle\DataFixtures\ORM;


use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Job\Entity\JobTitle;
use Domain\User\Entity\User;
use Faker\Factory;
use Faker\Provider\en_US\Company;

class JobLoad extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        $user = $em->getRepository(User::class)->findOneBy(['status' => User::STATUS_ACTIVE]);

        $faker = Factory::create();
        $faker->addProvider(new Company($faker));

        for ($i = 0; $i < 10; $i++) {
            $job = new JobTitle();
            $job->setName($faker->jobTitle);
            $job->setStatus(JobTitle::STATUS_ACTIVE);
            $job->setCreatedAt(new \DateTime());
            $job->setUpdatedAt(new \DateTime());
            $job->setCreatedBy($user);
            $job->setUpdatedBy($user);
            $em->persist($job);
        }

        $em->flush();
    }
}