<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\JobBundle\Event;


use Symfony\Component\EventDispatcher\Event;

class JobTitleEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'jobTitleEventBeforeInsert';
    public const EVENT_AFTER_INSERT = 'jobTitleEventAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'jobTitleEventBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'jobTitleEventAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'jobTitleEventBeforeDelete';
    public const EVENT_AFTER_DELETE = 'jobTitleEventAfterDelete';

    public const EVENT_BEFORE_FIND = 'jobTitleEventBeforeFind';
    public const EVENT_AFTER_FIND = 'jobTitleEventAfterFind';

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}