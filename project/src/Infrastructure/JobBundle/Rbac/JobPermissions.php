<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\JobBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class JobPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_JOB_TITLE_CREATE = 'jobTitleCreate';
    public const PERMISSION_JOB_TITLE_UPDATE = 'jobTitleUpdate';
    public const PERMISSION_JOB_TITLE_LIST = 'jobTitleList';
    public const PERMISSION_JOB_TITLE_LIST_PRIMARY = 'jobTitleListPrimary';
    public const PERMISSION_JOB_TITLE_DELETE = 'jobTitleDelete';

    public const PERMISSION_JOBS = [
        self::PERMISSION_JOB_TITLE_CREATE,
        self::PERMISSION_JOB_TITLE_UPDATE,
        self::PERMISSION_JOB_TITLE_LIST,
        self::PERMISSION_JOB_TITLE_DELETE,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_JOB_TITLE_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_JOB_TITLE_UPDATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_JOB_TITLE_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_JOB_TITLE_LIST_PRIMARY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_JOB_TITLE_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}