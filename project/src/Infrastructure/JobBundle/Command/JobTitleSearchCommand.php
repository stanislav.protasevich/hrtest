<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\JobBundle\Command;

use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class JobTitleSearchCommand
{
    use ObjectebleTrait;

    public $id;

    public $name;

    public $status;
}