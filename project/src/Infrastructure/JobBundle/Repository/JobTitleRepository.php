<?php

declare(strict_types=1);

namespace Infrastructure\JobBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Domain\Company\Entity\Company;
use Domain\Job\Entity\JobTitle;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method JobTitle|null find($id, $lockMode = null, $lockVersion = null)
 * @method JobTitle|null findOneBy(array $criteria, array $orderBy = null)
 * @method JobTitle[]    findAll()
 * @method JobTitle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JobTitleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, JobTitle::class);
    }

    /**
     * @param string $id
     * @return JobTitle|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?JobTitle
    {
        return $this->createQueryBuilder('j')
            ->where('j.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllActive($hydrationMode = Query::HYDRATE_OBJECT)
    {
        return $this->queryFindAllActive()->getQuery()->getResult($hydrationMode);
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder()
            ->where('jobTitle.status = :status')
            ->setParameter('status', Company::STATUS_ACTIVE);
    }

    public function findAllQueryBuilder($alias = 'jobTitle')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('jobTitle')
            ->select(['jobTitle.id as value', 'jobTitle.name as label'])
            ->getQuery()
            ->getArrayResult();
    }
}