<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\JobBundle\Service;


use Domain\Job\Entity\JobTitle;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\JobBundle\Command\JobTitleSearchCommand;

class JobTitleSearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'jobTitle.id',
        'name' => 'jobTitle.name',
        'status' => 'jobTitle.status',
    ];

    public function search(JobTitleSearchCommand $command)
    {
        $qb = $this->getEm()
            ->getRepository(JobTitle::class)
            ->findAllQueryBuilder('jobTitle')
            ->orderBy('jobTitle.name', 'ASC');

        if ($command->name) {
            $qb->andHaving('jobTitle.name LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$command->name));
        }

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('jobTitle.status IN (:status)')
                ->setParameter('status', $status);
        }

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('jobTitle.id IN (:id)')
                ->setParameter('id', $id);
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
