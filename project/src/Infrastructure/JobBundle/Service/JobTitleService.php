<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\JobBundle\Service;


use Doctrine\ORM\Query;
use Domain\Job\Entity\JobTitle;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\JobBundle\Command\JobTitleCreateCommand;
use Infrastructure\JobBundle\Command\JobTitleEditCommand;

class JobTitleService extends BaseService
{
    /**
     * @param JobTitleCreateCommand $command
     * @return JobTitle
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(JobTitleCreateCommand $command): JobTitle
    {
        $em = $this->getEm();

        $jobTile = $command->getJobTitle();
        $jobTile->setCreatedAt(new \DateTime());
        $jobTile->setUpdatedAt(new \DateTime());
        $jobTile->setCreatedBy($this->getUser());
        $jobTile->setUpdatedBy($this->getUser());

        $em->persist($jobTile);
        $em->flush();

        return $jobTile;
    }

    /**
     * @param JobTitleEditCommand $command
     * @return JobTitle
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(JobTitleEditCommand $command)
    {
        $em = $this->getEm();

        $jobTitle = $command->getJobTitle();
        $jobTitle->setUpdatedAt(new \DateTime());
        $jobTitle->setUpdatedBy($this->getUser());

        $em->persist($jobTitle);
        $em->flush();

        return $jobTitle;
    }

    public function delete(JobTitle $jobTitle): void
    {
        $em = $this->getEm();

        $em->remove($jobTitle);
        $em->flush();
    }

    /**
     * @param int $hydrationMode
     * @return JobTitle[]
     */
    public function findAll($hydrationMode = Query::HYDRATE_OBJECT)
    {
        return $this->getEm()->getRepository(JobTitle::class)->findAllActive($hydrationMode);
    }

    public function getAllJobTitles(): array
    {
        return $this->getEm()->getRepository(JobTitle::class)->findPrimaryAll();
    }
}