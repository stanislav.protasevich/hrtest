<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\EventListener;

use Infrastructure\OauthBundle\Event\Oauth2Event;
use Zend\EventManager\Event;

class Oauth2EventListener extends Event
{
    public static function getSubscribedEvents()
    {
        return [
            Oauth2Event::EVENT_BEFORE_LOGIN => 'onBeforeLogin',
            Oauth2Event::EVENT_AFTER_LOGIN => 'onAfterLogin',
            Oauth2Event::EVENT_FAILED_LOGIN => 'onFailedLogin',
            Oauth2Event::EVENT_BEFORE_CODE => 'onBeforeCode',
            Oauth2Event::EVENT_AFTER_CODE => 'onAfterCode',
            Oauth2Event::EVENT_BEFORE_TOKEN => 'onBeforeToken',
            Oauth2Event::EVENT_AFTER_TOKEN => 'onAfterToken',
            Oauth2Event::EVENT_ERROR_VALIDATION => 'onErrorValidation',
        ];
    }

    public function onBeforeCode(Oauth2Event $event)
    {

    }

    public function onAfterCode(Oauth2Event $event)
    {

    }

    public function onBeforeToken(Oauth2Event $event)
    {

    }

    public function onAfterToken(Oauth2Event $event)
    {

    }

    public function onBeforeLogin(Oauth2Event $event)
    {

    }

    public function onAfterLogin(Oauth2Event $event)
    {

    }

    public function onFailedLogin(Oauth2Event $event)
    {

    }
    
    public function onErrorValidation(Oauth2Event $event)
    {

    }
}