<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\EventListener;


use FOS\OAuthServerBundle\Model\AccessTokenManagerInterface;
use FOS\OAuthServerBundle\Model\RefreshTokenManagerInterface;
use Infrastructure\OauthBundle\Service\AccessTokenService;
use Infrastructure\UserBundle\Event\UserDismissalEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserDismissalClearTokensEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var AccessTokenManagerInterface
     */
    private $accessTokenManager;
    /**
     * @var AccessTokenService
     */
    private $service;
    /**
     * @var RefreshTokenManagerInterface
     */
    private $refreshTokenManager;

    public function __construct(
        ContainerInterface $container,
        AccessTokenManagerInterface $accessTokenManager,
        RefreshTokenManagerInterface $refreshTokenManager,
        AccessTokenService $service
    ) {
        $this->container = $container;
        $this->accessTokenManager = $accessTokenManager;
        $this->service = $service;
        $this->refreshTokenManager = $refreshTokenManager;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserDismissalEvent::EVENT_AFTER_DISMISSAL => 'onAfterDismissal',
        ];
    }

    public function onAfterDismissal(UserDismissalEvent $event)
    {
        $user = $event->getUser();

        $tokens = $this->service->findAllByUser($user);

        foreach ($tokens as $item) {
            $this->accessTokenManager->deleteToken($item);
            $this->refreshTokenManager->deleteToken($item);
        }
    }
}
