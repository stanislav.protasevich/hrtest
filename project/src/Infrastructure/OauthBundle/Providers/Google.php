<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\Providers;

use Infrastructure\OauthBundle\Client\OAuth2;

class Google extends OAuth2 implements Oauth2ProviderInterface
{
    /**
     * @inheritdoc
     */
    public $authUrl = 'https://accounts.google.com/o/oauth2/auth';

    /**
     * @inheritdoc
     */
    public $tokenUrl = 'https://accounts.google.com/o/oauth2/token';

    /**
     * @inheritdoc
     */
    public $apiBaseUrl = 'https://www.googleapis.com/plus/v1';

    public function __construct()
    {
        if ($this->scope === null) {
            $this->scope = implode(' ', [
                'profile',
                'email',
            ]);
        }
    }

    /**
     * Initializes authenticated user attributes.
     * @return array auth user attributes.
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function initUserData()
    {
        return $this->api('people/me', 'GET');
    }
}