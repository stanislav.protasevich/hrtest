<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\Command;


class Oauth2DeactivateCommand extends Oauth2Command
{

}