<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\Command;


use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Validator\Constraints\Password;
use Infrastructure\CommonBundle\Factory\PropertyAccessTrait;
use Infrastructure\CommonBundle\Validator\Constraints\Exist;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class Oauth2TokenCommand
{
    use PropertyAccessTrait;

    /**
     * @var null|string
     */
    private $clientId;
    /**
     * @var null|string
     */
    private $clientSecret;
    /**
     * @var null|string
     */
    private $grantType;
    /**
     * @var null|string
     */
    private $password;
    /**
     * @var null|string
     */
    private $username;
    /**
     * @var null|string
     */
    private $refreshToken;

    private $scope;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('username', new Exist([
            'targetClass' => User::class,
            'targetAttribute' => 'email',
            'filter' => ['status|=|' . User::STATUS_ACTIVE],
            'groups' => ['secondStep'],
        ]));

        $metadata->addPropertyConstraint('password', new Password([
            'groups' => ['secondStep'],
            'username' => 'username',
        ]));
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret(string $clientSecret): void
    {
        $this->clientSecret = $clientSecret;
    }

    /**
     * @return string
     */
    public function getGrantType()
    {
        return $this->grantType;
    }

    /**
     * @param string $grantType
     */
    public function setGrantType(string $grantType): void
    {
        $this->grantType = $grantType;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return null|string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param null|string $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return null|string
     */
    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    /**
     * @param null|string $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return mixed
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param mixed $scope
     */
    public function setScope($scope): void
    {
        $this->scope = $scope;
    }
}