<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\Command;


use Domain\User\Entity\ConnectedAccountProvider;
use Infrastructure\CommonBundle\Validator\Constraints\Exist;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;


class Oauth2Command
{
    /**
     * @Assert\NotBlank()
     * @var string
     */
    private $provider;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $state;

    public $userAgent;

    public $ip;
    
    public function __construct(?string $provider, ?string $code, ?string $state)
    {
        $this->provider = $provider;
        $this->code = $code;
        $this->state = $state;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('provider', new Exist([
            'targetClass' => ConnectedAccountProvider::class,
            'targetAttribute' => 'name',
            'filter' => ['status|=|' . ConnectedAccountProvider::STATUS_ACTIVE],
        ]));
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @return string
     */
    public function getState(): ?string
    {
        return $this->state;
    }
}