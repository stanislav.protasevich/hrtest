<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\Lib;


use Doctrine\ORM\EntityManager;
use Domain\User\Entity\User;
use FOS\OAuthServerBundle\Model\ClientInterface;
use Infrastructure\AuthBundle\Service\JwtTokenService;
use Infrastructure\OauthBundle\Command\Oauth2TokenCommand;
use OAuth2\IOAuth2GrantCode;
use OAuth2\IOAuth2RefreshTokens;
use OAuth2\Model\IOAuth2Client;

class OAuth2 extends \OAuth2\OAuth2
{
    /**
     * @var User
     */
    private $_user;

    /**
     * @param IOAuth2Client $client
     * @param Oauth2TokenCommand $command
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getGrantAccessTokenUserCredentials(IOAuth2Client $client, Oauth2TokenCommand $command)
    {
        if (!$client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        /* @var $em EntityManager */
        $em = app('doctrine.orm.default_entity_manager');
        $this->_user = $em->getRepository(User::class)->loadByEmail($command->getUsername());

        return [
            'data' => $this->getUser(),
        ];
    }

    public function getCheckScope($requiredScope, $availableScope): bool
    {
        return $this->checkScope($requiredScope, $availableScope);
    }

    /**
     * @param IOAuth2Client $client
     * @param Oauth2TokenCommand $command
     * @return array
     * @throws \OAuth2\OAuth2ServerException
     */
    public function getGrantAccessTokenRefreshToken(IOAuth2Client $client, Oauth2TokenCommand $command)
    {
        $result = $this->grantAccessTokenRefreshToken($client, [
            'refresh_token' => $command->getRefreshToken(),
        ]);

        $this->_user = $result['data'];

        return $result;
    }

    public function createAccessToken(IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        /* @var $jwt JwtTokenService */
        $jwt = app(JwtTokenService::class);

        $expires = time() + ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME));

        $token = [
            "access_token" => $jwt->create($this->getUser()->getId(), $expires),
            "expires_in" => ($access_token_lifetime ?: $this->getVariable(self::CONFIG_ACCESS_LIFETIME)),
            "token_type" => $this->getVariable(self::CONFIG_TOKEN_TYPE),
            "scope" => $scope,
        ];

        $this->storage->createAccessToken($token["access_token"], $client, $data, $expires, $scope);

        // Issue a refresh token also, if we support them
        if ($this->storage instanceof IOAuth2RefreshTokens && $issue_refresh_token === true) {
            $token["refresh_token"] = $this->genAccessToken();
            $this->storage->createRefreshToken(
                $token["refresh_token"],
                $client,
                $data,
                time() + ($refresh_token_lifetime ?: $this->getVariable(self::CONFIG_REFRESH_LIFETIME)),
                $scope
            );

            // If we've granted a new refresh token, expire the old one
            if (null !== $this->oldRefreshToken) {
                $this->storage->unsetRefreshToken($this->oldRefreshToken);
                $this->oldRefreshToken = null;
            }
        }

        if ($this->storage instanceof IOAuth2GrantCode) {
            if (null !== $this->usedAuthCode) {
                $this->storage->markAuthCodeAsUsed($this->usedAuthCode->getToken());
                $this->usedAuthCode = null;
            }
        }

        return $token;
    }

    public function getUser(): User
    {
        return $this->_user;
    }
}