<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class Oauth2Event extends Event
{
    public const EVENT_BEFORE_CODE = 'beforeOauth2EventCode';
    public const EVENT_AFTER_CODE = 'afterOauth2EventCode';

    public const EVENT_BEFORE_TOKEN = 'beforeOauth2EventToken';
    public const EVENT_AFTER_TOKEN = 'afterOauth2EventToken';

    public const EVENT_ERROR_VALIDATION = 'Oauth2EventErrorValidation';

    public const EVENT_BEFORE_LOGIN = 'beforeOauth2EventLogin';
    public const EVENT_AFTER_LOGIN = 'afterOauth2EventLogin';

    public const EVENT_FAILED_LOGIN = 'failedOauth2EventLogin';

    /**
     * @var array
     */
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}