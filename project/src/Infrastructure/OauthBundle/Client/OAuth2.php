<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\OauthBundle\Client;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class OAuth2 extends BaseOAuth
{
    public $version = '2.0';

    /**
     * @var string token request URL endpoint.
     */
    public $tokenUrl;

    /**
     * @var string authorize URL.
     */
    public $authUrl;

    /**
     * @var string auth request scope.
     */
    public $scope;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var array
     */
    private $token;

    /**
     * @var array
     */
    private $userAttributes;

    /**
     * @var array
     */
    private $providerOptions;

    /**
     * @param UrlGeneratorInterface $router
     */
    public function setRouter(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param SessionInterface $session
     */
    public function setSession(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Initializes authenticated user attributes.
     * @return array auth user attributes.
     */
    abstract protected function initUserData();

    /**
     * Composes user authorization URL.
     * @param array $params additional auth GET params.
     * @return string authorization URL.
     */
    public function buildAuthUrl(array $params = [])
    {
        $defaultParams = [
            'client_id' => $this->getClientId(),
            'response_type' => 'code',
            'redirect_uri' => $this->getReturnUrl()
        ];

        if (!empty($this->scope)) {
            $defaultParams['scope'] = $this->scope;
        }

        $authState = $this->generateAuthState();
        $this->setState('authState', $authState);
        $defaultParams['state'] = $authState;

        return $this->composeUrl($this->authUrl, array_merge($defaultParams, $params));
    }

    /**
     * @param null|string $code
     * @param null|string $state
     * @param array $params
     * @return array
     */
    public function fetchAccessToken(?string $code, ?string $state, array $params = [])
    {
        $authState = $this->getState('authState');
        $incomingState = $state;

        if (!isset($incomingState) || empty($authState) || strcmp($incomingState, $authState) !== 0) {
            throw new BadRequestHttpException('Invalid auth state parameter.');
        }

        $this->removeState('authState');

        $defaultParams = [
            'code' => $code,
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->getReturnUrl(),
        ];

        $params = $this->applyClientCredentials(array_merge($defaultParams, $params));

        return $this->createRequest($params);
    }

    /**
     * @return array list of user attributes
     */
    public function getUserAttributes()
    {
        if ($this->userAttributes === null) {
            $this->userAttributes = $this->initUserData();
        }

        return $this->userAttributes;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function createRequest(array $params)
    {

        $client = $this->getHttpClient();
        $response = $client->post($this->tokenUrl, [
            'form_params' => $params,
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new BadRequestHttpException('Request failed with code: ' . $response->getStatusCode() . ', message: ' . $response->getBody(), $response->getStatusCode());
        }

        $this->setToken($this->parseResponse($response));

        return $this->token;
    }

    public function setToken(array $token): void // TODO change modification after test
    {
        $this->token = $token;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function applyClientCredentials(array $params = [])
    {
        return array_merge($params, [
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
        ]);
    }

    /**
     * Generates the auth state value.
     * @return string auth state value.
     */
    protected function generateAuthState()
    {
        $baseString = get_class($this) . '-' . time();

        return hash('sha256', uniqid($baseString, true));
    }

    protected function getReturnUrl()
    {
        return $this->router->generate('api_auth_client_oauth2_auth', [
            'provider' => $this->getProvider()->getName()
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->getClientOptions('client_id');
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->getClientOptions('client_secret');
    }

    /**
     * @param string $name
     * @return string
     */
    protected function getClientOptions(string $name): string
    {
        if (!$this->providerOptions) {
            $this->providerOptions = $this->getProvider()->getOptionsArray();
        }

        if (!isset($this->providerOptions[$name])) {
            throw new BadRequestHttpException('Property ' . $name . ' does not exist in provider \'' . $this->getProvider()->getName() . '\' called in: ' . self::class);
        }

        return $this->providerOptions[$name];
    }

    /**
     * Returns persistent state value.
     * @param string $key state key.
     * @return string state value.
     */
    protected function getState($key)
    {
        return $this->session->get($this->getStateKeyPrefix() . $key);
    }

    /**
     * Sets persistent state.
     * @param string $key state key.
     * @param string $value state value
     */
    protected function setState($key, $value)
    {
        $this->session->set($this->getStateKeyPrefix() . $key, $value);
    }

    /**
     * Removes persistent state value.
     * @param string $key state key.
     * @return bool success.
     */
    protected function removeState($key)
    {
        return $this->session->remove($this->getStateKeyPrefix() . $key);
    }

    /**
     * Returns session key prefix, which is used to store internal states.
     * @return string session key prefix.
     */
    protected function getStateKeyPrefix()
    {
        return get_class($this) . '_';
    }

    /**
     * @param string $subPath
     * @param string $method
     * @param array $data
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function api(string $subPath, string $method = 'GET', array $data = [])
    {
        $client = $this->getHttpClient();

        $params = [
            'access_token' => $this->token['access_token'] ?? null
        ];

        $response = $client->request($method, $this->apiBaseUrl . '/' . $subPath, [
            'query' => array_merge($params, $data)
        ]);

        if ($response->getStatusCode() !== 200) {
            throw new BadRequestHttpException('Request failed with code: ' . $response->getStatusCode() . ', message: ' . $response->getBody(), $response->getStatusCode());
        }

        return $this->parseResponse($response);
    }
}
