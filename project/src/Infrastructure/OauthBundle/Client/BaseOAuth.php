<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\Client;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Domain\User\Entity\ConnectedAccountProvider;

abstract class BaseOAuth
{
    /**
     * @var string protocol version.
     */
    public $version = '1.0';
    /**
     * @var string API base URL.
     */
    public $apiBaseUrl;
    /**
     * @var string authorize URL.
     */
    public $authUrl;
    /**
     * @var string auth request scope.
     */
    public $scope;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var ConnectedAccountProvider
     */
    private $provider;

    /**
     * @return ConnectedAccountProvider
     */
    public function getProvider(): ConnectedAccountProvider
    {
        return $this->provider;
    }

    /**
     * @param ConnectedAccountProvider $provider
     */
    public function setProvider(ConnectedAccountProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Composes URL from base URL and GET params.
     * @param string $url base URL.
     * @param array $params GET params.
     * @return string composed URL.
     */
    protected function composeUrl($url, array $params = [])
    {
        if (!empty($params)) {
            if (strpos($url, '?') === false) {
                $url .= '?';
            } else {
                $url .= '&';
            }
            $url .= http_build_query($params, '', '&', PHP_QUERY_RFC3986);
        }

        return $url;
    }

    /**
     * @return Client
     */
    protected function getHttpClient(): Client
    {
        if (!$this->httpClient) {
            $this->httpClient = new Client([
                'timeout' => 2.0,
                'allow_redirects' => false,
            ]);
        }

        return $this->httpClient;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     */
    protected function parseResponse(ResponseInterface $response)
    {
        return json_decode($response->getBody()->getContents(), true);
    }

}