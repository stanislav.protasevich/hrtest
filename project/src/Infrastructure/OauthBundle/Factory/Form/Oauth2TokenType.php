<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\OauthBundle\Factory\Form;


use Domain\Oauth\Entity\{AccessToken, RefreshToken};
use Infrastructure\CommonBundle\Validator\Constraints\Exist;
use Infrastructure\OauthBundle\Command\Oauth2TokenCommand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class Oauth2TokenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clientId', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('clientSecret', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('grantType', ChoiceType::class, [
                'choices' => AccessToken::GRANT_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('username', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => AccessToken::GRANT_TYPE_PASSWORD
                    ]),
                ],
            ])
            ->add('password', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => AccessToken::GRANT_TYPE_PASSWORD
                    ]),
                ],
            ])
            ->add('refreshToken', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => [AccessToken::GRANT_TYPE_REFRESH_TOKEN]
                    ]),
                    new Exist([
                        'targetClass' => RefreshToken::class,
                        'targetAttribute' => 'token',
                        'groups' => [AccessToken::GRANT_TYPE_REFRESH_TOKEN]
                    ])
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();

                /* @var $data Oauth2TokenCommand */
                $data = $config->getData();

                $groups = ['Default'];
                if ($data->getGrantType()) {
                    $groups[] = $data->getGrantType();
                }

                return $groups;
            },
        ]);
    }
}
