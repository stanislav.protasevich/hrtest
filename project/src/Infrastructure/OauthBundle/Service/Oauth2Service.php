<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\Service;


use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Domain\User\Entity\ConnectedAccountProvider;
use Domain\User\Entity\User;
use Domain\User\Entity\UserConnectedAccount;
use Infrastructure\OauthBundle\Command\Oauth2DeactivateCommand;
use Infrastructure\OauthBundle\Exception\Oauth2ProviderNotLinkedException;
use Infrastructure\UserBundle\Repository\UserConnectedAccountRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Oauth2Service
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $_em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param User $user
     * @param ConnectedAccountProvider $provider
     * @param string $token
     * @return UserConnectedAccount
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function connect(User $user, ConnectedAccountProvider $provider, string $token): UserConnectedAccount
    {
        $em = $this->getEm();

        /* @var $userConnectedAccountRepository UserConnectedAccountRepository */
        $userConnectedAccountRepository = $em->getRepository(UserConnectedAccount::class);

        $association = $userConnectedAccountRepository->findOneByUserAndProvider($user, $provider);

        if ($association) {
            $account = $this->update($user, $provider, [
                'access_token' => $token,
            ]);
        } else {
            $account = $this->create($user, $provider, [
                'access_token' => $token,
            ]);
        }

        return $account;
    }

    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->container->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    /**
     * @param User $user
     * @param ConnectedAccountProvider $connectedAccountProvider
     * @param array $extra_data
     * @return UserConnectedAccount
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(User $user, ConnectedAccountProvider $connectedAccountProvider, array $extra_data)
    {
        $em = $this->getEm();

        /* @var $account UserConnectedAccount */
        $account = $em->getRepository(UserConnectedAccount::class)
            ->findOneByUserAndProvider($user, $connectedAccountProvider);

        if (!$account) {
            throw new Oauth2ProviderNotLinkedException();
        }

        $account->setExtraData($extra_data);
        $account->setUpdatedAt(new \DateTime());

        $em->flush();

        return $account;
    }

    /**
     * @param User $user
     * @param ConnectedAccountProvider $connectedAccountProvider
     * @param array $extra_data
     * @return UserConnectedAccount
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(User $user, ConnectedAccountProvider $connectedAccountProvider, array $extra_data)
    {
        $em = $this->getEm();

        $account = new UserConnectedAccount();
        $account->setUser($user);
        $account->setConnectedAccountProvider($connectedAccountProvider);
        $account->setExtraData($extra_data);
        $account->setCreatedAt(new \DateTime());
        $account->setUpdatedAt(new \DateTime());

        $em->persist($account);
        $em->flush();

        return $account;
    }

    /**
     * @param Oauth2DeactivateCommand $command
     * @return UserConnectedAccount
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deactivate(Oauth2DeactivateCommand $command)
    {
        $em = $this->getEm();

        /* @var $user User */
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        /* @var $provider ConnectedAccountProvider */
        $provider = $em->getRepository(ConnectedAccountProvider::class)->findOneByName($command->getProvider());

        /* @var $userConnectedAccountRepository UserConnectedAccountRepository */
        $userConnectedAccountRepository = $em->getRepository(UserConnectedAccount::class);

        /* @var $account UserConnectedAccount */
        $account = $userConnectedAccountRepository->findOneByUserAndProvider($user, $provider);

        if (!$account) {
            throw new Oauth2ProviderNotLinkedException();
        }

        $em->remove($account);
        $em->flush();

        return $account;
    }
}