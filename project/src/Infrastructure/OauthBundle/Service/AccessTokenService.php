<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\OauthBundle\Service;


use Domain\Oauth\Entity\AccessToken;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class AccessTokenService extends BaseService
{
    /**
     * @param User $user
     * @return AccessToken[]
     */
    public function findAllByUser(User $user)
    {
        return $this->getEm()->getRepository(AccessToken::class)->findAllByUser($user);
    }
}
