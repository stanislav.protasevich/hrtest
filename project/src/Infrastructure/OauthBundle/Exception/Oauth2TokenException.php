<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\OauthBundle\Exception;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Oauth2TokenException extends HttpException
{
    public function __construct(int $statusCode = null, string $message = null, \Exception $previous = null, array $headers = [], ?int $code = 0)
    {
        $translator = app('translator');
        $message = $translator->trans($message);
        parent::__construct($statusCode ?: Response::HTTP_BAD_REQUEST, $message, $previous, $headers, $code);
    }
}