<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class UserSignIn extends Event
{
    public const EVENT_ERROR_VALIDATION = 'userLoginErrorValidation';

    public const EVENT_BEFORE_LOGIN = 'userLoginErrorBeforeLogin';
    public const EVENT_AFTER_LOGIN = 'userLoginErrorAfterLogin';

    public const EVENT_FAILED_LOGIN = 'userLoginErrorFailedLogin';

    protected $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }
}