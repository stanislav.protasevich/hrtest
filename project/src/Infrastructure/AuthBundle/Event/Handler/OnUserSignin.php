<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Event\Handler;

use Infrastructure\AuthBundle\Event\UserSignIn;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OnUserSignin implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserSignIn::EVENT_BEFORE_LOGIN => 'onBeforeLogin',
            UserSignIn::EVENT_AFTER_LOGIN => 'onAfterLogin',
            UserSignIn::EVENT_FAILED_LOGIN => 'onFailedLogin',
            UserSignIn::EVENT_ERROR_VALIDATION => 'onErrorValidation',
        ];
    }

    public function onBeforeLogin(Event $event)
    {

    }

    public function onAfterLogin(UserSignIn $event)
    {

    }

    public function onFailedLogin(UserSignIn $event)
    {

    }

    public function onErrorValidation(UserSignIn $event)
    {

    }
}