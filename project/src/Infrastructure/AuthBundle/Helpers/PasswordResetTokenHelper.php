<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\AuthBundle\Helpers;

class PasswordResetTokenHelper
{
    /**
     * Generate random token
     */
    public static function generateResetToken(): string
    {
        return uniqid('', true) . '_' . time();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @param int $expire expire date
     * @return boolean
     */
    public static function isPasswordResetTokenValid(?string $token = '', ?int $expire = null): bool
    {
        if (empty($token)) {
            return false;
        }

        if (!$expire) {
            $expire = getParameter('auth.token.ttl') ?? 3600 * 24 * 30;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        
        return $timestamp + $expire >= time();
    }
}