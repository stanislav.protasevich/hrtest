<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Security;

use Domain\Oauth\Entity\AccessToken;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Web\ApiResponse;
use Infrastructure\OauthBundle\Lib\OAuth2;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use OAuth2\OAuth2AuthenticateException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Translation\TranslatorInterface;

class JwtTokenAuthenticator extends AbstractGuardAuthenticator implements UserIdentityInterface
{
    protected $message = 'Unauthenticated.';

    /**
     * @var JWTEncoderInterface
     */
    private $jWTEncoder;

    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var OAuth2
     */
    private $OAuth2;

    public function __construct(JWTEncoderInterface $jWTEncoder, TranslatorInterface $translator, ContainerInterface $container, OAuth2 $OAuth2)
    {
        $this->jWTEncoder = $jWTEncoder;
        $this->container = $container;
        $this->translator = $translator;
        $this->OAuth2 = $OAuth2;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return view($this->getDennyMessage(), Response::HTTP_UNAUTHORIZED);
    }

    protected function getDennyMessage()
    {
        return $this->translator->trans($this->message);
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request)
    {
        if (!$request->headers->get('Authorization')) {
            return false;
        }

        return true;
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * @param Request $request
     *
     * @return mixed Any non-null value
     *
     * @throws \UnexpectedValueException If null is returned
     */
    public function getCredentials(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );

        return $extractor->extract($request);
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return null|object|UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            /* @var $accessToken AccessToken */
            $accessToken = $this->OAuth2->verifyAccessToken($credentials);
        } catch (OAuth2AuthenticateException $exception) {
            $header = $exception->getResponseHeaders();
            $this->denyAccess($header['WWW-Authenticate'], $exception->getDescription());
        }

        return $accessToken->getUser();
    }

    /**
     * @param string $challenge
     * @param string $description
     */
    protected function denyAccess(string $challenge, string $description)
    {
        throw new UnauthorizedHttpException($challenge, $description);
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return ApiResponse|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return view($exception->getMessage(), Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return ApiResponse|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {

    }

    /**
     * Does this method support remember me cookies?
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    /**
     * Shortcut to create a PostAuthenticationGuardToken for you, if you don't really
     * care about which authenticated token you're using.
     *
     * @param UserInterface|User $user
     * @param string $providerKey
     *
     * @return PostAuthenticationGuardToken
     */
    public function createAuthenticatedToken(UserInterface $user, $providerKey)
    {
        return new PostAuthenticationGuardToken(
            $user,
            $providerKey,
            [] // TODO when roles done change
        );
    }
}
