<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Command;


use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\Exist;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserPasswordResetCommand extends BaseTokenPasswordCommand
{
    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('token', new Exist([
            'targetClass' => User::class,
            'targetAttribute' => 'passwordResetToken',
            'filter' => ['status|=|' . User::STATUS_ACTIVE],
        ]));
    }
}