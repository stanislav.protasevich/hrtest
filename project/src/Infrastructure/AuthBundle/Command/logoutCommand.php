<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Command;


use Domain\User\Entity\User;
use Symfony\Component\Validator\Constraints;

class logoutCommand
{
    /**
     * @var User
     */
    public $user;

    /**
     * @Constraints\NotBlank()
     */
    public $access_token;

    public function __construct(User $user, string $access_token)
    {
        $this->user = $user;
        $this->access_token = $access_token;
    }
}