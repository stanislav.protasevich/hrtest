<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Command;

use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Infrastructure\CommonBundle\Validator\Constraints\Exist;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class UserPasswordResetRequestCommand
{
    use ObjectebleTrait;

    /**
     * @Constraints\NotBlank()
     * @Constraints\Email()
     */
    public $email;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('email', new Exist([
            'targetClass' => 'Domain\User\Entity\User',
            'targetAttribute' => 'email',
            'filter' => ['status|=|' . User::STATUS_ACTIVE],
        ]));
    }
}