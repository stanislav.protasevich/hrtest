<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Command;

use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints;

abstract class BaseTokenPasswordCommand
{
    use ObjectebleTrait;

    /**
     * @Constraints\NotBlank()
     * @Infrastructure\AuthBundle\Validator\Constraints\AccessTokenValid()
     */
    public $token;

    /**
     * @Constraints\NotBlank()
     * @Constraints\Length(min="6")
     */
    public $password;
}