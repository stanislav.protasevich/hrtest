<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Domain\User\Entity\User;
use Infrastructure\AuthBundle\Command\LoginCommand;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\{Constraint, ConstraintValidator, Context\ExecutionContextInterface};

/**
 * @property EntityManagerInterface $em
 * @property EncoderFactoryInterface $encoder
 * @property ExecutionContextInterface $context
 * @property TranslatorInterface $translator
 */
class PasswordValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var EncoderFactoryInterface
     */
    private $encoder;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityManagerInterface $em, EncoderFactoryInterface $encoder, TranslatorInterface $translator)
    {
        $this->em = $em;
        $this->encoder = $encoder;
        $this->translator = $translator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $password The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @return void
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate($password, Constraint $constraint)
    {
        $errors = $this->context->getViolations()->count();

        if (empty($errors)) {

            /* @var $root LoginCommand */
            $root = $this->context->getRoot();
            $username = $constraint->username;

            /* @var $user User */
            $user = $this->em->getRepository(User::class)->loadByEmail($root->{$username});
            $encoder = $this->encoder->getEncoder($user);
            $valid = $encoder->isPasswordValid($user->getPasswordHash(), $password, $user->getSalt());

            if ($valid === false) {
                $this->context->buildViolation($this->translator->trans($constraint->message))->addViolation();
            }
        }
    }
}