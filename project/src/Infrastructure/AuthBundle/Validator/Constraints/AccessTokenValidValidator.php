<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Validator\Constraints;

use Infrastructure\AuthBundle\Helpers\PasswordResetTokenHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class AccessTokenValidValidator extends ConstraintValidator
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        $errors = $this->context->getViolations()->count();

        if (empty($errors) && PasswordResetTokenHelper::isPasswordResetTokenValid((string)$value) === false) {
            $translator = $this->container->get('translator');
            $this->context->buildViolation($translator->trans($constraint->message))->addViolation();
        }
    }
}