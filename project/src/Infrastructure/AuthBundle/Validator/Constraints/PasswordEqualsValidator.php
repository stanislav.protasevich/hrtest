<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\AuthBundle\Validator\Constraints;

use Infrastructure\AuthBundle\Command\UserPasswordResetCommand;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\{
    Constraint, ConstraintValidator
};

class PasswordEqualsValidator extends ConstraintValidator
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param string $password The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($password, Constraint $constraint)
    {
        $errors = $this->context->getViolations()->count();

        if (empty($errors)) {
            /* @var $root UserPasswordResetCommand */
            $root = $this->context->getRoot();

            $repassword = $root->repassword;

            if (strcmp($password, $repassword) !== 0) {
                $this->context->buildViolation($this->translator->trans($constraint->message))->addViolation();
            }
        }
    }
}