<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Oauth\Entity\AccessToken;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccessToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessToken[]    findAll()
 * @method AccessToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    /**
     * @param User $user
     * @return AccessToken[]
     */
    public function findAllByUser(User $user)
    {
        return $this->findBy([
            'user' => $user,
        ]);
    }
}
