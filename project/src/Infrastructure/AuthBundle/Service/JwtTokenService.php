<?php

declare(strict_types=1);

namespace Infrastructure\AuthBundle\Service;


use Lexik\Bundle\JWTAuthenticationBundle\Encoder\DefaultEncoder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class JwtTokenService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $jti
     * @param int $expires
     * @return string
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function create($jti, ?int $expires)
    {
        $currentTime = time();

        /* @var $requestStack RequestStack */
        $requestStack = $this->container->get('request_stack');
        $hostInfo = $requestStack->getCurrentRequest()->getHost();

        /* @var $encoder DefaultEncoder */
        $encoder = $this->container->get('lexik_jwt_authentication.encoder');

        $expire = $this->container->getParameter('auth.token.ttl');

        $token = $encoder->encode([
            'jti' => $jti,
            'iat' => $currentTime, // Issued at: timestamp of token issuing.
            'iss' => $hostInfo, // Issuer: A string containing the name or identifier of the issuer application. Can be a domain name and can be used to discard tokens from other applications.
            'aud' => $hostInfo,
            'nbf' => $currentTime, // Not Before: Timestamp of when the token should start being considered valid. Should be equal to or greater than iat. In this case, the token will begin to be valid 10 seconds
            'exp' => $expires ?: $currentTime + $expire,// Expire: Timestamp of when the token should cease to be valid. Should be greater than iat and nbf. In this case, the token will expire 60 seconds after being issued.
        ]);

        return $token;
    }
}