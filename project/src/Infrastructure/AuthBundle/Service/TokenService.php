<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\AuthBundle\Service;

use Domain\Oauth\Entity\{AccessToken, Token};
use Domain\User\Entity\User;
use FOS\OAuthServerBundle\Entity\AccessTokenManager;
use FOS\OAuthServerBundle\Model\TokenManagerInterface;
use Infrastructure\CommonBundle\Service\BaseService;
use OAuth2\IOAuth2Storage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TokenService extends BaseService
{
    /**
     * @var TokenManagerInterface
     */
    private $accessTokenManager;
    /**
     * @var TokenManagerInterface
     */
    private $refreshTokenManager;
    /**
     * @var IOAuth2Storage
     */
    private $auth2Storage;

    public function __construct(
        ContainerInterface $container,
        TokenManagerInterface $accessTokenManager,
        TokenManagerInterface $refreshTokenManager,
        IOAuth2Storage $auth2Storage
    ) {
        parent::__construct($container);
        $this->accessTokenManager = $accessTokenManager;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->auth2Storage = $auth2Storage;
    }

    public function removeToken(string $token)
    {
        /* @var $token AccessToken */
        $token = $this->auth2Storage->getAccessToken($token);
        $this->dropToken($token);
    }

    protected function dropToken(Token $token)
    {
        foreach ([$this->accessTokenManager, $this->refreshTokenManager] as $service) {
            /* @var $service AccessTokenManager */
            $service->deleteToken($token);
        }
    }

    public function removeAll(User $user)
    {
        $tokens = $this->getEm()->getRepository(AccessToken::class)->findAllByUser($user);
        foreach ($tokens as $token) {
            $this->dropToken($token);
        }
    }
}
