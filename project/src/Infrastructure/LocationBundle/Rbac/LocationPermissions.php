<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LocationBundle\Rbac;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class LocationPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_COUNTRY_LIST = 'locationCountryList';
    public const PERMISSION_REGION_LIST = 'locationRegionList';
    public const PERMISSION_CITY_LIST = 'locationCityList';

    public const PERMISSION_LOCATION = [
        self::PERMISSION_COUNTRY_LIST,
        self::PERMISSION_REGION_LIST,
        self::PERMISSION_CITY_LIST,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_COUNTRY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_REGION_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_CITY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
        ];
    }
}