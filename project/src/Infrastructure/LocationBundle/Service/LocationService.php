<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LocationBundle\Service;


use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\LocationBundle\Command\CountryListCommand;
use Infrastructure\LocationBundle\Command\RegionListCommand;

class LocationService extends BaseService
{
    /**
     * @param int $hydrationMode
     * @return City[]
     */
    public function getCityAll($hydrationMode = Query::HYDRATE_OBJECT): array
    {
        $em = $this->getEm();

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(City::class)->findAllQueryBuilder();
        $qb->orderBy('city.sortOrder', 'ASC');

        return $qb->getQuery()->getResult($hydrationMode);
    }

    /**
     * @return City[]
     */
    public function getCityPrimaryAll(): array
    {
        return $this->getEm()->getRepository(City::class)->findPrimaryAll();
    }

    /**
     * @return Country[]
     */
    public function getCountryPrimaryAll(): array
    {
        return $this->getEm()->getRepository(Country::class)->findPrimaryAll();
    }

    /**
     * @param RegionListCommand $command
     * @return Region[]
     */
    public function getRegionAll(RegionListCommand $command): array
    {
        $em = $this->getEm();

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(Region::class)->findAllQueryBuilder();

        if ($command->id && !empty($command->id)) {
            $qb->andWhere('region.id IN (:id)')->setParameter('id', (array)$command->id);
        }

        $qb->orderBy('region.sortOrder', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param CountryListCommand $command
     * @return Region[]
     */
    public function getCountryAll(CountryListCommand $command): array
    {
        $em = $this->getEm();

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(Country::class)->findAllQueryBuilder();

        if ($command->id && !empty($command->id)) {
            $qb->andWhere('country.id IN (:id)')->setParameter('id', (array)$command->id);
        }

        $qb->orderBy('country.sortOrder', 'ASC');

        return $qb->getQuery()->getResult();
    }
}