<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LocationBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Location\Entity\Country;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Country|null find($id, $lockMode = null, $lockVersion = null)
 * @method Country|null findOneBy(array $criteria, array $orderBy = null)
 * @method Country[]    findAll()
 * @method Country[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function findAllQueryBuilder($alias = 'country')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('country')
            ->select(['country.id as value', 'country.name as label'])
            ->orderBy('country.sortOrder', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}