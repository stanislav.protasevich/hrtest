<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LocationBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Location\Entity\City;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, City::class);
    }

    public function findAllQueryBuilder($alias = 'city')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('city')
            ->select(['city.id as value', 'city.name as label'])
            ->orderBy('city.sortOrder', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}