<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\LocationBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class CountryListCommand
{
    use ObjectebleTrait;

    public $id;
}