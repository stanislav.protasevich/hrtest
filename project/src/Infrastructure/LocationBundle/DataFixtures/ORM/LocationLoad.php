<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\LocationBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\Location\Entity\City;
use Domain\Location\Entity\Country;
use Domain\Location\Entity\Region;
use Faker\Factory;
use Faker\Provider\en_NG\Address;

class LocationLoad extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();
        $faker->addProvider(new Address($faker));

        for ($i = 0; $i < 3; $i++) {

            $country = new Country();
            $country->setName($faker->country);
            $country->setSortOrder($i);

            $manager->persist($country);

            for ($j = 0; $j < 5; $j++) {
                $region = new Region();
                $region->setName($faker->region);
                $region->setCountry($country);
                $region->setSortOrder($j);

                $manager->persist($region);

                for ($h = 0; $h < 10; $h++) {
                    $city = new City();
                    $city->setName($faker->city);
                    $city->setSortOrder($h);
                    $city->setCountry($country);
                    $city->setRegion($region);

                    $manager->persist($city);
                }

            }

        }


        $manager->flush();
    }
}