<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Command;


use Domain\User\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArchiveFileAttachCommand
{
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    private $title;
    /**
     * @var User
     */
    private $user;

    /**
     * ArchiveFileAttachCommand constructor.
     * @param null|UploadedFile $file
     * @param User $user
     */
    public function __construct(?UploadedFile $file = null, User $user)
    {
        $this->file = $file;
        $this->user = $user;
    }

    /**
     * @return UploadedFile
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }
}