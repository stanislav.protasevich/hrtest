<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Command;

use Domain\User\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class ArchiveFileUserAttachCommand extends ArchiveFileAttachCommand
{
    /**
     * @var User
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * ArchiveFileUserAttachCommand constructor.
     * @param null|UploadedFile $file
     * @param null|string $user
     */
    public function __construct(?UploadedFile $file, ?string $user)
    {
        parent::__construct($file);
        $this->user = $this->getUserModel($user);
    }

    protected function getUserModel(?string $user)
    {
        return $user ? em()->getRepository(User::class)->findOneActiveById($user) : null;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}