<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Service;

use Domain\Archive\Entity\File;
use Infrastructure\ArchiveBundle\Command\ArchiveFileAttachCommand;
use Infrastructure\ArchiveBundle\Command\ArchiveFileUserAttachCommand;
use Infrastructure\ArchiveBundle\Command\ArchiveFileViewCommand;
use Infrastructure\ArchiveBundle\Helpers\ArchiveHelper;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\CommonBundle\Service\StorageService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Stream;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArchiveFileService extends BaseService
{
    /**
     * @var StorageService
     */
    private $storageService;

    public function __construct(ContainerInterface $container, StorageService $storageService)
    {
        parent::__construct($container);
        $this->storageService = $storageService;
    }

    public function attach(ArchiveFileAttachCommand $command): File
    {
        $file = $command->getFile();

        $model = new File();
        $model->setUser($command->getUser());
        $model->setTitle($command->getTitle());

        return $this->save($model, $file);
    }

    protected function save(File $model, UploadedFile $file): File
    {
        $name = $this->getName();
        $dir = $this->getDir($name);

        $path = $this->getBaseDirPath() . DIRECTORY_SEPARATOR . $dir;

        FileHelper::createDirectory($path);

        $filePath = $path . DIRECTORY_SEPARATOR . $name;

        $file->move($path, $name);

        $now = new \DateTime();
        $em = $this->getEm();

        $mimeType = mime_content_type($filePath);

        if (!$model->getTitle()) {
            $model->setTitle($file->getClientOriginalName());
        }

        $model->setName($name);
        $model->setPath($dir);
        $model->setExtension($file->getClientOriginalExtension());
        $model->setMimeType($mimeType);
        $model->setSize($file->getSize());
        $model->setType(ArchiveHelper::type($mimeType));

        $model->setCreatedBy($this->getUser());
        $model->setUpdatedBy($this->getUser());
        $model->setCreatedAt($now);
        $model->setUpdatedAt($now);

        $em->persist($model);
        $em->flush();

        return $model;
    }

    public function getName(): string
    {
        return md5(StringHelper::microtimeLong());
    }

    protected function getDir(string $name): string
    {
        $dirs = [
            substr($name, 0, 2),
            substr($name, 2, 2),
        ];

        return implode(DIRECTORY_SEPARATOR, $dirs);
    }

    public function getBaseDirPath()
    {
        return $this->storageService->getPath() . DIRECTORY_SEPARATOR . 'archive';
    }

    public function getFile(ArchiveFileViewCommand $command): ?File
    {
        return $em = $this->getEm()->getRepository(File::class)->find($command->getId());
    }

    public function userAttach(ArchiveFileUserAttachCommand $command)
    {
        $file = $command->getFile();
        $model = new File();
        $model->setUser($command->getUser());

        return $this->save($model, $file);
    }

    public function remove(File $file): void
    {
        $path = $this->getBaseDirPath() . DIRECTORY_SEPARATOR . $file->getPath();

        $filePath = $path . DIRECTORY_SEPARATOR . $file->getName();

        $em = $this->getEm();

        $this->dropFile($filePath);

        $em->remove($file);
        $em->flush();
    }

    protected function dropFile(string $path)
    {
        if (file_exists($path)) {
            @unlink($path);
        }
    }

    /**
     * @param File $file
     * @param bool $onlyDownload
     * @return BinaryFileResponse
     */
    public function viewOrDownloadFile(File $file, bool $onlyDownload = false): BinaryFileResponse
    {
        $link = $this->getLink($file);

        if (!file_exists($link)) {
            throw new NotFoundHttpException("File {$file->getTitle()} don`t exist.");
        }

        $stream = new Stream($link);
        $content = new BinaryFileResponse($stream);

        if ($onlyDownload || !in_array($file->getMimeType(), File::MIME_TYPE_WEB)) {
            $content->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getTitle());
        }

        return $content;
    }

    public function getLink(File $file)
    {
        return $this->getBaseDirPath() . DIRECTORY_SEPARATOR . $file->getPath() . DIRECTORY_SEPARATOR . $file->getName();
    }

}