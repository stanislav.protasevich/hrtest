<?php
declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\ArchiveBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Archive\Entity\File;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, File::class);
    }

    public function findAllQueryBuilder($alias = 'file')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param User $user
     * @return User|null
     */
    public function findAllByUser(User $user)
    {
        return $this->findAllByUserQuery($user)->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllByUserQuery(User $user)
    {
        return $this->createQueryBuilder('file')
            ->where('file.user = :user')
            ->setParameter('user', $user);
    }
}
