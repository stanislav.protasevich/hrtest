<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureArchiveBundle extends Bundle
{

}