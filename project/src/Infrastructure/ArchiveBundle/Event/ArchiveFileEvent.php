<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Event;

use Domain\Archive\Entity\File;
use Symfony\Component\EventDispatcher\Event;

class ArchiveFileEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'archiveFileAttachEventBeforeInsert';
    public const EVENT_AFTER_INSERT = 'ArchiveFileAttachEventAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'ArchiveFileAttachEventBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'ArchiveFileAttachEventAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'ArchiveFileAttachEventBeforeDelete';
    public const EVENT_AFTER_DELETE = 'ArchiveFileAttachEventAfterDelete';

    /**
     * @var File
     */
    private $file;

    public function __construct(File $file)
    {

        $this->file = $file;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file;
    }
}