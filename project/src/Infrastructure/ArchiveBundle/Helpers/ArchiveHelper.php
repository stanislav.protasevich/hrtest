<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Helpers;


use Domain\Archive\Entity\File;

class ArchiveHelper
{
    public static function type(string $mimeType): string
    {
        $type = null;

        switch ($mimeType) {
            case in_array($mimeType, File::MIME_TYPE_IMAGES):
                $type = 'image';
                break;

            case in_array($mimeType, File::MIME_TYPE_DOCS):
                $type = 'document';
                break;

            case in_array($mimeType, File::MIME_TYPE_VIDEOS):
                $type = 'video';
                break;

            case in_array($mimeType, File::MIME_TYPE_ARCHIVES):
                $type = 'archive';
                break;
        }

        if (!$type) {
            throw new \LogicException(
                sprintf('Unknown file type: %s. Allowed: %s', $mimeType, implode(',', File::MIME_TYPES))
            );
        }

        return $type;
    }
}