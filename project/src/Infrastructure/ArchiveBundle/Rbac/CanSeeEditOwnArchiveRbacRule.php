<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Rbac;


use Domain\Archive\Entity\File;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;

class CanSeeEditOwnArchiveRbacRule implements RbacRuleInterface
{
    public function execute(User $user, $item, $params)
    {
        /* @var $model File */
        $model = $params['model'];

        if ($model->getUser() === $user) {
            return true;
        }

        if (Gate::can(RbacPermissions::ROLE_ADMINISTRATOR)) {
            return true;
        }

        if (Gate::can(RbacPermissions::ROLE_HR)) {
            return true;
        }

        return false;
    }
}