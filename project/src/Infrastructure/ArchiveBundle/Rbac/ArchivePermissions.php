<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class ArchivePermissions implements RbacPermissionsInterface
{
    public const PERMISSION_ARCHIVE_ATTACH = 'archiveAttach';
    public const PERMISSION_ARCHIVE_USER_ATTACH = 'archiveUserAttach';
    public const PERMISSION_ARCHIVE_FILE_VIEW = 'archiveFileView';
    public const PERMISSION_ARCHIVE_FILE_DOWNLOAD = 'archiveFileDownload';

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_ARCHIVE_ATTACH,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_ARCHIVE_USER_ATTACH,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_ARCHIVE_FILE_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'CanSeeEditOwnArchiveRbacRule'
            ),
            new RbacItemCommand(
                self::PERMISSION_ARCHIVE_FILE_DOWNLOAD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ],
                'CanSeeEditOwnArchiveRbacRule'
            ),
        ];
    }
}