<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\ArchiveBundle\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArchiveFileDetailNotFoundException extends NotFoundHttpException
{
    public function __construct(string $message = null, \Exception $previous = null, int $code = 0, array $headers = [])
    {
        $message = $message ?: app('translator')->trans('File not found.');

        parent::__construct($message, $previous, $code, $headers);
    }
}