<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Contracts;


use Domain\User\Entity\User;

interface RbacRuleInterface
{
    public function execute(User $user, $item, $params);
}