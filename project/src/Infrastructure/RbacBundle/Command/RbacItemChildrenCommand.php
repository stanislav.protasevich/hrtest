<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Command;


use Domain\Rbac\Entity\AuthItem;

class RbacItemChildrenCommand
{
    /**
     * @var AuthItem
     */
    private $name;
    /**
     * @var array
     */
    private $children;

    /**
     * @return AuthItem
     */
    public function getName(): ?AuthItem
    {
        return $this->name;
    }

    /**
     * @param AuthItem $name
     */
    public function setName(AuthItem $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }
}