<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Command;


use Domain\Rbac\Entity\AuthItem;

class RbacPermissionUsersListCommand
{
    /**
     * @var AuthItem
     */
    private $item;

    public function __construct(AuthItem $item)
    {
        $this->item = $item;
    }

    /**
     * @return AuthItem
     */
    public function getItem(): AuthItem
    {
        return $this->item;
    }
}