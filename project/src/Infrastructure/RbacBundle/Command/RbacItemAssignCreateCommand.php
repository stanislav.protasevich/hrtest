<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Command;

use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;

class RbacItemAssignCreateCommand
{
    /**
     * @var AuthItem
     */
    private $item;

    /**
     * @var User[]
     */
    private $assignment;

    /**
     * @return mixed
     */
    public function getAssignment()
    {
        return $this->assignment;
    }

    /**
     * @param mixed $assignment
     */
    public function setAssignment($assignment): void
    {
        $this->assignment = $assignment;
    }

    /**
     * @return AuthItem
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param AuthItem $item
     */
    public function setItem(AuthItem $item): void
    {
        $this->item = $item;
    }
}