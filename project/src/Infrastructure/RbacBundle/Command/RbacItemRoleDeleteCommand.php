<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Command;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Validator\Constraints\ProtectedItemConstraint;

class RbacItemRoleDeleteCommand
{
    /**
     * @var AuthItem
     * @ProtectedItemConstraint()
     */
    private $item;

    /**
     * @return AuthItem
     */
    public function getItem(): ?AuthItem
    {
        return $this->item;
    }

    /**
     * @param AuthItem $item
     */
    public function setItem(AuthItem $item): void
    {
        $this->item = $item;
    }
}