<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Command;


use Infrastructure\CommonBundle\Factory\PropertyAccessTrait;

class RbacItemCommand
{
    use PropertyAccessTrait;

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $ruleName;

    private $description;
    /**
     * @var array|null
     */
    private $parent;
    /**
     * @var array|null
     */
    private $children;
    /**
     * @var string
     */
    private $title;

    public function __construct(
        string $name,
        string $type,
        ?string $description = null,
        ?array $parent = [],
        ?string $ruleName = null,
        ?array $children = []
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->description = $description;
        $this->parent = $parent;
        $this->ruleName = $ruleName;
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getRuleName(): ?string
    {
        return $this->ruleName;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array|null
     */
    public function getParent(): ?array
    {
        return $this->parent;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return array|null
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }
}