<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Command;


class RbacAssignInitCommand
{
    /**
     * @var RbacItemCommand[]
     */
    private $items;
    /**
     * @var bool
     */
    private $clear;
    /**
     * @var array
     */
    private $rules;

    public function __construct(array $items, bool $clear = false, ?array $rules = [])
    {
        $this->items = $items;
        $this->clear = $clear;
        $this->rules = $rules;
    }

    /**
     * @return RbacItemCommand[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function isClear(): bool
    {
        return $this->clear;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }
}