<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Command;


use Domain\Rbac\Entity\AuthItem;

class RbacItemRoleCreateCommand
{
    /**
     * @var AuthItem
     */
    private $authItem;

    public function __construct(AuthItem $authItem)
    {
        $this->authItem = $authItem;
    }

    /**
     * @return AuthItem
     */
    public function getAuthItem(): AuthItem
    {
        return $this->authItem;
    }
}