<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Rbac\Entity\AuthRule;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuthRule|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthRule|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthRule[]    findAll()
 * @method AuthRule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthRuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuthRule::class);
    }

    public function findAllQueryBuilder($alias = 'rule')
    {
        return $this->createQueryBuilder($alias);
    }

    public function getAll(): array
    {
        return $this->fetchFromDb('select * from `auth_rule`');
    }

    /**
     * @param string $sql
     * @return array[]
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function fetchFromDb(string $sql)
    {
        return $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql)
            ->fetchAll();
    }
}
