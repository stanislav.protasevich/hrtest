<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuthItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthItem[]    findAll()
 * @method AuthItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuthItem::class);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getItemWithAssignmentsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('item');

        $qb->join('item.assignment', 'assignment')
            ->where('assignment = :user')
            ->setParameter(':user', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAssignmentsByUser(User $user)
    {
        $sql = 'select item_name from `auth_assignment` where user_id = :user';
        $params = [
            'user' => $user->getId(),
        ];
        $result = $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql, $params)
            ->fetchAll();

        return array_column($result, 'item_name');
    }

    public function getPermissionsByPermissionsArray(array $permissions)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(['item.name', 'item.type', 'item.title'])
            ->where('item.type = :type AND item.name IN (:names)')
            ->setParameter(':type', AuthItem::TYPE_PERMISSION)
            ->setParameter(':names', $permissions);

        return $qb->getQuery()->getArrayResult();
    }

    public function getUserPermissions(User $user)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(['item.name', 'item.type', 'item.title'])
            ->join('item.assignment', 'assignment')
            ->where('assignment = :user AND item.type = :type')
            ->setParameter(':user', $user)
            ->setParameter(':type', AuthItem::TYPE_PERMISSION);

        return $qb->getQuery()->getArrayResult();
    }

    public function getUserRoles(User $user)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(['item.name', 'item.type', 'item.title'])
            ->join('item.assignment', 'assignment')
            ->where('assignment = :user AND item.type = :type')
            ->setParameter(':user', $user)
            ->setParameter(':type', AuthItem::TYPE_ROLE);

        return $qb->getQuery()->getArrayResult();
    }

    public function getItems($hydrationMode = Query::HYDRATE_OBJECT)
    {
        return $this->createQueryBuilder('item')->getQuery()->getResult($hydrationMode);
    }

    public function findChildrens(): array
    {
        return $this->fetchFromDb('select * from `auth_item_child`');
    }

    /**
     * @param string $sql
     * @return array[]
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function fetchFromDb(string $sql)
    {
        return $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql)
            ->fetchAll();
    }

    public function findAllItems(): array
    {
        return $this->fetchFromDb('select * from `auth_item`');
    }

    public function getExistNames(): array
    {
        return $this->findAllQueryBuilder()
            ->select(['item.name'])
            ->getQuery()
            ->getResult(Query::HYDRATE_SCALAR);
    }

    public function findAllQueryBuilder($alias = 'item')
    {
        return $this->createQueryBuilder($alias);
    }

    public function getItemChildren(string $name)
    {
        $sql = 'select name, type, title from `auth_item`, `auth_item_child` where parent_name = :parent AND name = `child_name`';

        $params = [
            'parent' => $name,
        ];

        $result = $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql, $params)
            ->fetchAll();

        return $result;
    }

    public function getPermissionsChildren(array $names)
    {
        $qb = $this->createQueryBuilder('item')
            ->select(['item.name', 'item.type', 'item.title'])
            ->where('item.type = :type AND item.name IN (:name)')
            ->setParameter('type', AuthItem::TYPE_PERMISSION)
            ->setParameter('name', $names);

        return $qb->getQuery()->getArrayResult();
    }

    public function queryRole()
    {
        return $this->findAllQueryBuilder('item')
            ->where('item.type = :type')
            ->setParameter('type', AuthItem::TYPE_ROLE);
    }
}
