<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Rbac\Entity\AuthAssignment;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuthAssignment|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthAssignment|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthAssignment[]    findAll()
 * @method AuthAssignment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthAssignmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuthAssignment::class);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getAssignments(User $user): array
    {
        return $this->createQueryBuilder('assignment')
            ->select('assignment')
            ->where('assignment.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getArrayResult();
    }
}