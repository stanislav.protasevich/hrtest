<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class DuplicateParentChildrenConstraint extends Constraint
{
    public $message = 'Can not add this "{{ name }}" in to list.';

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }
}