<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Validator\Constraints;


use Domain\Rbac\Entity\AuthItem;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DuplicateParentChildrenConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value AuthItem[]
     * @param Constraint|DuplicateParentChildrenConstraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof DuplicateParentChildrenConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DuplicateParentChildrenConstraint');
        }

        if (empty($value)) {
            return;
        }

        /* @var $form Form */
        $form = $this->context->getRoot();

        /* @var $name AuthItem */
        $name = $form->get('name')->getData();

        foreach ($value as $item) {
            if ($item->getName() === $name->getName()) {
                /* @var $translator Translator */
                $translator = $this->container->get('translator');

                $this->context
                    ->buildViolation($translator->trans($constraint->message))
                    ->setParameter('{{ name }}', $name->getName())
                    ->addViolation();
            }
        }
    }
}