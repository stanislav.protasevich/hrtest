<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Validator\Constraints;

use Domain\Rbac\Entity\AuthItem;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProtectedItemConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value AuthItem[]
     * @param Constraint|ProtectedItemConstraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof ProtectedItemConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\ProtectedItemConstraint');
        }

        if (!$value instanceof AuthItem) {
            return;
        }

        $protected = $this->container->getParameter('rbac.protected.items');

        if (in_array($value->getName(), $protected)) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{{ name }}', $value->getName())
                ->addViolation();
        }
    }
}