<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Validator\Constraints;

use Doctrine\ORM\PersistentCollection;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Service\RbacService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AuthItemAssignedRoleConstraintValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var RbacService
     */
    private $rbacService;

    public function __construct(ContainerInterface $container, RbacService $rbacService)
    {
        $this->container = $container;
        $this->rbacService = $rbacService;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value User[]|User
     * @param Constraint|ProtectedItemConstraint $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof AuthItemAssignedRoleConstraint) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\AuthItemAssignedRoleConstraint');
        }

        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        $violation = $this->context
            ->buildViolation($translator->trans($constraint->message))
            ->setParameter('{{ name }}', $constraint->role);

        if (is_array($value) || $value instanceof PersistentCollection) {
            foreach ($value as $key => $item) {

                if ($item instanceof User) {
                    $roles = $this->rbacService->getRolesByUser($item);

                    if (array_key_exists($constraint->role, $roles) === false) {
                        $violation->atPath(sprintf('[%d]', $key))->addViolation();
                    }
                } else {
                    $violation->atPath(sprintf('[%d]', $key))->addViolation();
                }
            }
        } else {
            $roles = $this->rbacService->getRolesByUser($value);

            if (array_key_exists($constraint->role, $roles) === false) {
                $violation->addViolation();
            }
        }
    }
}
