<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Валидирует массив пользователей, на принадлежность к роле, должна быть установлена на прямую
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class AuthItemAssignedRoleConstraint extends Constraint
{
    public $message = 'User must have assign "{{ name }}" role.';

    public $role;

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::DEFAULT_GROUP,
            self::PROPERTY_CONSTRAINT,
            self::PROPERTY_CONSTRAINT,
        ];
    }
}
