<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Event;


use Domain\Rbac\Entity\AuthItem;
use Symfony\Component\EventDispatcher\Event;

class AuthItemInitEvent extends Event
{
    public const EVENT_BEFORE_INIT = 'RbacAssignEventBeforeInit';
    public const EVENT_AFTER_INIT = 'RbacAssignEventAfterInit';

    public const EVENT_BEFORE_INSERT = 'RbacAssignEventBeforeInsert';
    public const EVENT_AFTER_INSERT = 'RbacAssignEventAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'RbacAssignEventBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'RbacAssignEventAfterUpdate';

    /**
     * @var AuthItem[]
     */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return AuthItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
}