<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Event\Handler;


use Infrastructure\RbacBundle\Event\AuthItemChangeEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AuthItemChangeClearCacheListener implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthItemChangeEvent::EVENT_BEFORE_AUTH_ITEM_CHANGE => 'onBeforeChange',
            AuthItemChangeEvent::EVENT_AFTER_AUTH_ITEM_CHANGE => 'onAfterChange',
        ];
    }

    public function onAfterChange(Event $event)
    {

    }

    public function onBeforeChange(Event $event)
    {

    }
}