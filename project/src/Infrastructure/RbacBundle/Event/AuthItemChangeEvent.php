<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Event;


use Domain\Rbac\Entity\AuthItem;
use Symfony\Component\EventDispatcher\Event;

class AuthItemChangeEvent extends Event
{
    public const EVENT_BEFORE_AUTH_ITEM_CHANGE = 'AuthItemBeforeChangeEvent';
    public const EVENT_AFTER_AUTH_ITEM_CHANGE = 'AuthItemAfterChangeEvent';

    /**
     * @var AuthItem
     */
    private $item;

    public function __construct(AuthItem $item)
    {
        $this->item = $item;
    }

    /**
     * @return AuthItem
     */
    public function getItem(): AuthItem
    {
        return $this->item;
    }
}