<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Event;


use Domain\Rbac\Entity\AuthItem;
use Symfony\Component\EventDispatcher\Event;

class AuthItemAssignEvent extends Event
{
    public const EVENT_BEFORE_ASSIGN_CREATE = 'RbacAssignEventBeforeAssignCreate';
    public const EVENT_AFTER_ASSIGN_CREATE = 'RbacAssignEventAfterAssignCreate';

    public const EVENT_BEFORE_ASSIGN_REMOVE = 'RbacAssignEventBeforeAssignRemove';
    public const EVENT_AFTER_ASSIGN_REMOVE = 'RbacAssignEventAfterAssignRemove';
    
    /**
     * @var AuthItem
     */
    private $item;

    public function __construct(AuthItem $item)
    {
        $this->item = $item;
    }

    /**
     * @return AuthItem
     */
    public function getItem(): AuthItem
    {
        return $this->item;
    }
}