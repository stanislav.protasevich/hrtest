<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;

class RbacPermissions implements RbacPermissionsInterface
{
    public const ROLE_GUEST = 'Guest';
    public const ROLE_USER = 'User';
    public const ROLE_RECRUITER = 'Recruiter';
    public const ROLE_HR = 'Hr';
    public const ROLE_PROJECT_HEAD = 'ProjectHead';
    public const ROLE_HR_HEAD = 'HrHead';
    public const ROLE_ADMINISTRATOR = 'Administrator';

    public const ROLES = [
        self::ROLE_GUEST,
        self::ROLE_USER,
        self::ROLE_PROJECT_HEAD,
        self::ROLE_HR,
        self::ROLE_HR_HEAD,
        self::ROLE_ADMINISTRATOR,
    ];

    public const PERMISSION_RBAC_ASSIGN_PERMISSION = 'rbacAssignPermission';
    public const PERMISSION_RBAC_REMOVE_PERMISSION = 'rbacRemovePermission';
    public const PERMISSION_RBAC_PERMISSIONS_CHILDREN_UPDATE = 'rbacPermissionChildrenUpdate';
    public const PERMISSION_RBAC_ROLE_CREATE = 'rbacRoleCreate';
    public const PERMISSION_RBAC_ROLE_DELETE = 'rbacRoleDelete';
    public const PERMISSION_RBAC_LIST_PERMISSION = 'rbacListPermission';
    public const PERMISSION_RBAC_USER_LIST_PERMISSION = 'rbacUserListPermission';

    public function getPermissions(): array
    {
        return array_merge(
            static::roles(),
            static::permissions()
        );
    }

    private static function roles()
    {
        return [
            new RbacItemCommand(
                self::ROLE_GUEST,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_USER,
                ]
            ),

            new RbacItemCommand(
                self::ROLE_USER,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_PROJECT_HEAD,
                    self::ROLE_ADMINISTRATOR,
                ]
            ),

            new RbacItemCommand(
                self::ROLE_PROJECT_HEAD,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_ADMINISTRATOR
                ]
            ),

            new RbacItemCommand(
                self::ROLE_RECRUITER,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_HR,
                ],
                null,
                [
                    self::ROLE_USER,
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::ROLE_HR,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_HR_HEAD,
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::ROLE_HR_HEAD,
                AuthItem::TYPE_ROLE,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::ROLE_ADMINISTRATOR,
                AuthItem::TYPE_ROLE
            ),
        ];
    }

    private static function permissions()
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_RBAC_ASSIGN_PERMISSION,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_REMOVE_PERMISSION,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_LIST_PERMISSION,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_HR_HEAD,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_USER_LIST_PERMISSION,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_ROLE_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_PERMISSIONS_CHILDREN_UPDATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_RBAC_ROLE_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    self::ROLE_ADMINISTRATOR,
                ]
            ),
        ];
    }
}
