<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Factory\Form;

use Domain\Rbac\Entity\AuthItem;
use Domain\Rbac\Entity\AuthRule;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AuthItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'max' => 100,
                    ]),
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => AuthItem::TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('rule', EntityType::class, [
                'class' => AuthRule::class,
            ])
            ->add('children', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => AuthItem::class,
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['name'],
                    ]),
                ],
            ])
            ->add('parent', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => AuthItem::class,
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['name'],
                    ]),
                ],
            ])
            ->add('assignment', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => User::class,
                    'query_builder' => function (UserRepository $r) {
                        return $r->queryFindAllActive();
                    },
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ])
            ->add('description', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AuthItem::class,
            'csrf_protection' => false,
        ]);
    }
}