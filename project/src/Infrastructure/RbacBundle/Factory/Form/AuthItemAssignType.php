<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Factory\Form;

use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AuthItemAssignType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', EntityType::class, [
                'class' => AuthItem::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('assignment', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'by_reference' => false,
                'error_bubbling' => false,
                'mapped' => true,
                'entry_options' => [
                    'class' => User::class,
                    'query_builder' => function (UserRepository $r) {
                        return $r->queryFindAllActive();
                    },
                ],
                'constraints' => [
                    new NotNullCollection(),
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ]);
    }
}