<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Factory\Form;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Infrastructure\RbacBundle\Validator\Constraints\DuplicateParentChildrenConstraint;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;

class AuthItemPermissionChildrenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', EntityType::class, [
                'class' => AuthItem::class,
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('children', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => AuthItem::class,
                ],
                'constraints' => [
                    new UniqueCollections(['attributes' => 'name']),
                    new DuplicateParentChildrenConstraint()
                ]
            ]);
    }
}