<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Factory\Form;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Repository\AuthItemRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class AuthItemRoleEntityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', EntityType::class, [
                'class' => AuthItem::class,
                'query_builder' => function (AuthItemRepository $r) {
                    return $r->queryRole();
                },
                'constraints' => [
                    new NotBlank(),
                    new NotNull(),
                ],
            ]);
    }
}