<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Console;

use Application\Kernel;
use Infrastructure\CommonBundle\Console\AbstractBusCommand;
use Infrastructure\RbacBundle\Command\RbacAssignInitCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RbacInitPermissionsCommand extends AbstractBusCommand
{
    protected static $defaultName = 'rbac:init:permissions';

    protected $permissionsDir = 'Rbac';

    protected function configure()
    {
        $this
            ->setDescription('Init permissions.')
            ->addOption('clear', null, InputOption::VALUE_NONE, 'Clear all permissions.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* @var $ruleClassesArray \ReflectionClass[] */
        $ruleClassesArray = $this->readDir('Infrastructure', RbacRuleInterface::class);

        $rules = [];
        foreach ($ruleClassesArray as $item) {
            $rules[] = [
                'name' => $item->getShortName(),
                'data' => $item->getName(),
            ];
        }

        $clear = false;
        if ($input->getOption('clear')) {
            $clear = true;
        }

        /* @var $classesArray \ReflectionClass[] */
        $classesArray = $this->readDir('Infrastructure', RbacPermissionsInterface::class);

        $permissions = [];

        foreach ($classesArray as $class) {
            $result = call_user_func([$class->name, 'getPermissions']);

            if (!empty($result)) {
                $permissions = array_merge($permissions, $result);
            }
        }

        $result = $this->handle(
            new RbacAssignInitCommand($permissions, $clear, $rules)
        );

        $io = new SymfonyStyle($input, $output);
        $count = count($result);

        $io->success("Created {$count} new permissions");
    }

    protected function readDir(string $dir, string $needle): array
    {
        /* @var $kernel Kernel */
        $root = $this->getApplication()->getKernel()->getProjectDir().DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.$dir;

        $permissions = [];

        $handle = opendir($root);

        while (($item = readdir($handle)) !== false) {
            $path = $root.DIRECTORY_SEPARATOR.$item.DIRECTORY_SEPARATOR.$this->permissionsDir;
            if (is_dir($path)) {

                $handleRbacDir = opendir($path);

                while (($file = readdir($handleRbacDir)) !== false) {
                    $filePath = $path.DIRECTORY_SEPARATOR.$file;

                    if (preg_match('/^(\D.*?)\.php$/is', $file, $matches) && is_file($filePath)) {
                        $className = $matches[1];

                        $class = $dir.'\\'.$item.'\\'.$this->permissionsDir.'\\'.$className;
                        $reflect = new \ReflectionClass($class);

                        if ($reflect->implementsInterface($needle)) {
                            $permissions[] = $reflect;
                        }
                    }
                }
                closedir($handleRbacDir);
            }
        }

        closedir($handle);

        ksort($permissions);

        return $permissions;
    }
}