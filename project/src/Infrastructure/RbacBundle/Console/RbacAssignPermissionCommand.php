<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Console;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\CommonBundle\Console\AbstractBusCommand;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Infrastructure\RbacBundle\Command\RbacItemAssignCreateCommand;
use Infrastructure\RbacBundle\Factory\Form\AuthItemType;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\Request;

class RbacAssignPermissionCommand extends AbstractBusCommand
{
    protected static $defaultName = 'rbac:assign:user';

    protected $permissionsDir = 'Rbac';

    protected function configure()
    {
        $this
            ->setDescription('Assign permission to User.')
            ->addArgument('user', InputArgument::REQUIRED, 'User ID')
            ->addArgument('permission', InputArgument::REQUIRED, 'Permission or Role');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $userId = $input->getArgument('user');
        $permission = $input->getArgument('permission');

        $item = $this->getEm()->getRepository(AuthItem::class)->find($permission);

        if (is_null($item)) {
            throw new \Exception("Permission {$permission} not exist.");
        }

        $form = $this->createForm(AuthItemType::class, $item, [
            'method' => Request::METHOD_PUT,
        ]);

        $ids = ArrayHelper::getColumn($item->getAssignment()->getValues(), 'id');

        $this->processForm([
            'assignment' => array_merge($ids, (array)$userId),
        ], $form);

        $this->handle(
            new RbacItemAssignCreateCommand($item)
        );

        $io->success("Permission {$permission} assigned to user {$userId}.");
    }
}