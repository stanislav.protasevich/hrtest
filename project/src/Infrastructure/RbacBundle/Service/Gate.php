<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Service;


use Doctrine\ORM\EntityManagerInterface;
use Domain\Rbac\Entity\AuthItem;
use Domain\Rbac\Entity\AuthRule;
use Domain\User\Entity\User;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Predis\Client;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Gate
{
    public $cacheTtl = 300;

    protected $defaultRoles = [];
    /**
     * @var AuthItem[]
     */
    protected $items;
    /**
     * @var string
     */
    private $permission;
    /**
     * @var null
     */
    private $params;
    /**
     * @var User
     */
    private $_user;
    /**
     * @var EntityManagerInterface
     */
    private $_em;
    /**
     * @var AuthItem[]
     */
    private $_assignments = [];
    /**
     * @var AuthItem[]
     */
    private $_checkAccessAssignments;
    /**
     * @var AuthItem[]
     */
    private $parents = [];
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var string
     */
    private $cacheKey = 'rbac';
    /**
     * @var array
     */
    private $rules;

    private function __construct(string $permission, ?array $params = [])
    {
        $this->permission = $permission;
        $this->container = container();
        $this->params = $params;
    }

    public static function cannot(string $permission, ?array $params = [])
    {
        return static::can($permission, $params) ? false : true;
    }

    public static function can(string $permission, ?array $params = [])
    {
        return (new static($permission, $params))->checkAccess();
    }

    /**
     * @return bool
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Exception
     */
    protected function checkAccess(): bool
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        $userId = $user->getId();
        if (isset($this->_checkAccessAssignments[$userId])) {
            $assignments = $this->_checkAccessAssignments[$userId];
        } else {
            $assignments = $this->getAssignments($user);
            $this->_checkAccessAssignments[$userId] = $assignments;
        }

        if ($this->hasNoAssignments($assignments)) { //TODO
            return false;
        }

        $this->loadFromCache();

        return $this->checkAccessRecursive($user, $this->permission, $assignments, $this->params);
    }

    protected function getUser(): ?User
    {
        if (!$this->_user) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $this->_user = $user instanceof User ? $user : null;
        }

        return $this->_user;
    }

    public function getAssignments(User $user)
    {
        if (empty($this->_assignments)) {

            /* @var $service RbacService */
            $service = app(RbacService::class);

            $results = $service->getAssignments($user);

            foreach ($results as $result) {
                $this->_assignments[$result->getName()] = [
                    'name' => $result->getName(),
                    'type' => $result->getType(),
                    'created_at' => $result->getCreatedAt(),
                    'updated_at' => $result->getUpdatedAt(),
                ];
            }
        }

        return $this->_assignments;
    }

    protected function hasNoAssignments(array $assignments)
    {
        return empty($assignments) && empty($this->defaultRoles);
    }

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function loadFromCache()
    {
        if ($this->items !== null) {
            return;
        }

        $cache = $this->cacheClient();
        $data = $cache->get($this->cacheKey);
        if (is_array($data) && isset($data[0], $data[1], $data[2])) {
            list($this->items, $this->parents, $this->rules) = $data;
            return;
        }

        /* @var $items AuthItem[] */
        $items = $this->getEm()->getRepository(AuthItem::class)->findAllItems();
        $this->items = [];
        foreach ($items as $item) {
            $this->items[$item['name']] = [
                'name' => $item['name'],
                'type' => $item['type'],
                'rule_name' => $item['rule_name'],
            ];
        }

        $this->rules = [];
        $rules = $this->getEm()->getRepository(AuthRule::class)->getAll();
        foreach ($rules as $rule) {
            $this->rules[$rule['name']] = [
                'name' => $rule['name'],
                'data' => $rule['data'],
            ];
        }

        $this->parents = [];
        $children = $this->getEm()->getRepository(AuthItem::class)->findChildrens();
        foreach ($children as $child) {
            if (isset($this->items[$child['child_name']])) {
                $this->parents[$child['child_name']][] = $child['parent_name'];
            }
        }

        $cache->set($this->cacheKey, [$this->items, $this->parents, $this->rules ?: []]);
    }

    protected function cacheClient(): ?CacheInterface
    {
        $client = $this->container->hasParameter('rbac.client') ? $this->container->getParameter('rbac.client') : 'default';
        $cacheName = $this->container->hasParameter('rbac.cache') ? $this->container->getParameter('rbac.cache') : null;
        $ttl = $this->container->hasParameter('rbac.cache.ttl') ? $this->container->getParameter('rbac.cache.ttl') : $this->cacheTtl;

        /* @var $cache CacheInterface */
        $cache = null;
        switch ($cacheName) {
            case 'redis':
                /* @var $redicClient Client */
                $redisClient = $this->container->get('snc_redis.' . $client);
                $cache = new RedisCache($redisClient, 'rbac', $ttl);
                break;
        }

        return $cache;
    }

    protected function getEm(): EntityManagerInterface
    {
        if (!$this->_em) {
            $this->_em = em();
        }

        return $this->_em;
    }

    /**
     * @param User $user
     * @param string $permissionName
     * @param array $assignments
     * @param array $params
     * @param array $visited
     * @return bool
     * @throws \Exception
     */
    public function checkAccessRecursive(User $user, string $permissionName, array $assignments, array $params = [], array $visited = [])
    {
        if (!isset($this->items[$permissionName])) {
            return false;
        }

        $item = $this->items[$permissionName];

        if ($item['rule_name'] && !$this->executeRule($user, $item, $params)) {
            return false;
        }

        if (isset($assignments[$permissionName]) || in_array($permissionName, $this->defaultRoles)) {
            return true;
        }

        if (!empty($this->parents[$permissionName])) {

            foreach ($this->parents[$permissionName] as $parent) {

                if (in_array($parent, $visited)) {
                    continue;
                }

                $visited[] = $parent;

                if ($this->checkAccessRecursive($user, $parent, $assignments, $params, $visited)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param User $user
     * @param array $item
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    protected function executeRule(User $user, array $item, array $params = [])
    {
        $ruleName = $item['rule_name'] ?? null;
        if ($ruleName === null) {
            return true;
        }

        $rule = $this->rules[$ruleName] ?? null;

        if ($rule === null) {
            return false;
        }

        $class = $rule['data'];
        /* @var $object RbacRuleInterface */
        $object = new $class;

        if ($object instanceof RbacRuleInterface) {
            return $object->execute($user, $item, $params);
        }

        throw new \Exception("Rule not found: {$ruleName}");
    }
}
