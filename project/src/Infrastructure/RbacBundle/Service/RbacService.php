<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\RbacBundle\Service;


use Domain\Rbac\Entity\AuthItem;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class RbacService extends BaseService
{
    private $_assignments = [];

    private $_roles = [];

    private $_childrenList = [];

    /**
     * @param User $user
     * @return AuthItem[]
     */
    public function getAssignments(User $user)
    {
        if (!isset($this->_assignments[$user->getId()])) {
            $this->_assignments[$user->getId()] = $this->getEm()->getRepository(AuthItem::class)->getItemWithAssignmentsByUser($user);
        }

        return $this->_assignments[$user->getId()];
    }

    public function getRolesByUser(User $user)
    {
        if (!isset($this->_roles[$user->getId()])) {
            $result = $this->getEm()->getRepository(AuthItem::class)->getUserRoles($user);

            $roles = [];
            foreach ($result as $row) {
                $roles[$row['name']] = $row;
            }

            $this->_roles[$user->getId()] = $roles;
        }

        return $this->_roles[$user->getId()];
    }

    public function getChildRoles(array $role)
    {
        $result = [];
        $this->getChildrenRecursive($role['name'], $this->getChildrenList(), $result);

        $roles = [$role['name'] => $role];

        $roles += array_filter($this->getRoles(), function ($roleItem) use ($result) {
            return array_key_exists($roleItem['name'], $result);
        });

        return $roles;
    }

    protected function getChildrenRecursive($name, $childrenList, &$result)
    {
        if (isset($childrenList[$name])) {
            foreach ($childrenList[$name] as $key => $child) {
                if (isset($result[$child])) {
                    continue;
                }

                $result[$child] = true;
                $this->getChildrenRecursive($child, $childrenList, $result);
            }
        }
    }

    protected function getChildrenList(): array
    {
        if (empty($this->_childrenList)) {
            $results = $this->getEm()->getRepository(AuthItem::class)->findChildrens();

            $parents = [];

            foreach ($results as $row) {
                $parents[$row['parent_name']][] = $row['child_name'];
            }

            $this->_childrenList = $parents;
        }

        return $this->_childrenList;
    }

    public function getRoles()
    {
        return $this->getItems(AuthItem::TYPE_ROLE);
    }

    protected function getItems(string $type)
    {
        $results = $this->getEm()->getRepository(AuthItem::class)
            ->createQueryBuilder('a')
            ->select(['a.name', 'a.type', 'a.title'])
            ->where('a.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getArrayResult();

        $items = [];
        foreach ($results as $row) {
            $items[$row['name']] = $row;
        }

        return $items;
    }

    public function getChildren(string $name)
    {
        $results = $this->getEm()->getRepository(AuthItem::class)->getItemChildren($name);

        $children = [];
        foreach ($results as $row) {
            $children[$row['name']] = $row;
        }

        return $children;
    }

    public function getPermissions()
    {
        return $this->getItems(AuthItem::TYPE_PERMISSION);
    }

    public function getPermissionsByUser(User $user)
    {
        $directPermission = $this->getDirectPermissionsByUser($user);
        $inheritedPermission = $this->getInheritedPermissionsByUser($user);

        return array_merge($directPermission, $inheritedPermission);
    }

    protected function getDirectPermissionsByUser(User $user)
    {
        $result = $this->getEm()->getRepository(AuthItem::class)->getUserPermissions($user);

        $permissions = [];
        foreach ($result as $row) {
            $permissions[$row['name']] = $row;
        }

        return $permissions;
    }

    protected function getInheritedPermissionsByUser(User $user)
    {
        $results = $this->getEm()->getRepository(AuthItem::class)->getAssignmentsByUser($user);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($results as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $results = $this->getEm()->getRepository(AuthItem::class)->getPermissionsByPermissionsArray(array_keys($result));

        $permissions = [];
        foreach ($results as $row) {
            $permissions[$row['name']] = $row;
        }

        return $permissions;
    }

    /**
     * {@inheritdoc}
     */
    public function getPermissionsByRole(string $roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenRecursive($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }

        $results = $this->getEm()->getRepository(AuthItem::class)->getPermissionsChildren(array_keys($result));

        $permissions = [];
        foreach ($results as $row) {
            $permissions[$row['name']] = $row;
        }


        return $permissions;
    }

}
