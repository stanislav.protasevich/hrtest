<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\RbacBundle\Service;


use Domain\Rbac\Entity\AuthItem;
use Domain\Rbac\Entity\AuthRule;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Helpers\Inflector;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Factory\Form\AuthItemType;
use Infrastructure\RbacBundle\Repository\AuthItemRepository;
use Infrastructure\RbacBundle\Repository\AuthRuleRepository;
use Symfony\Component\Form\FormInterface;

class AuthItemService extends BaseService
{
    /**
     * @var AuthItem[]
     */
    private $_authItems = [];

    /**
     * Добавления разрешений
     *
     * @param RbacItemCommand[] $items
     * @param bool $clear очистить существующие
     * @param array|null $rules
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function append(array $items, bool $clear = false, ?array $rules = []): array
    {
        if ($clear) {
            $this->clearAll();
        }

        $this->createRules($rules);

        $created = $this->createNewPermissions($items);

        /* @var $authItems AuthItem[] */
        $authItems = $this->permissionsAll();

        $em = $this->getEm();

        foreach ($authItems as $authItem) {
            foreach ($items as $item) {

                if ($authItem->getName() !== $item->getName()) {
                    continue;
                }

                foreach ($item->getParent() as $parent) {
                    $exist = $authItem->getParent()->exists(function ($index, AuthItem $item) use ($parent) {
                        return $item->getName() === $parent;
                    });

                    if ($exist) {
                        continue;
                    }

                    if ($parentAuthItem = $em->getRepository(AuthItem::class)->find($parent)) {
                        $authItem->addParent($parentAuthItem);

                        $rule = $em->getRepository(AuthRule::class)->find($item->getRuleName() ?: '');

                        $authItem->setRule($rule);
                        $this->updateItem($authItem);
                    }
                }
            }
        }

        return $created;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function clearAll()
    {
        $em = $this->getEm();
        $authItems = $em->getRepository(AuthItem::class)->findAll();
        foreach ($authItems as $authItem) {
            $em->remove($authItem);
        }

        $authRules = $em->getRepository(AuthRuleRepository::class)->findAll();
        foreach ($authRules as $authRule) {
            $em->remove($authRule);
        }

        $em->flush();
    }

    /**
     * @param array|null $rules
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createRules(?array $rules): void
    {
        /* @var $authRules AuthRule[] */
        $authRules = $this->rulesAll();

        if (empty($authRules)) {
            foreach ($rules as $rule) {
                $this->createRule($rule['name'], $rule['data']);
            }
        } else {
//            foreach ($authRules as $authRule) { // TODO
//                foreach ($rules as $rule) {
//                    if ($authRule->getName() === $rule['name']) {
//                        continue;
//                    }
//
//                    $this->createRule($rule['name'], $rule['data']);
//                }
//            }
        }

    }

    /**
     * @return array
     */
    protected function rulesAll(): array
    {
        return $this->getEm()->getRepository(AuthRule::class)->findAll();
    }

    /**
     * @param string $name
     * @param string $data
     * @return AuthRule
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createRule(string $name, string $data): ?AuthRule
    {
        if ($this->getRule($name)) {
            return null;
        }

        $em = $this->getEm();
        $now = new \DateTime();

        $rule = new AuthRule();
        $rule->setName($name);
        $rule->setData($data);
        $rule->setCreatedAt($now);
        $rule->setUpdatedAt($now);

        $em->persist($rule);
        $em->flush();

        return $rule;
    }

    public function getRule(string $name): ?AuthRule
    {
        return $this->getEm()->getRepository(AuthRule::class)->find($name);
    }

    /**
     * @param RbacItemCommand[] $items
     * @return array
     */
    protected function createNewPermissions(array $items): array
    {
        $exist = $this->existPermissions();

//        $itemNames = ArrayHelper::getColumn($items, 'name');
//
//        $diff = array_diff($exist, $itemNames);

        $created = [];
        foreach ($items as $item) {

            if (in_array($item->getName(), $exist)) {
                continue;
            }

            $authItem = new AuthItem();
            $form = $this->createForm(AuthItemType::class, $authItem);
            $form->submit([
                'title' => $item->getTitle() ?: Inflector::camel2words($item->getName()),
                'name' => $item->getName(),
                'type' => $item->getType(),
            ], false);


            $this->createItem($authItem);

            $created[] = $authItem;
        }

//        foreach ($diff as $item) {
//            $this->dropItem($item);
//        }

        return $created;
    }

    /**
     * @return array
     */
    protected function existPermissions(): array
    {
        $authItems = $this->getEm()->getRepository(AuthItem::class)->getExistNames();

        return array_map('current', $authItems);
    }

    protected function createForm(string $type, $data = null, array $options = array()): FormInterface
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }

    public function createItem(AuthItem $authItem): AuthItem
    {
        $now = new \DateTime();

        $authItem->setCreatedAt($now);

        return $this->updateItem($authItem);
    }

    /**
     * @param AuthItem $authItem
     * @return AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateItem(AuthItem $authItem): AuthItem
    {
        $em = $this->getEm();

        $authItem->setUpdatedAt(new \DateTime());

        $em->persist($authItem);
        $em->flush();

        return $authItem;
    }

    /**
     * @return array
     */
    protected function permissionsAll(): array
    {
        return $this->getEm()->getRepository(AuthItem::class)->findAll();
    }

    public function dropItem(string $item)
    {
        $em = $this->getEm();
        $authItem = $em->getRepository(AuthItem::class)->find($item);
        $em->remove($authItem);
        $em->flush();
    }

    /**
     * @param AuthItem $authItem
     * @param array $children
     * @return AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateChildren(AuthItem $authItem, array $children): AuthItem
    {
        $em = $this->getEm();

        foreach ($authItem->getChildren() as $child) {
            $authItem->removeChild($child);
        }

        foreach ($children as $child) {
            $authItem->addChild($child);
        }

        $authItem->setUpdatedAt(new \DateTime());

        $em->persist($authItem);
        $em->flush();

        return $authItem;
    }

    /**
     * @param AuthItem $item
     * @param User[] $assignments
     * @return AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function assignRemove(AuthItem $item, array $assignments = []): AuthItem
    {
        $em = $this->getEm();

        $item->setUpdatedAt(new \DateTime());

        foreach ($assignments as $assignment) {

            $exist = $item->getAssignment()->exists(function ($key, User $element) use ($assignment) {
                return $element->getId() === $assignment->getId();
            });

            if (!$exist) {
                continue;
            }

            $item->removeAssignment($assignment);
        }

        $em->persist($item);
        $em->flush();

        return $item;
    }

    public function createRole(AuthItem $item)
    {
        $em = $this->getEm();

        $now = new \DateTime();

        if (!$item->getTitle()) {
            $item->setTitle(
                Inflector::camel2words($item->getName())
            );
        }

        $item->setCreatedAt($now);
        $item->setUpdatedAt($now);

        $em->persist($item);
        $em->flush();

        return $item;
    }

    /**
     * @param AuthItem $authItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(AuthItem $authItem): void
    {
        $em = $this->getEm();

        foreach ($authItem->getChildren() as $child) {
            $authItem->removeChild($child);
        }

        foreach ($authItem->getParent() as $parent) {
            $authItem->removeParent($parent);
        }

        foreach ($authItem->getAssignment() as $item) {
            $authItem->removeAssignment($item);
        }

        $em->persist($authItem);

        $em->remove($authItem);
        $em->flush();
    }

    /**
     * @param RbacItemCommand $command
     */
    public function createFromAnonItem(RbacItemCommand $command)
    {
        switch ($command->getType()) {
            case AuthItem::TYPE_ROLE:
                $this->createRoleIfNotExist(
                    $command->getName(), $command->getRuleName(), $command->getTitle(), $command->getDescription(), $command->getParent(), $command->getChildren()
                );
                break;

            case AuthItem::TYPE_PERMISSION:
                $this->createPermissionIfNotExist(
                    $command->getName(), $command->getRuleName(), $command->getTitle(), $command->getDescription(), $command->getParent(), $command->getChildren()
                );
                break;
        }
    }

    public function createRoleIfNotExist(string $name, ?string $rule = null, ?string $title = null, ?string $description = null, ?array $parents = [], ?array $children = [])
    {
        $this->createAuthItem($name, AuthItem::TYPE_ROLE, $rule, $title, $description, $parents, $children);
    }

    /**
     * @param string $name
     * @param string $type
     * @param string|null $rule
     * @param string|null $title
     * @param string|null $description
     * @param array|null $parents
     * @param $childrens
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function createAuthItem(string $name, string $type, ?string $rule, ?string $title, ?string $description, ?array $parents, ?array $childrens): void
    {
        $item = $this->getItem($name);
        if (!$item) {
            $item = new AuthItem();
            $item->setName($name);
            $item->setCreatedAt(new \DateTime());
            $item->setUpdatedAt(new \DateTime());
        }

        if ($rule && $ruleEntity = $this->getRule($rule)) {
            $item->setRule($ruleEntity);
        }

        $title = $title ? $title : Inflector::camel2words($name);
        $item->setTitle($title);
        $item->setDescription($description);
        $item->setType($type);

        if (!empty($parents)) {
            foreach ($parents as $parent) {
                $parentEntity = $this->getItem($parent);
                if ($this->isParentExist($item, $parent) === false && $parentEntity = $this->getItem($parent)) {
                    $item->addParent($parentEntity);
                }
            }
        }

        if (!empty($childrens)) {
            foreach ($childrens as $children) {
                if ($this->isChildrenExist($item, $children) === false && $childrenEntity = $this->getItem($children)) {
                    $item->addChild($childrenEntity);
                }
            }
        }

        $this->getEm()->persist($item);
        $this->getEm()->flush();
    }

    public function getItem(string $name): ?AuthItem
    {
        return $this->getEm()->getRepository(AuthItem::class)->find($name);
    }

    public function isParentExist(AuthItem $authItem, string $parent): bool
    {
        return $authItem->getParent()->exists(function ($index, AuthItem $item) use ($parent) {
            return $item->getName() === $parent;
        });
    }

    public function isChildrenExist(AuthItem $authItem, string $child): bool
    {
        return $authItem->getChildren()->exists(function ($index, AuthItem $item) use ($child) {
            return $item->getName() === $child;
        });
    }

    public function createPermissionIfNotExist(string $name, ?string $rule = null, ?string $title = null, ?string $description = null, ?array $parents = [], ?array $children = [])
    {
        $this->createAuthItem($name, AuthItem::TYPE_PERMISSION, $rule, $title, $description, $parents, $children);
    }

    /**
     * @param array $names
     * @return array|AuthItem[]
     */
    public function getBatchItems(array $names): array
    {
        return $this->getEm()->getRepository(AuthItem::class)->findBy(['name' => $names]);
    }

    /**
     * @param string $item
     * @param string $userId
     * @return AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAssignFromStrings(string $item, string $userId)
    {
        $item = $this->getItem($item);
        $user = $this->getEm()->getRepository(User::class)->find($userId);

        $this->assignCreate($item, [$user]);
    }

    /**
     * @param AuthItem $item
     * @param User[] $assignments
     * @return AuthItem
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function assignCreate(AuthItem $item, array $assignments = []): AuthItem
    {
        $em = $this->getEm();

        $item->setUpdatedAt(new \DateTime());

        foreach ($assignments as $assignment) {

            $exist = $item->getAssignment()->exists(function ($key, User $element) use ($assignment) {
                return $element->getId() === $assignment->getId();
            });

            if ($exist) {
                continue;
            }

            $item->addAssignment($assignment);
        }

        $em->persist($item);
        $em->flush();

        return $item;
    }

    public function addChildrenByString(string $parent, string $children)
    {
        $parent = $this->getItem($parent);

        if ($this->isChildrenExist($parent, $children) === false) {
            $em = $this->getEm();

            $children = $this->getItem($children);

            $parent->addChild($children);

            $em->persist($parent);
            $em->flush();
        }
    }

    /**
     * @param string $role
     * @param AuthItemRepository $repository
     * @return AuthItem
     */
    protected function populateCacheItems(string $role, AuthItemRepository $repository): AuthItem
    {
        if (!isset($this->_authItems[$role])) {
            $this->_authItems[$role] = $repository->find($role);
        }

        return $this->_authItems[$role];
    }
}
