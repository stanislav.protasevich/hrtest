<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Helpers;


use Domain\Blog\Entity\BlogPost;
use Domain\Blog\Entity\BlogPostComment;
use Domain\Blog\Entity\BlogPostDestination;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @mixin AbstractBusController
 */
trait BlogHelpersTrait
{
    public function findCommentOrFail(string $id, string $commentId): BlogPostComment
    {
        $post = $this->findOrFail($id);

        /** @var BlogPostComment $comment */
        $comment = $this->getEm()->getRepository(BlogPostComment::class)->findByPostAndId($post, $commentId);

        if (is_null($comment)) {
            throw new ModelNotFoundException(BlogPostComment::class, $commentId);
        }

        return $comment;
    }

    public function findOrFail(string $id): BlogPost
    {
        /** @var BlogPost $blog */
        $blog = $this->getEm()->getRepository(BlogPost::class)->findOneById($id);

        if (is_null($blog) ||
            ($blog->getStatus() === BlogPost::STATUS_DELETED && Gate::cannot(BlogPermissions::PERMISSION_BLOG_GRANT_ALL, ['model' => $blog]))
        ) {
            throw new ModelNotFoundException(BlogPost::class, $id);
        }

        return $blog;
    }

    public function canAccess(BlogPost $post)
    {
        /** @var User $user */
        $user = $this->getUser();

        $visibility = $post->getVisibility();

        if (
            $visibility === BlogPost::VISIBILITY_ALL ||
            Gate::can(BlogPermissions::PERMISSION_BLOG_GRANT_ALL, ['model' => $post]) ||
            Gate::can(RbacPermissions::ROLE_ADMINISTRATOR, ['model' => $post])
        ) {
            return;
        }

        $canAccess = false;

        switch ($visibility) {
            case BlogPost::VISIBILITY_LOCATION:

                $canAccess = $post->getDestination()->exists(function ($index, BlogPostDestination $destination) use ($user) {
                    $city = $destination->getCity()->getId();
                    $userCity = $user->getEmployee()->getCompanyAddress()->getCity()->getId();
                    return $city === $userCity;
                });

                break;

            case BlogPost::VISIBILITY_PROJECT:

                $canAccess = $post->getDestination()->exists(function ($index, BlogPostDestination $destination) use ($user) {
                    $project = $destination->getCompany()->getId();

                    $userCompanies = $user->getEmployeeCompanies()->exists(function ($indx, EmployeeCompany $employeeCompany) use ($project) {
                        return $employeeCompany->getCompany()->getId() === $project;
                    });

                    return $userCompanies;
                });

                break;
        }

        if (!$canAccess) {
            $this->throwAccessDeniedException();
        }
    }
}
