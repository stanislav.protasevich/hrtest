<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Helpers;


use Domain\Blog\Entity\BlogCategory;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @mixin AbstractBusController
 */
trait BlogCategoryHelpersTrait
{
    public function findOrFail(string $id): BlogCategory
    {
        $blog = $this->getEm()->getRepository(BlogCategory::class)->findOneById($id);

        if (is_null($blog)) {
            throw new ModelNotFoundException(BlogCategory::class, $id);
        }

        return $blog;
    }
}
