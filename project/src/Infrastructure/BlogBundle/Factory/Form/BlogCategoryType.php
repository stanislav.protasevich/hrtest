<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Factory\Form;

use Domain\Blog\Entity\BlogCategory;
use Domain\Blog\Entity\BlogPost;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class BlogCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => BlogPost::STATUSES,
                'empty_data' => (string)BlogPost::STATUS_ACTIVE
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $image = $data['image'] ?? null;

                $form->add('image', Base64EncodedFileType::class, [
                    'disabled' => StringHelper::isBase64File($image) === false
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogCategory::class,
            'csrf_protection' => false,
        ]);
    }
}
