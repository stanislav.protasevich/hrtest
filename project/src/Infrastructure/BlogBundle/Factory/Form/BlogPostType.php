<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Factory\Form;

use Domain\Blog\Entity\BlogCategory;
use Domain\Blog\Entity\BlogPost;
use Infrastructure\CommonBundle\Form\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Infrastructure\CommonBundle\Validator\Constraints\NotNullCollection;
use Infrastructure\CommonBundle\Validator\Constraints\UniqueCollections;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class BlogPostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 3,
                        'max' => 255,
                    ])
                ],
            ])
            ->add('content', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 5
                    ])
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => BlogPost::STATUSES,
                'empty_data' => (string)BlogPost::STATUS_ACTIVE
            ])
            ->add('visibility', ChoiceType::class, [
                'choices' => BlogPost::VISIBILITIES,
                'empty_data' => (string)BlogPost::VISIBILITY_ALL
            ])
            ->add('categories', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'entry_options' => [
                    'class' => BlogCategory::class,
                ],
                'constraints' => [
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $image = $data['image'] ?? null;

                $form
                    ->add('image', Base64EncodedFileType::class, [
                        'disabled' => StringHelper::isBase64File($image) === false
                    ])
                    ->add('destination', CollectionType::class, [
                        'entry_type' => BlogPostDestinationType::class,
                        'allow_add' => true,
                        'allow_delete' => true,
                        'error_bubbling' => false,
                        'by_reference' => false,
                        'entry_options' => ['blogPost' => $data],
                        'constraints' => [
                            new NotNullCollection([
                                'groups' => [BlogPost::VISIBILITY_LOCATION, BlogPost::VISIBILITY_PROJECT],
                            ]),
                        ],
                    ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $data BlogPost */
                $data = $form->getData();

                $groups = ['Default'];
                $groups[] = $data->getVisibility();

                return $groups;
            },
        ]);
    }
}
