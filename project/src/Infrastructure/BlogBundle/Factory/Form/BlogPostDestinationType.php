<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Factory\Form;

use Domain\Blog\Entity\BlogPost;
use Domain\Blog\Entity\BlogPostDestination;
use Domain\Company\Entity\Company;
use Domain\Location\Entity\City;
use Infrastructure\CompanyBundle\Repository\CompanyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class BlogPostDestinationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $visibility = $options['blogPost']['visibility'] ?? null;

        $builder
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'disabled' => $visibility !== BlogPost::VISIBILITY_PROJECT,
                'constraints' => [
                    new NotBlank([
                        'groups' => [BlogPost::VISIBILITY_PROJECT]
                    ]),
                ],
                'query_builder' => function (CompanyRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('city', EntityType::class, [
                'disabled' => $visibility !== BlogPost::VISIBILITY_LOCATION,
                'class' => City::class,
                'constraints' => [
                    new NotBlank([
                        'groups' => [BlogPost::VISIBILITY_LOCATION]
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPostDestination::class,
            'csrf_protection' => false,
            'blogPost' => null,
            'required' => ['blogPost'],
            'validation_groups' => function (FormInterface $form) {
                /** @var BlogPost $post */
                $post = $form->getParent()->getParent()->getData();

                $groups = ['Default'];

                $groups[] = $post->getVisibility();

                return $groups;
            },
        ]);
    }
}
