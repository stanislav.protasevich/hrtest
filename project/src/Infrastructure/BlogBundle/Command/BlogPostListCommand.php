<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BlogPostListCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $categories;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $status;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $createdBy;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $createdAt;

    /**
     * @var Request
     */
    public $request;

    public $fullAccess = false;
}
