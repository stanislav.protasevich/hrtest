<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command\Comment;


use Domain\Blog\Entity\BlogPost;

class BlogPostCommentPostListCommand
{
    /**
     * @var BlogPost
     */
    private $post;

    public function __construct(BlogPost $post)
    {
        $this->post = $post;
    }

    /**
     * @return BlogPost
     */
    public function getPost(): BlogPost
    {
        return $this->post;
    }
}
