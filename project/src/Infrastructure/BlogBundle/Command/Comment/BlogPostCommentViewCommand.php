<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command\Comment;


use Domain\Blog\Entity\BlogPostComment;

class BlogPostCommentViewCommand
{
    /**
     * @var BlogPostComment
     */
    private $comment;

    public function __construct(BlogPostComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return BlogPostComment
     */
    public function getComment(): BlogPostComment
    {
        return $this->comment;
    }
}
