<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command\Comment;


class BlogPostCommentRemoveCommand extends BlogPostCommentAddCommand
{

}
