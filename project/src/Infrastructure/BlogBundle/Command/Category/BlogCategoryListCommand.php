<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command\Category;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;

class BlogCategoryListCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $title;

    /**
     * @Assert\Type(type="string")
     * @var string|string[]
     */
    public $status;
}
