<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Command\Category;


use Domain\Blog\Entity\BlogCategory;

class BlogCategoryCreateCommand
{
    /**
     * @var BlogCategory
     */
    private $category;

    public function __construct(BlogCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return BlogCategory
     */
    public function getCategory(): BlogCategory
    {
        return $this->category;
    }
}
