<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Blog\Entity\BlogCategory;
use Domain\Blog\Entity\BlogPost;
use Domain\User\Entity\User;
use Infrastructure\BlogBundle\Command\BlogPostListCommand;
use Infrastructure\CommonBundle\Service\FilterSortService;

class BlogSearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'blog.id',
        'title' => 'blog.title',
        'status' => 'blog.status',
        'categories' => 'categories.title',
        'createdAt' => 'blog.createdAt',
        'createdBy' => ['createdByProfile.firstName', 'createdByProfile.middleName', 'createdByProfile.surname']
    ];

    public function search(BlogPostListCommand $command): QueryBuilder
    {
        $ids = $this->mainPosts($command->fullAccess);

        if (!$command->fullAccess) {
            $destinationIds = $this->destinationPosts();
            $ids = array_merge($ids, $destinationIds);
        }

        /* @var $qb QueryBuilder */
        $qb = $this->query()
            ->leftJoin('blog.categories', 'categories')
            ->leftJoin('blog.createdBy', 'createdBy')
            ->leftJoin('createdBy.profile', 'createdByProfile')
            ->where('blog.id IN (:id)')
            ->setParameter('id', $ids)
            ->addOrderBy('blog.createdAt', 'DESC');

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('blog.id IN (:id)')->setParameter('id', $id);
        }

        if ($command->title) {
            $qb->andWhere('blog.title LIKE :title')
                ->setParameter('title', $this->makeLikeParam((string)$command->title));
        }

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('blog.status IN (:status)')->setParameter('status', $status);
        }

        if ($command->createdBy && $createdBy = $this->paramsFromString($command->createdBy)) {
            $qb->andWhere('createdBy IN (:createdBy)')
                ->setParameter('createdBy', $this->getModel(User::class, $createdBy));
        }

        if ($command->categories && $categories = $this->paramsFromString($command->categories)) {
            $qb->andWhere('categories IN (:categories)')
                ->setParameter('categories', $this->getModel(BlogCategory::class, $categories));
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }

    /**
     * @param bool $fullAccess
     * @return array
     */
    protected function mainPosts(bool $fullAccess): array
    {
        $qb = $this->query()
            ->select('blog.id')
            ->addOrderBy('blog.createdAt', 'DESC')
            ->setMaxResults(200);

        if (!$fullAccess) {
            $qb->where('blog.visibility IN (:visibility)')
                ->setParameter('visibility', [BlogPost::VISIBILITY_ALL]);
        }

        $ids = array_map('current', $qb->getQuery()->getScalarResult());

        return $ids;
    }

    /**
     * @return QueryBuilder
     */
    protected function query(): QueryBuilder
    {
        return $this->getEm()->getRepository(BlogPost::class)->findAllQueryBuilder();
    }

    /**
     * @return array
     */
    protected function destinationPosts(): array
    {
        $qb = $this->query()
            ->select('blog.id')
            ->where('blog.visibility IN (:visibility)')
            ->innerJoin('blog.destination', 'destination')
            ->setParameter('visibility', [BlogPost::VISIBILITY_LOCATION, BlogPost::VISIBILITY_PROJECT])
            ->orWhere('destination.company = :company')
            ->setParameter('company',
                $this->getUser()->getMainEmployeeCompany()->getCompany() ?? null
            )
            ->orWhere('destination.city = :city')
            ->setParameter('city',
                $this->getUser()->getEmployee()->getCompanyAddress()->getCity() ?? null
            )
            ->setMaxResults(200);

        $ids = array_map('current', $qb->getQuery()->getScalarResult());

        return $ids;
    }
}
