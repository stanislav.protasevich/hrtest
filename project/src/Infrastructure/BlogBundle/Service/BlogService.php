<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Domain\Blog\Entity\BlogPost;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Service\BaseService;
use Symfony\Component\HttpFoundation\File\File;

class BlogService extends BaseService
{
    public function create(BlogPost $blogPost): BlogPost
    {
        $em = $this->getEm();

        $blogPost->setViewCount(0);
        $blogPost->setCreatedBy($this->getUser());
        $blogPost->setUpdatedBy($this->getUser());
        $blogPost->setCreatedAt($this->now());
        $blogPost->setUpdatedAt($this->now());
        
        $em->persist($blogPost);
        $em->flush();

        $this->setImage($blogPost);

        return $blogPost;
    }

    public function setImage(BlogPost $blogPost): void
    {
        $image = $blogPost->getImage();

        if ($image instanceof File) {
            $blogPost->setImage(
                FileHelper::saveFile($image, $blogPost->getRelativeImagePath())
            );

            $this->getEm()->persist($blogPost);
            $this->getEm()->flush();
        }
    }

    public function edit(BlogPost $blogPost): BlogPost
    {
        $em = $this->getEm();

        $blogPost->setUpdatedAt($this->now());
        $blogPost->setUpdatedBy($this->getUser());

        $em->persist($blogPost);
        $em->flush();

        $this->setImage($blogPost);

        return $blogPost;
    }

    public function delete(BlogPost $blogPost): void
    {
        $em = $this->getEm();

        $em->remove($blogPost);
        $em->flush();
    }

    public function addViewCount(BlogPost $blogPost): BlogPost
    {
        $em = $this->getEm();

        $blogPost->addViewCount();
        $em->persist($blogPost);
        $em->flush();

        return $blogPost;
    }
}
