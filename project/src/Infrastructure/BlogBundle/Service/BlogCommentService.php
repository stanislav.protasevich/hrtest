<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Domain\Blog\Entity\BlogPostComment;
use Infrastructure\CommonBundle\Service\BaseService;

class BlogCommentService extends BaseService
{
    public function add(BlogPostComment $comment): BlogPostComment
    {
        $em = $this->getEm();

        $comment->setCreatedBy($this->getUser());
        $comment->setUpdatedBy($this->getUser());
        $comment->setCreatedAt($this->now());
        $comment->setUpdatedAt($this->now());

        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    public function edit(BlogPostComment $comment): BlogPostComment
    {
        $em = $this->getEm();

        $comment->setUpdatedBy($this->getUser());
        $comment->setUpdatedAt($this->now());

        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    public function delete(BlogPostComment $comment): void
    {
        $em = $this->getEm();

        $em->remove($comment);
        $em->flush();
    }
}
