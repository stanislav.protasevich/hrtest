<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Blog\Entity\BlogCategory;
use Infrastructure\BlogBundle\Command\Category\BlogCategoryListCommand;
use Infrastructure\CommonBundle\Service\FilterSortService;

class BlogCategorySearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'category.id',
        'title' => 'category.title',
        'status' => 'category.status',
        'createdAt' => 'category.createdAt',
    ];

    public function search(BlogCategoryListCommand $command): QueryBuilder
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(BlogCategory::class)->findAllQueryBuilder();

        $qb->addOrderBy('category.createdAt', 'DESC');
        
        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('category.id IN (:id)')->setParameter('id', $id);
        }

        if ($command->title) {
            $qb->andWhere('category.title LIKE :title')
                ->setParameter('title', $this->makeLikeParam((string)$command->title));
        }

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('category.status IN (:status)')->setParameter('status', $status);
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
