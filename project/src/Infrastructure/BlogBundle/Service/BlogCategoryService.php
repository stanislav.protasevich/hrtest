<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Domain\Blog\Entity\BlogCategory;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Infrastructure\CommonBundle\Service\BaseService;
use Symfony\Component\HttpFoundation\File\File;

class BlogCategoryService extends BaseService
{
    public function create(BlogCategory $category): BlogCategory
    {
        $em = $this->getEm();

        $category->setCreatedBy($this->getUser());
        $category->setUpdatedBy($this->getUser());
        $category->setCreatedAt($this->now());
        $category->setUpdatedAt($this->now());

        $em->persist($category);
        $em->flush();

        $this->setImage($category);

        return $category;
    }

    public function setImage(BlogCategory $category): void
    {
        $image = $category->getImage();

        if ($image instanceof File) {
            $category->setImage(
                FileHelper::saveFile($image, $category->getRelativeImagePath())
            );

            $this->getEm()->persist($category);
            $this->getEm()->flush();
        }
    }

    public function edit(BlogCategory $category): BlogCategory
    {
        $em = $this->getEm();

        $category->setUpdatedAt($this->now());
        $category->setUpdatedBy($this->getUser());

        $em->persist($category);
        $em->flush();

        $this->setImage($category);

        return $category;
    }

    public function delete(BlogCategory $category): void
    {
        $em = $this->getEm();

        $em->remove($category);
        $em->flush();
    }

    public function getPrimary()
    {
        return $this->getEm()->getRepository(BlogCategory::class)->findPrimaryAll();
    }
}
