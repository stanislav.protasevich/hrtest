<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Blog\Entity\BlogPostComment;
use Infrastructure\BlogBundle\Command\Comment\BlogPostCommentPostListCommand;
use Infrastructure\CommonBundle\Service\FilterSortService;

class BlogCommentSearchService extends FilterSortService
{
    protected $sortMapping = [];

    public function search(BlogPostCommentPostListCommand $command): QueryBuilder
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(BlogPostComment::class)->findByPostQuery($command->getPost());

        $qb->addOrderBy('comments.createdAt', 'DESC');

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
