<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class BlogPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_BLOG_LIST = 'blogList';
    public const PERMISSION_BLOG_CREATE = 'blogCreate';
    public const PERMISSION_BLOG_EDIT = 'blogEdit';
    public const PERMISSION_BLOG_VIEW = 'blogView';
    public const PERMISSION_BLOG_DELETE = 'blogDelete';
    public const PERMISSION_BLOG_GRANT_ALL = 'blogGrantAll';

    public const PERMISSION_BLOG_COMMENTS_VIEW = 'blogCommentsView';
    public const PERMISSION_BLOG_COMMENTS_ADD = 'blogCommentsAdd';
    public const PERMISSION_BLOG_COMMENTS_EDIT = 'blogCommentsEdit';
    public const PERMISSION_BLOG_COMMENTS_REMOVE = 'blogCommentsRemove';

    public const PERMISSION_BLOG_CATEGORY_LIST = 'blogCategoryList';
    public const PERMISSION_BLOG_CATEGORY_CREATE = 'blogCategoryCreate';
    public const PERMISSION_BLOG_CATEGORY_EDIT = 'blogCategoryEdit';
    public const PERMISSION_BLOG_CATEGORY_VIEW = 'blogCategoryView';
    public const PERMISSION_BLOG_CATEGORY_DELETE = 'blogCategoryDelete';
    public const PERMISSION_BLOG_CATEGORY_GRANT_ALL = 'blogCategoryGrantAll';

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_BLOG_COMMENTS_REMOVE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_COMMENTS_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ],
                'BlogCommentEditRule'
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_COMMENTS_ADD,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_COMMENTS_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_GRANT_ALL,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_BLOG_CATEGORY_GRANT_ALL,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
        ];
    }
}
