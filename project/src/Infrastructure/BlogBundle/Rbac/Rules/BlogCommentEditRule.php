<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Rbac\Rules;


use Domain\Blog\Entity\BlogPostComment;
use Domain\User\Entity\User;
use Infrastructure\BlogBundle\Rbac\BlogPermissions;
use Infrastructure\RbacBundle\Contracts\RbacRuleInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;
use Infrastructure\RbacBundle\Service\Gate;

class BlogCommentEditRule implements RbacRuleInterface
{
    public function execute(User $user, $item, $params)
    {
        if (Gate::can(RbacPermissions::ROLE_ADMINISTRATOR) ||
            Gate::can(BlogPermissions::PERMISSION_BLOG_GRANT_ALL)
        ) {
            return true;
        }

        /** @var BlogPostComment $model */
        $model = $params['model'];

        if ($user->getId() === $model->getCreatedBy()->getId()) {
            return true;
        }

        return false;
    }
}
