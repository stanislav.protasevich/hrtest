<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Domain\Blog\Entity\BlogPost;
use Domain\Blog\Entity\BlogPostComment;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method BlogPostComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPostComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPostComment[]    findAll()
 * @method BlogPostComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostCommentRepository extends NestedTreeRepository
{
    public function __construct(EntityManagerInterface $em, ClassMetadata $class)
    {
        parent::__construct($em, $class);
    }

    public function tree(BlogPost $post)
    {
        $this->setChildrenIndex('children');
        return $this->buildTreeArray(
            $this->findComments($post)
        );
    }

    public function findComments(BlogPost $post, int $limit = 30)
    {
        return $this
            ->findAllQueryBuilder()
            ->select([
                'comments.id',
                'comments.content',
                'comments.lft',
                'comments.lvl',
                'comments.rgt',
                'DATE_FORMAT(comments.createdAt, \'%Y-%m-%dT%TZ\') AS created_at',
                'DATE_FORMAT(comments.updatedAt, \'%Y-%m-%dT%TZ\') AS updated_at',
            ])
            ->where('comments.post = :post')
            ->orderBy('comments.root, comments.lft', 'ASC')
            ->addOrderBy('comments.createdAt', 'DESC')
            ->setParameter('post', $post)
            ->setMaxResults($limit)
            ->getQuery()
            ->getArrayResult();
    }

    public function findAllQueryBuilder($alias = 'comments')
    {
        return $this->createQueryBuilder($alias);
    }

    public function findByPostAndId(BlogPost $post, string $id): ?BlogPostComment
    {
        return $this
            ->findAllQueryBuilder()
            ->where('comments.post = :post AND comments.id = :id')
            ->setParameter('post', $post)
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param BlogPost $post
     * @return BlogPostComment[]
     */
    public function findByPost(BlogPost $post): array
    {
        return $this->findByPostQuery($post)->getQuery()->getResult();
    }

    public function findByPostQuery(BlogPost $post): QueryBuilder
    {
        return $this
            ->findAllQueryBuilder()
            ->where('comments.post = :post')
            ->setParameter('post', $post);
    }
}
