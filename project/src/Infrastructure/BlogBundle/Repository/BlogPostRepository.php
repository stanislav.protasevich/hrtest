<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Blog\Entity\BlogPost;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlogPost::class);
    }

    public function findAllQueryBuilder($alias = 'blog')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param string $id
     * @return BlogPost|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?BlogPost
    {
        return $this->createQueryBuilder('blog')
            ->where('blog.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
