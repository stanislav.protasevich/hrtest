<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Blog\Entity\BlogCategory;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BlogCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogCategory[]    findAll()
 * @method BlogCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogCategoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BlogCategory::class);
    }

    public function findAllQueryBuilder($alias = 'category')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param string $id
     * @return BlogCategory|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?BlogCategory
    {
        return $this->createQueryBuilder('category')
            ->where('category.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('category')
            ->select(['category.id as value', 'category.title as label'])
            ->where('category.status = :status')
            ->setParameter('status', BlogCategory::STATUS_ACTIVE)
            ->getQuery()
            ->getArrayResult();
    }
}
