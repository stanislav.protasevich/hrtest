<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureBlogBundle extends Bundle
{

}
