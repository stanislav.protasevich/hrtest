<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\EventListener;


use Infrastructure\BlogBundle\Command\BlogPostViewCommand;
use Infrastructure\BlogBundle\Service\BlogService;
use Infrastructure\CommonBundle\Event\CommandEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class BlogViewCountEventListener implements EventSubscriberInterface
{
    /**
     * @var BlogService
     */
    private $service;

    public function __construct(BlogService $service)
    {
        $this->service = $service;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            BlogPostViewCommand::class => 'onViewAddCount'
        ];
    }

    /**
     * @param CommandEvent $event
     */
    public function onViewAddCount($event)
    {
        if (!$event instanceof CommandEvent) {
            return;
        }

        /** @var BlogPostViewCommand $command */
        $command = $event->getCommand();

        $this->service->addViewCount($command->getBlogPost());
    }
}
