<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Event;


use Domain\Blog\Entity\BlogPostComment;
use Symfony\Component\EventDispatcher\Event;

class BlogPostCommentEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeBlogPostInsert';
    public const EVENT_AFTER_INSERT = 'afterBlogPostInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeBlogPostUpdate';
    public const EVENT_AFTER_UPDATE = 'afterBlogPostUpdate';

    public const EVENT_BEFORE_DELETE = 'beforeBlogPostDelete';
    public const EVENT_AFTER_DELETE = 'afterBlogPostDelete';
    /**
     * @var BlogPostComment
     */
    private $comment;

    public function __construct(BlogPostComment $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return BlogPostComment
     */
    public function getComment(): BlogPostComment
    {
        return $this->comment;
    }
}
