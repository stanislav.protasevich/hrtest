<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Event;


use Domain\Blog\Entity\BlogPost;
use Symfony\Component\EventDispatcher\Event;

class BlogPostEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeBlogPostInsert';
    public const EVENT_AFTER_INSERT = 'afterBlogPostInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeBlogPostUpdate';
    public const EVENT_AFTER_UPDATE = 'afterBlogPostUpdate';

    public const EVENT_BEFORE_DELETE = 'beforeBlogPostDelete';
    public const EVENT_AFTER_DELETE = 'afterBlogPostDelete';

    /**
     * @var BlogPost
     */
    private $blogPost;

    public function __construct(BlogPost $blogPost)
    {

        $this->blogPost = $blogPost;
    }

    /**
     * @return BlogPost
     */
    public function getBlogPost(): BlogPost
    {
        return $this->blogPost;
    }
}
