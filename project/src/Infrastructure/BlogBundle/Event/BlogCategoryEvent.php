<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\BlogBundle\Event;


use Domain\Blog\Entity\BlogCategory;
use Symfony\Component\EventDispatcher\Event;

class BlogCategoryEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'beforeBlogCategoryInsert';
    public const EVENT_AFTER_INSERT = 'afterBlogCategoryInsert';

    public const EVENT_BEFORE_UPDATE = 'beforeBlogCategoryUpdate';
    public const EVENT_AFTER_UPDATE = 'afterBlogCategoryUpdate';

    public const EVENT_BEFORE_DELETE = 'beforeBlogCategoryDelete';
    public const EVENT_AFTER_DELETE = 'afterBlogCategoryDelete';

    /**
     * @var BlogCategory
     */
    private $category;

    public function __construct(BlogCategory $category)
    {
        $this->category = $category;
    }

    /**
     * @return BlogCategory
     */
    public function getCategory(): BlogCategory
    {
        return $this->category;
    }
}
