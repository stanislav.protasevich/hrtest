<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\SystemBundle\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Domain\System\Entity\Currency;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SystemCurrencyService
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EntityManager
     */
    private $_em;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $em = $this->getEm();

        /* @var $qb QueryBuilder */
        $qb = $em->getRepository(Currency::class)->findAllQueryBuilder();
        $qb->orderBy('currency.sortOrder', 'ASC');

        return $qb->getQuery()->getResult();
    }

    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->container->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }
}