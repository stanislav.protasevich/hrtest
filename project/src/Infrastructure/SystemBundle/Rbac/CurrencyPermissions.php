<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\SystemBundle\Rbac;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class CurrencyPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_SYSTEM_CURRENCY_LIST = 'systemCurrencyList';

    public const PERMISSION_SKILLS = [
        self::PERMISSION_SYSTEM_CURRENCY_LIST,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_SYSTEM_CURRENCY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_RECRUITER,
                ]
            ),
        ];
    }
}