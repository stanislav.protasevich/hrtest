<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\SystemBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureSystemBundle extends Bundle
{

}