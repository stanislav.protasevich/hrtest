<?php

declare(strict_types=1);

namespace Infrastructure\SystemBundle\DataFixtures\ORM;


use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\System\Entity\Currency;

class CurrencyLoad extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $em
     */
    public function load(ObjectManager $em)
    {
        $model = new Currency();
        $model->setName('USD Dollar');
        $model->setCodeNumber(840);
        $model->setCodeSymbol('USD');
        $model->setCreatedAt(new \DateTime());
        $model->setSortOrder(1);
        $model->setUpdatedAt(new \DateTime());

        $em->persist($model);

        $em->flush();
    }
}