<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Factory\Form;


use Domain\Company\Entity\Company;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Entity\JobTitle;
use Domain\User\Entity\User;
use Infrastructure\CompanyBundle\Repository\CompanyRepository;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmployeeToCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (CompanyRepository $r) {
                    return $r->createQueryBuilder('c')
                        ->where('c.status = :status')
                        ->setParameter('status', Company::STATUS_ACTIVE);
                },
            ])
            ->add('user', EntityType::class, [
                'class' => User::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (UserRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('job_title', EntityType::class, [
                'class' => JobTitle::class,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('isMain', CheckboxType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmployeeCompany::class,
            'csrf_protection' => false,
        ]);
    }
}