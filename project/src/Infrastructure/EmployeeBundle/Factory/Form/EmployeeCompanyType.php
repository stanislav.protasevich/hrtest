<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Factory\Form;


use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Job\Entity\JobTitle;
use Domain\System\Entity\Currency;
use Infrastructure\CommonBundle\Form\Type\BooleanType;
use Infrastructure\CompanyBundle\Repository\CompanyRepository;
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Infrastructure\JobBundle\Repository\JobTitleRepository;
use Infrastructure\SystemBundle\Repository\CurrencyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class EmployeeCompanyType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (CompanyRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('jobTitle', EntityType::class, [
                'class' => JobTitle::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (JobTitleRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('isMain', BooleanType::class, [
                'constraints' => [
                    new NotNull(),
                ],
            ])
            ->add('salary', NumberType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
            ])
            ->add('salaryIncrease', NumberType::class, [
                'empty_data' => "0",
                'constraints' => [
                    new GreaterThanOrEqual([
                        'value' => 0,
                    ]),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => EmployeeCompany::STATUSES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (CurrencyRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('workType', ChoiceType::class, [
                'choices' => EmployeeCompany::WORK_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('probationPeriod', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('startAt', DateType::class, [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('endAt', DateType::class, [
                'widget' => 'single_text',
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();

                $em = $this->container->get('doctrine.orm.entity_manager');

                $companyId = $data['company'] ?? '';

                $company = $em->getRepository(Company::class)->findOneActiveById(is_string($companyId) ? (string)$companyId : null);

                $form->add('department', EntityType::class, [
                    'class' => Department::class,
                    'constraints' => [
//                        new NotBlank(), // TODO
                    ],
                    'query_builder' => function (DepartmentRepository $r) use ($company) {
                        return $r->queryFindAllActiveByCompany($company);
                    },
                ]);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmployeeCompany::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();

                $groups = ['Default'];

                if ($method === Request::METHOD_PUT) {
                    $groups[] = 'edit';
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = 'create';
                }

                return $groups;
            },
        ]);
    }
}