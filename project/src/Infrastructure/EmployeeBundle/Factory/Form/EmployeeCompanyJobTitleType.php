<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Factory\Form;


use Domain\Job\Entity\JobTitle;
use Infrastructure\JobBundle\Repository\JobTitleRepository;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmployeeCompanyJobTitleType extends AbstractType
{
    /**
     * @var JobTitleRepository
     */
    private $jobTitles;

    public function __construct(JobTitleRepository $jobTitles)
    {
        $this->jobTitles = $jobTitles;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JobTitle::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();

                $groups = ['Default'];

                if ($method === Request::METHOD_PUT) {
                    $groups[] = 'edit';
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = 'create';
                }

                return $groups;
            },
        ]);
    }
}