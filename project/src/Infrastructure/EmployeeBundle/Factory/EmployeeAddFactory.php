<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Factory;


use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\Employee\Factory\EmployeeAddFactoryInterface;
use Infrastructure\EmployeeBundle\Event\EmployeeCompanyEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EmployeeAddFactory implements EmployeeAddFactoryInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container, EventDispatcherInterface $dispatcher)
    {
        $this->container = $container;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param EmployeeCompany $employeeCompany
     * @return EmployeeCompany
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EmployeeCompany $employeeCompany)
    {
        $this->beforeSave();

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $employeeCompany->setCreatedAt(new \DateTime());
        $employeeCompany->setUpdatedAt(new \DateTime());
        $employeeCompany->setCreatedBy($user);
        $employeeCompany->setUpdatedBy($user);

        $em->persist($employeeCompany);
        $em->flush();

        $this->afterSave($employeeCompany);

        return $employeeCompany;
    }

    protected function beforeSave()
    {
        $this->dispatcher->dispatch(EmployeeCompanyEvent::EVENT_BEFORE_INSERT);
    }

    protected function afterSave($model)
    {
        $this->dispatcher->dispatch(EmployeeCompanyEvent::EVENT_AFTER_INSERT,
            new EmployeeCompanyEvent(compact('model'))
        );
    }
}