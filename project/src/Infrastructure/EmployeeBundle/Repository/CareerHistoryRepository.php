<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Employee\Entity\CareerHistory;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CareerHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CareerHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CareerHistory[]    findAll()
 * @method CareerHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CareerHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CareerHistory::class);
    }

    public function findAllQueryBuilder($alias = 'ch')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * Возвращает первый день когда пользователя приняли на работу
     *
     * @param User $user
     * @return CareerHistory|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCareerStartDay(User $user): ?CareerHistory
    {
        return $this->createQueryBuilder('ch')
            ->setMaxResults(1)
            ->orderBy('ch.createdAt', 'ASC')
            ->where('ch.user = :user AND ch.type = :type')
            ->setParameter('user', $user)
            ->setParameter('type', EmployeeCompany::STATUS_EMPLOYED)
            ->getQuery()
            ->getOneOrNullResult();
    }

}
