<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\Employee\Entity\EmployeeCompany;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EmployeeCompany|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmployeeCompany|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmployeeCompany[]    findAll()
 * @method EmployeeCompany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployeeCompanyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EmployeeCompany::class);
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findCountriesPrimaryAll()
    {
        $sql = 'select c.id as value, c.name as label 
                  from `em_employee_company` as ec
                      left join `u_user` as u ON ec.user_id = u.id
                      left join `u_user_address` address ON address.user_id = u.id
                      left join `lc_country` as c ON c.id = address.country_id
                  WHERE c.id IS NOT NULL
                group by c.id ORDER BY c.sort_order ASC';

        return $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql)
            ->fetchAll();
    }

    public function findAllQueryBuilder($alias = 'ec')
    {
        return $this->createQueryBuilder($alias);
    }
}
