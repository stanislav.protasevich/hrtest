<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Event;

use Domain\Employee\Entity\EmployeeCompany;
use Symfony\Component\EventDispatcher\Event;

class EmployeeCompanyEvent extends Event
{
    /**
     * @var EmployeeCompany
     */
    private $company;

    public function __construct(EmployeeCompany $company)
    {
        $this->company = $company;
    }

    /**
     * @return EmployeeCompany
     */
    public function getCompany(): EmployeeCompany
    {
        return $this->company;
    }
}
