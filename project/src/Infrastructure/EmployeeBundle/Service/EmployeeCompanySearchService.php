<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Employee\Entity\EmployeeCompany;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\EmployeeBundle\Command\EmployeeCompanyListCommand;

class EmployeeCompanySearchService extends FilterSortService
{
    public const START_AT_YEAR = 'year';
    public const START_AT_MONTH = 'month';

    protected $sortMapping = [
        'startAt' => 'ec.startAt',
        'probationPeriod' => 'ec.probationPeriod',
    ];

    public function search(EmployeeCompanyListCommand $command): QueryBuilder
    {
        $qb = $this->getEm()->getRepository(EmployeeCompany::class)->findAllQueryBuilder('ec');

        if ($command->startAt && $command->startAtFormat === self::START_AT_YEAR && $period = $this->periodFromString($command->startAt)) {
            [$start, $end] = $period;
            $qb->andWhere('ec.startAt BETWEEN :start AND :end')
                ->setParameter('start', $start)
                ->setParameter('end', $end);

        } elseif ($command->startAt && $command->startAtFormat === self::START_AT_MONTH && $period = $this->periodFromString($command->startAt)) {
            [$start, $end] = $period;
            $qb->andWhere('DATE_FORMAT(ec.startAt,\'%m-%d\') BETWEEN :start AND :end')
                ->setParameter('start', $start->format('m-d'))
                ->setParameter('end', $end->format('m-d'));
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
