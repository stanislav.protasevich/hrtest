<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\Employee\Entity\CareerHistory;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\EmployeeBundle\Command\CareerHistorySearchCommand;

class CareerHistorySearchService extends BaseService
{
    public function search(CareerHistorySearchCommand $command)
    {
        $qb = $this->getQuery();

        if ($command->userBy) {
            $qb->andWhere('ch.userBy IN (:userBy)')->setParameter('userBy', (array)$command->userBy);
        }

        if ($command->companyId) {
            $qb->andWhere('ch.companyId IN (:companyId)')->setParameter('companyId', (array)$command->companyId);
        }

        if ($command->jobTitleId) {
            $qb->andWhere('ch.jobTitleId IN (:jobTitleId)')->setParameter('jobTitleId', (array)$command->jobTitleId);
        }

        if ($command->type) {
            $qb->andWhere('ch.type IN (:type)')->setParameter('type', (array)$command->type);
        }

        return $qb;
    }

    public function getQuery(): QueryBuilder
    {
        return $this->getEm()->getRepository(CareerHistory::class)->findAllQueryBuilder();
    }
}