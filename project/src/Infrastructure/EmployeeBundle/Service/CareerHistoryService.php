<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Service;


use Domain\Employee\Entity\CareerHistory;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;

class CareerHistoryService extends BaseService
{
    /**
     * @param EmployeeCompany $company
     * @return CareerHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function employed(EmployeeCompany $company): CareerHistory
    {
        return $this->create($company, EmployeeCompany::STATUS_EMPLOYED);
    }

    /**
     * @param EmployeeCompany $company
     * @param string $type
     * @return CareerHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(EmployeeCompany $company, string $type)
    {
        $em = $this->getEm();

        $history = new CareerHistory();

        $history->setType($type);
        $history->setCompany($company->getCompany());
        $history->setUser($company->getUser());
        $history->setJobTitle($company->getJobTitle());

        $history->setCreatedAt($this->now());
        $history->setUpdatedAt($this->now());
        $history->setCreatedBy($this->getUser());
        $history->setUpdatedBy($this->getUser());

        $em->persist($history);

        $em->flush();

        return $history;
    }

    /**
     * @param EmployeeCompany $company
     * @return CareerHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function dismissed(EmployeeCompany $company): CareerHistory
    {
        return $this->create($company, EmployeeCompany::STATUS_DISMISSED);
    }

    /**
     * @param EmployeeCompany $company
     * @return CareerHistory
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function maternityLeave(EmployeeCompany $company): CareerHistory
    {
        return $this->create($company, EmployeeCompany::STATUS_MATERNITY_LEAVE);
    }

    /**
     * @param User $user
     * @return CareerHistory|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCareerStartDay(User $user): ?CareerHistory
    {
        return $this->getEm()->getRepository(CareerHistory::class)->getCareerStartDay($user);
    }
}
