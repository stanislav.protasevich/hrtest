<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Service;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Domain\Employee\Entity\EmployeeCompany;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\EmployeeBundle\Command\EmployeeCompanyCommand;
use Infrastructure\EmployeeBundle\Event\EmployeeCompanyEvent;
use Symfony\Component\PropertyAccess\Exception\InvalidPropertyPathException;

class EmployeeService extends BaseService
{
    /**
     * @param User $user
     * @param array $companies
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function multipleAdd(User $user, array $companies)
    {
        foreach ($companies as $employee_company) {
            if (!$employee_company instanceof EmployeeCompanyCommand) {
                throw new InvalidPropertyPathException(
                    'Object '.get_class($employee_company).' myst be instance of '.EmployeeCompanyCommand::class
                );
            }

            $this->addToCompany($user, $employee_company);
        }
    }

    /**
     * @param User $user
     * @param EmployeeCompanyCommand $command
     * @return EmployeeCompany
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addToCompany(User $user, EmployeeCompanyCommand $command): EmployeeCompany
    {
        event(EmployeeCompanyEvent::EVENT_BEFORE_INSERT);

        $em = $this->getEm();

        $employeeCompany = new EmployeeCompany();
        $employeeCompany->setCompany($command->company);
        $employeeCompany->setDepartment($command->department);
        $employeeCompany->setCreatedBy($this->getUser());
        $employeeCompany->setUpdatedBy($this->getUser());
        $employeeCompany->setCreatedAt(new \DateTime());
        $employeeCompany->setUpdatedAt(new \DateTime());

        $user->addEmployeeCompany($employeeCompany);

        $em->persist($employeeCompany);
        $em->flush();

        event(EmployeeCompanyEvent::EVENT_AFTER_INSERT, new EmployeeCompanyEvent([
            'model' => $employeeCompany,
        ]));

        return $employeeCompany;
    }

    /**
     * @param User $user
     * @param PersistentCollection|EmployeeCompany[]|ArrayCollection $employeeCompanies
     * @return EmployeeCompany[]
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateCompanyEmployee(User $user, $employeeCompanies)
    {
        $em = $this->getEm();

        event(EmployeeCompanyEvent::EVENT_BEFORE_UPDATE);

        $items = [];

        foreach ($employeeCompanies as $employeeCompany) {

            if (!$employeeCompany->getCreatedBy()) {
                $employeeCompany->setCreatedBy($this->getUser());
            }

            $employeeCompany->setUpdatedBy($this->getUser());

            $user->addEmployeeCompany($employeeCompany);

            $em->persist($employeeCompany);

            $items[] = $employeeCompany;
        }

        event(EmployeeCompanyEvent::EVENT_AFTER_UPDATE, new EmployeeCompanyEvent([
            'model' => $items,
        ]));

        return $items;
    }

    public function getCountriesPrimaryAll()
    {
        return $this->getEm()->getRepository(EmployeeCompany::class)->findCountriesPrimaryAll();
    }
}
