<?php

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Command;

use Doctrine\ORM\EntityRepository;
use Domain\Company\Entity\Company;
use Domain\Company\Entity\Department;
use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Infrastructure\CompanyBundle\Repository\CompanyRepository;
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Exception;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EmployeeCompanyCommand extends AbstractType implements DataMapperInterface
{
    use ObjectebleTrait;

    public $company;
    public $department;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department): void
    {
        $this->department = $department;
    }

    /**
     * Maps properties of some data to a list of forms.
     *
     * @param mixed $data Structured data
     * @param FormInterface[]|\Traversable $forms A list of {@link FormInterface} instances
     *
     * @throws Exception\UnexpectedTypeException if the type of the data parameter is not supported
     */
    public function mapDataToForms($data, $forms)
    {
        /* @var $data self */
        prnx($data);
        $forms = iterator_to_array($forms);
        $forms['company']->setData($data->company ?? null);
        $forms['department']->setData($data->department ?? null);
    }

    /**
     * Maps the data of a list of forms into the properties of some data.
     *
     * @param FormInterface[]|\Traversable $forms A list of {@link FormInterface} instances
     * @param mixed $data Structured data
     *
     * @throws Exception\UnexpectedTypeException if the type of the data parameter is not supported
     */
    public function mapFormsToData($forms, &$data)
    {
        /* @var $data self */
        $forms = iterator_to_array($forms);
        prnx($forms['company']->getData());
        $data->company = $forms['company']->getData();
        $data->department = $forms['department']->getData();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', EntityType::class, [
                'class' => Company::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (CompanyRepository $r) {
                    return $r->createQueryBuilder('c')
                        ->where('c.status = :status')
                        ->setParameter('status', Company::STATUS_ACTIVE);
                },
            ])
            ->add('department', EntityType::class, [
                'class' => Department::class,
                'constraints' => [
                    new NotBlank(),
                ],
                'query_builder' => function (EntityRepository $d) {
                    return $d->createQueryBuilder('d')
                        ->where('d.status = :status')
                        ->setParameter('status', Department::STATUS_ACTIVE);
                },
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                /* @var $data array */
                $data = $event->getData();
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmployeeCompanyCommand::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $config FormBuilder */
                $config = $form->getRoot()->getConfig();
                $method = $config->getMethod();

                $groups = ['Default'];

                if ($method === Request::METHOD_PUT) {
                    $groups[] = 'edit';
                } elseif ($method === Request::METHOD_POST) {
                    $groups[] = 'create';
                }

                return $groups;
            },
        ]);
    }
}