<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class EmployeeCompanyListCommand
{
    use ObjectebleTrait;

    /**
     * @var string|string[]
     */
    public $startAt;

    /**
     * @var string|string[]
     */
    public $probationPeriod;

    /**
     * @var string
     */
    public $startAtFormat;
}
