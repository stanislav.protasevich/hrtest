<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class CareerHistorySearchCommand
{
    use ObjectebleTrait;

    public $userBy;

    public $companyId;

    public $jobTitleId;

    public $type;
}