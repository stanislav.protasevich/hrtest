<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Rbac;

use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class CareerHistoryPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_CAREER_HISTORY_VIEW = 'careerHistoryView';

    public const PERMISSION_CAREER_HISTORIES = [
        self::PERMISSION_CAREER_HISTORY_VIEW,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_CAREER_HISTORY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}