<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\EmployeeBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class EmployeeCompanyPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_EMPLOYEE_COMPANY_LIST = 'employeeCompanyList';

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(self::PERMISSION_EMPLOYEE_COMPANY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
        ];
    }
}
