<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */
namespace Infrastructure\EmployeeBundle\EventListener;

use Infrastructure\EmployeeBundle\Service\CareerHistoryService;
use Infrastructure\UserBundle\Event\UserDismissalEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmployeeDismissalEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CareerHistoryService
     */
    private $service;

    public function __construct(ContainerInterface $container, CareerHistoryService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserDismissalEvent::EVENT_AFTER_DISMISSAL => 'onAfterDismissal',
        ];
    }

    public function onAfterDismissal(UserDismissalEvent $event)
    {

    }
}
