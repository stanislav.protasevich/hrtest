<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\EventListener;


use Infrastructure\UserBundle\Event\UserDismissalEvent;
use Infrastructure\UserBundle\Service\UserCompanyService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserDismissalEmployeeCompanyEventListener implements EventSubscriberInterface
{
    /**
     * @var UserCompanyService
     */
    private $service;

    public function __construct(UserCompanyService $service)
    {
        $this->service = $service;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            UserDismissalEvent::EVENT_AFTER_DISMISSAL => 'onAfterDismissal',
        ];
    }

    public function onAfterDismissal(UserDismissalEvent $event)
    {
        $this->service->dismissal($event->getUser());
    }
}
