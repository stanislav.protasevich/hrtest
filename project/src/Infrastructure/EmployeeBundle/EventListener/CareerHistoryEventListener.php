<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\EmployeeBundle\EventListener;

use Domain\Employee\Entity\EmployeeCompany;
use Infrastructure\EmployeeBundle\Event\CareerHistoryEvent;
use Infrastructure\EmployeeBundle\Service\CareerHistoryService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CareerHistoryEventListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var CareerHistoryService
     */
    private $service;

    public function __construct(ContainerInterface $container, CareerHistoryService $service)
    {
        $this->container = $container;
        $this->service = $service;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            EmployeeCompany::STATUS_EMPLOYED => 'onEmployed',
            EmployeeCompany::STATUS_DISMISSED => 'onDismissed',
            EmployeeCompany::STATUS_MATERNITY_LEAVE => 'onMaternityLeave',
        ];
    }

    public function onDismissed(CareerHistoryEvent $event)
    {
        $this->service->dismissed($event->getCompany());
    }

    public function onEmployed(CareerHistoryEvent $event)
    {
        $this->service->employed($event->getCompany());
    }

    public function onMaternityLeave(CareerHistoryEvent $event)
    {
        $this->service->maternityLeave($event->getCompany());
    }
}
