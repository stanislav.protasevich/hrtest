<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle;


use Infrastructure\CommonBundle\DependencyInjection\Compiler\QueuePass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureCommonBundle extends Bundle
{
    public function boot()
    {
        parent::boot();
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new QueuePass(), PassConfig::TYPE_AFTER_REMOVING);
    }
}