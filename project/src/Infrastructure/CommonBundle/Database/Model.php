<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Database;

use Infrastructure\CommonBundle\Contracts\Model\Arrayable;
use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class Model implements Arrayable
{
    use ArrayableTrait;
    use ObjectebleTrait;
}