<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Database;


use Doctrine\ORM\QueryBuilder;
use Infrastructure\CommonBundle\Service\BaseService;
use Symfony\Component\HttpFoundation\Request;

/**
 * @mixin BaseService
 */
trait SortableParamsTrait
{
    /**
     * @var string
     */
    private $sortParam = 'sort';
    /**
     * @var string
     */
    private $separator = ',';

    /**
     * @param Request $request
     * @param QueryBuilder $qb
     * @param array $sortMapping
     * @param string|null $defaultField
     * @param string|null $defaultOrder
     * @return void
     */
    protected function sorting(?Request $request, QueryBuilder $qb, array $sortMapping = [], string $defaultField = null, string $defaultOrder = 'ASC'): void
    {
        if (!$request) {
            return;
        }

        $hasOrder = false;

        if ($sort = $request->query->get($this->sortParam)) {
            $hasOrder = true;
            foreach ($this->parseSortParam((string)$sort) as $attribute) {
                $descending = false;

                if (strncmp($attribute, '-', 1) === 0) {
                    $descending = true;
                    $attribute = substr($attribute, 1);
                }

                if ($attr = $sortMapping[$attribute] ?? null) {

                    $order = $descending ? 'DESC' : 'ASC';

                    if (is_array($attr)) {
                        foreach ($attr as $item) {
                            $qb->addOrderBy($item, $order);
                        }
                    } else {
                        $qb->addOrderBy($attr, $order);
                    }
                }
            }
        }

        if (!$hasOrder && $defaultField && $defaultOrder) {
            $qb->addOrderBy($defaultField, $defaultOrder);
        }
    }

    /**
     * @param $param
     * @return array
     */
    protected function parseSortParam($param): array
    {
        return is_scalar($param) ? explode($this->separator, $param) : [];
    }
}
