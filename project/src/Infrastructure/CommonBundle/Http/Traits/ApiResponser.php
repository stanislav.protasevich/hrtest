<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CommonBundle\Http\Traits;


use Infrastructure\CommonBundle\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponser
{
    /**
     * Build success response
     * @param  string|array $data
     * @param  int $code
     * @return \Infrastructure\CommonBundle\Http\Response|Response
     */
    public function successResponse($data, ?int $code = Response::HTTP_OK)
    {
        return $this->response($data, $code);
    }

    protected function response($message, $code): Response
    {
        if ($message instanceof Response) {
            return $message->setStatusCode($code);
        }

        return response($message, $code)->header('Content-Type', 'application/json');
    }

    public function errorResponse($message, $code)
    {
        return $this->errorPlainResponse(['message' => $message, 'code' => $code], $code);
    }

    /**
     * Build error responses
     * @param  string|array $message
     * @param  int $code
     * @return JsonResponse
     */
    public function errorPlainResponse($message, $code)
    {
        return response()->json($message, $code);
//        return response()->json($message, $code);
    }

    /**
     * Build error responses
     * @param  string|array $message
     * @param  int $code
     * @return \Infrastructure\CommonBundle\Http\Response|Response
     */
    public function errorMessage($message, $code)
    {
        return $this->response($message, $code);
    }
}
