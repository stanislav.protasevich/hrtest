<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Web;

class ApiResponse extends BaseApiResponse
{
    public $groups = ['api'];
}