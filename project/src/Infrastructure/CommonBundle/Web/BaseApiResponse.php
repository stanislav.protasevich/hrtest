<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Web;

use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\CommonBundle\Serializer\ObjectNormalizer;
use Infrastructure\CommonBundle\Serializer\ResourceNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class BaseApiResponse
{
    /**
     * @var array
     */
    public $groups = [];
    /**
     * @var string
     */
    public $serializeFormat = 'json';
    /**
     * @var int
     */
    public $status = null;
    /**
     * @var array
     */
    public $headers = [];
    /**
     * @var JsonResponse
     */
    private $response;

    private $encodingOptions = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE;

    public function __construct(JsonResponse $response)
    {
        $this->response = $response;
    }

    public function groups($groups = null)
    {
        if ($groups) {
            $this->groups = (array)$groups;
        }

        return $this;
    }

    /**
     * @param null $data
     * @param array $headers
     * @return JsonResponse|Response
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function setContent($data = null, array $headers = [])
    {
        if ($data instanceof Response) {
            return $data;
        }

        $content = $this->populateContent($data);
        $normalized = $this->normalize($content);

        return $this->json($normalized, Response::HTTP_OK, $headers, false);
    }

    /**
     * @param null $content
     * @return mixed|string
     */
    protected function populateContent($content = null)
    {
        return $content;
    }

    /**
     * @param $data PaginatedCollection
     * @return bool|float|int|string
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    protected function normalize($data)
    {
        return $this->getSerializer()->normalize($data);
    }

    /**
     * @return Serializer
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    protected function getSerializer()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [
            new ResourceNormalizer(),
            new ObjectNormalizer($this->groups),
        ];

        return new Serializer($normalizers, $encoders);
    }

    protected function json($data = null, int $status = Response::HTTP_OK, array $headers = [], bool $json = false)
    {
        $this->response->setCharset('UTF-8');

        if ($json) {
            $this->response->setJson($data);
        } else {
            $this->response->setData($data);
        }

//        $this->response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
        $this->response->setEncodingOptions($this->encodingOptions);

        if ($this->status) {
            $this->response->setStatusCode($this->status);
        } else {
            $this->response->setStatusCode($status);
        }

        $headersArray = array_merge($this->headers, $headers);
        foreach ($headersArray as $key => $header) {
            $this->response->headers->set($key, $header);
        }

        return $this->response;
    }

    public function setHeaders(array $headers = [])
    {
        $this->headers = array_merge($this->headers, $headers);
        return $this;
    }

    public function setEncodingOptions($encodingOptions)
    {
        $this->encodingOptions = $this->encodingOptions | (int)$encodingOptions;

        return $this;
    }
}