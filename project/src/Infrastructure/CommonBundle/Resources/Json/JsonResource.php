<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Resources\Json;

use Infrastructure\CommonBundle\Pagination\PaginatedCollection;
use Infrastructure\CommonBundle\Resources\Resource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class JsonResource extends Resource
{
    /**
     * Create new anonymous resource collection.
     *
     * @param  mixed $resource
     * @return JsonResource
     */
    public static function collection($resource)
    {
        if ($resource instanceof PaginatedCollection) {
            return PaginatedResource::make($resource, get_called_class());
        } else {
            return CollectionResource::make($resource, get_called_class());
        }
    }

    public function toArray()
    {
        return $this->resolve($this->getRequest());
    }

    /**
     * Resolve the resource to an array.
     *
     * @param null $request
     * @return array
     */
    public function resolve($request = null)
    {
        return $this->map(
            $this->resource === null ? null : $this->fields($request)
        );
    }

    protected function getRequest(): Request
    {
        /* @var $request_stack RequestStack */
        $request_stack = app('request_stack');

        return $request_stack->getCurrentRequest();
    }

    /**
     * Return dummy data
     *
     * @return array
     */
    public function dummy()
    {
        return [];
    }
}
