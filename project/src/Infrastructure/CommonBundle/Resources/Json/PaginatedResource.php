<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Resources\Json;


use Infrastructure\CommonBundle\Pagination\PaginatedCollection;

/**
 * @property JsonResource[] $resource
 */
class PaginatedResource extends JsonResource
{
    /**
     * @var PaginatedCollection
     */
    private $pagination;

    public function __construct($resource, $pagination)
    {
        parent::__construct($resource);
        $this->pagination = $pagination;
    }

    public static function make(PaginatedCollection $collection, $collects)
    {
        $resources = array_map(function ($item) use ($collects) {
            return new $collects($item);
        }, $collection->getData());

        return new static($resources, $collection);
    }

    public function toArray()
    {
        $data = [];

        foreach ($this->resource as $resource) {
            $data[] = $resource->toArray();
        }

        $this->pagination->setData($data);

        return $this->pagination;
    }
}