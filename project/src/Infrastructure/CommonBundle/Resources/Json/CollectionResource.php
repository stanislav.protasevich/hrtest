<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Resources\Json;


use Infrastructure\CommonBundle\Pagination\PaginatedCollection;

/**
 * @property JsonResource[] $resource
 */
class CollectionResource extends JsonResource
{
    /**
     * @var PaginatedCollection|array
     */
    private $collection;

    public function __construct($resource, $collection)
    {
        parent::__construct($resource);
        $this->collection = $collection;
    }

    public static function make($collection, $collects)
    {
        $resources = array_map(function ($item) use ($collects) {
            return new $collects($item);
        }, $collection);

        return new static($resources, $collection);
    }

    public function toArray()
    {
        $data = [];
        foreach ($this->resource as $resource) {
            $data[] = $resource->toArray();
        }

        return $data;
    }
}