<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Resources;

use Doctrine\Common\Annotations\AnnotationReader;
use Infrastructure\CommonBundle\Contracts\Model\Arrayable;
use Infrastructure\RbacBundle\Annotation\Gate as GateAnnotation;
use Infrastructure\RbacBundle\Service\Gate;
use Symfony\Component\PropertyAccess\Exception\InvalidPropertyPathException;
use Zend\EventManager\Exception\InvalidCallbackException;

abstract class Resource implements Arrayable
{

    /**
     * The resource instance.
     *
     * @var mixed
     */
    public $resource;

    /**
     * @var \ReflectionClass
     */
    protected $reflectionClass;
    /**
     * @var AnnotationReader
     */
    protected $reader;

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function __construct($resource)
    {
        $this->resource = $resource;

        $this->reflectionClass = is_object($resource) ? new \ReflectionClass(get_class($resource)) : null;
        $this->reader = new AnnotationReader();
    }

    abstract public function toArray();

    public function fields()
    {
        return [];
    }

    /**
     * Returns the value of an object property.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when executing `$value = $object->property;`.
     * @param string $name the property name
     * @return mixed the property value
     */
    public function __get($name)
    {
        $getter = 'get' . $name;

        if (method_exists($this->resource, $getter)) {
            return $this->resource->$getter();
        } elseif (property_exists($this->resource, $name)) {
            return $this->resource->{$name};
        } elseif (method_exists($this, 'set' . $name)) {
            throw new InvalidCallbackException('Getting write-only property: ' . get_class($this) . '::' . $name);
        }

        throw new InvalidPropertyPathException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Calls the named method which is not a class method.
     *
     * Do not call this method directly as it is a PHP magic method that
     * will be implicitly called when an unknown method is being invoked.
     * @param string $name the method name
     * @param array $params method parameters
     * @throws \LogicException when calling unknown method
     * @return mixed the method return value
     */
    public function __call($name, $params)
    {
        if (method_exists($this->resource, $name)) {
            return call_user_func([$this->resource, $name], $params);
        }

        throw new \LogicException('Calling unknown method: ' . get_class($this) . "::$name()");
    }

    protected function map($resource)
    {
        if ($resource === null || is_string($resource) && strpos($resource, '__') === 0) {
            return null;
        }

        if (!is_array($resource)) {
            return $resource;
        }

        $data = [];

        foreach ($resource as $key => $value) {

            $value = $this->secureField($value, (string)$key);

            switch (gettype($value)) {
                case 'integer':
                    $data[$key] = (int)$value;
                    break;
                case 'string':
                    $data[$key] = (string)$value;
                    break;
                case 'boolean':
                    $data[$key] = (bool)$value;
                    break;

                case 'array':
                    $data[$key] = $this->map($value);
                    break;

                case 'object':

                    if ($value instanceof \Closure) {
                        $data[$key] = $value->call($this->resource, $this->resource);
                    } elseif ($value instanceof Arrayable) {
                        $data[$key] = $value->toArray();
                    } elseif ($value instanceof \DateTime) {
                        $data[$key] = $value;
                    }

                    break;

                default:
                    $data[$key] = $value;
            }
        }

        return $data;
    }

    /**
     * @param $value
     * @param string $key
     * @return string|integer|bool|object|null
     */
    protected function secureField($value, string $key)
    {
        if ($value && $this->reflectionClass && $this->reflectionClass->hasProperty($key)) {

            $property = $this->reflectionClass->getProperty($key);

            /** @var GateAnnotation $gateProp */
            if ($gateProp = $this->reader->getPropertyAnnotation($property, GateAnnotation::class)) {
                if (Gate::cannot($gateProp->permission)) {
                    $value = is_array($value) ? [] : null;
                }
            }
        }

        return $value;
    }
}
