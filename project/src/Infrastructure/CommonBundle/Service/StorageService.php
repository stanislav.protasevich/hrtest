<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Service;


class StorageService
{
    /**
     * @var string
     */
    private $targetDirectory;
    /**
     * @var string
     */
    private $targetDirectoryPublic;
    /**
     * @var string
     */
    private $webPath;

    public function __construct(string $targetDirectory, string $targetDirectoryPublic, string $webPath)
    {
        $this->targetDirectory = $targetDirectory;
        $this->targetDirectoryPublic = $targetDirectoryPublic;
        $this->webPath = $webPath;
    }

    /**
     * Direct path on server
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->getTargetDirectory();
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }

    /**
     * Web path for view in browser
     *
     * @return string
     */
    public function getWebPath()
    {
        return $this->webPath;
    }

    /**
     * @return string
     */
    public function getTargetDirectoryPublic(): string
    {
        return $this->targetDirectoryPublic;
    }
}