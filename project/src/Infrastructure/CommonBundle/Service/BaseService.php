<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Service;


use Doctrine\ORM\EntityManager;
use Domain\User\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseService
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var EntityManager
     */
    protected $_em;
    /**
     * @var User
     */
    protected $_user;

    /**
     * @var Request
     */
    protected $_request;
    /**
     * @var \DateTime
     */
    private $_now;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function __destruct()
    {
        if ($this->_em) {
            $this->_em->close();
            $this->_em = null;
        }
    }

    /**
     * @return EntityManager
     */
    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->container->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    /**
     * @return User
     */
    protected function getUser(): User
    {
        if (!$this->_user) {
            $this->_user = $this->container->get('security.token_storage')->getToken()->getUser();
        }

        return $this->_user;
    }

    public function setUser(User $user)
    {
        $this->_user = $user;
    }

    /**
     * @return Request
     */
    protected function getRequest(): ?Request
    {
        if (!$this->_request) {
            /** @var RequestStack $requestStack */
            $requestStack = $this->getContainer()->get('request_stack');

            $this->_request = $requestStack->getCurrentRequest();
        }

        return $this->_request;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return \DateTime
     */
    protected function now(): \DateTime
    {
        if (!$this->_now) {
            $this->_now = new \DateTime();
        }

        return clone $this->_now;
    }

    protected function event(string $name, Event $event = null)
    {
        if (!$this->dispatcher) {
            $this->dispatcher = $this->get('event_dispatcher');
        }

        $this->dispatcher->dispatch($name, $event);
    }

    public function get(string $name)
    {
        return $this->getContainer()->get($name);
    }
}
