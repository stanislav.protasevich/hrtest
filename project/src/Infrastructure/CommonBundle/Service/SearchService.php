<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Service;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Zend\EventManager\Exception\InvalidCallbackException;

/**
 */
class SearchService
{
    /**
     * @var Client
     */
    private static $instance;

    public function __construct()
    {
        if (!static::$instance) {
            static::$instance = ClientBuilder::create()
                ->setHosts($this->getHosts())
                ->setRetries(1)
                ->build();
        }
    }

    protected function getHosts()
    {
        return explode(',', $_SERVER['ELASTICSEARCH_HOSTS'] ?? '');
    }

    public function __call($name, $arguments)
    {
        if (method_exists(static::$instance, $name)) {
            return call_user_func([static::$instance, $name], ...$arguments);
        }

        throw new InvalidCallbackException('Method does not exist: ' . get_class($this) . '::' . $name);
    }
}