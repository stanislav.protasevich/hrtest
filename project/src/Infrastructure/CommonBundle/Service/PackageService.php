<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Service;


use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class PackageService
{
    /**
     * @var Package
     */
    private $package;

    /**
     * @var Request
     */
    private $request;

    public function __construct(StorageService $service, RequestStack $requestStack)
    {
        $this->package = new PathPackage($service->getWebPath(), new EmptyVersionStrategy());
        $this->request = $requestStack->getCurrentRequest();
    }

//    public function __call($name, $arguments)
//    {
//        if (method_exists($this->package, $name)) {
//            return call_user_func([$this->package, $name], ...$arguments);
//        }
//
//        throw new InvalidCallbackException('Method does not exist: ' . get_class($this) . '::' . $name);
//    }

    public function getUrl(string $path)
    {
        if ($this->isAbsoluteUrl($path)) {
            return $path;
        }

        return $this->request->getSchemeAndHttpHost() . $this->package->getUrl($path);
    }

    public function isAbsoluteUrl($url)
    {
        return false !== strpos($url, '://') || '//' === substr($url, 0, 2);
    }

    public function getBasePath()
    {
        return $this->package->getBasePath();
    }
}