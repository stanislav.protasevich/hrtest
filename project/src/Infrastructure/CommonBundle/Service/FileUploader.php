<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Service;

use Application\Kernel;
use Infrastructure\CommonBundle\Helpers\FileHelper;
use Symfony\Component\HttpFoundation\File\File;

class FileUploader
{
    private $path;
    
    /**
     * @var StorageService
     */
    private $storageService;

    public function __construct(StorageService $storageService)
    {
        $this->storageService = $storageService;
    }

    /**
     * @param File $file
     * @return string
     * @throws \Exception
     */
    public function upload(File $file)
    {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();

        $path = $this->getFullPath();

        FileHelper::createDirectory($path);

        $file->move($path, $fileName);

        return $fileName;
    }

    public function getFullPath()
    {
        return $this->getTargetDirectory();
    }

    public function getTargetDirectory()
    {
        return $this->storageService->getPath() . DIRECTORY_SEPARATOR . $this->getPath();
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
}