<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Service;

use Infrastructure\CommonBundle\Database\LikeQueryHelpers;
use Infrastructure\CommonBundle\Database\SortableParamsTrait;

class FilterSortService extends BaseService
{
    use LikeQueryHelpers, SortableParamsTrait;

    /**
     * Маппинг для сортировок, используется, например:
     * [
     *      'id' => 'user.id',
     *      'email' => 'user.email',
     *      'name' => ['profile.firstName', 'profile.middleName', 'profile.surname']
     * ];
     * где ключ это значение которое пришло в параметрах с запросом, а значение это поля в БД через join
     * @var array
     */
    protected $sortMapping = [];

    /**
     * Конвертирует параметры в строку через разделитель в массив
     * @param string $params
     * @param string $delimiter
     * @return array
     */
    protected function paramsFromString(string $params, string $delimiter = ','): array
    {
        return array_map(function ($i) {
            return filter_var($i, FILTER_SANITIZE_STRING);
        }, explode($delimiter, $params));
    }

    /**
     * Преобразование параметра, 2018-11-01T00:00 to 2019-01-31T00:00 в массив дат
     * @param string $params
     * @return \DateTime[]
     */
    protected function periodFromString(string $params): ?array
    {
        return $this->processPeriodParamsDate($params, 'stringToDate');
    }

    protected function processPeriodParamsDate(string $params, string $function)
    {
        $period = explode('to', $params);

        if (!is_array($period) && count($period) !== 2) {
            return null;
        }

        $periods = array_map(function ($item) use ($function) {
            return call_user_func([$this, $function], trim($item));
        }, $period);

        if (count(array_filter($periods)) !== 2) {
            return null;
        }

        return $periods;
    }

    /**
     * Преобразование параметра, 01-13 to 02-10 в массив
     * @param string $params
     * @return array|null
     */
    protected function periodMonthParamsFromString(string $params): ?array
    {
        return $this->processPeriodParamsDate($params, 'stringToMonthDayDate');
    }

    protected function stringToMonthDayDate(?string $date): ?string
    {
        preg_match('/^[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}(:[0-9]{2})?)?/', $date, $matches);

        if (!isset($matches[0])) {
            return null;
        }

        return $matches[0];
    }

    protected function stringToDate(?string $date): ?\DateTime
    {
        preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}(:[0-9]{2})?)?/', $date, $matches);

        if (!isset($matches[0])) {
            return null;
        }

        try {
            return new \DateTime(trim($matches[0]));
        } catch (\Exception $exception) {
            return null;
        }
    }

    /**
     * @param string $class
     * @param array $ids
     * @return array|object[]
     */
    protected function getModel(string $class, array $ids = []): array
    {
        $items = [];

        foreach ($ids as $item) {

            if (isset($items[$item])) {
                continue;
            }

            if ($result = $this->getEm()->getRepository($class)->find(filter_var($item, FILTER_SANITIZE_STRING))) {
                $items[$item] = $result;
            }
        }

        return array_values($items);
    }
}
