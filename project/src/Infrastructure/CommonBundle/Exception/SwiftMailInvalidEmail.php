<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

class SwiftMailInvalidEmail extends \Exception
{

    protected $statusCode;

    public function __construct(string $message = "", int $statusCode = Response::HTTP_NOT_ACCEPTABLE, int $code = 0, \Throwable $previous = null)
    {
        $this->statusCode = $statusCode;
        parent::__construct($message, $code, $previous);
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
