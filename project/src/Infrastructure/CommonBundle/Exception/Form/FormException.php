<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Exception\Form;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FormException extends HttpException
{
    /**
     * @var array
     */
    private $messages = [];

    /**
     * FormException constructor.
     * @param int $statusCode
     * @param string|null $message
     * @param \Exception|null $previous
     * @param array $headers
     * @param int|null $code
     * @param array $messages
     */
    public function __construct(int $statusCode, string $message = null, \Exception $previous = null, array $headers = [], ?int $code = 0, array $messages = [])
    {
        $this->messages = $messages;
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    /**
     * @param array $messages
     * @return static
     */
    public static function withMessages(array $messages)
    {
        return new static(Response::HTTP_UNPROCESSABLE_ENTITY, null, null, [], null, $messages);
    }

    /**
     * @return array
     */
    public function getMessages(): array
    {
        return $this->messages;
    }
}