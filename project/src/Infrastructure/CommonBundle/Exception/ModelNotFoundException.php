<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Exception;


use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Infrastructure\CommonBundle\Helpers\EntityHelper;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ModelNotFoundException extends \RuntimeException
{
    public function __construct($model, $ids, string $message = "", int $code = 0, Throwable $previous = null)
    {
        $this->setModel($model, $ids);
        parent::__construct($this->message, Response::HTTP_NOT_FOUND, $previous);
    }

    /**
     * Set the entity and instance ids.
     *
     * @param  string $model
     * @param  int|array $ids
     * @return $this
     */
    public function setModel($model, $ids = [])
    {
        $ids = ArrayHelper::wrap($ids);

        $model = EntityHelper::name($model);

        $this->message = "No query results for model [{$model}]";

        if (count($ids) > 0) {
            $this->message .= ' ' . implode(', ', $ids);
        } else {
            $this->message .= '.';
        }

        return $this;
    }
}
