<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Serializer;


use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\PersistentCollection;
use Infrastructure\CommonBundle\Serializer\Annotation\ExclusionPolicy;
use Infrastructure\CommonBundle\Serializer\Annotation\Expose;
use Infrastructure\CommonBundle\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ObjectNormalizer implements NormalizerInterface
{
    private $maxNestingLevel = 0;

    /**
     * @var AnnotationReader
     */
    private $annotationReader;
    /**
     * @var array
     */
    private $groups;

    /**
     * ObjectNormalizer constructor.
     * @param array $groups
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function __construct(array $groups = [])
    {
        $this->annotationReader = new AnnotationReader();
        $this->groups = $groups;
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param mixed $object Object to normalize
     * @param string $format Format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|int|float|bool
     *
     * @throws \ReflectionException
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return $this->resolve($object);
    }

    /**
     * @param $object
     * @return array
     * @throws \ReflectionException
     */
    protected function resolve($object)
    {
        return is_array($object) ? $this->map($object) : $this->toArray($object);
    }

    /**
     * @param $object
     * @return array
     * @throws \ReflectionException
     */
    protected function map($object)
    {
        $data = [];

        foreach ($object as $key => $item) {
            $data[] = $this->toArray($item);
        }

        return $data;
    }

    /**
     * @param $object
     * @param int $maxNestingLevel
     * @return array
     * @throws \ReflectionException
     */
    protected function toArray($object, $maxNestingLevel = 0)
    {
        if (!is_object($object)) {
            return $object;
        }

        $reflectionClass = new \ReflectionClass(get_class($object));
        $exclusion = $this->isClassExclusion($reflectionClass);

        $attributes = $reflectionClass->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED);

        $data = [];

        foreach ($attributes as $attribute) {

            $attributeName = $attribute->name;

            if (strpos($attributeName, '__') === 0) {
                continue;
            }

            $getter = 'get' . $attributeName;

            if ($maxNestingLevel > $this->maxNestingLevel) {
                break;
            }

            if (method_exists($object, $getter)) {
                $reflectionMethod = new \ReflectionMethod($object, $getter);
                if (!$reflectionMethod->isPublic()) {
                    continue;
                }

                $value = call_user_func([$object, $getter]);
            } elseif (property_exists($object, $attributeName)) {
                if (!$attribute->isPublic() || $attribute->isStatic()) {
                    continue;
                }
                $value = $object->$attributeName;
            } else {
                continue;
            }

            if ($exclusion) {
                $reflectionProperty = new \ReflectionProperty($object, $attributeName);
                $propertyAnnotations = $this->annotationReader->getPropertyAnnotations($reflectionProperty);

                $exposes = true;
                $exposesGroups = [];

                foreach ($propertyAnnotations as $propertyAnnotation) {
                    if ($propertyAnnotation instanceof Expose) {
                        $exposes = false;
                    }
                    if ($propertyAnnotation instanceof Groups) {
                        $exposesGroups = $propertyAnnotation->groups;
                    }
                }

                // исключать
                if ($exposes) {
                    continue;
                }

                if (!empty($this->groups)) {
                    $diff = array_diff($exposesGroups, $this->groups);
                    if (!empty($diff)) {
                        continue;
                    }
                }
            }

            if ($value instanceof PersistentCollection) {
                $maxNestingLevel++;
                foreach ($value as $key => $persistentCollection) {
                    $data[$attributeName][$key] = $this->toArray($persistentCollection, $maxNestingLevel);
                }

            } elseif (is_object($value)) {
                $maxNestingLevel++;
                $data[$attributeName] = $this->toArray($value, $maxNestingLevel);
            } elseif (is_array($value)) {
                $maxNestingLevel++;
                foreach ($value as $key => $persistentCollection) {
                    $data[$attributeName][$key] = $this->toArray($persistentCollection, $maxNestingLevel);
                }
            } else {
                $data[$attributeName] = $value;
            }
        }

        return $data;
    }

    protected function isClassExclusion(\ReflectionClass $reflectionClass): bool
    {
        $classAnnotations = $this->annotationReader->getClassAnnotations($reflectionClass);

        $exclusion = false;

        foreach ($classAnnotations as $classAnnotation) {
            if ($classAnnotation instanceof ExclusionPolicy) {
                $exclusion = $classAnnotation->policy === ExclusionPolicy::ALL ? true : false;
            }
        }
        return $exclusion;
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize
     * @param string $format The format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        if (is_object($data)) {
            return true;
        } elseif (is_array($data)) {
            return is_object(array_shift($data));
        }

        return false;
    }

    /**
     * @param int $maxNestingLevel
     */
    public function setMaxNestingLevel(int $maxNestingLevel): void
    {
        $this->maxNestingLevel = $maxNestingLevel;
    }
}