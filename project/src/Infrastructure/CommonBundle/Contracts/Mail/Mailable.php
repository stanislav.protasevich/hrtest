<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Contracts\Mail;


interface Mailable
{
    /**
     * Send the message using the given mailer.
     *
     * @return void
     */
    public function send();
}