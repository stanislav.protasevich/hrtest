<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Contracts\Support;


interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0);
}