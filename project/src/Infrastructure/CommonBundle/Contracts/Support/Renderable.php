<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Contracts\Support;


interface Renderable
{
    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render();
}