<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Factory\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\{DateType, TextType};
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\{NotBlank, NotNull};

class JsonFormFactory
{
    protected $typesMap = [
        'date' => DateType::class,
        'text' => TextType::class,
        'string' => TextType::class,
        'entity' => EntityType::class,
    ];

    protected $constraints = [
        'required' => [
            NotBlank::class,
            NotNull::class,
        ]
    ];

    /**
     * @var FormBuilder
     */
    private $builder;

    public function __construct(FormBuilder $builder)
    {
        $this->builder = $builder;
    }

    public function __invoke()
    {

    }

    public function add(array $item)
    {

        $options = $item['options'] ?? [];

        $constraint = [];

        if (isset($item['required']) && $item['required']) {
            $constraint[] = new NotBlank();
        }

        $this->builder->add(
            $item['name'],
            $options['type'] ?? TextType::class,
            [
                'constraints' => $constraint,
            ]
        );
        
        return $this;
    }
}