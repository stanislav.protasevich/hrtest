<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Factory;

use Symfony\Component\DependencyInjection\Container;

trait ModelFactoryTrait
{
    private $scenario = ['Default'];

    /**
     * @param string $scenario
     * @return ModelFactoryTrait
     */
    public function setScenario($scenario)
    {
        if (is_array($scenario)) {
            $this->scenario = array_merge($this->scenario, $scenario);
        } else {
            $this->scenario[] = $scenario;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getScenario(): array
    {
        return $this->scenario;
    }

    /**
     * @param string $type
     * @return array
     * @throws \ReflectionException
     */
    public function methods($type = 'set')
    {
        $class = new \ReflectionClass($this);

        $names = [];

        foreach ($class->getMethods(\ReflectionProperty::IS_PUBLIC) as $method) {
            if (!$method->isStatic() && strstr($method->getName(), $type)) {
                $names[] = $method->getName();
            }
        }

        return $names;
    }

    /**
     * @param array $values
     * @throws \ReflectionException
     */
    public function setAttributes(array $values)
    {
        if (is_array($values)) {
            $methods = array_flip($this->methods());
            foreach ($values as $name => $value) {
                $name = 'set' . Container::camelize($name);
                if (isset($methods[$name])) {
                    call_user_func(array($this, $name), $value);
                }
            }
        }
    }

    /**
     * @param array|null $data
     * @throws \ReflectionException
     */
    public function load(?array $data = [])
    {
        if (!empty($data)) {
            $this->setAttributes($data);
        }
    }
}