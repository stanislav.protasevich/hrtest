<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Factory;

trait ObjectebleTrait
{
    use PropertyAccessTrait;

    public function __construct(array $values = [])
    {
        foreach ($values as $property => $value) {

            $setter = 'set'.$property;

            if (method_exists($this, $setter)) {
                call_user_func(array($this, $setter), $value);
            } elseif (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}