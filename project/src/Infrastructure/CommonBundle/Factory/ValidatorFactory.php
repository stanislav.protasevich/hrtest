<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Factory;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilderInterface;

class ValidatorFactory
{
    /** @var ValidatorBuilderInterface */
    private $validator;

    /**
     * CommandCommandValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate($command)
    {
        $errors = [];

        if (($violations = $this->validator->validate($command))) {
            /* @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }
        }

        return $errors;
    }
}