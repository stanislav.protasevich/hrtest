<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Factory;


use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\CommonBundle\Exception\Form\FormFactoryException;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

abstract class AbstractFactory
{
    public const
        CREATE = 'POST',
        REPLACE = 'PUT',
        UPDATE = 'PATCH';

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var string
     */
    protected $formClass;

    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;

        if (!$this->formClass) {
            throw new FormFactoryException();
        }
    }

    /**
     * @param string $action
     * @param array $data
     * @param null $object
     * @return mixed
     * @throws FormException
     */
    protected function execute(string $action = self::CREATE, array $data, $object = null)
    {
        $form = $this->createForm($action, $object)->submit($data, self::UPDATE !== $action);

        if (!$form->isValid()) {
            throw FormException::withMessages($this->getErrorsFromForm($form));
        }

        return $form->getData();
    }

    private function createForm(string $action = self::CREATE, $object = null): FormInterface
    {
        return $this->formFactory->create($this->formClass, $object, [
            'method' => $action
        ]);
    }

    protected function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            /* @var $error FormError */
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }
}