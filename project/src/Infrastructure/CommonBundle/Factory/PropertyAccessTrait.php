<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Factory;


use Symfony\Component\PropertyAccess\Exception\InvalidPropertyPathException;
use Zend\EventManager\Exception\InvalidCallbackException;


trait PropertyAccessTrait
{

    public function __get($name)
    {
        $getter = 'get'.$name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (method_exists($this, 'set'.$name)) {
            throw new InvalidCallbackException('Getting write-only property: '.get_class($this).'::'.$name);
        }

        throw new InvalidPropertyPathException('Getting unknown property: '.get_class($this).'::'.$name);
    }

    public function __set($name, $value)
    {
        $setter = 'set'.$name;
        if (method_exists($this, $setter)) {
            $this->$setter($value);
        } elseif (method_exists($this, 'get'.$name)) {
            throw new InvalidCallbackException('Setting read-only property: '.get_class($this).'::'.$name);
        } else {
            throw new InvalidPropertyPathException('Setting unknown property: '.get_class($this).'::'.$name);
        }
    }

    public function __isset($name)
    {
        $getter = 'get'.$name;
        if (method_exists($this, $getter)) {
            return $this->$getter() !== null;
        }

        return false;
    }
}