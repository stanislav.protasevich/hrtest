<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Factory;


class DynamicObject
{
    private $_attributes = [];

    public function getAttributes()
    {
        return $this->_attributes;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return $this->_attributes[$name];
        }

        return null;
    }

    public function __set($name, $value)
    {
        $this->_attributes[$name] = $value;
    }

    public function __isset($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            return isset($this->_attributes[$name]);
        }

        return false;
    }

    public function __unset($name)
    {
        if (array_key_exists($name, $this->_attributes)) {
            unset($this->_attributes[$name]);
        }
    }

    public function setBatchAttributes(array $data = [])
    {
        foreach ($data as $key => $item) {
            $this->$key = $item;
        }
    }
}
