<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */


declare(strict_types=1);

namespace Infrastructure\CommonBundle\Helpers;


use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class FormHelper
{
    public static function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            /* @var $error FormError */
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = static::getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
//                    if (ArrayHelper::isAssociative($childErrors) || ArrayHelper::isMulti($childErrors)) {
//                        $errors[$childForm->getName()] = $childErrors;
//                    } else {
//                        $errors[$childForm->getName()] = $childErrors[0];
//                    }
                }
            }
        }

        return $errors;
    }

    /**
     * Собирает все поля формы в массив
     * @param FormInterface $form
     * @return array
     */
    protected static function getFieldsTree(FormInterface $form): array
    {
        $fields = [];

        foreach ($form->all() as $key => $item) {
            $fields[$key] = self::getFieldsTree($item);
        }

        return $fields;
    }
}