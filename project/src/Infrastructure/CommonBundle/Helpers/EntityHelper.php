<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Helpers;


class EntityHelper
{
    public static function name($entity): string
    {
        if (is_object($entity)) {
            $entity = get_class($entity);
        }

        if (!is_string($entity)) {
            throw new \LogicException('Entity must be string type.');
        }

        $data = explode('\\', $entity);

        return $data[count($data) - 1];
    }
}
