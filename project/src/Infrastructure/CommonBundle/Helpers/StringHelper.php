<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Helpers;


class StringHelper
{
    public static function microtimeLong()
    {
        return str_replace('.', '', microtime(true));
    }

    /**
     * Safely casts a float to string independent of the current locale.
     *
     * The decimal separator will always be `.`.
     * @param float|int $number a floating point number or integer.
     * @return string the string representation of the number.
     */
    public static function floatToString($number)
    {
        // . and , are the only decimal separators known in ICU data,
        // so its safe to call str_replace here
        return str_replace(',', '.', (string)$number);
    }

    public static function isBase64File(?string $file = '')
    {
        if (empty($file)) {
            return false;
        }

//        $pattern = '/^data:image\/\w+;base64,\S+$/';
        $pattern = '/^data\:\w+\/[\w\.-]+\;base64\,\S+$/';

        preg_match($pattern, $file, $matches, PREG_OFFSET_CAPTURE);

        return !empty($matches);
    }


    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param  int $length
     * @return string
     * @throws \Exception
     */
    public static function random($length = 16)
    {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = random_bytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }
}