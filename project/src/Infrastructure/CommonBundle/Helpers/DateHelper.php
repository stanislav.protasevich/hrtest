<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Helpers;


class DateHelper
{
    public static function isEqual(\DateTime $dateStart, \DateTime $dateEnd, string $format = 'Y-m-d'): bool
    {
        return $dateStart->format($format) === $dateEnd->format($format);
    }
}
