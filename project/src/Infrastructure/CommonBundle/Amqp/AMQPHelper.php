<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Amqp;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AMQPHelper
{
    public const EXCHANGE_NOTIFICATIONS = 'notification.v1.alert';
    public const QUEUE_NOTIFICATIONS = 'notification.v1.alert';

    public static function initNotifications(AMQPChannel $channel, string $name, string $type): void
    {
        $channel->queue_declare($name, false, false, false, true);
        $channel->exchange_declare($name, $type, false, false, true);
        $channel->queue_bind($name, $name);
    }

    public static function registerShutdown(AMQPStreamConnection $connection, AMQPChannel $channel): void
    {
        register_shutdown_function(function (AMQPChannel $channel, AMQPStreamConnection $connection) {
            $channel->close();
            $connection->close();
        }, $channel, $connection);
    }
}