<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Consumer;


use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailConsumer implements ConsumerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var \Swift_Transport
     */
    private $swiftTransport;

    public function __construct(ContainerInterface $container, \Swift_Transport $swiftTransport)
    {
        $this->container = $container;
        $this->swiftTransport = $swiftTransport;
    }

    /**
     * @param AMQPMessage $msg
     * @return void
     */
    public function execute(AMQPMessage $msg)
    {
        $body = $msg->getBody();
        try {
            $message = unserialize($body);
prnx($message);
            $transport = $this->getTransport();
            $transport->send($message);
            $transport->stop();

        } catch (\Exception $exception) {
            echo 'ERROR'.PHP_EOL;
            $this->container->get('old_sound_rabbit_mq.delayed_send_email_producer')->publish($body);
        }
    }

    /** @return \Swift_Transport */
    protected function getTransport()
    {
        /** @var \Swift_Transport $swiftTransport */
        $swiftTransport = $this->container->get('swiftmailer.mailer.default.transport.real');

        if (!$swiftTransport->isStarted()) {
            $swiftTransport->start();
        }

        return $swiftTransport;
    }
}
