<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Listener;

use GuzzleHttp\Exception\ClientException;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use Infrastructure\CommonBundle\Http\Traits\ApiResponser;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Translation\TranslatorInterface;

class ResponseExceptionListener
{
    use ApiResponser;

    /**
     * @var Kernel
     */
    private $kernel;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(Kernel $kernel, LoggerInterface $logger, ContainerInterface $container)
    {
        $this->kernel = $kernel;
        $this->logger = $logger;
        $this->translator = $container->get('translator');
        $this->container = $container;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $event->setResponse(
                $this->errorMessage($exception->getMessage(), Response::HTTP_METHOD_NOT_ALLOWED, $exception)
            );
        }

        if ($exception instanceof FormException) {
            return $event->setResponse(
                $this->errorPlainResponse(
                    $exception->getMessages(),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                )
            );
        }

        if ($exception instanceof ModelNotFoundException) {
            return $event->setResponse(
                $this->errorMessage($exception->getMessage(), Response::HTTP_NOT_FOUND, $exception)
            );
        }

        if($exception instanceof HttpException) {
            $code = $exception->getStatusCode();
            $message = $exception->getMessage();
            return $event->setResponse(
                $this->errorMessage($message, $code, $exception)
            );
        }

        if ($exception instanceof ClientException) {
            return $event->setResponse(
                $this->errorMessage(
                    $exception->getResponse()->getBody(),
                    $exception->getCode(),
                    $exception
                )
            );
        }

        if ($this->kernel->isDebug()) {
            return $event->setResponse(
                $this->errorMessage(
                    $exception->getMessage(),
                    Response::HTTP_INTERNAL_SERVER_ERROR,
                    $exception
                )
            );
        }

        return $event->setResponse(
            $this->errorMessage(
                $this->getProdResponse(),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $exception
            )
        );
    }

    protected function errorMessage($message, $code, \Exception $exception)
    {
        $content['message'] = $message;

        if ($this->kernel->isDebug()) {
            $content['trace'] = $exception->getTrace();
        }

        return $this->errorPlainResponse($content, $code);
    }

    protected function getProdResponse()
    {
        return $this->translator->trans('An internal server error occurred.');
    }

    protected function render($message, $code)
    {
        return response($message, $code)->header('Content-Type', 'application/json');
    }
}
