<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Infrastructure\CommonBundle\Listener;


use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class RequestListener
{
    public function onKernelController(FilterControllerEvent $event)
    {
    }
}