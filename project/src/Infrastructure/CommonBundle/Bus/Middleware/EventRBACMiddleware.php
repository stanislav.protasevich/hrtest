<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Bus\Middleware;


use League\Tactician\Middleware;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventRBACMiddleware implements Middleware
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $returnValue = $next($command);

        $this->dispatcher->dispatch(get_class($command));
        
        return $returnValue;
    }
}