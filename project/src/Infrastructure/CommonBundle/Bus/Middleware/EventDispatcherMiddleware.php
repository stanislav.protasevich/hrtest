<?php declare(strict_types=1);

namespace Infrastructure\CommonBundle\Bus\Middleware;

use Infrastructure\CommonBundle\Event\CommandEvent;
use League\Tactician\CommandBus;
use League\Tactician\Middleware;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventDispatcherMiddleware implements Middleware
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param object $command
     * @param callable|CommandBus $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
//        (new \ReflectionClass($command))->getShortName(),
//        $this->dispatcher->dispatch(
//            get_class($command),
//            new CommandEvent($command)
//        );

        return $next($command);
    }
}
