<?php

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Bus\Middleware;

use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use League\Tactician\Middleware;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\TraceableValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EventValidationMiddleware implements Middleware
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ValidatorInterface|TraceableValidator
     */
    protected $validator;

    /**
     * Dependency Injection constructor.
     *
     * @param LoggerInterface $logger
     * @param ValidatorInterface $validator
     */
    public function __construct(LoggerInterface $logger, ValidatorInterface $validator)
    {
        $this->logger = $logger;
        $this->validator = $validator;
    }

    /**
     * @param object $command
     * @param callable $next
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $groups = null;

        if (property_exists($command, 'groups')) {
            $groups = array_merge(['Default'], (array)$command->groups);
        }

        $violations = $this->validator->validate($command, null, $groups);

        if (count($violations) !== 0) {
            $errors = [];

            foreach ($violations as $violation) {
//                $errors[$violation->getPropertyPath()] = $violation->getMessage();
                $field = $violation->getPropertyPath();
                $message = $violation->getMessage();

                ArrayHelper::setValue($errors, $field, [$message]);
            }

            $this->logger->error('Validation exception', $errors);

            throw FormException::withMessages($errors);
        }

        return $next($command);
    }
}