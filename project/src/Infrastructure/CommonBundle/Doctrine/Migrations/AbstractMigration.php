<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Doctrine\Migrations;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractMigration extends \Doctrine\Migrations\AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EntityManager
     */
    private $_em;

    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->container->get('doctrine.orm.entity_manager');
        }

        return $this->_em;
    }

    public function get(string $name)
    {
        return $this->container->get($name);
    }
}