<?php declare(strict_types=1);

/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Doctrine\Listener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Infrastructure\CommonBundle\Doctrine\Model;

class ModelListener
{
    /**
     * @ORM\PrePersist()
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function prePersistHandler(Model $model, LifecycleEventArgs $event)
    {
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
    }

    /**
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function postPersistHandler(Model $model, LifecycleEventArgs $event)
    {
    }

    /**
     * @ORM\PreUpdate()
     * @param Model $model
     * @param PreUpdateEventArgs $event
     */
    public function preUpdateHandler(Model $model, PreUpdateEventArgs $event)
    {
        $model->setChanges(
            $event->getEntityChangeSet()
        );
    }

    /**
     * @ORM\PostUpdate()
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function postUpdateHandler(Model $model, LifecycleEventArgs $event)
    {
    }

    /**
     * @ORM\PostRemove()
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function postRemoveHandler(Model $model, LifecycleEventArgs $event)
    {
    }

    /**
     * @ORM\PreRemove()
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function preRemoveHandler(Model $model, LifecycleEventArgs $event)
    {
    }

    /**
     * @ORM\PreFlush()
     * called second
     * @param Model $model
     * @param PreFlushEventArgs $event
     */
    public function preFlushHandler(Model $model, PreFlushEventArgs $event)
    {
    }

    /**
     * @ORM\PostLoad()
     * called first
     * @param Model $model
     * @param LifecycleEventArgs $event
     */
    public function postLoadHandler(Model $model, LifecycleEventArgs $event)
    {
    }
}
