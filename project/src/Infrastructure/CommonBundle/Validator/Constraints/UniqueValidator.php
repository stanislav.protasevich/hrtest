<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{
    Constraint, ConstraintValidator
};

class UniqueValidator extends ConstraintValidator
{
    protected $operations = [
        '=', '!=', '!==', '>', '<'
    ];

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct()
    {
        $this->container = app('service_container');
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint|Unique $constraint The constraint for the validation
     * @return void
     * @throws \Doctrine\ORM\ORMException
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        if (!empty($this->execute($value, $constraint))) {
            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('value', $value)
                ->addViolation();
        }
    }

    /**
     * @param $value
     * @param Constraint|Unique $constraint The constraint for the validation
     * @return array
     */
    protected function execute($value, Constraint $constraint)
    {
        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $attribute = $constraint->targetAttribute;

        $query = $em->createQueryBuilder()
            ->select("t")
            ->from($constraint->targetClass, 't')
            ->where("t.$attribute =:attribute")
            ->setParameter('attribute', $value);
        
        if ($filters = $constraint->filter) {

            if (!is_array($filters)) {
                throw new \InvalidArgumentException('Property filter must be type of array.');
            }

            foreach ($filters as $key => $filter) {
                $this->createQuery($filter, $key, $query);
            }
        }

        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param $value
     * @param $filter
     * @param $key
     * @param $query
     */
    protected function createQuery(string $filter, int $key, QueryBuilder $query): void
    {
        [$attribute, $operation, $value] = explode('|', $filter);

        if (!in_array($operation, $this->operations)) {
            throw new \InvalidArgumentException('Property operation invalid.');
        }

        $root = $this->context->getRoot();

        if ($root instanceof FormInterface) {
            $root = $root->getData();
        }

        $methodName = 'get' . Container::camelize($value);

        if (method_exists($root, $methodName)) {
            $value = call_user_func([$root, $methodName]);
        }

        $attrName = "attribute$key";

        $query->andWhere("t.$attribute $operation :$attrName")
            ->setParameter($attrName, $value);
    }
}