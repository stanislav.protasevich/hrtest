<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class UniqueCollectionsValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param array|ArrayCollection $value
     * @param Constraint|UniqueCollections $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueCollections) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UniqueCollections');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }

        if ($value instanceof ArrayCollection || $value instanceof PersistentCollection) {
            $value = $value->getValues();
        }

        if (!is_array($value)) {
            throw new \InvalidArgumentException('Value must be array.');
        }

        $items = [];

        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        foreach ($value as $key => $item) {

            if (!$item) {
                continue;
            }

            $result = $this->validateAttribute($item, $constraint);

            if (empty($result)) {
                continue;
            }

            $result = (string)crc32(serialize($result));

            if (in_array($result, $items)) {
                $this->context
                    ->buildViolation($translator->trans($constraint->message))
                    ->atPath(sprintf('[%d]', $key))
                    ->addViolation();
            }

            $items[] = $result;
        }
    }

    /**
     * @param $item
     * @param UniqueCollections $constraint
     * @return array
     */
    protected function validateAttribute($item, UniqueCollections $constraint): array
    {
        $attributes = (array)$constraint->attributes;

        $tmpItem = [];
        foreach ($attributes as $attribute) {

            if ($rules = $constraint->onlyWhen) {
                if (is_array($rules)) {
                    foreach ($rules as $key => $rule) {
                        $find = $this->resolveValueByAttribute($item, $key);

                        if ($find === $rule) {
                            $tmpItem[][$attribute] = $this->resolveValueByAttribute($item, $attribute);
                        }
                    }
                } else {
                    throw new \InvalidArgumentException('Attribute must be array.');
                }
            } else {
                $tmpItem[$attribute][] = $this->resolveValueByAttribute($item, $attribute);
            }
        }

        return $tmpItem;
    }

    protected function resolveValueByAttribute($item, string $attribute)
    {
        $methodName = 'get' . $attribute;

        if (!method_exists($item, $methodName)) {
            throw new \InvalidArgumentException('Attribute ' . $attribute . ' don`t exist in ' . get_class($item));
        }

        return call_user_func([$item, $methodName]);
    }
}