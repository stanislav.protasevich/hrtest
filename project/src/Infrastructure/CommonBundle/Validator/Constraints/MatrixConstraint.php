<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class MatrixConstraint extends Constraint
{
    public $message = 'Must be at least {{ count }} values.';

    public $messageNotValid = 'Value is not valid.';
    public $messageMaxSize = 'Invalid value size, need {{ size }}.';

    /**
     * @var integer
     */
    public $size = 1;

    public $requiredMin = 1;

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [
            self::CLASS_CONSTRAINT,
            self::PROPERTY_CONSTRAINT,
        ];
    }
}