<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint};

class ExistValidator extends UniqueValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint|Unique $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (empty($this->execute($value, $constraint))) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{value}', (string)$value)
                ->addViolation();
        }
    }
}
