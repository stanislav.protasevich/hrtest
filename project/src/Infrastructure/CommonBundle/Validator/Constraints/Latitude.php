<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class Latitude extends Constraint
{
    public $pattern = '/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/';

    public $message = 'The string "{{ string }}" should adhere to ISO 6709 ex. +40.20361';

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return 'Infrastructure\CommonBundle\Validator\Constraints\LatitudeLongitudeValidator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return [self::CLASS_CONSTRAINT, self::PROPERTY_CONSTRAINT];
    }
}