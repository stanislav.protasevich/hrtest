<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\{Constraint, Constraints\NotBlankValidator};

class NotNullCollectionValidator extends NotBlankValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value instanceof ArrayCollection || $value instanceof PersistentCollection) {
            $value = array_filter($value->getValues());
        }

        parent::validate($value, $constraint);
    }
}
