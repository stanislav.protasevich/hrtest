<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;


use Doctrine\Common\Collections\ArrayCollection;
use Infrastructure\UserBundle\ValueObject\UserPerformanceFormVo;
use Symfony\Component\Form\Form;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MatrixConstraintValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint|MatrixConstraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        /* @var $form Form */
        $form = $this->context->getRoot();

        $accept = ["B", "C", "D"];

        if (!is_array($value)) {

        } else {
            $keys = array_keys($value);

            $valuesCount = 0;
            foreach ($keys as $key) {
                $input = $value[$key] ?? null;

                if (!$input) {
                    $valuesCount++;
                } elseif ((is_string($input) || is_numeric($input)) === false) {
                    $value[$key] = (string)$input;
                }
            }

            $requiredMin = $constraint->requiredMin;

            $valuesCount = 0;
            foreach ($value as $item) {
                if (!is_null($item)) {
                    $valuesCount++;
                }
            }

            if ($valuesCount < $requiredMin) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ count }}', $requiredMin)
                    ->addViolation();
                return;
            }
        }


//        $size = $constraint->size;
//
//        if (count($value) > $size) {
//            $this->context->buildViolation($constraint->messageMaxSize)
//                ->setParameter('{{ size }}', $size)
//                ->addViolation();
//
//            return;
//        }


        /* @var $model UserPerformanceFormVo */
        $model = $form->getData();

        $payload = $constraint->payload;

        $keyName = $payload['attribute'] ?? null;

        if ($keyName) {

            if (!$model->descriptions instanceof ArrayCollection) {
                $model->descriptions = new ArrayCollection();
            }

            if (isset($payload['description'])) {
                $model->descriptions->set($keyName, $payload['description']);
            }
        }
    }
}