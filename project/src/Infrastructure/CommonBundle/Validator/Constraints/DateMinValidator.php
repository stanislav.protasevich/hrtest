<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DateMinValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param \DateTime $value
     * @param Constraint $constraint
     * @throws \Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof DateMin) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\DateMin');
        }

        if (!empty($this->context->getViolations()->count())) {
            return;
        }
        
        $now = new \DateTime();
        if ($constraint->offset && is_integer($constraint->offset)) {
            $now->add(
                new \DateInterval('P' . $constraint->offset . 'D')
            );
        }

        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        if ($value < $now) {
            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{{ value }}', $now->format('d.m.Y'))
                ->addViolation();
        }
    }
}