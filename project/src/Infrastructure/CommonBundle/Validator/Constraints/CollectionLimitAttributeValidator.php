<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CollectionLimitAttributeValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct()
    {
        $this->container = app('service_container');
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint|CollectionLimitAttribute $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof CollectionLimitAttribute) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UniqueCollections');
        }

        $attribute = 'get' . $constraint->attribute;
        $limit = $constraint->limit;

        $count = 0;

        $needle = $constraint->value;

        $isValid = true;

        foreach ($value as $item) {
            if (is_object($item) && method_exists($item, $attribute)) {
                $val = call_user_func([$item, $attribute]);

                if ($val === $needle) {
                    $count++;
                }
            }

            if ($count > $limit) {
                $isValid = false;
                break;
            }
        }

        if ($isValid === false) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');
            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->setParameter('{{ attribute }}', $constraint->attribute)
                ->setParameter('{{ number }}', $limit)
                ->addViolation();
        }
    }
}