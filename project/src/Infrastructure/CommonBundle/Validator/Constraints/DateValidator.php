<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Validator\Constraints;


use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\{Constraint, ConstraintValidator};

class DateValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint|Unique $constraint The constraint for the validation
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Date) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Date');
        }

        $d = \DateTime::createFromFormat($constraint->format, $value);

        if (($d && $d->format($constraint->format) === $value) === false) {
            /* @var $translator Translator */
            $translator = $this->container->get('translator');

            $this->context
                ->buildViolation($translator->trans($constraint->message))
                ->addViolation();
        }
    }
}