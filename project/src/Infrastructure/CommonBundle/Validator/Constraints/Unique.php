<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */
declare(strict_types=1);

namespace Infrastructure\CommonBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 */
class Unique extends Constraint
{
    public $message = '"{{ value }}" has already been taken.';

    public $targetClass;

    public $targetAttribute;

    /**
     * Allow arguments as ['id|!=|id'], or
     *  [
     *      'id|!=|id',
     *      'status|=|active'],
     *  ]
     *
     * , where first argument - property name, filter operation, and value,
     * can accept scalar value or link to a field
     *
     */
    public $filter;

    /**
     * @inheritdoc
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this) . 'Validator';
    }

    /**
     * @inheritdoc
     * @return array|string
     */
    public function getTargets()
    {
        return array(self::CLASS_CONSTRAINT, self::PROPERTY_CONSTRAINT);
    }
}