<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Message;


abstract class EmailNotification
{
    /**
     * The person the message is from.
     *
     * @var array
     */
    public $from = [];

    /**
     * The "to" recipients of the message.
     *
     * @var array
     */
    public $to = [];

    /**
     * The "cc" recipients of the message.
     *
     * @var array
     */
    public $cc = [];

    /**
     * The "bcc" recipients of the message.
     *
     * @var array
     */
    public $bcc = [];

    /**
     * The "reply to" recipients of the message.
     *
     * @var array
     */
    public $replyTo = [];

    /**
     * The subject of the message.
     *
     * @var string
     */
    public $subject;

    /**
     * The view to use for the message.
     *
     * @var string
     */
    public $view;

    /**
     * The view data for the message.
     *
     * @var array
     */
    public $viewData = [];

    /**
     * Message encoding
     *
     * @var string
     */
    public $contentType = 'text/html';

    /**
     * Set the subject of the message.
     *
     * @param  string $subject
     * @return $this
     */
    public function subject(string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set the view and view data for the message.
     *
     * @param  string $view
     * @param  array $data
     * @return $this
     */
    public function view(string $view, array $data = [])
    {
        $this->view = $view;
        $this->viewData = array_merge($this->viewData, $data);

        return $this;
    }

    /**
     * Set recipients of the message.
     *
     * @param $address
     * @param null $name
     * @return $this
     */
    public function to($address, $name = null)
    {
        $this->to = [
            $address => $name
        ];

        return $this;
    }

    abstract public function build();

    /**
     * Event when mail could not send
     *
     * @param \Exception $exception
     */
    public function onFail(\Exception $exception)
    {

    }

    /**
     * Event before send
     *
     * @param \Swift_Message $message
     */
    public function beforeSend(\Swift_Message $message)
    {

    }

    /**
     * Event on success message send
     *
     * @param \Swift_Message $message
     * @param \Swift_Mailer $mailer
     */
    public function afterSend(\Swift_Message $message, \Swift_Mailer $mailer)
    {

    }

    /**
     * Event always call after code execute
     */
    public function onEnd()
    {

    }

    /**
     * Event before create message
     */
    public function onStart()
    {

    }
}
