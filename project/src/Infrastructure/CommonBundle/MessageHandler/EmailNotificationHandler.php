<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\MessageHandler;


use Infrastructure\CommonBundle\Message\EmailNotification;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailNotificationHandler implements MessageHandlerInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var \Swift_Message
     */
    private $message;

    public function __construct(\Swift_Mailer $mailer, \Swift_Message $message, \Twig_Environment $twig, TranslatorInterface $translator)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->message = $message;
    }

    public function __invoke(EmailNotification $notification)
    {
        /** @var EmailNotification $object */
        $object = $notification->build();

        $object->onStart();

        $this->message
            ->setSubject(
                $this->translator->trans($object->subject)
            )
            ->setTo($object->to)
            ->setBody(
                $this->twig->render($object->view, $object->viewData),
                $object->contentType
            );

        try {
            $object->beforeSend($this->message);

            $this->mailer->send($this->message);

            $object->afterSend($this->message, $this->mailer);
        } catch (\Exception $exception) {
            $object->onFail($exception);
        } finally {
            $object->onEnd();
        }
    }
}
