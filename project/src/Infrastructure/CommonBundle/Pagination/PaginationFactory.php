<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Pagination;


use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

/**
 * @property RouterInterface $router
 * @property Response $response
 * @property int $maxPerPage
 * @property int $page
 * @property Request $request
 */
class PaginationFactory
{
    /**
     * @var RouterInterface
     */
    private $router;

    private $maxPerPage = 20;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var int
     */
    private $page = 1;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(RouterInterface $router, RequestStack $request, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->request = $request->getCurrentRequest();
        $this->logger = $logger;
    }

    public function createCollection(QueryBuilder $qb, string $route, array $routeParams = []): PaginatedCollection
    {
        $originalRouteParams = $routeParams;

        $page = (int)$this->request->query->get('page', $this->page);
        $perPage = (int)$this->request->query->get('per-page', $this->maxPerPage);

        $adapter = new DoctrineORMAdapter($qb);
        $pager = new Pagerfanta($adapter);
        $pager->setAllowOutOfRangePages(true);
//        $pager->setNormalizeOutOfRangePages(true);
        $pager->setCurrentPage($page);
        $pager->setMaxPerPage($perPage);

        $items = [];
        try {
            foreach ($pager->getCurrentPageResults() as $result) {
                $items[] = $result;
            }
        } catch (QueryException $exception) {
            $this->logger->error($exception->getTraceAsString());
            throw new \InvalidArgumentException($exception->getMessage());
        }

        $routeParams = array_merge($routeParams, $this->request->query->all());

        $paginatedCollection = new PaginatedCollection($items);

        $createLinkUrl = function ($targetPage) use ($route, $routeParams) {
            return $this->router->generate($route, array_merge($routeParams, ['page' => $targetPage]));
        };

        $paginatedCollection->setMeta(
            $page,
            $perPage,
            $pager->getNbResults(),
            $this->router->generate($route, $originalRouteParams),
            (int)$page,
            (int)$pager->getNbPages()
        );

        $paginatedCollection->addLink('first', $createLinkUrl(1));
        $paginatedCollection->addLink('last', $createLinkUrl($pager->getNbPages()));
        $paginatedCollection->addLink('next', $pager->hasNextPage() ? $createLinkUrl($pager->getNextPage()) : null);
        $paginatedCollection->addLink('prev', $pager->hasPreviousPage() ? $createLinkUrl($pager->getPreviousPage()) : null);

        return $paginatedCollection;
    }

    /**
     * @param int $maxPerPage
     * @return PaginationFactory
     */
    public function setMaxPerPage(int $maxPerPage): PaginationFactory
    {
        $this->maxPerPage = $maxPerPage;

        return $this;
    }

    /**
     * @param int $page
     * @return PaginationFactory
     */
    public function setPage(int $page): PaginationFactory
    {
        $this->page = $page;

        return $this;
    }

}
