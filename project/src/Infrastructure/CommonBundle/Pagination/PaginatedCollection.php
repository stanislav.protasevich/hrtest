<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CommonBundle\Pagination;

use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class PaginatedCollection
{
    use ObjectebleTrait;

    public $data;

    public $links = [];

    public $meta = [];

    /**
     * PaginatedCollection constructor.
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->data = $items;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data = []): void
    {
        $this->data = $data;
    }

    public function addLink(string $ref, ?string $url = null)
    {
        $this->links[$ref] = $url;
    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }

    /**
     * @param int $page
     * @param $perPage
     * @param int $total
     * @param string $path
     * @param int $from
     * @param int $last
     */
    public function setMeta(int $page, $perPage, int $total, string $path, int $from, int $last): void
    {
        $this->meta = [
            'current_page' => $page,
            'per_page' => $perPage,
            'total' => $total,
            'path' => $path,
            'from' => $from,
            'last_page' => $last,
            'to' => count($this->data),
        ];
    }

}