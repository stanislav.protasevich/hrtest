<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\DependencyInjection\Chain;


class TaggedServices
{
    private $services;

    public function __construct()
    {
        $this->services = [];
    }

    /**
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }

    public function addService($service)
    {
        $this->services[] = $service;
    }
}