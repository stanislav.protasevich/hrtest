<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\DependencyInjection\Compiler;


use Infrastructure\CommonBundle\DependencyInjection\Chain\TaggedServices;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class QueuePass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition(
            TaggedServices::class
        );

        foreach ($container->findTaggedServiceIds('enqueue.client.processor') as $key => $taggedServiceId) {
            $definition->addMethodCall('addService', [new Reference($key)]);
        }
    }
}