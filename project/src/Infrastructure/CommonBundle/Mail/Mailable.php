<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Mail;

use Infrastructure\CommonBundle\Contracts\Mail\Mailable as MailableContract;
use Symfony\Component\Translation\TranslatorInterface;

class Mailable implements MailableContract
{
    /**
     * The person the message is from.
     *
     * @var array
     */
    public $from = [];

    /**
     * The "to" recipients of the message.
     *
     * @var array
     */
    public $to = [];
    /**
     * The "cc" recipients of the message.
     *
     * @var array
     */
    public $cc = [];
    /**
     * The "bcc" recipients of the message.
     *
     * @var array
     */
    public $bcc = [];
    /**
     * The "reply to" recipients of the message.
     *
     * @var array
     */
    public $replyTo = [];
    /**
     * The subject of the message.
     *
     * @var string
     */
    public $subject;
    /**
     * The view to use for the message.
     *
     * @var string
     */
    public $view;

    public $body;

    public function body(string $view, $body): Mailable
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param array $from
     */
    public function from(array $from): Mailable
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param array $to
     */
    public function to(array $to): Mailable
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param array $cc
     */
    public function cc(array $cc): Mailable
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * @param array $bcc
     */
    public function bcc(array $bcc): Mailable
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * @param array $replyTo
     */
    public function replyTo(array $replyTo): Mailable
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @param string $subject
     */
    public function subject(string $subject): Mailable
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Send the message using the given mailer.
     *
     * @return void
     */
    public function send()
    {
        /* @var $translator TranslatorInterface */
        $translator = app('translator');
        prnx(1);

//        $message = (new \Swift_Message($translator->trans($this->subject)))
//            ->setFrom($this);

//            ->setFrom('root@localhost.com', 'HR Admin')// TODO need DI
//            ->setTo($user->getEmail())
//            ->setBody(
//                $twig->render('@User/emails/register.html.twig', [
//                    'token' => $user->getPasswordResetToken()
//                ]),
//                'text/html'
//            );

//        $mailer->send($this->getView());
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     */
    public function view(string $view): Mailable
    {
        $this->view = $view;

        return $this;
    }
}