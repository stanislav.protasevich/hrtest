base64-encoded-file
===================

## Installation

```bash
$ php composer.phar require hshn/base64-encoded-file
```

## Usage

```php
<?php

use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;

$file = new Base64EncodedFile(base64_encode($data));

$file->getPathname(); // "/path/to/file"
$file instanceof Symfony\Component\HttpFoundation\File\File; // true
```


### Integration for symfony/form

```php
<?php

use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;

$form = $formBuilder
    // symfony 2.7
    ->add('file', new Base64EncodedFileType())
    // symfony 2.8~
    ->add('file', Base64EncodedFileType::class)
    ->getForm();
```