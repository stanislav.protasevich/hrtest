<?php
/**
 * Created by PhpStorm.
 * User: yura
 * Date: 22.08.18
 * Time: 18:49
 */

namespace Infrastructure\CommonBundle\Form\Type;


use Infrastructure\CommonBundle\Form\DataTransformer\BooleanTypeToBooleanTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BooleanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(
            new BooleanTypeToBooleanTransformer($options['true_values'], $options['false_values'])
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'compound' => false,
            'true_values' => [1, '1', true, 'true'],
            'false_values' => [0, '0', false, 'false'],
        ]);
    }
}