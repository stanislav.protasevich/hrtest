<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Console;

use League\Tactician\CommandBus;

class AbstractBusCommand extends AbstractCommand
{
    /**
     * @var CommandBus
     */
    private $bus;
    /**
     * @var CommandBus
     */
    private $queryBus;

    public function __construct(CommandBus $bus, CommandBus $queryBus, ?string $name = null)
    {
        parent::__construct($name);
        $this->bus = $bus;
        $this->queryBus = $queryBus;
    }

    /**
     * @param object $commandRequest
     * @return mixed
     */
    public function handle($commandRequest)
    {
        return $this->bus->handle($commandRequest);
    }

    /**
     * @param object $commandRequest
     * @return mixed
     */
    public function ask($commandRequest)
    {
        return $this->queryBus->handle($commandRequest);
    }
}