<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CommonBundle\Console;

use Doctrine\ORM\EntityManager;
use Infrastructure\CommonBundle\Exception\Form\FormException;
use Infrastructure\CommonBundle\Helpers\FormHelper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Form\FormInterface;

class AbstractCommand extends ContainerAwareCommand
{
    private $_em;

    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @final
     * @param string $type
     * @param null $data
     * @param array $options
     * @return FormInterface
     */
    protected function createForm(string $type, $data = null, array $options = []): FormInterface
    {
        return $this->getContainer()->get('form.factory')->create($type, $data, $options);
    }

    protected function getEm(): EntityManager
    {
        if (!$this->_em) {
            $this->_em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        }

        return $this->_em;
    }

    /**
     * @param array $data
     * @param FormInterface $form
     * @param bool $clearMissing
     */
    protected function processForm(array $data = [], FormInterface $form, bool $clearMissing = false)
    {
        $form->submit($data, $clearMissing);

        if (!$form->isValid()) {

            prnx(FormHelper::getErrorsFromForm($form));

            throw FormException::withMessages(FormHelper::getErrorsFromForm($form));
        }
    }
}