<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\TimeOff\Entity\Policy;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\TimeOffBundle\Command\PolicySearchCommand;

class PolicySearchService extends BaseService
{
    public function search(PolicySearchCommand $command)
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(Policy::class)->findAllQueryBuilder();

        if ($command->id && !empty($command->id)) {
            $qb->andWhere('policy.id IN (:id)')->setParameter('id', (array)$command->id);
        }

        return $qb;
    }
}