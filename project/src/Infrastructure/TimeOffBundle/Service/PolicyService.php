<?php declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Service;


use Domain\TimeOff\Entity\Policy;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\TimeOffBundle\Command\PolicyEditCommand;
use Infrastructure\TimeOffBundle\Command\PolicyViewCommand;

class PolicyService extends BaseService
{
    /**
     * @param Policy $policy
     * @return Policy
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(Policy $policy): Policy
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $policy->setCreatedBy($this->getUser());
            $policy->setUpdatedBy($this->getUser());
            $policy->setCreatedAt(new \DateTime());
            $policy->setUpdatedAt(new \DateTime());

            $em->persist($policy);

            $em->flush();
            $em->getConnection()->commit();

            return $policy;
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
    }

    /**
     * @param PolicyEditCommand $command
     * @return Policy
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Exception
     */
    public function edit(PolicyEditCommand $command): Policy
    {
        $em = $this->getEm();

        $em->getConnection()->beginTransaction();

        try {
            $policy = $command->getPolicy();
            $policy->setUpdatedBy($this->getUser());
            $policy->setUpdatedAt(new \DateTime());

            $em->persist($policy);

            $em->flush();
            $em->getConnection()->commit();

            return $policy;
        } catch (\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
    }

    public function view(PolicyViewCommand $command)
    {

    }

    public function delete(Policy $policy): void
    {
        $em = $this->getEm();

        $em->remove($policy);
        $em->flush();
    }
}
