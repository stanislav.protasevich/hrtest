<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Service;


use Doctrine\ORM\QueryBuilder;
use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\CommonBundle\Service\FilterSortService;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeSearchCommand;

class TimeOffSearchService extends FilterSortService
{
    protected $sortMapping = [
        'id' => 'timeOfType.id',
        'status' => 'timeOfType.status',
        'paid' => 'timeOfType.paid',
        'name' => 'timeOfType.name',
        'description' => 'timeOfType.description',
        'color' => 'timeOfType.calendarColor',
    ];

    public function search(TimeOffTypeSearchCommand $command)
    {
        /* @var $qb QueryBuilder */
        $qb = $this->getEm()->getRepository(TimeOffType::class)->findAllQueryBuilder();

        if ($command->id && $id = $this->paramsFromString($command->id)) {
            $qb->andWhere('timeOfType.id IN (:id)')
                ->setParameter('id', $id);
        }

        if ($command->status && $status = $this->paramsFromString($command->status)) {
            $qb->andWhere('timeOfType.status IN (:status)')
                ->setParameter('status', $status);
        }

        if ($command->name) {
            $qb->andWhere('timeOfType.name LIKE :name')
                ->setParameter('name', $this->makeLikeParam((string)$command->name));
        }

        if ($command->description) {
            $qb->andWhere('timeOfType.description LIKE :description')
                ->setParameter('description', $this->makeLikeParam((string)$command->description));
        }

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}