<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Service;


use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\CommonBundle\Service\BaseService;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeCreateCommand;
use Infrastructure\TimeOffBundle\Command\TimeOffTypeEditCommand;

class TimeOffService extends BaseService
{
    /**
     * @param TimeOffTypeCreateCommand $command
     * @return TimeOffType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(TimeOffTypeCreateCommand $command): TimeOffType
    {
        $em = $this->getEm();

        $timeOffType = $command->getTimeOffType();
        $timeOffType->setCreatedAt($this->now());
        $timeOffType->setUpdatedAt($this->now());
        $timeOffType->setCreatedBy($this->getUser());
        $timeOffType->setUpdatedBy($this->getUser());

        $em->persist($timeOffType);
        $em->flush();

        return $timeOffType;
    }

    /**
     * @param TimeOffTypeEditCommand $command
     * @return TimeOffType
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(TimeOffTypeEditCommand $command): TimeOffType
    {
        $em = $this->getEm();

        $timeOffType = $command->getTimeOffType();
        $timeOffType->setUpdatedAt($this->now());
        $timeOffType->setUpdatedBy($this->getUser());

        $em->persist($timeOffType);
        $em->flush();

        return $timeOffType;
    }

    public function delete(TimeOffType $timeOffType)
    {
        $em = $this->getEm();

        $em->remove($timeOffType);
        $em->flush();
    }

    public function getPrimaryAll(): array
    {
        return $this->getEm()->getRepository(TimeOffType::class)->findPrimaryAll();
    }
}