<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;
use Symfony\Component\Validator\Constraints as Assert;

class TimeOffTypeSearchCommand
{
    use ObjectebleTrait;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $id;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $name;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $description;

    /**
     * @Assert\Type(type="string")
     * @var string
     */
    public $status;
}