<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Command;


use Domain\TimeOff\Entity\Policy;

class PolicyViewCommand
{
    /**
     * @var Policy
     */
    private $policy;

    public function __construct(Policy $policy)
    {
        $this->policy = $policy;
    }

    /**
     * @return Policy
     */
    public function getPolicy(): Policy
    {
        return $this->policy;
    }
}