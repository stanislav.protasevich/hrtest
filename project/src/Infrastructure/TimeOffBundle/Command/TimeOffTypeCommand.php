<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Command;


use Domain\TimeOff\Entity\TimeOffType;

class TimeOffTypeCommand
{
    /**
     * @var TimeOffType
     */
    private $timeOffType;

    public function __construct(TimeOffType $timeOffType)
    {
        $this->timeOffType = $timeOffType;
    }

    /**
     * @return TimeOffType
     */
    public function getTimeOffType(): TimeOffType
    {
        return $this->timeOffType;
    }
}