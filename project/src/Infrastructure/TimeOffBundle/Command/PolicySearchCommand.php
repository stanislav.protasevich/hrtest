<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class PolicySearchCommand
{
    use ObjectebleTrait;

    public $id;
}