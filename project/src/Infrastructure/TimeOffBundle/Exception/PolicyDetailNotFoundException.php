<?php

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Exception;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PolicyDetailNotFoundException extends NotFoundHttpException
{
    public function __construct(string $message = null, \Exception $previous = null, int $code = 0, array $headers = [])
    {
        $message = $message ?: app('translator')->trans('Policy not found.');

        parent::__construct($message, $previous, $code, $headers);
    }
}