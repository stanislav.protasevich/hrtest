<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Event;


use Domain\TimeOff\Entity\Policy;
use Symfony\Component\EventDispatcher\Event;

class PolicyEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'policyEventBeforeInsert';
    public const EVENT_AFTER_INSERT = 'policyEventAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'policyEventBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'policyEventAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'policyEventBeforeDelete';
    public const EVENT_AFTER_DELETE = 'policyEventAfterDelete';

    /**
     * @var Policy
     */
    private $policy;

    public function __construct(Policy $policy)
    {

        $this->policy = $policy;
    }
}
