<?php
declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Infrastructure\CommonBundle\Helpers\ArrayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Form\Form;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniquePolicyAssignValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param $value
     * @param Constraint $constraint
     * @return void
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniquePolicyAssign) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__ . '\UniquePolicyAssign');
        }

        if ($this->context->getViolations()->count()) {
            return;
        }

        if (!$value instanceof TimeOffType) {
            return;
        }

        /* @var $em EntityManager */
        $em = $this->container->get('doctrine.orm.entity_manager');

        /* @var $form Form */
        $form = $this->context->getRoot();

        /* @var $policy Policy */
        $policy = $form->getData();

        $type = $policy->getType();

        /* @var $translator Translator */
        $translator = $this->container->get('translator');

        $foundedPolicy = $em->getRepository(Policy::class)->findOneBy([
            'timeOffType' => $value,
            'type' => $type
        ]);

        switch ($type) {
            case Policy::TYPE_COMMON:

                if ($foundedPolicy) {
                    $this->context->buildViolation($translator->trans($constraint->messageAllReadyExist))->addViolation();
                }

                break;

            case Policy::TYPE_USER:

                $duplicated = [];

                foreach ($foundedPolicy->getAssignUsers() as $assignUser) {
                    foreach ($policy->getAssignUsers() as $willAssignUser) {
                        if ($willAssignUser->getId() === $assignUser->getId()) {
                            $duplicated[] = $willAssignUser;
                        }
                    }
                }

                if (!empty($duplicated)) {
                    $count = count($duplicated);

                    $this->context->buildViolation(
                        $translator->transChoice($constraint->messageAllReadyUserAssignedExist, $count, [
                            '{{ user }}' => implode(', ', ArrayHelper::getColumn($duplicated, 'profile.fullname'))
                        ])
                    )->addViolation();
                }

                break;
        }
    }
}