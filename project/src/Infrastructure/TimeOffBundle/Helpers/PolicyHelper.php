<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Helpers;


use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\TimeOffBundle\Factory\PolicyFactory;

class PolicyHelper
{
    public static function userPolicy(TimeOffType $timeOffType, User $user): Policy
    {
        /** @var Policy $data */
        $data = null;

        foreach ($timeOffType->getPolicies() as $policy) {
            switch ($policy->getType()) {

                case Policy::TYPE_COMMON:
                    $data = $policy;
                    break;

                case Policy::TYPE_USER:

                    break;

                case Policy::TYPE_DEPARTMENT:

                    break;
            }

            if ($data) {
                break;
            }
        }

        return $data ?: PolicyFactory::build($timeOffType);
    }
}
