<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Helpers;


use Domain\TimeOff\Entity\Policy;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @mixin AbstractBusController
 */
trait PolicyHelpersTrait
{
    public function findOrFail(string $id): Policy
    {
        $policy = $this->getEm()->getRepository(Policy::class)->find($id);

        if (is_null($policy)) {
            throw new ModelNotFoundException(Policy::class, $id);
        }

        return $policy;
    }
}
