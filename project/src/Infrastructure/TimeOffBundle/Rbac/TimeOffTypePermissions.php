<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class TimeOffTypePermissions implements RbacPermissionsInterface
{
    public const PERMISSION_TIME_OFF_TYPE_CREATE = 'timeOffTypeCreate';
    public const PERMISSION_TIME_OFF_TYPE_EDIT = 'timeOffTypeEdit';
    public const PERMISSION_TIME_OFF_TYPE_LIST = 'timeOffTypeList';
    public const PERMISSION_TIME_OFF_TYPE_LIST_PRIMARY = 'timeOffTypeListPrimary';
    public const PERMISSION_TIME_OFF_TYPE_DELETE = 'timeOffTypeDelete';
    public const PERMISSION_TIME_OFF_TYPES = [
        self::PERMISSION_TIME_OFF_TYPE_CREATE,
        self::PERMISSION_TIME_OFF_TYPE_EDIT,
        self::PERMISSION_TIME_OFF_TYPE_LIST,
        self::PERMISSION_TIME_OFF_TYPE_DELETE,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_TIME_OFF_TYPE_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_TIME_OFF_TYPE_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_TIME_OFF_TYPE_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_TIME_OFF_TYPE_LIST_PRIMARY,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_TIME_OFF_TYPE_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
        ];
    }
}