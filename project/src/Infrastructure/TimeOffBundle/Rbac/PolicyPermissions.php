<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class PolicyPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_POLICY_CREATE = 'policyCreate';
    public const PERMISSION_POLICY_UPDATE = 'policyUpdate';
    public const PERMISSION_POLICY_DELETE = 'policyDelete';
    public const PERMISSION_POLICY_LIST = 'policyList';
    public const PERMISSION_POLICY_VIEW = 'policyView';
    public const PERMISSION_POLICIES = [
        self::PERMISSION_POLICY_CREATE,
        self::PERMISSION_POLICY_UPDATE,
        self::PERMISSION_POLICY_LIST,
        self::PERMISSION_POLICY_VIEW,
    ];

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_POLICY_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_POLICY_UPDATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_POLICY_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_POLICY_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
            new RbacItemCommand(self::PERMISSION_POLICY_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_ADMINISTRATOR,
                ]
            ),
        ];
    }
}
