<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Factory;


use Carbon\Carbon;
use Domain\TimeOff\Entity\TimeOffType;
use Ramsey\Uuid\Uuid;

class TimeOffTypeFactory
{
    public static function build(): TimeOffType
    {
        $now = Carbon::now();

        $timeOffType = new TimeOffType();
        $timeOffType->setName('Generated time off type.');
        $timeOffType->setCreatedAt($now);
        $timeOffType->setUpdatedAt($now);
        $timeOffType->setDescription('Automatic generated time off type.');
        $timeOffType->setStatus(TimeOffType::STATUS_ACTIVE);

        $reflection = new \ReflectionClass($timeOffType);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($timeOffType, (string)Uuid::uuid4());

        return $timeOffType;
    }
}
