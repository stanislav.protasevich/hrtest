<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\Factory;


use Carbon\Carbon;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Ramsey\Uuid\Uuid;

class PolicyFactory
{
    public static function build(TimeOffType $timeOffType): Policy
    {
        $now = Carbon::now();

        $policy = new Policy();
        $policy->setTimeOffType($timeOffType);
        $policy->setCreatedAt($now);
        $policy->setUpdatedAt($now);
        
        $reflection = new \ReflectionClass($policy);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($policy, (string)Uuid::uuid4());

        return $policy;
    }
}
