<?php

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Factory\Form;


use Domain\TimeOff\Entity\TimeOffType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TimeOffTypeInput extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('trackTime', ChoiceType::class, [
                'choices' => TimeOffType::TRACK_TIMES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => TimeOffType::STATUSES,
                'empty_data' => TimeOffType::STATUS_ACTIVE,
            ])
            ->add('showInCalendar', CheckboxType::class)
            ->add('paid', CheckboxType::class)
            ->add('icon', TextType::class)
            ->add('calendarColor', TextType::class)
            ->add('description', TextareaType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TimeOffType::class,
            'csrf_protection' => false,
        ]);
    }
}