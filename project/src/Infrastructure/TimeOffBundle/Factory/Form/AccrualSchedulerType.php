<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Factory\Form;


use Domain\TimeOff\Entity\AccrualSchedule;
use Infrastructure\CommonBundle\Validator\Constraints\Date;
use Infrastructure\TimeOffBundle\Validator\Constraints\AnnuallyMod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class AccrualSchedulerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('beginsInterval', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(1),
                ],
            ])
            ->add('beginsType', ChoiceType::class, [
                'choices' => AccrualSchedule::BEGINS_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('hours', IntegerType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(1),
                ],
            ])
            ->add('interval', ChoiceType::class, [
                'choices' => AccrualSchedule::INTERVALS,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('maximumHours', IntegerType::class)
            ->add('allowCarryOver', ChoiceType::class, [
                'choices' => AccrualSchedule::ALLOW_CARRY_OVERS,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('weeklyMod', ChoiceType::class, [
                'choices' => AccrualSchedule::WEEKLY_MODS,
                'constraints' => [
                    new NotBlank([
                        'groups' => AccrualSchedule::INTERVAL_WEEKLY,
                    ]),
                ],
            ])
            ->add('monthlyMod', IntegerType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => AccrualSchedule::INTERVAL_MONTHLY,
                    ]),
                    new Range([
                        'min' => 1,
                        'max' => 31,
                    ]),
                ],
            ])
            ->add('annuallyMod', TextType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => AccrualSchedule::INTERVAL_ANNUALLY,
                    ]),
                    new Date([
                        'format' => AccrualSchedule::ANNUALLY_MOD_FORMAT,
                        'groups' => AccrualSchedule::INTERVAL_ANNUALLY,
                    ]),
                ],
            ])
            ->add('revokeInterval', IntegerType::class, [
                'empty_data' => '1',
                'constraints' => [
                    new Range([
                        'min' => 1,
                    ]),
                ],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $form = $event->getForm();
                $data = $event->getData();
            });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AccrualSchedule::class,
            'csrf_protection' => false,
            'validation_groups' => function (FormInterface $form) {
                /* @var $data AccrualSchedule */
                $data = $form->getData();

                $interval = $data->getInterval();
                $groups = ['Default'];

                switch ($interval) {
                    case AccrualSchedule::INTERVAL_WEEKLY:
                        $groups[] = AccrualSchedule::INTERVAL_WEEKLY;
                        break;

                    case AccrualSchedule::INTERVAL_MONTHLY:
                        $groups[] = AccrualSchedule::INTERVAL_MONTHLY;
                        break;

                    case AccrualSchedule::INTERVAL_ANNUALLY:
                        $groups[] = AccrualSchedule::INTERVAL_ANNUALLY;
                        break;
                }

                return $groups;
            },
        ]);
    }
}