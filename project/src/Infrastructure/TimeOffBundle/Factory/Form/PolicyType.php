<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Factory\Form;

use Domain\Company\Entity\Department;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\CommonBundle\Validator\Constraints\{NotNullCollection, UniqueCollections};
use Infrastructure\CompanyBundle\Repository\DepartmentRepository;
use Infrastructure\TimeOffBundle\Repository\TimeOffTypeRepository;
use Infrastructure\TimeOffBundle\Validator\Constraints\UniquePolicyAssign;
use Infrastructure\UserBundle\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{CheckboxType, ChoiceType, CollectionType, NumberType, TextType};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PolicyType extends AbstractType
{
    /**
     * @var PolicyValidationGroupResolver
     */
    private $groupResolver;

    public function __construct(PolicyValidationGroupResolver $groupResolver)
    {
        $this->groupResolver = $groupResolver;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => Policy::TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('timeOffType', EntityType::class, [
                'class' => TimeOffType::class,
                'constraints' => [
                    new NotBlank(),
                    new UniquePolicyAssign(),
                ],
                'query_builder' => function (TimeOffTypeRepository $r) {
                    return $r->queryFindAllActive();
                },
            ])
            ->add('maxCountAtTime', NumberType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('frequencyOfUse', NumberType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('frequencyType', ChoiceType::class, [
                'choices' => Policy::FREQUENCY_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('inRow', CheckboxType::class)
            ->add('inRowInterval', NumberType::class, [
                'constraints' => [
                    new NotBlank([
                        'groups' => ['inRow'],
                    ]),
                ],
            ])
            ->add('inRowType', ChoiceType::class, [
                'choices' => Policy::IN_ROW_TYPES,
                'constraints' => [
                    new NotBlank([
                        'groups' => ['inRow'],
                    ]),
                ],
            ])
            ->add('firstBeginsInterval', NumberType::class, [
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('firstBeginsType', ChoiceType::class, [
                'choices' => Policy::FREQUENCY_TYPES,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('countType', ChoiceType::class, [
                'choices' => Policy::COUNT_TYPES,
                'empty_data' => Policy::COUNT_TYPE_DAYS,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('status', ChoiceType::class, [
                'choices' => Policy::STATUSES,
                'empty_data' => Policy::STATUS_ACTIVE,
                'constraints' => [
                    new NotBlank(),
                ],
            ])
            ->add('assignDepartments', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'mapped' => false,
                'entry_options' => [
                    'class' => Department::class,
                    'query_builder' => function (DepartmentRepository $r) {
                        return $r->queryFindAllActive();
                    },
                ],
                'constraints' => [
                    new NotNullCollection([
                        'groups' => [Policy::TYPE_DEPARTMENT],
                    ]),
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ])
            ->add('assignUsers', CollectionType::class, [
                'entry_type' => EntityType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'error_bubbling' => false,
                'by_reference' => false,
                'entry_options' => [
                    'class' => User::class,
                    'query_builder' => function (UserRepository $r) {
                        return $r->queryFindAllActive();
                    },
                ],
                'constraints' => [
                    new NotNullCollection([
                        'groups' => [Policy::TYPE_USER],
                    ]),
                    new UniqueCollections([
                        'attributes' => ['id'],
                    ]),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Policy::class,
            'csrf_protection' => false,
            'validation_groups' => $this->groupResolver,
        ]);
    }
}
