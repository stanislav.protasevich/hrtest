<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Factory\Form;

use Domain\TimeOff\Entity\Policy;
use Symfony\Component\Form\FormInterface;

class PolicyValidationGroupResolver
{
    public function __invoke(FormInterface $form)
    {
        /* @var $data Policy */
        $data = $form->getData();

        $groups = ['Default'];

        if ($data->getType()) {
            $groups[] = $data->getType();
        }

        if ($data->getInRow() === false) {
            $groups[] = 'inRow';
        }
        
        return $groups;
    }
}