<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\DataFixtures\ORM;


use Carbon\Carbon;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;
use Infrastructure\UserBundle\DataFixtures\ORM\UserLoad;

class TimeOffLoad extends Fixture implements DependentFixtureInterface
{
    protected $list = [
        [
            'name' => 'Bereavement',
            'trackTime' => TimeOffType::TRACK_TIME_DAYS,
            'paid' => false,
            'showInCalendar' => false,
            'calendarColor' => '#808080',
        ],
        [
            'name' => 'Comp/In Lieu Time',
            'trackTime' => TimeOffType::TRACK_TIME_HOURS,
            'paid' => true,
            'showInCalendar' => false,
            'calendarColor' => '#800000',
        ],
        [
            'name' => 'FMLA',
            'trackTime' => TimeOffType::TRACK_TIME_HOURS,
            'paid' => true,
            'showInCalendar' => true,
            'calendarColor' => '#808000',
        ],
        [
            'name' => 'Sick',
            'trackTime' => TimeOffType::TRACK_TIME_HOURS,
            'paid' => true,
            'showInCalendar' => true,
            'calendarColor' => '#00FFFF',
        ],
        [
            'name' => 'Vacation',
            'trackTime' => TimeOffType::TRACK_TIME_HOURS,
            'paid' => true,
            'showInCalendar' => true,
            'calendarColor' => '#008000',
        ],
    ];

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserLoad::class,
        ];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(User::class);
        $user = $userRepo->findAll();

        foreach ($this->list as $item) {
            $time = new TimeOffType();
            $time->setName($item['name']);
            $time->setTrackTime($item['trackTime']);
            $time->setPaid($item['paid']);
            $time->setShowInCalendar($item['showInCalendar']);
            $time->setCalendarColor($item['calendarColor']);
            $time->setCreatedBy($user[0]);
            $time->setUpdatedBy($user[0]);
            $time->setCreatedAt(new \DateTime());
            $time->setUpdatedAt(new \DateTime());
            $time->setStatus(TimeOffType::STATUS_ACTIVE);

            $manager->persist($time);
        }

        $manager->flush();
    }
}