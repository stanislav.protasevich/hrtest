<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\TimeOffBundle\DataFixtures\ORM;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Domain\TimeOff\Entity\AccrualSchedule;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Domain\User\Entity\User;

class PolicyLoad extends Fixture implements DependentFixtureInterface
{
    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            TimeOffLoad::class,
        ];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function load(ObjectManager $manager)
    {
        $time = $manager->getRepository(TimeOffType::class)->findByName('Vacation');
        $userRepo = $manager->getRepository(User::class);
        $user = $userRepo->findAll();

        $policy = new Policy();
        $policy->setName('Vacation Full-Time');
        $policy->setTimeOffType($time);
        $policy->setType(Policy::TYPE_COMMON);
        $policy->setCountType(Policy::COUNT_TYPE_DAYS);
        $policy->setMaxCountAtTime(14);
        $policy->setFrequencyOfUse(1);
        $policy->setFrequencyType(Policy::FREQUENCY_TYPE_MONTHS);
        $policy->setInRow(false);
        $policy->setInRowInterval(1);
        $policy->setInRowType(Policy::IN_ROW_TYPE_DAYS);
        $policy->setFirstBeginsInterval(1);
        $policy->setFirstBeginsType(Policy::FIRST_BEGIN_TYPE_DAYS);


//        $policy->setFirstAccrual(Policy::FIRST_ACCRUAL_FULL);

//        $accrualSchedule = new AccrualSchedule();
//        $accrualSchedule->setBeginsInterval(1);
//        $accrualSchedule->setBeginsType(AccrualSchedule::BEGINS_TYPE_DAYS);
//        $accrualSchedule->setHours(1);
//        $accrualSchedule->setInterval(AccrualSchedule::INTERVAL_MONTHLY);
//        $accrualSchedule->setMaximumHours(120.0);
//        $accrualSchedule->setAllowCarryOver(AccrualSchedule::ALLOW_CARRY_OVER_LIMITED);
//
//        $accrualSchedule->setInterval(AccrualSchedule::INTERVAL_MONTHLY);
//        $accrualSchedule->setRevokeInterval(rand(1, 6));
//
//        $policy->setAccrualSchedule($accrualSchedule);

        $policy->setStatus(Policy::STATUS_ACTIVE);
        $policy->setCreatedBy($user[0]);
        $policy->setUpdatedBy($user[0]);
        $policy->setCreatedAt(new \DateTime());
        $policy->setUpdatedAt(new \DateTime());

        $manager->persist($policy);
        $manager->flush();
    }
}
