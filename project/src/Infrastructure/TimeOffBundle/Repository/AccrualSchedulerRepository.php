<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\TimeOff\Entity\AccrualSchedule;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccrualSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccrualSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccrualSchedule[]    findAll()
 * @method AccrualSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class AccrualSchedulerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccrualSchedule::class);
    }

    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('accrual_scheduler');
    }
}