<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\TimeOff\Entity\TimeOffType;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TimeOffType|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeOffType|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeOffType[]    findAll()
 * @method TimeOffType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class TimeOffTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TimeOffType::class);
    }

    public function findAllQueryBuilder($alias = 'timeOfType')
    {
        return $this->createQueryBuilder($alias);
    }

    public function queryFindAllActive()
    {
        return $this->findAllQueryBuilder('t')
            ->where('t.status = :status')
            ->setParameter('status', TimeOffType::STATUS_ACTIVE);
    }

    /**
     * @param string $id
     * @return TimeOffType|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?TimeOffType
    {
        return $this->createQueryBuilder('t')
            ->where('t.id = :id AND t.status = :status')
            ->setParameter('id', $id)
            ->setParameter('status', TimeOffType::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $name
     * @return TimeOffType|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByName(string $name): ?TimeOffType
    {
        return $this->createQueryBuilder('t')
            ->where('t.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Доступные для пользователя типы отпусков
     * @param int $userId
     * @return array
     */
    public function loadUserAvailableTypes(int $userId): array
    {
        return $this->createQueryBuilder('t')
            ->select('t', 'policy', 'policy_assign')
            ->leftJoin('t.policy', 'policy')
            ->leftJoin('policy.policy_assign', 'policy_assign')
            ->where('policy_assign.user_id is null')
            ->orWhere('policy_assign.user_id = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Проверка назначен уже этот тип отпуска или нет для пользователя
     *
     * @param int $userId
     * @return array
     */
    public function loadAssignedTypeForUser(int $id, int $userId)
    {
        return $this->createQueryBuilder('t')
            ->select('t', 'policy', 'policy_assign')
            ->leftJoin('t.policy', 'policy')
            ->leftJoin('policy.policy_assign', 'policy_assign')
            ->where('t.id = :id AND policy_assign.user_id = :userId')
            ->setParameter('id', $id)
            ->setParameter('userId', $userId)
            ->groupBy('t.id')
            ->getQuery()
            ->execute();
    }

    public function findPrimaryAll()
    {
        return $this->createQueryBuilder('t')
            ->select(['t.id as value', 't.name as label'])
            ->getQuery()
            ->getArrayResult();
    }
}