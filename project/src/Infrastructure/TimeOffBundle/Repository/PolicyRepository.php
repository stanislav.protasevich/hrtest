<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\TimeOffBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Domain\TimeOff\Entity\Policy;
use Domain\TimeOff\Entity\TimeOffType;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Policy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Policy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Policy[]    findAll()
 * @method Policy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 */
class PolicyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Policy::class);
    }

    public function findAllQueryBuilder($alias = 'policy')
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param string $id
     * @return Policy|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(string $id): ?Policy
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id AND p.status = :status')
            ->setParameter('id', $id)
            ->setParameter('status', Policy::STATUS_ACTIVE)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllByTimeOffType(TimeOffType $timeOffType)
    {
        return $this->createQueryBuilder('p')
            ->where('p.timeOffType = :timeOffType')
            ->setParameter('timeOffType', $timeOffType)
            ->orderBy('p.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }
}