<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Rbac;


use Domain\Rbac\Entity\AuthItem;
use Infrastructure\RbacBundle\Command\RbacItemCommand;
use Infrastructure\RbacBundle\Contracts\RbacPermissionsInterface;
use Infrastructure\RbacBundle\Rbac\RbacPermissions;

class CalendarPermissions implements RbacPermissionsInterface
{
    public const PERMISSION_CALENDAR_LIST = 'userCalendarList';
    public const PERMISSION_CALENDAR_CREATE = 'userCalendarCreate';
    public const PERMISSION_CALENDAR_EDIT = 'userCalendarEdit';
    public const PERMISSION_CALENDAR_DELETE = 'userCalendarDelete';
    public const PERMISSION_CALENDAR_VIEW = 'userCalendarView';

    public const PERMISSION_CALENDAR_GRANT_ALL = 'userCalendarGrantAll';

    public function getPermissions(): array
    {
        return [
            new RbacItemCommand(
                self::PERMISSION_CALENDAR_LIST,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_CALENDAR_CREATE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_CALENDAR_EDIT,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_CALENDAR_DELETE,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_HR,
                ]
            ),
            new RbacItemCommand(
                self::PERMISSION_CALENDAR_VIEW,
                AuthItem::TYPE_PERMISSION,
                null,
                [
                    RbacPermissions::ROLE_USER,
                ]
            ),
        ];
    }
}
