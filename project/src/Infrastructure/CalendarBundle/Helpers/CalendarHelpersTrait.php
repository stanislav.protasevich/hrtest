<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Helpers;

use Domain\Calendar\Entity\Calendar;
use Infrastructure\CommonBundle\Exception\ModelNotFoundException;
use UI\RestBundle\Controller\AbstractBusController;

/**
 * @property AbstractBusController $this
 */
trait CalendarHelpersTrait
{
    public function findOrFail(string $id): Calendar
    {
        $calendar = $this->getEm()->getRepository(Calendar::class)->findOneById($id);

        if (is_null($calendar)) {
            throw new ModelNotFoundException(Calendar::class, $id);
        }

        return $calendar;
    }
}
