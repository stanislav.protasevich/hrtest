<?php
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Infrastructure\CalendarBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class InfrastructureCalendarBundle extends Bundle
{

}
