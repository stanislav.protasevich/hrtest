<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Command;


use Infrastructure\CommonBundle\Factory\ObjectebleTrait;

class CalendarListCommand
{
    use ObjectebleTrait;

    /**
     * @var string
     */
    public $text;
    /**
     * @var string
     */
    public $date;

    /**
     * @var string
     */
    public $repeat;
}
