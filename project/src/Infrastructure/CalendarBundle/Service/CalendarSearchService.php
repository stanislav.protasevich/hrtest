<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Service;


use Carbon\Carbon;
use Doctrine\ORM\QueryBuilder;
use Domain\Calendar\Entity\Calendar;
use Infrastructure\CalendarBundle\Command\CalendarListCommand;
use Infrastructure\CommonBundle\Service\FilterSortService;

class CalendarSearchService extends FilterSortService
{
    protected $sortMapping = [
        'date' => 'calendar.date',
        'repeat' => 'calendar.repeat',
        'text' => ['calendar.title', 'calendar.description'],
    ];

    public function search(CalendarListCommand $command): QueryBuilder
    {
        /* @var $qb QueryBuilder */
        $qb = $this
            ->getEm()
            ->getRepository(Calendar::class)
            ->findAllQueryBuilder()
            ->andWhere('calendar.repeat = :repeat')
            ->setParameter('repeat', $command->repeat ?: Calendar::REPEAT_YEARLY);

        if ($command->text) {
            $qb->andWhere('calendar.title LIKE :text OR calendar.description LIKE :text')
                ->setParameter('text', $this->makeLikeParam((string)$command->text));
        }

        if ($command->date && $date = $this->periodFromString($command->date)) {
            /** @var \DateTime[] $date */
            [$start, $end] = $date;
        } else {
            $start = Carbon::now()->subMonth()->startOfMonth();
            $end = Carbon::now()->addMonth()->endOfMonth();
        }

        $qb
            ->andWhere('DATE_FORMAT(calendar.date,\'%m-%d\') BETWEEN :start AND :end')
            ->setParameter('start', $start->format('m-d'))
            ->setParameter('end', $end->format('m-d'));

        $this->sorting($this->getRequest(), $qb, $this->sortMapping);

        return $qb;
    }
}
