<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Service;


use Domain\Calendar\Entity\Calendar;
use Infrastructure\CommonBundle\Service\BaseService;

class CalendarService extends BaseService
{
    public function create(Calendar $calendar): Calendar
    {
        $calendar->setCreatedAt($this->now());
        $calendar->setCreatedBy($this->getUser());

        return $this->edit($calendar);
    }

    public function edit(Calendar $calendar): Calendar
    {
        $em = $this->getEm();

        $calendar->setUpdatedAt($this->now());
        $calendar->setUpdatedBy($this->getUser());

        $em->persist($calendar);

        $em->flush();

        return $calendar;
    }

    public function delete(Calendar $calendar): void
    {
        $em = $this->getEm();

        $em->remove($calendar);

        $em->flush();
    }
}
