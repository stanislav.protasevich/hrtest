<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Infrastructure\CalendarBundle\Event;


use Domain\Calendar\Entity\Calendar;
use Symfony\Component\EventDispatcher\Event;

class CalendarEvent extends Event
{
    public const EVENT_BEFORE_INSERT = 'calendarBeforeInsert';
    public const EVENT_AFTER_INSERT = 'calendarAfterInsert';

    public const EVENT_BEFORE_UPDATE = 'calendarBeforeUpdate';
    public const EVENT_AFTER_UPDATE = 'calendarAfterUpdate';

    public const EVENT_BEFORE_DELETE = 'calendarBeforeDelete';
    public const EVENT_AFTER_DELETE = 'calendarAfterDelete';

    public const EVENT_BEFORE_FIND = 'calendarBeforeFind';
    public const EVENT_AFTER_FIND = 'calendarAfterFind';
    /**
     * @var Calendar
     */
    private $calendar;

    public function __construct(Calendar $calendar)
    {

        $this->calendar = $calendar;
    }

    /**
     * @return Calendar
     */
    public function getCalendar(): Calendar
    {
        return $this->calendar;
    }
}
