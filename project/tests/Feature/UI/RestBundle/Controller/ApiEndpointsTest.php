<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Feature\UI\RestBundle\Controller;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Infrastructure\CommonBundle\Helpers\Inflector;
use Infrastructure\CommonBundle\Helpers\StringHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiEndpointsTest extends KernelTestCase
{
    private $_routes;
    /**
     * @var Client
     */
    private $client;

    public function testEndpoints()
    {
        foreach ($this->_routes as $route) {
            call_user_func(
                [$this, lcfirst(Inflector::camelize('test_' . $route['name']))],
                $route
            );
        }
    }

    public function __call($name, $arguments)
    {
        $route = $arguments[0];

        try {
            $res = $this->client->request(
                isset($route['methods']) ? $route['methods'][0] : Request::METHOD_GET,
                $route['url']
            );
        } catch (ClientException $clientException) {
            $this->assertTrue(
                in_array(
                    $clientException->getCode(),
                    [
                        Response::HTTP_UNAUTHORIZED,
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        Response::HTTP_OK
                    ]
                ), sprintf('%s: code %s not in array', $route['name'], $clientException->getCode()));
        }
    }

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->client = new Client([
            'base_uri' => 'http://balancer'
        ]);

        $router = static::$container->get('router');
        /** @var $collection \Symfony\Component\Routing\RouteCollection */
        $collection = $router->getRouteCollection();
        $allRoutes = $collection->all();

        $routes = [];

        /** @var $params \Symfony\Component\Routing\Route */
        foreach ($allRoutes as $route => $params) {

            $path = $params->getPath();
            if (stristr($route, 'api') === false) {
                continue;
            }

            if (preg_match_all('/\{(.*?)\}/', $path, $matches)) {

                try {
                    $query = [];
                    foreach ($matches[1] as $match) {
                        $query[$match] = StringHelper::random(16);
                    }

                    $routes[] = [
                        'name' => $route,
                        'url' => $router->generate($route, $query),
                        'methods' => $params->getMethods(),
                    ];

                } catch (\Exception $exception) {
                }

            } else {
                $routes[] = [
                    'name' => $route,
                    'url' => $path,
                    'methods' => $params->getMethods(),
                ];
            }

        }

        $this->_routes = $routes;
    }
}
