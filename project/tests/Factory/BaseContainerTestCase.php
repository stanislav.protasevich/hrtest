<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Tests\Factory;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BaseContainerTestCase extends KernelTestCase
{
    public function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
    }

    public function get(string $name)
    {
        return static::$container->get($name);
    }
}
