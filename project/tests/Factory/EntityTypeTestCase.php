<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Tests\Factory;


use Doctrine\ORM\EntityManager;
use Domain\User\Entity\User;
use Ramsey\Uuid\Uuid;

class EntityTypeTestCase extends BaseContainerTestCase
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->em = $this->get('doctrine.orm.default_entity_manager');

        $this->em->getConnection()->beginTransaction();
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->em = null;
    }

    public function getUser(): User
    {
        $user = new User();
        $reflection = new \ReflectionClass($user);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($user, (string)Uuid::uuid4());

        return $user;
    }
}
