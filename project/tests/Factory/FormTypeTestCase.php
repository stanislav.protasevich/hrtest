<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Tests\Factory;

use Infrastructure\CommonBundle\Helpers\FormHelper;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormTypeTestCase extends KernelTestCase
{
    protected function form($class, string $type, array $request = [], string $method = Request::METHOD_POST): array
    {
        $model = is_string($class) ? new $class() : $class;

        $form = $this->createForm($type, $model);

        return $this->processForm($request, $form, $model);
    }

    protected function createForm(string $type, $data = null, array $options = array()): FormInterface
    {
        $kernel = self::bootKernel();
        return static::$container->get('form.factory')->create($type, $data, $options);
    }

    /**
     * @param array $request
     * @param FormInterface $form
     * @param object $model
     * @return array
     */
    protected function processForm(array $request = [], FormInterface $form, object $model)
    {
        $form->submit($request, true);

        return [$model, FormHelper::getErrorsFromForm($form)];
    }
}