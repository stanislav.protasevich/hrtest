<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);


namespace Tests\Factory;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FeatureTypeTestCase extends KernelTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->client = new Client([
            'base_uri' => 'http://backend_nginx/api/v1/auth/'
        ]);
    }
}
