<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Tests\Factory;

use League\Tactician\CommandBus;
use UI\RestBundle\Controller\TestAbstractBusController;

class ControllerTestCase extends EntityTypeTestCase
{
    /**
     * @var TestAbstractBusController
     */
    private $controller;

    protected function controller(string $className): TestAbstractBusController
    {
        if (!$this->controller) {
            /** @var CommandBus $bus */
            $bus = $this->get('tactician.commandbus.default');
            /** @var CommandBus $query */
            $query = $this->get('tactician.commandbus.query');

            $reflection = new \ReflectionClass($className);

            $this->controller = $reflection->newInstanceArgs([
                'container' => static::$container,
                'bus' => $bus,
                'queryBus' => $query
            ]);
        }

        return $this->controller;
    }
}
