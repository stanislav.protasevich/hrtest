<?php
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

declare(strict_types=1);

namespace Tests\Unit\Infrastructure\LeavesBundle\Service\IntervalService;


use Domain\Policy\Entity\AccrualSchedule;
use Domain\Policy\Entity\Policy;
use Domain\User\Entity\User;
use Infrastructure\LeavesBundle\Service\IntervalService\AnnuallyIntervalService;
use Tests\Factory\EntityTypeTestCase;

class AnnuallyIntervalServiceTest extends EntityTypeTestCase
{
    /**
     * @var AnnuallyIntervalService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();
        $kernel = self::bootKernel();
        $this->service = static::$container->get('leave.interval.annually');

        $policy = $this->em->getRepository(Policy::class)->find('47ba4543-90b4-11e8-b4e0-02420a010032');
        $accrualSchedule = $policy->getAccrualSchedule();

        $accrualSchedule->setInterval(AccrualSchedule::INTERVAL_ANNUALLY);

        $this->service->setAccrualSchedule($accrualSchedule);

        $user = $this->em->getRepository(User::class)->findOneBy(['email' => 'admin@admin.com']);
        $this->service->setUser($user);
    }

    public function testCareerStart()
    {
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('02.02.2017'));
        $this->service->getAccrualSchedule()->setBeginsInterval(1);
        $this->service->getAccrualSchedule()->setBeginsType(AccrualSchedule::BEGINS_TYPE_DAYS);

        $this->assertEquals(new \DateTime('03.02.2017'), $this->service->getCareerStart());
    }

    public function testStartPeriod()
    {
        $date = '25.10';
        $this->service->getAccrualSchedule()->setAnnuallyMod($date);
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('01.01.2017'));
        $this->service->getAccrualSchedule()->setBeginsInterval(2);
        $this->service->getAccrualSchedule()->setBeginsType(AccrualSchedule::BEGINS_TYPE_DAYS);

        $this->assertEquals(new \DateTime($date . '.2017'), $this->service->getStartPeriod());
    }

    public function testEndPeriod()
    {
        $date = '25.10';
        $this->service->getAccrualSchedule()->setAnnuallyMod($date);
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('01.01.2017'));

        $this->assertEquals(
            \DateTime::createFromFormat(AccrualSchedule::ANNUALLY_MOD_FORMAT, $date),
            $this->service->getEndPeriod()
        );
    }

    public function testCareerPeriodInterval()
    {
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('01.01.2017'));

        $period = $this->service->getCareerPeriodInterval();

        $this->assertEquals($this->service->getCareerStart(), $period->first());
        $this->assertEquals($this->service->getEndPeriod(), $period->last());
    }

    public function testActivePeriodInterval()
    {
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('01.01.2017'));
        $this->service->getAccrualSchedule()->setBeginsInterval(1);
        $this->service->getAccrualSchedule()->setBeginsType(AccrualSchedule::BEGINS_TYPE_DAYS);

        $period = $this->service->getActivePeriodInterval();

        $this->assertEquals($this->service->getStartPeriod(), $period->first());
        $this->assertEquals($this->service->getEndPeriod(), $period->last());
    }

    public function testAvailableHours()
    {
        $history = $this->service->getCareerHistory();
        $history->setCreatedAt(new \DateTime('01.01.2017'));
        $this->service->getAccrualSchedule()->setBeginsInterval(1);
        $this->service->getAccrualSchedule()->setBeginsType(AccrualSchedule::BEGINS_TYPE_DAYS);
        $this->service->setNow(new \DateTime('01.07.2018'));

        $this->assertEquals(
            $this->service->getAccrualSchedule()->getHours() / 12 * 6,
            $this->service->getAvailableHours()
        );
    }
}