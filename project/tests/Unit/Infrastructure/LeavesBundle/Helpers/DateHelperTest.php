<?php declare(strict_types=1);
/**
 * Created by Yuriy Hritsaiy.
 * Email: yu.hritsaiy@gmail.com
 */

namespace Tests\Unit\Infrastructure\LeavesBundle\Helpers;


use Domain\TimeOff\Entity\Policy;
use Infrastructure\LeavesBundle\Helpers\DateHelper;
use PHPUnit\Framework\TestCase;

class DateHelperTest extends TestCase
{
//    public function testValidSpec()
//    {
//        $policy = PolicyFactory::build(TimeOffTypeFactory::build());
//        $policy->setFrequencyType(Policy::FREQUENCY_TYPE_DAYS);
//        $policy->setFrequencyOfUse(1);
//
////        $periodStartDate = new \DateTime('2018-01-01');
//
//        $frequencyOfUse = $policy->getFrequencyOfUse();
//        $frequencyType = $policy->getFrequencyType();
//
//        $this->assertTrue(true);
//
////
////        prnx($spec);
////
////
////        $endDate = DateHelper::currentPeriodEndDate($spec, $periodStartDate);
////        $startDate = DateHelper::currentPeriodStartDate($spec, $endDate);
////
////        prnx($startDate);
//
//    }

    public function testValidPeriodDesignator()
    {
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_DAYS, 1), 'P1D');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_DAYS, 31), 'P31D');

        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_WEEKS, 1), 'P1W');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_WEEKS, 12), 'P12W');

        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_MONTHS, 1), 'P1M');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_MONTHS, 12), 'P12M');

        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 1), 'P1Y');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 5), 'P5Y');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 10), 'P10Y');

        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_QUARTERLY, 1), 'P3M');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_QUARTERLY, 2), 'P6M');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_QUARTERLY, 3), 'P9M');
        $this->assertEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_QUARTERLY, 4), 'P12M');
    }

    public function testInValidPeriodDesignator()
    {
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_MONTHS, 4), 'P1M');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_MONTHS, 10), 'P12M');

        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 4), 'P1Y');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 7), 'P5Y');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 3), 'P10Y');

        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_DAYS, 2), 'P1D');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_DAYS, 3), 'P31D');

        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_WEEKS, 3), 'P1W');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_WEEKS, 6), 'P1W');

        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_YEARS, 2), 'P1Y');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_MONTHS, 2), 'P1M');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_WEEKS, 2), 'P1W');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_DAYS, 2), 'P1D');
        $this->assertNotEquals(DateHelper::periodDesignator(Policy::FREQUENCY_TYPE_QUARTERLY, 2), 'P3M');
    }

    public function testValidDaysStartEndDatePeriod()
    {
        $period = DateHelper::startEndDatePeriod(
            'P1D',
            new \DateTime('2011-06-17'),
            new \DateTime('2014-11-21')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2014-11-20');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-11-21');

        //
        $period = DateHelper::startEndDatePeriod(
            'P5D',
            new \DateTime('2011-06-17'),
            new \DateTime('2014-07-11')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2014-07-06');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-07-11');

        //
        $period = DateHelper::startEndDatePeriod(
            'P10D',
            new \DateTime('2011-06-17'),
            new \DateTime('2014-08-15')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2014-08-10');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-08-20');
    }

    public function testValidMonthsStartEndDatePeriod()
    {
        $period = DateHelper::startEndDatePeriod(
            'P2M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-01-01')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2012-11-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P1M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-01-01')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2012-12-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P5M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-10-23')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2013-07-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-12-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P8M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-10-23')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2013-09-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-05-01');
    }

    public function testValidQuartersStartEndDatePeriod()
    {
        $period = DateHelper::startEndDatePeriod(
            'P3M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-01-01')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2012-10-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P6M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-01-01')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2012-07-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P9M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-10-23')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2013-04-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P12M',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-10-23')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2013-01-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2014-01-01');
    }

    public function testValidYearsStartEndDatePeriod()
    {
        $period = DateHelper::startEndDatePeriod(
            'P1Y',
            new \DateTime('2011-01-01'),
            new \DateTime('2013-01-01')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2012-01-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2013-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P3Y',
            new \DateTime('2011-01-02'),
            new \DateTime('2015-02-03')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2014-01-02');
        $this->assertEquals($period->last()->format('Y-m-d'), '2017-01-02');

        //
        $period = DateHelper::startEndDatePeriod(
            'P5Y',
            new \DateTime('2011-01-01'),
            new \DateTime('2018-10-23')
        );
        $this->assertEquals($period->first()->format('Y-m-d'), '2016-01-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2021-01-01');

        //
        $period = DateHelper::startEndDatePeriod(
            'P8Y',
            new \DateTime('2001-01-01'),
            new \DateTime('2013-10-23')
        );

        $this->assertEquals($period->first()->format('Y-m-d'), '2009-01-01');
        $this->assertEquals($period->last()->format('Y-m-d'), '2017-01-01');
    }
}
