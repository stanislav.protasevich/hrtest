<?php declare(strict_types=1);
/**
 *  Created by Yuriy Hritsaiy.
 *  Email: yu.hritsaiy@gmail.com
 */

namespace Tests\Unit\Infrastructure\UserBundle\Service;


use Domain\User\Entity\Hobby;
use Infrastructure\UserBundle\Service\HobbyService;
use Symfony\Component\HttpFoundation\Request;
use Tests\Factory\EntityTypeTestCase;

class HobbyServiceTest extends EntityTypeTestCase
{
    /**
     * @var HobbyService
     */
    private $service;

    public function setUp()
    {
        parent::setUp();

        $this->service = $this->get(HobbyService::class);
    }

    public function testSuccessCreate()
    {
        $name = 'TestHobby';
        $hobby = $this->service->create($name);

        $this->assertInstanceOf(Hobby::class, $hobby);
        $this->assertEquals($name, $hobby->getId());
    }

    public function testSuccessCreateFromRequest()
    {
        $hobbies = ['testOne', 'testTwo'];

        $request = new Request([], [
            'hobbies' => $hobbies
        ]);
        $request->setMethod(Request::METHOD_POST);

        /** @var Hobby[] $created */
        $created = $this->service->createHobbyFromRequest($request);
        
        $this->assertTrue(is_array($created));
        $this->assertTrue(count($created) === 2);

        foreach ($created as $key => $hobby) {
            $this->assertEquals($hobbies[$key], $hobby->getId());
        }
    }
}
